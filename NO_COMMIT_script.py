import os
import sys

from products.models import RealProduct

proj_path = "/path/to/my/project/"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myproject.settings")
sys.path.append(proj_path)

os.chdir(proj_path)

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()


def set_obschaia_tsena(container):
    for product in container():
        product.property_json['obshchaia-tsena'] = product.property_json[
            'tsena-so-skidkoi'] if product.property_json[
                                       'tsena-so-skidkoi'] != 0 else \
            product.property_json['stoimost-arendy-v-mesiats']

        print(product.property_json['obshchaia-tsena'])
        product.save()


set_obschaia_tsena(RealProduct.objects.all())