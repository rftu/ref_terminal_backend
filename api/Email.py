import logging
import smtplib
from abc import ABCMeta, abstractmethod
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os.path import basename

from api.models import User
from api.models.email import EmailConfig, Email

logger = logging.getLogger(__name__)


class BaseEmail(object):
    """
    Factory generating an email message
    """
    __metaclass__ = ABCMeta

    def __init__(self, kwargs):
        self.email_type = Email.objects.get(type=kwargs.pop('email_type'))
        if not self.to_email:
            self.to_email = kwargs.pop('to_email')
        self.text = self.email_type.template
        self.subject = self.email_type.subject_name
        try:
            if not self.tags:
                self.tags = kwargs
        except AttributeError:
            self.tags = kwargs

    @abstractmethod
    def build_email(self):
        pass

    def send_email(self, files: list):
        config = EmailConfig.get_solo()

        domain = config.email_domain
        msg = MIMEMultipart('mixed')

        msg['Subject'] = self.subject
        msg['From'] = config.email_from + "@" + domain
        msg['To'] = self.to_email

        text_msg = MIMEText(self.text, 'html')
        msg.attach(text_msg)
        if files:
            for file in files:
                part = MIMEApplication(open(file, 'rb').read())
                part.add_header('Content-Disposition',
                                'attachment', filename=basename(file))
                msg.attach(part)

        s = smtplib.SMTP(config.smtp_server, config.smtp_port)

        # s.login(config.smtp_login + "" + domain, config.smtp_password)
        try:
            s.sendmail(msg['From'], msg['To'], msg.as_string())
        except smtplib.SMTPRecipientsRefused:
            s.quit()
            return logger.error('Registration email not send.')
        return s.quit()

    @staticmethod
    def factory(kwargs):
        email = EMAIL_LIST[kwargs['email_type']]
        return email(kwargs)


class FullContractClientEmail(BaseEmail):
    def __init__(self, kwargs):
        from api.models import OrderModel
        order_id = kwargs['order']
        order = OrderModel.objects.get(id=order_id)

        self.to_email = order.properties['email']
        self.tags = kwargs.update({'client_info': order.properties,
                                   'orders': order.managers_orders.all(),
                                   'order': order})
        super().__init__(kwargs)

    def build_email(self):
        from api.helpers.docx import build_doc
        self.text = self.text.format(fio=self.tags.get('client_info', {}).get('fio'))

        path = []
        sale_path = build_doc(data=self.tags,
                              order_id=self.tags['order'].id,
                              template='Продажа-клиент',
                              doc_name='Договор-продажа')
        rent_month_path = build_doc(data=self.tags,
                                    order_id=self.tags['order'].id,
                                    template='Аренда-месяц-клиент',
                                    doc_name='Договор-аренда-год')
        rent_year_path = build_doc(data=self.tags,
                                   order_id=self.tags['order'].id,
                                   template='Аренда-год-клиент',
                                   doc_name='Договор-аренда-месяц')

        for order in self.tags['orders']:
            if order.rent_month_items:
                if rent_month_path not in path:
                    path.append(rent_month_path)
            if order.rent_year_items:
                if rent_year_path not in path:
                    path.append(rent_year_path)
            if order.sale_items:
                if sale_path not in path:
                    path.append(sale_path)

        return super().send_email(files=path)


class FullContractManagerEmail(BaseEmail):
    def __init__(self, kwargs):
        from api.models import OrderManagerModel
        manager_id = kwargs['manager']
        order_id = kwargs['order']
        order = OrderManagerModel.objects.get(id=order_id)
        manager = User.objects.get(id=manager_id)

        self.to_email = manager.email
        self.tags = kwargs.update({'order': order, 'manager': manager})
        super().__init__(kwargs)

    def build_email(self):
        from api.helpers.docx import build_doc
        self.text = self.text.format(fio=self.tags['client_info'].get('fio', ''),
                                     phone=self.tags['client_info'].get('phone', ''),
                                     email=self.tags['client_info'].get('email', ''),
                                     )

        path = []
        sale_path = build_doc(data=self.tags,
                              order_id=self.tags['order'].id,
                              template='Продажа',
                              doc_name='Договор-продажа')
        rent_month_path = build_doc(data=self.tags,
                                    order_id=self.tags['order'].id,
                                    template='Аренда-месяц',
                                    doc_name='Договор-аренда-месяц')
        rent_year_path = build_doc(data=self.tags,
                                   order_id=self.tags['order'].id,
                                   template='Аренда-год',
                                   doc_name='Договор-аренда-год')

        order = self.tags['order']
        if order.rent_month_items:
            if rent_month_path not in path:
                path.append(rent_month_path)
        if order.rent_year_items:
            if rent_year_path not in path:
                path.append(rent_year_path)
        if order.sale_items:
            if sale_path not in path:
                path.append(sale_path)

        return super().send_email(files=path)


class PaymentIsDueEmail(BaseEmail):
    def build_email(self):
        self.text = self.text.format(username=self.tags['username'])
        return super().send_email(files=[])


class FeedbackEmail(BaseEmail):
    def __init__(self, kwargs):
        self.to_email = EmailConfig.get_solo().company_email
        super(FeedbackEmail, self).__init__(kwargs)

    def build_email(self):
        self.text = self.text.format(name=self.tags.get('name'),
                                     phone=self.tags.get('phone'),
                                     description=self.tags.get('description')
                                                 or '')
        return super().send_email(files=[])


EMAIL_LIST = {
    'full_contract_email_client': FullContractClientEmail,
    'full_contract_email_manager': FullContractManagerEmail,
    'payment_is_due_email_to_manager': PaymentIsDueEmail,
    'send_feedback': FeedbackEmail
}
