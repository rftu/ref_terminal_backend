from rest_framework.exceptions import APIException


class ContainerSold(APIException):
    status_code = 412
    default_detail = 'Эти контейнеры уже проданы, арендованы или зарезервированы'
    precondition_failed = 'payment_of_container'

    def __init__(self, containers_id: list):
        from products.models import RealProduct
        super(ContainerSold, self).__init__()
        containers = [{"name": container.name,
                       "id": container.id} for container in
                      RealProduct.objects.filter(pk__in=containers_id)]
        self.detail = {"error": self.default_detail, "containers": containers}


class NoContentArgument(APIException):
    status_code = 400
    default_detail = 'Одно или несколько полей незаполнены'
    precondition_failed = 'filed_not_content'
