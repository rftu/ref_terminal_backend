from ckeditor.widgets import CKEditorWidget
from django import forms

from api.models.email import Email


class EmailForm(forms.ModelForm):
    class Meta:
        model = Email
        fields = '__all__'
        widgets = {
            'template': CKEditorWidget(),
        }
