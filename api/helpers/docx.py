import os

from docxtpl import DocxTemplate

from ref_terminal_backend.settings import MEDIA_ROOT
import datetime


def build_doc(data, order_id, template, doc_name):
    # get template and render context
    doc = DocxTemplate("api/templates/" + template + ".docx")
    # context = {'var': "Тестовый тест"}

    data.update(
        {'date_now': datetime.datetime.now().date().strftime("%d/%m/%y")})
    context = data
    doc.render(context)

    # create paths
    doc_name = doc_name + "№" + str(order_id) + ".docx"

    path_dir = os.path.join(MEDIA_ROOT, str(order_id))
    try:
        os.mkdir(path_dir)
    except FileExistsError:
        pass

    # save file
    path = os.path.join(path_dir, doc_name)
    doc.save(path)
    return path
