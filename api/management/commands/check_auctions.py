from django.core.management import BaseCommand

from api.models import OrderModel
from products.models import RealProduct


class Command(BaseCommand):

    @staticmethod
    def check_auctions():
        for product in RealProduct.objects.filter(property_json__auction=True):
            completed = OrderModel.complete_auction(product)
            print('{}: {}'.format(completed and 'Завершен' or 'Не завершен', product))

    def handle(self, *args, **options):
        self.check_auctions()
