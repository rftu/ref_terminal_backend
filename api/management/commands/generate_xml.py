from django.core.management import BaseCommand

from products.models import RealProduct
from lxml import etree


class Command(BaseCommand):

    @staticmethod
    def generate_xml():
        base_url = "https://refterminal.ru/"
        root = etree.Element("urlset", **{
            "xmlns": "http://www.sitemaps.org/schemas/sitemap/0.9",

        })

        def generate_url(url):
            url_tree = etree.SubElement(root, "url")
            loc = etree.SubElement(url_tree, 'loc').text = base_url + url

        generate_url('landing')
        generate_url('faq')
        generate_url('catalog')
        for product in RealProduct.objects.filter(property_json__publish=True):
            generate_url('container/{}'.format(product.translate))
        f = open("seo/sitemap.xml", "wb")
        f.write(etree.tostring(root, xml_declaration=True))
        f.close()

    def handle(self, *args, **options):
        self.generate_xml()
