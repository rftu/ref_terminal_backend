from django.core.management import BaseCommand

from products.models import RealProduct


class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'

    def set_obschaia_tsena(self):
        for product in RealProduct.objects.all():
            print("Arenda:{}; So skidkoy:{}".format(
                product.property_json.get('stoimost-arendy-v-mesiats'),
                product.property_json.get('tsena-so-skidkoi')
            ))
            product.property_json[
                'obshchaia-tsena'] = product.property_json.get(
                'tsena-so-skidkoi') if product.property_json.get(
                'tsena-so-skidkoi') not in [0, None] else \
                product.property_json.get('stoimost-arendy-v-mesiats')

            print(product.property_json['obshchaia-tsena'])
            product.save()

    def handle(self, *args, **options):
        self.set_obschaia_tsena()
