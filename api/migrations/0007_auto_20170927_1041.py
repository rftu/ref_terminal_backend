# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-27 00:41
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0025_auto_20170922_1627'),
        ('api', '0006_auto_20170926_2145'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='tradeitem',
            unique_together=set([('container', 'manager_order')]),
        ),
    ]
