# coding=utf-8
from django.db import models


class Bonus(models.Model):
    name = models.CharField('Название бонуса', max_length=255)
    manager = models.ForeignKey('User', related_name="bonuses",
                                related_query_name="bonus",
                                verbose_name='Менеджер')
    activate_sum = models.IntegerField('Сумма активации бонуса', default=0)
    discount = models.FloatField('Скидка', null=True)
    type = models.CharField(choices=(
        ('percent', 'Процент'),
        ('total', 'Сумма',),
        ('text', 'Текст')
    ), max_length=20,  verbose_name='Тип скидки')
    description = models.CharField('Описание бонуса', max_length=255,
                                   help_text='В случае текстового бонуса является условием «то»')
    if_condition = models.CharField('Условие «если»', max_length=255,
                                    blank=True, null=True, default=None)
    created_at = models.DateField(
        verbose_name='Дата создания', auto_now_add=True,
        blank=True, null=True,
    )
    expiration_date = models.DateField(
        verbose_name='Дата окончания действия',
        blank=True, null=True, default=None,
    )

    def __str__(self):
        return "{}-{}".format(self.name, self.manager.last_name)

    class Meta:
        verbose_name = "Бонус"
        verbose_name_plural = "Бонусы"
