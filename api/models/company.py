# coding=utf-8
from datetime import timedelta

from django.db import models


class Company(models.Model):
    name = models.CharField('Название Компании', max_length=255, unique=True)
    payment_interval = models.DurationField('Интервал оплаты', null=True,
                                            default=timedelta(days=2))
    critical_time = models.DurationField('Время оповещения', null=True, default=timedelta(days=2))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'
