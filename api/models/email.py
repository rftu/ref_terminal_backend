from django.db import models
from solo.models import SingletonModel


class Email(models.Model):
    FULL_CONTRACT_CLIENT = 'full_contract_email_client'
    FULL_CONTRACT_MANAGER = 'full_contract_email_manager'
    FEEDBACK = 'send_feedback'
    EMAIL_TYPE = (
        (FULL_CONTRACT_CLIENT, 'Контракт заполнен (Клиент)'),
        (FULL_CONTRACT_MANAGER, 'Контракт заполнен (менеджер)'),
        (FEEDBACK, 'Вопрос с РефТерминала')
    )

    subject_name = models.CharField(null=True, max_length=100,
                                    verbose_name='Тема письма', unique=True)
    type = models.CharField(max_length=100, choices=EMAIL_TYPE,
                            verbose_name='Назначение письма', unique=True)
    template = models.TextField(null=True, verbose_name='Шаблон письма')

    class Meta:
        verbose_name = 'Email-сообщение'
        verbose_name_plural = 'Email-сообщения'

    def __str__(self):
        return self.subject_name


class EmailConfig(SingletonModel):
    company_email = models.CharField(
        max_length=100,
        default='test@email.ru',
        verbose_name='Email компании'
    )
    email_domain = models.CharField(
        max_length=100,
        default='sandbox1a31d145656346e3984e03760dcf516e.mailgun.org',
        verbose_name='Домен')
    smtp_login = models.CharField(
        max_length=150,
        default='postmaster@',
        verbose_name='Логин')
    smtp_password = models.CharField(
        max_length=100,
        default='03f0dd40dca3bf024e71af05b6e42476',
        verbose_name='Пароль')
    email_from = models.CharField(
        max_length=50,
        default='manager',
        verbose_name='Email Префикс')
    smtp_server = models.CharField(
        max_length=50,
        default='smtp.mailgun.org',
        verbose_name='SMTP сервер домен')
    smtp_port = models.IntegerField(
        verbose_name='SMTP сервер порт',
        default='587',
    )

    class Meta:
        verbose_name = 'Конфигурация отправки Email'
        verbose_name_plural = 'Конфигурация отправки Email'

    def __unicode__(self):
        return u"Site Configuration"
