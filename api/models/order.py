import collections
import datetime

from django.contrib.postgres.fields import JSONField
from django.core.serializers import json
from django.db import models, IntegrityError
from django.db.models import Sum, F, Max, Min, Q
from django.utils import timezone
from rest_framework.exceptions import NotFound

from api.custom_exc import ContainerSold
from . import User


class OrderManagerManager(models.Manager):
    def get_queryset(self):
        result = super(OrderManagerManager, self).get_queryset().annotate(
            total_price=Sum(
                models.Case(
                    models.When(trade_items__action=TradeItem.SALE,
                                then='trade_items__total_cost'),
                    output_field=models.CharField()
                )
            ),
            total_price_rent_month=Sum(
                models.Case(
                    models.When(trade_items__action=TradeItem.RENT_MONTH,
                                then='trade_items__total_cost'),
                    output_field=models.CharField()
                )
            ),
            total_price_rent_year=Sum(
                models.Case(
                    models.When(trade_items__action=TradeItem.RENT_YEAR,
                                then='trade_items__total_cost'),
                    output_field=models.CharField()
                )
            ),
        )
        return result


class OrderManagerModel(models.Model):
    objects = OrderManagerManager()
    manager = models.ForeignKey(User, verbose_name='Менеджер')
    last_time = models.DateTimeField(blank=True, default=None,
                                     verbose_name='Крайний срок оплаты',
                                     null=True)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы по менеджерам'

    def __str__(self):
        return "{}-{}#{}".format(self.manager.last_name,
                                 self._meta.verbose_name,
                                 self.pk)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(OrderManagerModel, self).save()

    def get_bonuses(self):
        order = OrderManagerModel.objects.get(pk=self.pk)
        total_price = order.total_price if order.total_price is not None else 0
        return self.manager.bonuses.filter(discount__isnull=False).annotate(
            apply=models.Case(
                models.When(activate_sum__lte=total_price,
                            then=models.Value(True)),
                default=models.Value(False),
                output_field=models.BooleanField()
            )
        ).order_by('-activate_sum')

    def apply_bonus(self):
        total_price = OrderManagerModel.objects.get(pk=self.pk).total_price
        if total_price is None:
            return 0
        try:
            bonus = self.bonuses.filter(apply=True).annotate(
               price_with_bonus=models.Case(
                   models.When(type='percent',
                               then=total_price * (
                                   (100 - F('discount')) / 100)),
                   models.When(type='total',
                               then=total_price - F('discount')),
                   default=total_price,
                   output_field=models.FloatField(),
               )
            ).aggregate(Min('price_with_bonus'))
            if bonus.get('price_with_bonus__min'):
                total_price = bonus['price_with_bonus__min']
        except ValueError:
            total_price = 0
        except IndexError:
            pass
        except Exception as e:
            raise ValueError('Что-то пошло не так: ' + e.__str__())
        return total_price

    def get_sale_items(self):
        items = []
        for item in self.trade_items.all():
            if item.action == TradeItem.SALE:
                items.append(item)
        return items

    def get_rent_month_items(self):
        items = []
        for item in self.trade_items.all():
            if item.action == TradeItem.RENT_MONTH:
                items.append(item)
        return items

    def get_rent_year_items(self):
        items = []
        for item in self.trade_items.all():
            if item.action == TradeItem.RENT_YEAR:
                items.append(item)
        return items

    def get_rent_items(self):
        items = []
        for item in self.trade_items.all():
            if item.action in [TradeItem.RENT_YEAR, TradeItem.RENT_MONTH]:
                items.append(item)
        return items

    sale_items = property(get_sale_items)
    rent_month_items = property(get_rent_month_items)
    rent_year_items = property(get_rent_year_items)
    rent_items = property(get_rent_items)

    price_with_apply_bonus = property(apply_bonus)
    bonuses = property(get_bonuses)


class TradeItem(models.Model):
    RENT_YEAR = 'rent_year'
    RENT_MONTH = 'rent_month'
    SALE = 'sale'
    AUCTION = 'auction'
    ACTION_TYPE = (
        (RENT_YEAR, 'Аренда/год'),
        (RENT_MONTH, 'Аренда/месяц'),
        (SALE, 'Продажа'),
        (AUCTION, 'Аукцион')
    )
    container = models.ForeignKey('products.RealProduct',
                                  verbose_name='Контейнер', related_name='items')
    action = models.CharField('Тип товарной позиции', choices=ACTION_TYPE,
                              default=SALE, max_length=25, null=True)
    total_cost = models.FloatField('Стоимость позиции')
    manager_order = models.ForeignKey(OrderManagerModel, null=True,
                                      default=None, verbose_name='Менеджер',
                                      related_name='trade_items')

    class Meta:
        unique_together = ('container', 'manager_order')
        verbose_name_plural = 'Элементы ордеров'
        verbose_name = 'Элемент ордера'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        choices = {TradeItem.RENT_YEAR: "stoimost-arendy-v-god",
                   TradeItem.RENT_MONTH: "stoimost-arendy-v-mesiats",
                   TradeItem.SALE: "tsena-so-skidkoi",}
        if self.action != self.AUCTION:
            self.total_cost = self.container.property_json[choices[self.action]]
        return super().save()

    def can_buy(self):
        result = self.container.property_json['status'] not in ['Зарезервирован', 'Арендован', 'Продан']
        result &= self.container.property_json.get(self.container.MODERATION_PASSED_PROPERTY_FIELD, True)
        return result


class OrderManager(models.Manager):
    def create(self, *args, **kwargs):
        if len(kwargs) == 0 or (len(kwargs) == 1 and 'user' in kwargs):
            order = self.model(*args, **kwargs)
            order.save()
            return order
        try:
            items = kwargs['trade_items']
            kwargs.pop('trade_items', None)
            if isinstance(items, collections.Iterable):
                order = self.model(*args, **kwargs)
                order.save()
                for trade_item in items:
                    try:
                        order_manager = order.managers_orders.all().get(
                            manager=trade_item.container.manager)
                    except:
                        order_manager = OrderManagerModel.objects.create(
                            manager=trade_item.container.manager)
                        order.managers_orders.add(order_manager)
                    trade_item.manager_order = order_manager
                    try:
                        trade_item.save()
                        trade_item.container.save()
                    except IntegrityError:
                        pass
                return order
            else:
                raise ValueError('Контейнеры должны быть в виде списка')

        except KeyError:
            raise Exception('Пожалуйста, добавьте контейнер')

    def get_queryset(self):
        result = super(OrderManager, self).get_queryset().annotate(
            total_price=Sum(
                models.Case(
                    models.When(
                        managers_orders__trade_items__action=TradeItem.SALE,
                        then='managers_orders__trade_items__total_cost'),
                    output_field=models.CharField()
                )
            ),
            total_price_rent_month=Sum(
                models.Case(
                    models.When(
                        managers_orders__trade_items__action=TradeItem.RENT_MONTH,
                        then='managers_orders__trade_items__total_cost'),
                    output_field=models.CharField()
                )
            ),
            total_price_rent_year=Sum(
                models.Case(
                    models.When(
                        managers_orders__trade_items__action=TradeItem.RENT_YEAR,
                        then='managers_orders__trade_items__total_cost'),
                    output_field=models.CharField()
                )
            ),
            total_price_auction=Sum(
                models.Case(
                    models.When(
                        managers_orders__trade_items__action=TradeItem.AUCTION,
                        then='managers_orders__trade_items__total_cost'),
                    output_field=models.CharField()
                )
            ),
        )
        return result


class OrderModel(models.Model):
    managers_orders = models.ManyToManyField(OrderManagerModel,
                                             related_name='main_order',
                                             related_query_name='main_order'
                                             )
    objects = OrderManager()
    properties = JSONField(encoder=json.DjangoJSONEncoder, default=dict,
                           null=True)
    auction_properties = JSONField(encoder=json.DjangoJSONEncoder, default=dict,
                           null=True, help_text='Аукцион')

    user = models.ForeignKey(to='api.User', on_delete=models.SET_NULL,
                             blank=True, null=True, default=None,
                             verbose_name='Пользователь',
                             )

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    edited_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def pay(self, silent=False):
        if self.properties:
            from api.tasks import async_email_send
            for order in self.managers_orders.all():
                reserved_containers = []
                for trade_item in order.trade_items.all():
                    if not trade_item.can_buy():
                        if silent is False:
                            reserved_containers.append(trade_item.container.id)
                    else:
                        if not trade_item.container.property_json.get('auction', False) is True:
                            trade_item.container.change_container_status(status='Зарезервирован')
                if silent is False and reserved_containers:
                    raise ContainerSold(containers_id=reserved_containers)

                # set last_time to manager_order
                if self.properties and order.manager.company:
                    order.last_time = datetime.datetime.now() \
                                      + order.manager.company.payment_interval
                    order.save()

                async_email_send(order=order.id,
                                 client_info=self.properties,
                                 manager=order.manager.id,
                                 email_type='full_contract_email_manager')
            async_email_send(order=self.id,
                             email_type='full_contract_email_client')
        else:
            raise NotFound({"error": "Нет данных о покупателе"})

    @classmethod
    def get_orders_for_auction(cls, container):
        return cls.objects.filter(managers_orders__trade_items__container=container).order_by('managers_orders__trade_items__total_cost')

    @classmethod
    def complete_auction(cls, container, force=False):
        completed = False
        if force or not container.can_bet_by_time:
            orders = cls.get_orders_for_auction(container)
            if orders.exists() and not orders.filter(auction_properties__is_win=True).exists():
                o = orders.last()
                o.set_winner()
                o.pay(silent=True)
                completed = True
            if container.property_json['status'] == 'Свободен':
                container.change_container_status('Зарезервирован')
        return completed

    def set_winner(self):
        self.auction_properties['is_win'] = True
        self.save()

    def add_items(self, item):
        if isinstance(item, TradeItem):
            try:
                order = self.managers_orders.all().get(
                    manager=item.container.manager)
            except:
                order = OrderManagerModel.objects.create(
                    manager=item.container.manager)
                self.managers_orders.add(order)
            item.manager_order = order
            try:
                TradeItem.objects.get(container__id=item.container.id,
                                      manager_order=item.manager_order).delete()
                item.save()
            except IntegrityError:
                pass
            except TradeItem.DoesNotExist:
                item.save()
            item.container.save()

    def remove_items(self, item):
        if isinstance(item, TradeItem):
            try:
                order = self.managers_orders.all().get(
                    manager=item.container.manager)
            except:
                raise Exception('Не найдена торговая позиция')
            try:
                item.container.save()
                TradeItem.objects.get(container=item.container.id,
                                      manager_order=order.id).delete()
                if not order.trade_items.all():
                    self.managers_orders.remove(order)
            except:
                raise Exception('Не найдена торговая позиция')

    def get_items(self):
        # TODO постараться избежать итерирования
        containers = []
        for order in self.managers_orders.all():
            containers.extend(list(order.trade_items.all()))
        return containers

    def get_bonuses(self):
        bonuses = []
        for order in self.managers_orders.all():
            bonuses.extend(order.bonuses)
        return bonuses

    def apply_bonuses(self):
        total_price = 0
        for order in self.managers_orders.all():
            total_price += order.price_with_apply_bonus
        return total_price

    trade_items = property(get_items)
    bonuses = property(get_bonuses)
    price_with_all_bonuses = property(apply_bonuses)

    @classmethod
    def get_statistics_context(cls, manager, max_count=5):
        """
        Примечание: при использовании стоимостей контейнеров бонусы не учитываются.
        """
        qs = Q(managers_orders__manager=manager)
        qs &= (Q(managers_orders__trade_items__container__property_json__auction=False) &
               Q(managers_orders__trade_items__container__property_json__status__in=['Продан', 'Арендован', ])) | \
              (Q(auction_properties__is_win=True))
        orders = cls.objects.filter(qs).order_by('-created_at')

        short_float = (lambda a: '{:,.1f}'.format(a).replace(',', ' '))

        items = []
        for o in orders:
            items.extend([(i, o.created_at) for i in o.trade_items])
        total_count = len(items)

        max_count = max_count or total_count
        more_count = max(0, len(items) - max_count)
        if more_count:
            items = items[:max_count]

        count = len(items)
        amount = sum(i.total_cost for i, _ in items)
        average = amount / count if count else 0.0

        headers = ('Дата заказа', 'Контейнер', 'Стоимость, руб.')
        rows = [(created_at, i.container, short_float(i.total_cost)) for (i, created_at) in items]

        month = timezone.now() - datetime.timedelta(days=30)
        month_items = []
        for i, created_at in items:
            if created_at >= month:
                month_items.append((i, created_at))

        month_total_count = len(month_items)
        month_amount = sum(i.total_cost for i, _ in month_items)
        month_average = month_amount / month_total_count if month_total_count else 0.0

        context = {
            'manager': manager,
            'total_count': total_count,
            'more_count': more_count,
            'headers': headers,
            'rows': rows,
            'amount': short_float(amount),
            'average': short_float(average),
            'month_total_count': month_total_count,
            'month_amount': short_float(month_amount),
            'month_average': short_float(month_average),
        }
        return context


class ManagerQuestQuerySet(models.QuerySet):

    def filter_by_status(self, active=True):
        q = Q(**{'expiration_date__{}'.format(active and 'gt' or 'lte'): timezone.now()})
        return self.filter(q)


class ManagerQuest(models.Model):

    class Meta:
        verbose_name = 'Квест'
        verbose_name_plural = 'Квесты'

    CONDITION_ORDERS_COUNT = 'orders_count'
    CONDITION_ORDERS_AMOUNT = 'orders_amount'
    CONDITION_CHOICES = (
        (CONDITION_ORDERS_COUNT, 'Количество продаж'),
        (CONDITION_ORDERS_AMOUNT, 'Итоговая сумма продаж'),
    )

    name = models.CharField(
        verbose_name='Наименование квеста',
        max_length=100,
    )
    condition_type = models.CharField(
        verbose_name='Тип условия', max_length=16,
        default=CONDITION_ORDERS_COUNT, choices=CONDITION_CHOICES,
    )
    condition_value = models.FloatField(
        verbose_name='Условие',
    )
    prize = models.CharField(
        verbose_name='Приз', max_length=50,
    )

    created_at = models.DateTimeField(
        verbose_name='Дата создания', auto_now_add=True,
    )
    expiration_date = models.DateTimeField(
        verbose_name='Дата окончания действия',
    )
    participants = models.ManyToManyField(
        to='api.User', related_name='self_quests',
        verbose_name='Участники',
        blank=True,
    )

    objects = ManagerQuestQuerySet.as_manager()

    def __str__(self):
        return str(self.name)

    def get_orders_for_managers(self, managers):
        orders = OrderModel.objects.filter(managers_orders__trade_items__container__manager__in=managers,
                                           managers_orders__trade_items__container__property_json__status__in=(
                                               'Продан', 'Арендован'),
                                           created_at__gte=self.created_at,
                                           created_at__lt=self.expiration_date)
        return orders

    def check_for_orders(self, orders):
        """
        Проверка набора заказов на выполнение текущего квеста.
        """
        if self.condition_type == self.CONDITION_ORDERS_COUNT:
            return orders.count() >= self.condition_value

        elif self.condition_type == self.CONDITION_ORDERS_AMOUNT:
            items = []
            for o in orders:
                items.extend([(i, o.created_at) for i in o.trade_items])
            amount = sum(i.total_cost for i, _ in items)
            return amount >= int(self.condition_value)

        return False

    def get_success_managers(self):
        """
        Возвращает список менеджеров, которые успешно выполнили данный квест.
        """
        managers = self.participants.all() or User.objects.filter(is_staff=True)
        # TODO: filter by managers in orders all instances.
        all_orders = self.get_orders_for_managers(managers)
        success_managers = []
        for m in managers:
            # if m.id != 20:
            #     continue
            orders = all_orders.filter(managers_orders__trade_items__container__manager__in=(m,))
            if self.check_for_orders(orders):
                success_managers.append(m)
        return success_managers

    def has_success_manager(self, manager):
        """
        Возвращает, входит ли конкретный менеджер в список успешно выполнивших данный квест.
        """
        if self.participants.count() >= 1 and not self.participants.filter(id=manager.id).exists():
            return False
        orders = self.get_orders_for_managers((manager,))
        result = self.check_for_orders(orders)
        return result

    @classmethod
    def get_successes_for_manager(cls, manager):
        """
        Вернуть список квестов, которые данный менеджер успешно выполнил.
        """
        quests = cls.objects.filter(Q(participants__isnull=True) | Q(participants__in=(manager,)))
        return [q for q in quests if q.has_success_manager(manager)]
