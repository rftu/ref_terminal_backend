from django.conf import settings
from django.contrib.auth.models import AbstractUser, PermissionsMixin, \
    BaseUserManager
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        from django.contrib.auth.models import Group
        g = Group.objects.get(name='Manager')
        g.user_set.add(instance)
        Token.objects.create(user=instance)


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.id, filename)


class UserManager(BaseUserManager):
    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        extra_fields.setdefault('is_active', True)
        user = self.model(email=email, username=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create(self, *args, **kwargs):
        return self._create_user(*args, **kwargs)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email, password, **extra_fields)


class User(AbstractUser, PermissionsMixin):
    email = models.EmailField('Системный Email', unique=True)
    last_name = models.CharField('ФИО', max_length=100)
    is_staff = models.BooleanField(
        _('Менеджер РефТерминала'),
        default=False,
        help_text=_('Отметьте, если пользователь должен иметь доступ '
                    'к интерфейсу администратора'),
    )
    is_active = models.BooleanField(
        _('Активен'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )

    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    objects = UserManager()
    REQUIRED_FIELDS = []
    patronymic = models.CharField(max_length=255, null=True,
                                  verbose_name='Отчество')
    phone = models.CharField(max_length=255, null=True, verbose_name='Телефон')
    contact_email = models.EmailField(null=True,
                                      verbose_name='Контактный Email')
    company = models.ForeignKey('Company', related_name="managers",
                                related_query_name="manager",
                                verbose_name='Компания', null=True, blank=True)
    photo = models.ImageField(null=True, upload_to=user_directory_path,
                              verbose_name='Фотография')
    is_vip = models.BooleanField(default=False,
                                 help_text='Пользователь имеет платную подписку?')
    city = models.CharField(max_length=255, null=True, blank=True,
                            verbose_name='Город')

    def save(self, *args, **kwargs):
        if self.email:
            self.username = self.email
        if self.username and not self.pk:
            self.email = self.username
        if self.is_staff:
            self.is_vip = True
        super(User, self).save()

    def clean(self):
        super(User, self).clean()
        if self.is_staff and self.company is None:
            raise ValidationError('The Company must be set')

    class Meta:
        verbose_name = 'Менеджер'
        verbose_name_plural = 'Менеджеры'

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email


class ChangeVIPRequest(models.Model):

    class Meta:
        verbose_name = 'Запрос на платную подписку'
        verbose_name_plural = 'Запросы на платную подписку'

    user = models.ForeignKey(to='api.User', on_delete=models.CASCADE, verbose_name='Пользователь')
    has_access = models.BooleanField(verbose_name='Есть доступ к подписке', default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}: {}'.format(self.user, 'есть доступ' if self.has_access else 'нет доступа')

    def save(self, **kwargs):
        if self.has_access is not self.user.is_vip:
            self.user.is_vip = self.has_access
            self.user.save()
        super(ChangeVIPRequest, self).save(**kwargs)