from django.conf.urls import url
from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.contrib.admin.utils import quote
from django.contrib.auth.admin import UserAdmin
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.html import format_html
from solo.admin import SingletonModelAdmin

from api.forms import EmailForm
from api.models.order import ManagerQuest
from api.models.user import ChangeVIPRequest
from products.admins.manager_admin import manager_admin
from .models import *


@admin.register(Email)
class EmailAdmin(admin.ModelAdmin):
    form = EmailForm


class IsWinFilter(SimpleListFilter):
    title = 'Победитель аукциона'
    parameter_name = 'is_win'

    def lookups(self, request, model_admin):
        return (
            ('yes', 'Да'),
            ('no', 'Нет'),
        )

    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset
        is_win = self.value() == 'yes'
        return queryset.filter(auction_properties__is_win=is_win)


class OrderModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'managers_orders_count', 'properties', 'auction_properties', 'user', 'created_at', 'edited_at', 'is_win', )
    list_filter = (IsWinFilter, )
    list_per_page = 10

    def managers_orders_count(self, obj):
        return obj.managers_orders.count()

    managers_orders_count.short_description = 'Заказов по менеджерам'

    def is_win(self, obj):
        value = obj.auction_properties.get('is_win')
        return value

    is_win.short_description = 'Победитель аукциона'
    is_win.boolean = True


class OrderManagerModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'manager', 'last_time', )
    list_per_page = 10


class IsActiveQuest(SimpleListFilter):
    title = 'Статус'
    parameter_name = 'is_active'

    def lookups(self, request, model_admin):
        return (
            ('active', 'Активные'),
            ('inactive', 'Неактивные'),
        )

    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset
        return queryset.model.objects.filter_by_status(self.value())


class ManagerQuestAdmin(admin.ModelAdmin):
    list_display = ('name', 'condition', 'prize', 'created_at', 'expiration_date', )
    list_filter = (IsActiveQuest, )
    readonly_fields = ('success_managers', )

    def condition(self, obj):
        return '{}: {}'.format(obj.get_condition_type_display(), obj.condition_value)

    condition.short_description = 'Условие'

    def success_managers(self, obj):
        managers = obj.get_success_managers()
        if not managers:
            return 'Пусто'
        view_name = 'admin:%s_%s_change' % (self.opts.app_label, User._meta.model_name)
        buttons = [
            '<a class="button" href="{}">{}</a>'.format(
                reverse(view_name, args=(quote(m.pk),), current_app=self.admin_site.name),
                m,
            ) for m in managers
        ]
        return format_html('<br><br>'.join(buttons))

    success_managers.short_description = 'Менеджеры, выполнившие квест'


admin.site.register(Company)
admin.site.register(OrderModel, OrderModelAdmin)
admin.site.register(OrderManagerModel, OrderManagerModelAdmin)
admin.site.register(ManagerQuest, ManagerQuestAdmin)
admin.site.register(EmailConfig, SingletonModelAdmin)


class UserAdminModel(UserAdmin):
    list_display = ('username', 'email', 'last_name', 'is_staff')
    fieldsets = (
        (None, {
            'fields': (
                'company', 'last_name', 'email', 'password', 'contact_email',
                'phone', 'is_staff', 'is_active', 'is_vip', 'city',
            )
        }),
        ('Статистика по квестам', {
            'fields': (
                'sold_containers_html', 'last_login', 'success_quests',
            ),
        })
    )
    readonly_fields = ('sold_containers_html', 'last_login', 'success_quests', )

    change_list_template = 'admin/manager_change_list.html'

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        formfield = super(UserAdminModel, self).formfield_for_foreignkey(db_field, request, **kwargs)
        if db_field.name == 'company':
            formfield.empty_label = 'Частное лицо'
        return formfield

    def get_queryset(self, request):
        qs = super(UserAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.exclude(is_staff=True, is_superuser=True)

    def has_change_permission(self, request, obj=None):
        if not obj:
            return True
        return not obj.is_superuser or request.user.is_superuser

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                '^(?P<pk>\d+)/sold_containers/$',
                self.admin_site.admin_view(self.sold_containers),
                name='manager_sold_containers'
            ),
            url(
                'manager_statistics/$',
                self.admin_site.admin_view(self.manager_statistics),
                name='manager_statistics'
            )
        ]
        return custom_urls + urls

    def sold_containers(self, request, pk):
        obj = self.model.objects.get(pk=pk)
        context = OrderModel.get_statistics_context(obj, None)

        context.update({
            'bread_name': 'Проданные контейнеры',
            'has_change_permission': self.has_change_permission(request, obj),
            'opts': self.opts,
            'object': obj,
        })
        return render(request, 'admin/sold_containers.html', context=context)

    def manager_statistics(self, request):
        managers = User.objects.filter(is_staff=True)
        context = {
            'opts': self.opts,
            'bread_name': 'Продажи (статистика)'
        }
        statistics = {}
        for m in managers:
            statistics[m] = OrderModel.get_statistics_context(m, None)
        context['statistics'] = statistics
        return render(request, 'admin/manager_statistics.html', context=context)

    def sold_containers_html(self, obj, max_count=5):
        context = OrderModel.get_statistics_context(obj, max_count)
        text = render_to_string('admin/partials/sold_containers.html', context=context)
        if context['more_count']:
            text += '<a class="button" href="{}">Просмотреть все</a>'.format(
                reverse('admin:manager_sold_containers', args=(obj.pk,))
            )
        return format_html(text)

    sold_containers_html.short_description = 'Продажи'

    def success_quests(self, obj):
        view_name = 'admin:%s_%s_change' % (self.opts.app_label, ManagerQuest._meta.model_name)
        quests = ManagerQuest.get_successes_for_manager(obj)
        buttons = [
            '<a class="button" href="{}">{}</a>'.format(
                reverse(view_name, args=(quote(q.pk),), current_app=self.admin_site.name),
                q,
            ) for q in quests
        ]
        return format_html('<br><br>'.join(buttons))

    success_quests.short_description = 'Успешно завершенные квесты'


class ChangeVIPRequestAdmin(admin.ModelAdmin):
    list_display = ('user_name', 'user_email', 'user_city', 'has_access', 'created_at',)
    list_filter = ('has_access',)

    def user_name(self, obj):
        return '{} {}'.format(obj.user.first_name, obj.user.last_name)

    user_name.short_description = 'ФИО'

    def user_email(self, obj):
        return obj.user.contact_email or obj.user.email

    user_email.short_description = 'Почта'

    def user_city(self, obj):
        return obj.user.city

    user_city.short_description = 'Город'


admin.site.register(User, UserAdminModel)
admin.site.register(ChangeVIPRequest, ChangeVIPRequestAdmin)

manager_admin.register(ChangeVIPRequest, ChangeVIPRequestAdmin)
manager_admin.register(User, UserAdminModel)
manager_admin.register(Email, EmailAdmin)
manager_admin.register(Company)
