from rest_framework import permissions
from api.models import User


class IsStaffOrTargetUser(permissions.BasePermission):
    def has_permission(self, request, view):
        try:
            user_profile = User.objects.get(
                pk=view.kwargs['pk'])
        except:
            return False

        if request.user == user_profile:
            return True

        return False

    def has_object_permission(self, request, view, obj):
        return request.user.is_staff or obj == request.user


class IsManagerUser(permissions.BasePermission):
    def has_permission(self, request, view):
        try:
            if request.user.groups.get(name='Manager'):
                return True
            else:
                return False
        except:
            return False
