import random
import string

from rest_framework import fields, serializers
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer, Serializer, CharField

from api.models import Bonus, User, Company


class CompanySerializer(ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'


class UserAuthSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('password', 'email', 'patronymic', 'first_name',
                  'last_name', 'phone', 'contact_email', 'company',
                  'photo')
        write_only_fields = ('password',)
        read_only_fields = (
            'is_staff', 'is_superuser', 'is_active', 'date_joined',)

    # def update(self, instance, validated_data):
    #     instance.set_password(validated_data.get('password'))
    #     return instance

    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        return user


class FullUserSerializer(ModelSerializer):
    company = CompanySerializer()
    is_vip_requested = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('email', 'patronymic', 'first_name',
                  'last_name', 'phone', 'contact_email', 'company',
                  'photo', 'id', 'is_vip', 'is_staff', 'city',
                  'is_vip_requested',
                  )
        read_only_fields = ('is_staff', 'is_vip_requested',)

    def get_is_vip_requested(self, obj):
        return obj.changeviprequest_set.exists()

    def update(self, instance, validated_data):
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        result = super().to_representation(instance)
        try:
            result.update({'photo': instance.photo.url})
        except ValueError:
            pass
        return result


def random_pass():
    password = ''
    for x in range(10):
        password += random.choice(string.ascii_letters + string.digits)
    return password


class UserForgotPasswordSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('email',)

    def update(self, instance, validated_data):
        new_password = random_pass()
        user = User.objects.get(email=validated_data['email'])
        user.set_password(new_password)

        from api.tasks import async_email_send
        async_email_send.delay(username=user.username,
                               new_password=new_password,
                               to_email=user.email,
                               email_type='forgot_password')
        user.save()
        return instance


class UserChangeEmailSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('email',)

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            raise ValidationError("This email already used")
        return value

    def update(self, instance, validated_data):
        from api.tasks import async_email_send
        async_email_send.delay(username=instance.username,
                               to_email=instance.email,
                               new_email=validated_data.get("email"),
                               email_type='change_email_email')

        instance.email = validated_data.get("email")

        async_email_send.delay(username=instance.username,
                               to_email=instance.email,
                               new_email=validated_data.get("email"),
                               email_type='change_email_email')
        instance.save()
        return instance


class ChangePasswordSerializer(Serializer):
    old_password = CharField(required=True)
    new_password = CharField(required=True)


class FeedbackSerializer(Serializer):
    name = fields.CharField(max_length=100)
    phone = fields.CharField(max_length=100)
    description = fields.CharField(max_length=255, required=False)


def validate_user(self, attrs):
    return FullUserSerializer(attrs).data


class BonusSerializer(ModelSerializer):
    manager = FullUserSerializer(required=False)
    expiration_date = serializers.DateField(allow_null=True, required=False, format='%d/%m/%Y', input_formats=['%d/%m/%Y'])

    class Meta:
        model = Bonus
        fields = ('pk', 'manager', 'type', 'activate_sum', 'discount', 'name',
                  'description', 'expiration_date', 'if_condition')
        read_only_fields = ('pk',)


class BonusCreateSerializer(ModelSerializer):
    # manager = FullUserSerializer(required=True)
    expiration_date = serializers.DateField(allow_null=True, required=False, format='%d/%m/%Y', input_formats=['%d/%m/%Y'])

    class Meta:
        model = Bonus
        fields = ('pk', 'manager', 'type', 'activate_sum', 'discount', 'name',
                  'description', 'expiration_date', 'if_condition')
        read_only_fields = ('pk',)
