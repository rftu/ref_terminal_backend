from rest_framework import fields
from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from api.models import OrderModel, User
from api.models.order import TradeItem
from products.models import SpecialOffer, ContainerTracking, ContainerTrackingStep
from products.serializers.products import ProductSerializer


class FinallyUpdateOrderSerializer(serializers.Serializer):
    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        instance.properties = validated_data
        instance.save()
        return instance


class PrivatePersonOrderSerializer(FinallyUpdateOrderSerializer):
    type = fields.CharField(max_length=255)
    inn = fields.CharField(max_length=255)
    snils = fields.CharField(max_length=255)
    address = fields.CharField(max_length=255)
    phone = fields.CharField(max_length=255)
    email = fields.EmailField()
    fio = fields.CharField(max_length=255)
    propiska = fields.CharField(max_length=255)
    pasport = fields.CharField(max_length=255)


class IPOrderSerializer(FinallyUpdateOrderSerializer):
    inn = fields.CharField()
    type = fields.CharField(max_length=255)
    ogrnip = fields.CharField(max_length=255)
    address = fields.CharField(max_length=255)
    phone = fields.CharField(max_length=255)
    email = fields.EmailField()
    fio = fields.CharField(max_length=255)
    propiska = fields.CharField(max_length=255)
    pasport = fields.CharField(max_length=255)


class OrganizationOrderSerializer(FinallyUpdateOrderSerializer):
    org_name = fields.CharField(max_length=255)
    inn = fields.CharField()
    type = fields.CharField(max_length=255)
    kpp = fields.CharField(max_length=255)
    checking_account = fields.CharField(max_length=255)
    correspondent_account = fields.CharField(max_length=255)
    foreign_currency_account = fields.CharField(max_length=255)
    bank = fields.CharField(max_length=255)
    bik_bank = fields.CharField(max_length=255)
    kpp_bank = fields.CharField(max_length=255)
    address = fields.CharField(max_length=255)
    post_adress = fields.CharField(max_length=255)
    phone = fields.CharField(max_length=255)
    email = fields.EmailField()
    fio = fields.CharField(max_length=255)
    okved = fields.CharField(max_length=255)
    okpo = fields.CharField(max_length=255)
    oktmo = fields.CharField(max_length=255)
    dolznost = fields.CharField(max_length=255)
    city = fields.CharField(max_length=255)


class FullTradeItemSerializer(serializers.ModelSerializer):
    container = ProductSerializer(allow_null=True, required=False, )

    class Meta:
        model = TradeItem
        fields = ('container', 'action', 'total_cost')


class TradeItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = TradeItem
        fields = ('container', 'action', 'total_cost')

    def to_internal_value(self, data):
        return TradeItem(container_id=data['container'], action=data['action'], total_cost=data.get('total_cost', 0))


class MainOrderSerializer(serializers.ModelSerializer):
    items = TradeItemSerializer(allow_null=True, required=False,
                                many=True)

    def create(self, validated_data):
        if 'request' in self.context:
            user = self.context['request'].user
            if user.is_anonymous:
                user = None
        elif 'user' in validated_data:
            user = validated_data['user']
        else:
            user = None
        try:
            order = OrderModel.objects.create(
                trade_items=validated_data['items'],
                user=user
            )
        except KeyError:
            order = OrderModel.objects.create(
                user=user
            )
        return order

    class Meta:
        model = OrderModel
        fields = ('items', 'pk', 'properties', 'user')
        read_only_fields = ('properties',)


class ShortOrderSerializer(serializers.ModelSerializer):
    common_images = SerializerMethodField(required=False)

    def get_common_images(self, obj):
        images = []
        for i, item in enumerate(obj.get_items()):
            image = item.container.content_gallery.first()
            if image is not None:
                images.append({'full_image': image.image_url,
                               'id': len(images) + 1})
        return images

    class Meta:
        model = OrderModel
        fields = ('pk', 'properties', 'created_at', 'edited_at', 'common_images')
        read_only_fields = ('properties', 'created_at', 'edited_at', 'common_images')


class UpdateOrderSerializer(serializers.Serializer):
    items = TradeItemSerializer(allow_null=True, required=False,
                                many=True)
    add_items = TradeItemSerializer(allow_null=True, required=False,
                                    many=True)
    remove_items = TradeItemSerializer(allow_null=True, required=False,
                                       many=True)

    def create(self, validated_data):
        return OrderModel.objects.create(
            trade_items=validated_data['items']
        )

    def update(self, instance, validated_data):
        try:
            [instance.add_items(data)
             for data in validated_data['add_items']]
        except KeyError:
            pass
        try:
            [instance.remove_items(data) for data in
             validated_data['remove_items']]
        except KeyError:
            pass
        return instance


class FullOrderSerializer(serializers.ModelSerializer):
    rent_trade_items = SerializerMethodField(allow_null=True, required=False)
    sale_trade_items = SerializerMethodField(allow_null=True, required=False)
    auction_trade_items = SerializerMethodField(allow_null=True, required=False)
    price_with_all_bonuses = SerializerMethodField(required=False)
    bonuses = SerializerMethodField(required=False)
    total_price = SerializerMethodField(required=False)
    total_price_rent_month = SerializerMethodField(required=False)
    total_price_rent_year = SerializerMethodField(required=False)
    total_price_auction = SerializerMethodField(required=False)

    def get_rent_trade_items(self, obj):
        items = [item for item in obj.trade_items if
                 set(item.action) >= set('rent')]
        return FullTradeItemSerializer(instance=items, many=True).data

    def get_sale_trade_items(self, obj):
        items = [item for item in obj.trade_items if item.action == 'sale']
        return FullTradeItemSerializer(instance=items, many=True).data

    def get_auction_trade_items(self, obj):
        items = [item for item in obj.trade_items if
                 set(item.action) >= set('auction')]
        return FullTradeItemSerializer(instance=items, many=True).data

    def get_price_with_all_bonuses(self, obj):
        return obj.price_with_all_bonuses

    def get_bonuses(self, obj):
        from api.serializers import BonusSerializer
        return BonusSerializer(instance=obj.bonuses, many=True).data

    def get_total_price(self, obj):
        return obj.total_price

    def get_total_price_rent_month(self, obj):
        return obj.total_price_rent_month

    def get_total_price_rent_year(self, obj):
        return obj.total_price_rent_year

    def get_total_price_auction(self, obj):
        return obj.total_price_auction

    class Meta:
        model = OrderModel
        fields = (
            'rent_trade_items', 'sale_trade_items', 'auction_trade_items',
            'pk', 'properties', 'price_with_all_bonuses',
            'bonuses', 'total_price', 'total_price_rent_month',
            'total_price_rent_year', 'total_price_auction',
            'created_at', 'edited_at',
        )
        read_only_fields = ('properties',)


class ContainerTrackingStepSerializer(serializers.ModelSerializer):
    date_at = SerializerMethodField(required=False)
    time_at = SerializerMethodField(required=False)

    def get_date_at(self, obj):
        return obj.passage_date.strftime('%d.%m.%Y')

    def get_time_at(self, obj):
        return obj.passage_date.strftime('%H:%M')

    class Meta:
        model = ContainerTrackingStep
        fields = (
            'id', 'passage_date', 'location',
            'get_status_display', 'date_at', 'time_at',
        )


class ContainerTrackingSerializer(serializers.ModelSerializer):
    container = ProductSerializer(allow_null=True, required=False)
    steps = ContainerTrackingStepSerializer(allow_null=True, many=True, required=False)

    # todo: добавить поля
    # my_order_id - номер заказа
    # fio - ФИО получателя

    class Meta:
        model = ContainerTracking
        fields = (
            'container', 'steps', 'get_service_type_display',
            'departure_date', 'delivery_date',
            'id', 'status', 'dep_date',
        )
        read_only_fields = ('container',)


class SpecialOfferSerializer(serializers.ModelSerializer):

    class Meta:
        model = SpecialOffer
        fields = (
            'id', 'name', 'user', 'containers',
            'total_cost', 'total_with_discount',
        )
        read_only_fields = ('id', 'containers',)


class FullSpecialOfferSerializer(serializers.ModelSerializer):
    containers = SerializerMethodField(required=False)

    def get_containers(self, obj):
        return ProductSerializer(instance=obj.containers.all(), many=True).data

    class Meta:
        model = SpecialOffer
        fields = (
            'containers', 'total_cost', 'total_with_discount',
        )
        read_only_fields = ('containers',)
