from datetime import datetime, timedelta

from celery.schedules import crontab
from celery.utils.log import get_task_logger

from api.models import OrderManagerModel
from ref_terminal_backend.celery import app

from api.Email import BaseEmail

logger = get_task_logger(__name__)


# @app.task
def async_email_send(**kwargs):
    BaseEmail.factory(kwargs).build_email()


@app.task
def check_payment_status():
    time_threshold = datetime.now() + timedelta(days=2)
    orders = OrderManagerModel.objects.filter(last_time__lte=time_threshold)
    for order in orders:
        manager = order.manager
        if datetime.now() + manager.company.critical_time > order.last_time:
            async_email_send.delay(email_type='full_contract_email_client',
                                   to_email=manager.email,
                                   data={'data': 'custom_data'})


app.add_periodic_task(crontab(hour='*/1', minute=0), check_payment_status)


@app.task
def update_sitemap():
    from api.management.commands.generate_xml import Command
    Command.generate_xml()


app.add_periodic_task(crontab(hour='*/5', minute=0), update_sitemap)


@app.task
def check_auctions():
    from api.management.commands.check_auctions import Command
    Command.check_auctions()


app.add_periodic_task(crontab(hour='*/1', minute=0), check_auctions)
