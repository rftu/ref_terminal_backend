from django.test import TestCase, TransactionTestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from api.models import User, Company


# Create your tests here.


class UserRegisterTestCase(APITestCase):
    def test_create_account(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse('api:main:accounts-list')
        from api.models import Company
        company = Company.objects.create(name='test')

        data = {'email': 'test@te.re',
                'password': '123456aA',
                'company': company.pk}

        response = self.client.post(url, data, format='json')
        from rest_framework.authtoken.models import Token
        self.assertEqual(Token.objects.count(), 1)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().email, 'test@te.re')

    def test_create_2_accounts(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse('api:main:accounts-list')
        from api.models import Company
        company = Company.objects.create(name='test')

        data = {'email': 'test@te.re',
                'password': '123456aA',
                'company': company.pk}

        response = self.client.post(url, data, format='json')

        data = {'email': 'dasdasd@sdasd.ru',
                'password': '123456232sA',
                'company': company.pk}

        response = self.client.post(url, data, format='json')
        from rest_framework.authtoken.models import Token
        self.assertEqual(Token.objects.count(), 2)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 2)

    def test_update_account(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse('api:main:accounts-list')
        company = Company.objects.create(name='test')

        data = {'email': 'test@te.re',
                'password': '123456aA',
                'company': company.pk}
        response = self.client.post(url, data, format='json')
        url = reverse('api:main:get_auth_token')
        data = {
            'username': 'test@te.re',
            'password': '123456aA'}

        response = self.client.post(url, data)
        token = response.data.get('token')
        url = reverse('api:main:accounts-change-password')
        data = {'password': '1234562Aa'}
        response = self.client.post(url, data, format='json',
                                    **{'HTTP_AUTHORIZATION': 'Token {}'.format(
                                        token), })
        from rest_framework.authtoken.models import Token
        self.assertEqual(Token.objects.count(), 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().email, 'test@te.re')

    def test_data_update_account(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse('api:main:accounts-list')
        company = Company.objects.create(name='test')

        data = {'email': 'test@te.re',
                'password': '123456aA',
                'company': company.pk}
        response = self.client.post(url, data, format='json')
        url = reverse('api:main:get_auth_token')
        data = {
            'username': 'test@te.re',
            'password': '123456aA'}

        response = self.client.post(url, data)
        token = response.data.get('token')
        url = reverse('api:main:account_put_get')
        headers = {'HTTP_AUTHORIZATION': 'Token {}'.format(
            token), }
        response = self.client.get(url, **headers)
        data = {'first_name': 'Danil',
                'last_name': 'you',
                'phone': '12321312',
                'contact_email': 'de@de.ru'
                # 'company': company.pk
                }

        response = self.client.put(url, data, format='json',
                                   **headers)
        from rest_framework.authtoken.models import Token
        self.assertEqual(Token.objects.count(), 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().first_name, 'Danil')

    def test_login(self):
        url = reverse('api:main:accounts-list')
        from api.models import Company
        company = Company.objects.create(name='test')

        data = {'email': 'test@te.re',
                'password': '123456aA',
                'company': company.pk}
        response = self.client.post(url, data, format='json')
        url = reverse('api:main:get_auth_token')
        data = {
            'username': 'test@te.re',
            'password': '123456aA'}

        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)

    def test_get_self_account(self):
        url = reverse('api:main:accounts-list')
        from api.models import Company
        company = Company.objects.create(name='test')

        data = {'email': 'test@te.re',
                'password': '123456aA',
                'company': company.pk}
        response = self.client.post(url, data, format='json')
        url = reverse('api:main:get_auth_token')
        data = {
            'username': 'test@te.re',
            'password': '123456aA'}

        response = self.client.post(url, data)
        token = response.data['token']
        headers = {'HTTP_AUTHORIZATION': 'Token {}'.format(
            token)}
        url = reverse('api:main:account_put_get')
        response = self.client.get(url, **headers)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['email'], 'test@te.re')


class BonusesTestCase(APITestCase):
    def set_up(self):
        from django.contrib.auth.models import Group
        self.group = Group.objects.create(name='Manager')

        # self.type_product =

    def test_create_bonus(self):
        company = Company.objects.create(name='test')
        from django.contrib.auth.models import Group
        group = Group.objects.create(name='Manager')
        url = reverse('api:main:accounts-list')
        data = {'email': 'test@te.re',
                'password': '123456aA',
                'company': company.pk}
        response = self.client.post(url, data, format='json')
        url = reverse('api:main:get_auth_token')
        data = {'username': 'test@te.re',
                'password': '123456aA'}
        user = User.objects.get(email='test@te.re')
        group.user_set.add(user)
        response = self.client.post(url, data)
        token = response.data.get('token')
        data = {
            'name': 'bonus',
            'activate_sum': '1234',
            'discount': '100.5',
            'type': 'total',
            'description': 'zopa'
        }
        headers = {'HTTP_AUTHORIZATION': 'Token {}'.format(
            token)}
        url = reverse('api:main:bonus-create')
        response = self.client.post(url, data, format='json', **headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('name'), 'bonus')
        self.assertEqual(response.data.get('manager'),
                         user.pk)

    def test_update_bonus(self):
        company = Company.objects.create(name='test')
        from django.contrib.auth.models import Group
        from api.models import Bonus
        group = Group.objects.create(name='Manager')
        url = reverse('api:main:accounts-list')
        data = {'email': 'test@te.re',
                'password': '123456aA',
                'company': company.pk}
        response = self.client.post(url, data, format='json')
        url = reverse('api:main:get_auth_token')
        data = {'username': 'test@te.re',
                'password': '123456aA'}
        user = User.objects.get(email='test@te.re')
        group.user_set.add(user)
        response = self.client.post(url, data)
        token = response.data.get('token')
        data = {
            'name': 'bonus',
            'activate_sum': '1234',
            'discount': '100.5',
            'type': 'total',
            'description': 'zopa'
        }
        headers = {'HTTP_AUTHORIZATION': 'Token {}'.format(
            token)}
        url = reverse('api:main:bonus-create')
        response = self.client.post(url, data, format='json', **headers)
        data = {
            'name': 'test',
            'activate_sum': '123',
            'discount': '10.5',
            'type': 'total',
            'description': 'zopa2'

        }
        headers = {'HTTP_AUTHORIZATION': 'Token {}'.format(
            token)}
        id = response.data['pk']
        url = reverse('api:main:bonus-update', kwargs={'pk': id})
        response = self.client.post(url, data, pk=id,
                                    format='json', **headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('name'), 'test')
        self.assertEqual(response.data.get('manager'),
                         user.pk)

        self.assertEqual(Bonus.objects.all().count(),
                         1)

    def test_create_bonus_no_manager(self):
        company = Company.objects.create(name='test')
        from django.contrib.auth.models import Group
        group = Group.objects.create(name='Manager')
        url = reverse('api:main:accounts-list')
        data = {'email': 'test@te.re',
                'password': '123456aA',
                'company': company.pk}
        url = reverse('api:main:accounts-list')
        response = self.client.post(url, data, format='json')
        url = reverse('api:main:get_auth_token')
        data = {'username': 'test@te.re',
                'password': '123456aA'}
        user = User.objects.get(email='test@te.re')
        response = self.client.post(url, data)
        token = response.data.get('token')
        data = {
            'name': 'bonus',
            'activate_sum': '1234',
            'discount': '100.5',
            'type': 'total'
        }
        headers = {'HTTP_AUTHORIZATION': 'Token {}'.format(
            token)}
        url = reverse('api:main:bonus-create')
        response = self.client.post(url, data, format='json', **headers)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_bonus_no_user(self):
        data = {
            'name': 'bonus',
            'activate_sum': '1234',
            'discount': '100.5',
            'type': 'total'
        }

        url = reverse('api:main:bonus-create')
        response = self.client.post(url, data, format='json', )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class OrdersTestCasePart1(TransactionTestCase):
    def setUp(self):
        from products.models import TypeProduct
        from products.models.main import FloatFieldProduct

        self.type_product = TypeProduct.objects.create(
            name='main'
        )
        field = FloatFieldProduct.objects.create(name='Цена со скидкой',
                                                 translate='tsena-so-skidkoi')
        field_1 = FloatFieldProduct.objects.create(name='Цена со скидкой',
                                                   translate='stoimost-arendy-v-mesiats')
        self.type_product.fields.add(field, field_1)

    def test_create_order_with_two_same_container(self):
        from api.models import OrderModel
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem

        company = Company.objects.create(name='test')
        type = TypeProduct.objects.get(name='main')

        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)
        product = RealProduct.objects.create(name='test', type=type,
                                             manager=user,
                                             property_json={
                                                 'tsena-so-skidkoi': 10000,
                                                 'stoimost-arendy-v-mesiats': 5000
                                             })
        containers = [product, product]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]
        order = OrderModel.objects.create(trade_items=trade_items)

        self.assertEqual(len(order.trade_items), 1)


class OrdersTestCase(TestCase):
    def setUp(self):
        from products.models import TypeProduct
        from products.models.main import FloatFieldProduct

        self.type_product = TypeProduct.objects.create(
            name='main'
        )
        field = FloatFieldProduct.objects.create(name='Цена со скидкой',
                                                 translate='tsena-so-skidkoi')
        field_1 = FloatFieldProduct.objects.create(name='Цена со скидкой',
                                                   translate='stoimost-arendy-v-mesiats')
        self.type_product.fields.add(field, field_1)

    def test_create_order_with_one_container_no_list_error(self):
        from api.models import OrderModel
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem

        company = Company.objects.create(name='test')

        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)
        type = TypeProduct.objects.get(name='main')

        product = RealProduct.objects.create(name='test', type=type,
                                             manager=user,
                                             property_json={
                                                 'tsena-so-skidkoi': 10000,
                                                 'stoimost-arendy-v-mesiats': 5000
                                             })
        try:
            trade_items = TradeItem(container=product,
                                    action='sale')
            order = OrderModel.objects.create(trade_items=trade_items)
        except Exception as e:
            self.assertRaisesMessage(e, 'containers must be iterable')

    def test_create_order(self):
        from api.models import OrderModel

        order = OrderModel.objects.create()

        self.assertEqual(OrderModel.objects.count(), 1)

    def test_create_order_with_one_container_kwarg_error(self):
        from api.models import OrderModel
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem

        company = Company.objects.create(name='test')
        type = TypeProduct.objects.get(name='main')

        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)
        product = RealProduct.objects.create(name='test', type=type,
                                             manager=user,
                                             property_json={
                                                 'tsena-so-skidkoi': 10000,
                                                 'stoimost-arendy-v-mesiats': 5000
                                             })
        try:
            containers = [product]
            trade_items = [TradeItem(container=container,
                                     action='sale') for
                           container in containers]
            order = OrderModel.objects.create(trad_items=trade_items)
        except Exception as e:
            self.assertRaisesMessage(e, 'please add containers')

    def test_create_order_with_one_container(self):
        from api.models import OrderModel
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem

        company = Company.objects.create(name='test')
        type = TypeProduct.objects.get(name='main')

        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)
        product = RealProduct.objects.create(name='test', type=type,
                                             manager=user,
                                             property_json={
                                                 'tsena-so-skidkoi': 10000,
                                                 'stoimost-arendy-v-mesiats': 5000
                                             })
        containers = [product]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]
        order = OrderModel.objects.create(trade_items=trade_items)
        self.assertEqual(order.managers_orders.all().count(), 1)

    def test_create_order_with_two_same_container_2(self):
        from api.models import OrderModel
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem

        company = Company.objects.create(name='test')
        type = TypeProduct.objects.get(name='main')

        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)
        product = RealProduct.objects.create(name='test', type=type,
                                             manager=user,
                                             property_json={
                                                 'tsena-so-skidkoi': 10000,
                                                 'stoimost-arendy-v-mesiats': 5000
                                             })
        trade_item = TradeItem.objects.create(container=product,
                                              action='sale')
        order = OrderModel.objects.create(trade_items=[trade_item])
        order.add_items(trade_item)
        self.assertEqual(len(order.trade_items), 1)

    def test_create_order_remove_container_good(self):
        from api.models import OrderModel
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem

        company = Company.objects.create(name='test')
        type = TypeProduct.objects.get(name='main')

        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)
        product = RealProduct.objects.create(name='test', type=type,
                                             manager=user,
                                             property_json={
                                                 'tsena-so-skidkoi': 10000,
                                                 'stoimost-arendy-v-mesiats': 5000
                                             })
        product2 = RealProduct.objects.create(name='test2', type=type,
                                              manager=user,
                                              property_json={
                                                  'tsena-so-skidkoi': 10000,
                                                  'stoimost-arendy-v-mesiats': 5000
                                              })
        containers = [product, product2]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]
        order = OrderModel.objects.create(trade_items=trade_items)
        order.remove_items(trade_items[0])
        self.assertEqual(len(order.trade_items), 1)

    def test_create_order_remove_container_bad(self):
        from api.models import OrderModel
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem

        company = Company.objects.create(name='test')
        type = TypeProduct.objects.get(name='main')

        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)
        product = RealProduct.objects.create(name='test', type=type,
                                             manager=user,
                                             property_json={
                                                 'tsena-so-skidkoi': 10000,
                                                 'stoimost-arendy-v-mesiats': 5000
                                             })
        product2 = RealProduct.objects.create(name='test2', type=type,
                                              manager=user,
                                              property_json={
                                                  'tsena-so-skidkoi': 10000,
                                                  'stoimost-arendy-v-mesiats': 5000
                                              })
        product3 = RealProduct.objects.create(name='test3', type=type,
                                              manager=user,
                                              property_json={
                                                  'tsena-so-skidkoi': 10000,
                                                  'stoimost-arendy-v-mesiats': 5000
                                              })
        containers = [product, product2]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]
        order = OrderModel.objects.create(trade_items=trade_items)
        try:
            order.remove_items(product3)
        except Exception as e:
            self.assertRaisesMessage(e, 'Container not found')

    def test_create_order_with_2_containers_one_manager(self):
        from api.models import OrderModel
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem

        company = Company.objects.create(name='test')
        type = TypeProduct.objects.get(name='main')

        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)
        product = RealProduct.objects.create(name='test', type=type,
                                             manager=user,
                                             property_json={
                                                 'tsena-so-skidkoi': 10000,
                                                 'stoimost-arendy-v-mesiats': 5000
                                             })
        product2 = RealProduct.objects.create(name='test2', type=type,
                                              manager=user,
                                              property_json={
                                                  'tsena-so-skidkoi': 10000,
                                                  'stoimost-arendy-v-mesiats': 5000
                                              })
        containers = [product, product2]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]
        order = OrderModel.objects.create(trade_items=trade_items)
        self.assertEqual(order.managers_orders.all().count(), 1)

    def test_create_order_with_2_containers_for_2_managers(self):
        from api.models import OrderModel
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem

        company = Company.objects.create(name='test')
        type = TypeProduct.objects.get(name='main')

        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)
        product = RealProduct.objects.create(name='test', type=type,
                                             manager=user,
                                             property_json={
                                                 'tsena-so-skidkoi': 10000,
                                                 'stoimost-arendy-v-mesiats': 5000
                                             })
        product2 = RealProduct.objects.create(name='test2', type=type,
                                              manager=user,
                                              property_json={
                                                  'tsena-so-skidkoi': 10000,
                                                  'stoimost-arendy-v-mesiats': 5000
                                              })

        user2 = User.objects.create(email='te@te.re',
                                    password='123456aA',
                                    company=company)
        product3 = RealProduct.objects.create(name='test', type=type,
                                              manager=user2,
                                              property_json={
                                                  'tsena-so-skidkoi': 10000,
                                                  'stoimost-arendy-v-mesiats': 5000
                                              })
        product4 = RealProduct.objects.create(name='test2', type=type,
                                              manager=user2,
                                              property_json={
                                                  'tsena-so-skidkoi': 10000,
                                                  'stoimost-arendy-v-mesiats': 5000
                                              })
        containers = [product, product2, product3, product4]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]
        order = OrderModel.objects.create(trade_items=trade_items)
        self.assertEqual(order.managers_orders.all().count(), 2)
        self.assertEqual(
            order.managers_orders.all().get(
                manager=user2).trade_items.all().count(),
            2)
        self.assertEqual(
            order.managers_orders.all().get(
                manager=user).trade_items.all().count(),
            2)

    def test_create_order_and_add_2_containers_for_2_managers(self):
        from api.models import OrderModel
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem

        company = Company.objects.create(name='test')
        type = TypeProduct.objects.get(name='main')

        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)
        product = RealProduct.objects.create(name='test', type=type,
                                             manager=user,
                                             property_json={
                                                 'tsena-so-skidkoi': 10000,
                                                 'stoimost-arendy-v-mesiats': 5000
                                             })
        product2 = RealProduct.objects.create(name='test2', type=type,
                                              manager=user,
                                              property_json={
                                                  'tsena-so-skidkoi': 10000,
                                                  'stoimost-arendy-v-mesiats': 5000
                                              })

        user2 = User.objects.create(email='te@te.re',
                                    password='123456aA',
                                    company=company)
        product3 = RealProduct.objects.create(name='test', type=type,
                                              manager=user2,
                                              property_json={
                                                  'tsena-so-skidkoi': 10000,
                                                  'stoimost-arendy-v-mesiats': 5000
                                              })
        product4 = RealProduct.objects.create(name='test2', type=type,
                                              manager=user2,
                                              property_json={
                                                  'tsena-so-skidkoi': 10000,
                                                  'stoimost-arendy-v-mesiats': 5000
                                              })
        containers = [product, product2, product3, product4]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]
        order = OrderModel.objects.create()
        for item in trade_items:
            order.add_items(item)

        self.assertEqual(order.managers_orders.all().count(), 2)
        self.assertEqual(
            order.managers_orders.all().get(
                manager=user2).trade_items.all().count(),
            2)
        self.assertEqual(
            order.managers_orders.all().get(
                manager=user).trade_items.all().count(),
            2)


class BonusInOrders(TestCase):
    def setUp(self):

        company = Company.objects.create(name='test')
        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)

        from products.models import RealProduct, TypeProduct
        from products.models.main import FloatFieldProduct
        self.type_product = TypeProduct.objects.create(
            name='main'
        )
        field = FloatFieldProduct.objects.create(name='Цена со скидкой',
                                                 translate='tsena-so-skidkoi')
        field_1 = FloatFieldProduct.objects.create(name='Цена со скидкой',
                                                   translate='stoimost-arendy-v-mesiats')
        self.type_product.fields.add(field, field_1)

        RealProduct.objects.create(name='test1',
                                   type=self.type_product,
                                   manager=user,
                                   property_json={
                                       'tsena-so-skidkoi': 10000,
                                       'stoimost-arendy-v-mesiats': 5000
                                   })

    def test_order_with_one_product_with_bonuses(self):
        from products.models import RealProduct
        from api.models import Bonus, OrderModel
        from api.models.order import TradeItem
        manager = User.objects.get(email='test@te.re')
        for i in range(4):
            Bonus.objects.create(name='test {}'.format(i),
                                 discount=i * 234, manager=manager)

        containers = [RealProduct.objects.get(name='test1')]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]
        order = OrderModel.objects.create(trade_items=trade_items)

        bonuses = order.bonuses

        self.assertEqual(len(bonuses), 4)

    def test_order_with_many_products_and_one_manager_with_bonuses(self):
        from products.models import RealProduct, TypeProduct
        from api.models import Bonus, OrderModel
        from api.models.order import TradeItem
        manager = User.objects.get(email='test@te.re')
        for i in range(4):
            Bonus.objects.create(name='test {}'.format(i),
                                 discount=i * 234, manager=manager)

        type = TypeProduct.objects.get(name='main')

        containers = [RealProduct.objects.get(name='test1'),
                      RealProduct.objects.create(name='test2',
                                                 type=type,
                                                 manager=manager,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 }),

                      RealProduct.objects.create(name='test3',
                                                 type=type,
                                                 manager=manager,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 })]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]

        order = OrderModel.objects.create(trade_items=trade_items)

        bonuses = order.bonuses

        self.assertEqual(len(bonuses), 4)

    def test_order_with_many_products_and_many_manager_with_bonuses(self):
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem
        from api.models import Bonus, OrderModel
        company = Company.objects.get(name='test')
        manager2 = User.objects.create(email='dasdsad@te.re',
                                       password='123456aA',
                                       company=company)
        manager = User.objects.get(email='test@te.re')
        for i in range(4):
            Bonus.objects.create(name='test {}'.format(i),
                                 discount=i * 234, manager=manager)
            Bonus.objects.create(name='test2 {}'.format(i),
                                 discount=i * 234, manager=manager2)

        type = TypeProduct.objects.get(name='main')

        containers = [RealProduct.objects.get(name='test1'),
                      RealProduct.objects.create(name='test2',
                                                 type=type,
                                                 manager=manager2,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 }),

                      RealProduct.objects.create(name='test3',
                                                 type=type,
                                                 manager=manager,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 })]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]

        order = OrderModel.objects.create(trade_items=trade_items)

        bonuses = order.bonuses

        self.assertEqual(len(bonuses), 8)

    def test_total_price_order(self):
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem
        from api.models import Bonus, OrderModel
        company = Company.objects.get(name='test')
        manager2 = User.objects.create(email='dasdsad@te.re',
                                       password='123456aA',
                                       company=company)
        manager = User.objects.get(email='test@te.re')
        for i in range(4):
            Bonus.objects.create(name='test {}'.format(i),
                                 activate_sum=30000,
                                 discount=i * 234, manager=manager)
            Bonus.objects.create(name='test2 {}'.format(i),
                                 activate_sum=10000,
                                 discount=i * 234, manager=manager)

        type = TypeProduct.objects.get(name='main')

        containers = [RealProduct.objects.get(name='test1'),
                      RealProduct.objects.create(name='test2',
                                                 type=type,
                                                 manager=manager2,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 }),

                      RealProduct.objects.create(name='test3',
                                                 type=type,
                                                 manager=manager,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 })]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]

        order = OrderModel.objects.create(trade_items=trade_items)

        order = OrderModel.objects.get(pk=order.pk)

        self.assertEqual(order.total_price, 30000)

    def test_order_count_active_bonuses(self):
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem
        from api.models import Bonus, OrderModel
        company = Company.objects.get(name='test')
        manager2 = User.objects.create(email='dasdsad@te.re',
                                       password='123456aA',
                                       company=company)
        manager = User.objects.get(email='test@te.re')
        for i in range(4):
            Bonus.objects.create(name='test {}'.format(i), activate_sum=30000,
                                 discount=i * 234, manager=manager)
            Bonus.objects.create(name='test2 {}'.format(i), activate_sum=10000,
                                 discount=i * 234, manager=manager)

        type = TypeProduct.objects.get(name='main')

        containers = [RealProduct.objects.get(name='test1'),
                      RealProduct.objects.create(name='test2',
                                                 type=type,
                                                 manager=manager2,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 }),

                      RealProduct.objects.create(name='test3',
                                                 type=type,
                                                 manager=manager,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 })]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]

        order = OrderModel.objects.create(trade_items=trade_items)

        bonuses = order.bonuses

        active_bonuses = [i for i in bonuses if i.apply]
        self.assertEqual(len(active_bonuses), 4)

    def test_order_count_containers(self):
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem
        from api.models import Bonus, OrderModel
        company = Company.objects.get(name='test')
        manager2 = User.objects.create(email='dasdsad@te.re',
                                       password='123456aA',
                                       company=company)
        manager = User.objects.get(email='test@te.re')
        for i in range(4):
            Bonus.objects.create(name='test {}'.format(i), activate_sum=30000,
                                 discount=i * 234, manager=manager)
            Bonus.objects.create(name='test2 {}'.format(i), activate_sum=10000,
                                 discount=i * 234, manager=manager)

        type = TypeProduct.objects.get(name='main')

        containers = [RealProduct.objects.get(name='test1'),
                      RealProduct.objects.create(name='test2',
                                                 type=type,
                                                 manager=manager2,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 }),

                      RealProduct.objects.create(name='test3',
                                                 type=type,
                                                 manager=manager,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 })]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]

        order = OrderModel.objects.create(trade_items=trade_items)

        self.assertEqual(len(order.trade_items), 3)

    def test_apply_bonus(self):
        from products.models import RealProduct, TypeProduct
        from api.models import Bonus, OrderModel
        from api.models.order import TradeItem

        company = Company.objects.get(name='test')

        manager = User.objects.get(email='test@te.re')
        Bonus.objects.create(name='test ', activate_sum=20000,
                             discount=1000, manager=manager, type='total')

        type = TypeProduct.objects.get(name='main')

        containers = [RealProduct.objects.get(name='test1'),
                      RealProduct.objects.create(name='test2',
                                                 type=type,
                                                 manager=manager,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 }),

                      RealProduct.objects.create(name='test3',
                                                 type=type,
                                                 manager=manager,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 })]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]

        order = OrderModel.objects.create(trade_items=trade_items)

        self.assertEqual(order.price_with_all_bonuses, 29000)

    def test_apply_2_bonus(self):
        from products.models import RealProduct, TypeProduct
        from api.models import Bonus, OrderModel
        from api.models.order import TradeItem
        company = Company.objects.get(name='test')

        manager = User.objects.get(email='test@te.re')
        manager2 = User.objects.create(email='dasdsad@te.re',
                                       password='123456aA',
                                       company=company)
        Bonus.objects.create(name='test ', activate_sum=20000,
                             discount=1000, manager=manager, type='total')
        Bonus.objects.create(name='test ', activate_sum=1000,
                             discount=50, manager=manager2, type='percent')

        type = TypeProduct.objects.get(name='main')

        containers = [RealProduct.objects.get(name='test1'),
                      RealProduct.objects.create(name='test2',
                                                 type=type,
                                                 manager=manager2,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 }),

                      RealProduct.objects.create(name='test3',
                                                 type=type,
                                                 manager=manager,
                                                 property_json={
                                                     'tsena-so-skidkoi': 10000,
                                                     'stoimost-arendy-v-mesiats': 5000
                                                 })]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]

        order = OrderModel.objects.create(trade_items=trade_items)

        self.assertEqual(order.price_with_all_bonuses, 24000)


class OrderViewsTestCase(APITestCase):
    def setUp(self):
        company = Company.objects.create(name='test')
        user = User.objects.create(email='test@te.re',
                                   password='123456aA',
                                   company=company)

        from products.models import RealProduct, TypeProduct
        from products.models.main import FloatFieldProduct
        self.type_product = TypeProduct.objects.create(
            name='main'
        )
        field = FloatFieldProduct.objects.create(name='Цена со скидкой',
                                                 translate='tsena-so-skidkoi')
        field_1 = FloatFieldProduct.objects.create(name='Цена со скидкой',
                                                   translate='stoimost-arendy-v-mesiats')
        self.type_product.fields.add(field, field_1)

        RealProduct.objects.create(name='test1',
                                   type=self.type_product,
                                   manager=user,
                                   property_json={
                                       'tsena-so-skidkoi': 10000,
                                       'stoimost-arendy-v-mesiats': 5000
                                   }),
        RealProduct.objects.create(name='test2',
                                   type=self.type_product,
                                   manager=user,
                                   property_json={
                                       'tsena-so-skidkoi': 10000,
                                       'stoimost-arendy-v-mesiats': 5000
                                   }),
        RealProduct.objects.create(name='test3',
                                   type=self.type_product,
                                   manager=user,
                                   property_json={
                                       'tsena-so-skidkoi': 10000,
                                       'stoimost-arendy-v-mesiats': 5000
                                   })

    def test_create_order(self):
        from products.models import RealProduct
        from api.models import OrderModel
        url = reverse('api:main:order-create')

        ids = [container.pk for container in RealProduct.objects.all()]
        data = {"items": [{'container': prod_id, 'action': 'sale'} for
                          prod_id in ids]}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 201)
        order = OrderModel.objects.get()
        self.assertEqual(len(order.trade_items), 3)

    def test_update_order_add_containers(self):
        from products.models import RealProduct
        from api.models import OrderModel
        url = reverse('api:main:order-create')

        ids = [container.pk for container in RealProduct.objects.all()]
        data = {"items": [
            {'container': ids[0],
             'action': 'sale'}
        ]}
        response = self.client.post(url, data, format='json')
        id = response.data['pk']

        url = reverse('api:main:update_containers', kwargs={'pk': id})

        data = {'add_items': [{'container': prod_id, 'action': 'sale'} for
                              prod_id in [ids[1], ids[2]]],
                'remove_items': []}

        response = self.client.put(url, data, pk=id, format='json')
        order = OrderModel.objects.get()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(order.trade_items), 3)

    def test_update_order_remove_containers(self):
        from products.models import RealProduct
        from api.models import OrderModel
        url = reverse('api:main:order-create')

        ids = [container.pk for container in RealProduct.objects.all()]
        data = {"items": [{'container': prod_id, 'action': 'sale'} for
                          prod_id in ids]}
        response = self.client.post(url, data, format='json')
        id = response.data['pk']

        url = reverse('api:main:update_containers', kwargs={'pk': id})
        data = {'remove_items': [{'container': prod_id, 'action': 'sale'} for
                                 prod_id in [ids[1], ids[2]]],
                'add_items': []}

        response = self.client.put(url, data, pk=id, format='json')
        order = OrderModel.objects.get()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(order.trade_items), 1)

    def test_update_info(self):
        from products.models import RealProduct
        from api.models import OrderModel
        url = reverse('api:main:order-create')

        ids = [container.pk for container in RealProduct.objects.all()]
        data = {"items": [
            {'container': ids[0],
             'action': 'sale'}
        ]}
        response = self.client.post(url, data, format='json')
        id = response.data['pk']

        url = reverse('api:main:update_user_info_in_order', kwargs={'pk': id})
        data = {
            "org_name": "123",
            "inn": "123",
            "type": "organization",
            "kpp": "123",
            "checking_account": "123",
            "correspondent_account": "123",
            "foreign_currency_account": "123",
            "bank": "123",
            "bik_bank": "123",
            "kpp_bank": "123",
            "address": "123",
            "post_adress": "123",
            "phone": "123",
            "email": "de@de.ru",
            "fio": "123",
            "okved": "123",
            "okpo": "123",
            "oktmo": "123",
            "dolznost": "123",
        }
        response = self.client.put(url, data, pk=id, format='json')
        order = OrderModel.objects.get()
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data['properties'], data)
        self.assertDictEqual(order.properties, data)

    def test_update_info_bad(self):
        from products.models import RealProduct
        from api.models import OrderModel
        url = reverse('api:main:order-create')

        ids = [container.pk for container in RealProduct.objects.all()]
        data = {"items": [{
            'container': ids[0],
            'action': 'sale'}
        ]}
        response = self.client.post(url, data, format='json')
        id = response.data['pk']

        url = reverse('api:main:update_user_info_in_order', kwargs={'pk': id})
        data = {
            "org_name": "123",
            "inn": "123",
            "type": "organization",
            "kpp": "123",
            "bank": "123",
            "bik_bank": "123",
            "kpp_bank": "123",
            "address": "123",
            "post_adress": "123",
            "phone": "123",
            "email": "de@de.ru",
            "fio": "123",
            "okved": "123",
            "okpo": "123",
            "oktmo": "123",
        }
        response = self.client.put(url, data, pk=id, format='json')
        order = OrderModel.objects.get()
        self.assertEqual(response.status_code, 400)
        self.assertNotEqual(order.properties, data)

    def test_get_full_order(self):
        from products.models import RealProduct
        url = reverse('api:main:order-create')

        ids = [container.pk for container in RealProduct.objects.all()]
        data_items = {"items": [{
            'container': ids[0],
            'action': 'sale'}
        ]}
        response = self.client.post(url, data_items, format='json')
        id = response.data['pk']

        url = reverse('api:main:update_user_info_in_order', kwargs={'pk': id})
        data = {
            "org_name": "123",
            "inn": "123",
            "type": "organization",
            "kpp": "123",
            "checking_account": "123",
            "correspondent_account": "123",
            "foreign_currency_account": "123",
            "bank": "123",
            "bik_bank": "123",
            "kpp_bank": "123",
            "address": "123",
            "post_adress": "123",
            "phone": "123",
            "email": "de@de.ru",
            "fio": "123",
            "okved": "123",
            "okpo": "123",
            "oktmo": "123",
            "dolznost": "123",
        }
        response = self.client.put(url, data, format='json')
        id = response.data['pk']
        url = reverse('api:main:get_order', kwargs={'pk': id})
        response = self.client.get(url, pk=id)
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data['properties'], data)
        self.assertEqual(len(response.data['trade_items']), len(data_items['items']))

    def test_order_save_valid_last_time(self):
        """
        In progress...
        """
        from api.models import OrderModel
        from products.models import RealProduct, TypeProduct
        from api.models.order import TradeItem

        type = TypeProduct.objects.get(name='main')

        user = User.objects.get(email='test@te.re')
        product = RealProduct.objects.create(name='test', type=type,
                                             manager=user,
                                             property_json={
                                                 'tsena-so-skidkoi': 10000,
                                                 'stoimost-arendy-v-mesiats': 5000
                                             })
        containers = [product]
        trade_items = [TradeItem(container=container,
                                 action='sale') for
                       container in containers]
        order = OrderModel.objects.create(trade_items=trade_items)
        url = reverse('api:main:update_user_info_in_order',
                      kwargs={'pk': order.id})
        data = {
            "org_name": "123",
            "inn": "123",
            "type": "organization",
            "kpp": "123",
            "checking_account": "123",
            "correspondent_account": "123",
            "foreign_currency_account": "123",
            "bank": "123",
            "bik_bank": "123",
            "kpp_bank": "123",
            "address": "123",
            "post_adress": "123",
            "phone": "123",
            "email": "de@de.ru",
            "fio": "123",
            "okved": "123",
            "okpo": "123",
            "oktmo": "123",
            "dolznost": "123",
        }

        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 200)
