from django.conf.urls import url, include
from django.conf.urls.static import static

from rest_framework.authtoken import views as rest_framework_views

from ref_terminal_backend import settings
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
# router.register(r'accounts', views.UserView, 'accounts')
urlpatterns = [
                  url(r'^', include(router.urls)),
                  url(r'manager/', include([
                      url(r'(?P<manager>\w+)/bonuses/',
                          views.ManagerBonusesView.as_view({'get': 'list'}),
                          name='bonus-list'),
                      url(r'bonuses/(?P<pk>[^/]+)$',
                          views.ManagerBonusesView.as_view({'post': 'update',
                                                            'delete': 'destroy'}),
                          name='bonus-update'),
                      url(r'bonuses/',
                          views.ManagerBonusesView.as_view({'post': 'create'}),
                          name='bonus-create'),

                      url(r'self/$',
                          views.UserSelfView.as_view({'get': 'retrieve',
                                                      'put': 'partial_update'}),
                          name='account_put_get'),
                      url(r'self/change_password/$',
                          views.UserSelfView.as_view({'put': 'change_password'}),
                          name='account_change_password'),
                      url(r'self/change_photo/',
                          views.UserSelfView.as_view({'put': 'change_photo'}),
                          name='account_change_photo'),
                      url(r'self/pay_vip/$',
                          views.UserSelfView.as_view({'put': 'pay_vip'}),
                          name='account_pay_vip'),

                  ])),
                  url(r'order/', include([
                      url(r'create/',
                          views.OrderCreate.as_view(), name='order-create'),
                      url(r'^add_containers/(?P<pk>[^/]+)$',
                          views.order_update_containers,
                          name='update_containers'),
                      url(r'create_bet/',
                          views.order_create_bet,
                          name='order-create-bet'),
                      url(r'^update_user_info_in_order/(?P<pk>[^/]+)$',
                          views.update_user_info_in_order,
                          name='update_user_info_in_order'),
                      url(r'^pay/(?P<pk>\d+)$', views.order_pay_view,
                          name='pay_order'),
                      url(r'(?P<pk>[^/]+)$',
                          views.OrderGet.as_view(),
                          name='get_order'),
                      url(r'my/(?P<pk>[^/]+)$',
                          views.UserOrderGet.as_view(),
                          name='get_order_full'),
                      url(r'my/$',
                          views.UserOrderListGet.as_view(),
                          name='my_orders'),
                  ])),

                  url(r'tracking/(?P<container__pk>[^/]+)$',
                      views.ContainerTrackingView.as_view(),
                      name='get_tracking'),

                  url(r'special/', include([
                      url(r'all/$',
                          views.SpecialOfferListGet.as_view(),
                          name='get_special_offer_list'),
                      url(r'available/$',
                          views.AvailableSpecialOfferListGet.as_view(),
                          name='get_available_special_offer_list'),
                      url(r'containers/$',
                          views.SpecialOfferContainersListGet.as_view(),
                          name='get_special_offer_containers'),
                      url(r'add/$',
                          views.SpecialOfferView.as_view({
                              'post': 'create'}),
                          name='special_offer_create'),
                      url(r'get/(?P<pk>\d+)$',
                          views.SpecialOfferGetView.as_view(),
                          name='special_offer_full_get'),
                      url(r'(?P<pk>\d+)$',
                          views.SpecialOfferView.as_view({
                              'put': 'update',
                              'get': 'retrieve',
                              'delete': 'destroy'}),
                          name='special_offer_del_put_get'),
                  ])),

                  url(r'^token/', include([
                      url(r'^auth_token/$',
                          rest_framework_views.obtain_auth_token,
                          name='get_auth_token'),

                  ])),

                  # Help route?
                  url(r'^help/$', views.feedback_view,
                      name='send email to company')

              ] + static(settings.MEDIA_URL,
                         document_root=settings.MEDIA_ROOT)
