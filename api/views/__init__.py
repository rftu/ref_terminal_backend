from .user_view import *
from .bonus_view import ManagerBonusesView
from .order_views import *

from rest_framework.decorators import api_view
from rest_framework.response import Response

from api.serializers import FeedbackSerializer
from api.tasks import async_email_send


@api_view(['POST'])
def feedback_view(request):
    if request.method == 'POST':
        serializer = FeedbackSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.data
            async_email_send.delay(name=data['name'],
                                   phone=data['phone'],
                                   description=data.get('description'),
                                   email_type='send_feedback'
                                   )
            return Response('Success', status=200)
        else:
            return Response(serializer.errors, status=400)
