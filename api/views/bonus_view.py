# coding=utf-8
from rest_framework import viewsets
from api.serializers import BonusCreateSerializer, BonusSerializer
from api.models import Bonus, User
from rest_framework.permissions import AllowAny
from api.permissions import IsManagerUser
from rest_framework.response import Response
from rest_framework.decorators import api_view


class ManagerBonusesView(viewsets.ModelViewSet):
    queryset = Bonus.objects.all()
    model = Bonus

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return BonusSerializer
        else:
            return BonusCreateSerializer

    def get_permissions(self):
        return (AllowAny() if self.request.method == 'GET'
                else IsManagerUser()),

    def list(self, request, *args, **kwargs):
        manager_pk = kwargs['manager']
        manager = User.objects.get(pk=manager_pk)
        bonuses = manager.bonuses.all()
        serializer = self.get_serializer(bonuses, many=True)

        return Response(serializer.data, status=200)

    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        data = request.data
        data['manager'] = request.user.pk
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=201, headers=headers)

    def update(self, request, *args, **kwargs):
        data = request.data
        bonus_pk = kwargs['pk']
        bonus = Bonus.objects.get(pk=bonus_pk)
        serializer = self.get_serializer(instance=bonus,
                                         data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=201)

