# coding=utf-8
from django.db.models import Q, Count
from django.shortcuts import get_object_or_404
from rest_framework import generics, viewsets
from rest_framework.decorators import api_view
from rest_framework.parsers import MultiPartParser, JSONParser
from rest_framework.permissions import AllowAny, IsAuthenticated, BasePermission
from rest_framework.response import Response

from api.models import OrderModel, User
from api.serializers import order_serializer
from api.serializers.order_serializer import ContainerTrackingSerializer
from products.custom_exc import EmptySpecialOffer
from products.models import RealProduct, SpecialOffer, ContainerTracking
from products.pagination_class import NormalResultsSetPagination


class OrderGet(generics.RetrieveAPIView):
    serializer_class = order_serializer.FullOrderSerializer
    permission_classes = (AllowAny,)
    queryset = OrderModel.objects.all()


class UserOrderListGet(generics.ListAPIView):
    serializer_class = order_serializer.ShortOrderSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        orders = []
        for o in OrderModel.objects.filter(user=self.request.user, pk__gte=230):
            if 'is_win' not in o.auction_properties or o.auction_properties['is_win'] is True:
                orders.append(o)
        return orders


class UserOrderGet(generics.RetrieveAPIView):
    serializer_class = order_serializer.FullOrderSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        print('get queryset')
        orders = []
        for o in OrderModel.objects.filter(user=self.request.user):
            if 'is_win' not in o.auction_properties or o.auction_properties['is_win'] is True:
                orders.append(o)
        return orders


class OrderCreate(generics.CreateAPIView):
    serializer_class = order_serializer.MainOrderSerializer
    permission_classes = (AllowAny,)


class OrderUpdate(generics.UpdateAPIView):
    serializer_class = order_serializer.UpdateOrderSerializer
    permission_classes = (AllowAny,)
    queryset = OrderModel.objects.all()


@api_view(['GET'])
def order_pay_view(request, pk):
    full_order = OrderModel.objects.get(pk=pk)
    full_order.pay()
    return Response({'status': True}, status=200)


@api_view(['PUT'])
def container_reset_status_view(request, pk):
    container = RealProduct.objects.get(pk=pk)
    container.reset_container_status()
    return Response({'status': True}, status=200)


@api_view(['PUT'])
def order_update_containers(request, pk):
    order = OrderModel.objects.get(pk=pk)
    serializer = order_serializer.UpdateOrderSerializer
    order_update = serializer(order, request.data)
    if order_update.is_valid():
        order_update.save()
        data = order_serializer.MainOrderSerializer(order).data
        return Response(data, status=200)

    else:
        return Response(status=400)


@api_view(['PUT'])
def order_create_bet(request):
    error_msg = ''
    error_code = None

    serializer = order_serializer.MainOrderSerializer(data={'user': request.user.id})
    if serializer.is_valid():
        new_item = request.data.get('item', {})

        total_cost = new_item.get('total_cost', 0)
        container = RealProduct.objects.get(id=new_item['container'])

        if not error_msg:
            if OrderModel.get_orders_for_auction(container).filter(auction_properties__is_win=True).exists():
                error_msg = 'Аукцион окончен!'
                error_code = 406

        if not error_msg:
            next_amount = container.get_next_bet_amount()
            if next_amount > total_cost:
                v = '{:,d}'.format(next_amount).replace(',', ' ')
                error_msg = 'Минимальная ставка: {} руб.'.format(v)
                error_code = 412

        if not error_msg:
            order = serializer.save()

            order.properties = request.data.get('properties', {})
            order.auction_properties = {'is_win': False, 'betAmount': total_cost}
            order.save()

            order_update = order_serializer.UpdateOrderSerializer(
                instance=order, data={'add_items': [new_item, ]}
            )
            if order_update.is_valid():
                order_update.save()

                if total_cost >= container.property_json.get('tsena-vykupa-aukcion', 0):
                    OrderModel.complete_auction(container, force=True)

        data = order_serializer.ProductSerializer(container).data
        response = Response(data={'data': data, 'err': error_msg}, status=error_code or 200)

    else:
        response = Response(status=500)

    return response


@api_view(['PUT'])
def update_user_info_in_order(request, pk):
    order = OrderModel.objects.get(pk=pk)
    serializer = serializer_types[request.data['type']]
    order_update = serializer(order, request.data)
    if order_update.is_valid():
        order_update.save()
        data = order_serializer.MainOrderSerializer(order).data
        return Response(data, status=200)
    else:
        return Response(status=400)


serializer_types = {
    'ip': order_serializer.IPOrderSerializer,
    'private': order_serializer.PrivatePersonOrderSerializer,
    'organization': order_serializer.OrganizationOrderSerializer
}


class ContainerTrackingView(generics.RetrieveAPIView):
    serializer_class = ContainerTrackingSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return get_object_or_404(ContainerTracking, container__pk=self.kwargs['container__pk'])


class IsVip(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return request.user and (request.user.is_staff or request.user.is_vip)


class SpecialOfferContainersListGet(generics.ListAPIView):
    serializer_class = order_serializer.ProductSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        a = Q(property_json__auction=True)
        b = Q(**{'property_json__{}'.format(RealProduct.MODERATION_PASSED_PROPERTY_FIELD): False})
        qs = a | b
        queryset = RealProduct.objects.filter(manager=self.request.user).exclude(qs).order_by('-pk')
        return queryset


class SpecialOfferListGet(generics.ListAPIView):
    serializer_class = order_serializer.SpecialOfferSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return SpecialOffer.objects.annotate(num_containers=Count('containers')).\
            filter(user=self.request.user, num_containers__gt=0)


class AvailableSpecialOfferListGet(generics.ListAPIView):
    serializer_class = order_serializer.SpecialOfferSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return SpecialOffer.objects.annotate(num_containers=Count('containers')).\
            filter(num_containers__gt=0)


class SpecialOfferView(viewsets.ModelViewSet):
    serializer_class = order_serializer.SpecialOfferSerializer
    parser_classes = (MultiPartParser, JSONParser,)
    pagination_class = NormalResultsSetPagination
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return SpecialOffer.objects.annotate(num_containers=Count('containers')).\
            filter(num_containers__gt=0)

    def get_object(self):
        return get_object_or_404(SpecialOffer, pk=self.kwargs['pk'])

    def update(self, request, *args, **kwargs):
        data = request.data
        data['user'] = request.user.id
        offer_pk = kwargs['pk']
        offer = SpecialOffer.objects.get(pk=offer_pk)

        products = SpecialOffer.get_products_for_offer(data['containers'], offer_pk)
        if not products.exists():
            raise EmptySpecialOffer()
        offer.containers = products
        offer.save()
        serializer = self.get_serializer(instance=offer,
                                         data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=201)

    def create(self, request, *args, **kwargs):
        data = request.data
        products = SpecialOffer.get_products_for_offer(data['containers'])
        if not products.exists():
            raise EmptySpecialOffer()
        offer = SpecialOffer.objects.create(
            user=User.objects.get(id=request.user.id),
            name=data['name'],
        )
        offer.containers = products
        offer.save()
        serializer = self.get_serializer(instance=offer,
                                         data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=201)


class SpecialOfferGetView(generics.RetrieveAPIView):
    serializer_class = order_serializer.FullSpecialOfferSerializer
    parser_classes = (MultiPartParser, JSONParser,)
    pagination_class = NormalResultsSetPagination
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return SpecialOffer.objects.get(pk=self.kwargs['pk'])
