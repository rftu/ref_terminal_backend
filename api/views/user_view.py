from rest_framework import viewsets, status
from rest_framework.decorators import list_route
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from api.models import User
from api.models.user import ChangeVIPRequest
from api.permissions import IsStaffOrTargetUser
from api.serializers import UserAuthSerializer, FullUserSerializer, ChangePasswordSerializer


class UserView(viewsets.ModelViewSet):
    serializer_class = UserAuthSerializer
    model = User

    def get_permissions(self):
        return (AllowAny() if self.request.method == 'POST'
                else IsStaffOrTargetUser()),

    @list_route(methods=['post'], permission_classes=[IsStaffOrTargetUser],
                url_name='change-password')
    def set_password(self, request):
        instance = request.user
        instance.set_password(request.data.get('password'))
        user_data = self.serializer_class(instance)
        return Response(data=user_data.data)


class UserSelfView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = FullUserSerializer
    parser_classes = (MultiPartParser, JSONParser,)
    model = User

    def get_object(self):
        return self.request.user

    def change_password(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = ChangePasswordSerializer(data=request.data)

        if serializer.is_valid():
            if not self.object.check_password(serializer.data.get('old_password')):
                return Response({'message': 'Введенный текущий пароль не совпадает.'},
                                status=status.HTTP_400_BAD_REQUEST)
            self.object.set_password(serializer.data.get('new_password'))
            self.object.save()
            return Response(status=status.HTTP_200_OK)

        return Response(status=status.HTTP_400_BAD_REQUEST)

    def pay_vip(self, request, *args, **kwargs):
        user = self.get_object()
        if ChangeVIPRequest.objects.filter(user=user).exists():
            return Response({'message': 'Вы уже отправили заявку на платную подписку. Запрос отменен'},
                             status=status.HTTP_400_BAD_REQUEST)
        else:
            ChangeVIPRequest.objects.create(user=user)
            return Response(status=status.HTTP_206_PARTIAL_CONTENT)
