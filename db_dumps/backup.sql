--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: api_bonus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE api_bonus (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    activate_sum integer NOT NULL,
    discount double precision,
    type character varying(20) NOT NULL,
    description character varying(255) NOT NULL,
    manager_id integer NOT NULL
);


ALTER TABLE api_bonus OWNER TO postgres;

--
-- Name: api_bonus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_bonus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_bonus_id_seq OWNER TO postgres;

--
-- Name: api_bonus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_bonus_id_seq OWNED BY api_bonus.id;


--
-- Name: api_company; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE api_company (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    critical_time interval,
    payment_interval interval
);


ALTER TABLE api_company OWNER TO postgres;

--
-- Name: api_company_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_company_id_seq OWNER TO postgres;

--
-- Name: api_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_company_id_seq OWNED BY api_company.id;


--
-- Name: api_email; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE api_email (
    id integer NOT NULL,
    subject_name character varying(100),
    type character varying(100) NOT NULL,
    template text
);


ALTER TABLE api_email OWNER TO postgres;

--
-- Name: api_email_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_email_id_seq OWNER TO postgres;

--
-- Name: api_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_email_id_seq OWNED BY api_email.id;


--
-- Name: api_emailconfig; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE api_emailconfig (
    id integer NOT NULL,
    company_email character varying(100) NOT NULL,
    email_domain character varying(100) NOT NULL,
    smtp_login character varying(150) NOT NULL,
    smtp_password character varying(100) NOT NULL,
    email_from character varying(50) NOT NULL,
    smtp_server character varying(50) NOT NULL,
    smtp_port integer NOT NULL
);


ALTER TABLE api_emailconfig OWNER TO postgres;

--
-- Name: api_emailconfig_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_emailconfig_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_emailconfig_id_seq OWNER TO postgres;

--
-- Name: api_emailconfig_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_emailconfig_id_seq OWNED BY api_emailconfig.id;


--
-- Name: api_ordermanagermodel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE api_ordermanagermodel (
    id integer NOT NULL,
    manager_id integer NOT NULL,
    last_time timestamp with time zone
);


ALTER TABLE api_ordermanagermodel OWNER TO postgres;

--
-- Name: api_ordermanagermodel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_ordermanagermodel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_ordermanagermodel_id_seq OWNER TO postgres;

--
-- Name: api_ordermanagermodel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_ordermanagermodel_id_seq OWNED BY api_ordermanagermodel.id;


--
-- Name: api_ordermodel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE api_ordermodel (
    id integer NOT NULL,
    properties jsonb
);


ALTER TABLE api_ordermodel OWNER TO postgres;

--
-- Name: api_ordermodel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_ordermodel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_ordermodel_id_seq OWNER TO postgres;

--
-- Name: api_ordermodel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_ordermodel_id_seq OWNED BY api_ordermodel.id;


--
-- Name: api_ordermodel_managers_orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE api_ordermodel_managers_orders (
    id integer NOT NULL,
    ordermodel_id integer NOT NULL,
    ordermanagermodel_id integer NOT NULL
);


ALTER TABLE api_ordermodel_managers_orders OWNER TO postgres;

--
-- Name: api_ordermodel_managers_orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_ordermodel_managers_orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_ordermodel_managers_orders_id_seq OWNER TO postgres;

--
-- Name: api_ordermodel_managers_orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_ordermodel_managers_orders_id_seq OWNED BY api_ordermodel_managers_orders.id;


--
-- Name: api_tradeitem; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE api_tradeitem (
    id integer NOT NULL,
    action character varying(25),
    total_cost double precision NOT NULL,
    container_id integer NOT NULL,
    manager_order_id integer
);


ALTER TABLE api_tradeitem OWNER TO postgres;

--
-- Name: api_tradeitem_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_tradeitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_tradeitem_id_seq OWNER TO postgres;

--
-- Name: api_tradeitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_tradeitem_id_seq OWNED BY api_tradeitem.id;


--
-- Name: api_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE api_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(100) NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    is_superuser boolean NOT NULL,
    patronymic character varying(255),
    phone character varying(255),
    contact_email character varying(254),
    photo character varying(100),
    company_id integer
);


ALTER TABLE api_user OWNER TO postgres;

--
-- Name: api_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE api_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE api_user_groups OWNER TO postgres;

--
-- Name: api_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_user_groups_id_seq OWNER TO postgres;

--
-- Name: api_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_user_groups_id_seq OWNED BY api_user_groups.id;


--
-- Name: api_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_user_id_seq OWNER TO postgres;

--
-- Name: api_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_user_id_seq OWNED BY api_user.id;


--
-- Name: api_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE api_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE api_user_user_permissions OWNER TO postgres;

--
-- Name: api_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE api_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE api_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: api_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE api_user_user_permissions_id_seq OWNED BY api_user_user_permissions.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE authtoken_token OWNER TO postgres;

--
-- Name: content_gallery_image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE content_gallery_image (
    id integer NOT NULL,
    image character varying(100) NOT NULL,
    "position" integer NOT NULL,
    object_id integer NOT NULL,
    content_type_id integer NOT NULL,
    CONSTRAINT content_gallery_image_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE content_gallery_image OWNER TO postgres;

--
-- Name: content_gallery_image_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE content_gallery_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE content_gallery_image_id_seq OWNER TO postgres;

--
-- Name: content_gallery_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE content_gallery_image_id_seq OWNED BY content_gallery_image.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO postgres;

--
-- Name: products_boolfieldproduct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_boolfieldproduct (
    fieldproduct_ptr_id integer NOT NULL,
    content boolean
);


ALTER TABLE products_boolfieldproduct OWNER TO postgres;

--
-- Name: products_charfieldproduct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_charfieldproduct (
    fieldproduct_ptr_id integer NOT NULL,
    content character varying(500)
);


ALTER TABLE products_charfieldproduct OWNER TO postgres;

--
-- Name: products_fieldproduct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_fieldproduct (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    translate character varying(255) NOT NULL,
    polymorphic_ctype_id integer,
    required boolean NOT NULL,
    order_number integer NOT NULL
);


ALTER TABLE products_fieldproduct OWNER TO postgres;

--
-- Name: products_fieldproduct_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_fieldproduct_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_fieldproduct_id_seq OWNER TO postgres;

--
-- Name: products_fieldproduct_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_fieldproduct_id_seq OWNED BY products_fieldproduct.id;


--
-- Name: products_filefieldproduct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_filefieldproduct (
    fieldproduct_ptr_id integer NOT NULL,
    content character varying(100)
);


ALTER TABLE products_filefieldproduct OWNER TO postgres;

--
-- Name: products_filterproduct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_filterproduct (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    translate character varying(255) NOT NULL,
    field_id integer
);


ALTER TABLE products_filterproduct OWNER TO postgres;

--
-- Name: products_filterproduct_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_filterproduct_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_filterproduct_id_seq OWNER TO postgres;

--
-- Name: products_filterproduct_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_filterproduct_id_seq OWNED BY products_filterproduct.id;


--
-- Name: products_floatfieldproduct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_floatfieldproduct (
    fieldproduct_ptr_id integer NOT NULL,
    content double precision
);


ALTER TABLE products_floatfieldproduct OWNER TO postgres;

--
-- Name: products_objectfieldproduct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_objectfieldproduct (
    fieldproduct_ptr_id integer NOT NULL,
    property jsonb
);


ALTER TABLE products_objectfieldproduct OWNER TO postgres;

--
-- Name: products_objectfieldproduct_content; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_objectfieldproduct_content (
    id integer NOT NULL,
    objectfieldproduct_id integer NOT NULL,
    fieldproduct_id integer NOT NULL
);


ALTER TABLE products_objectfieldproduct_content OWNER TO postgres;

--
-- Name: products_objectfieldproduct_content_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_objectfieldproduct_content_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_objectfieldproduct_content_id_seq OWNER TO postgres;

--
-- Name: products_objectfieldproduct_content_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_objectfieldproduct_content_id_seq OWNED BY products_objectfieldproduct_content.id;


--
-- Name: products_realproduct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_realproduct (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    property_json jsonb,
    price double precision NOT NULL,
    manager_id integer,
    type_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    edited_at timestamp with time zone NOT NULL,
    translate character varying(255) NOT NULL
);


ALTER TABLE products_realproduct OWNER TO postgres;

--
-- Name: products_realproduct_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_realproduct_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_realproduct_id_seq OWNER TO postgres;

--
-- Name: products_realproduct_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_realproduct_id_seq OWNED BY products_realproduct.id;


--
-- Name: products_selectfieldproduct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_selectfieldproduct (
    fieldproduct_ptr_id integer NOT NULL
);


ALTER TABLE products_selectfieldproduct OWNER TO postgres;

--
-- Name: products_selectfieldproduct_content; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_selectfieldproduct_content (
    id integer NOT NULL,
    selectfieldproduct_id integer NOT NULL,
    fieldproduct_id integer NOT NULL
);


ALTER TABLE products_selectfieldproduct_content OWNER TO postgres;

--
-- Name: products_selectfieldproduct_content_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_selectfieldproduct_content_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_selectfieldproduct_content_id_seq OWNER TO postgres;

--
-- Name: products_selectfieldproduct_content_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_selectfieldproduct_content_id_seq OWNED BY products_selectfieldproduct_content.id;


--
-- Name: products_sortproduct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_sortproduct (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    translate character varying(100) NOT NULL,
    field_id integer
);


ALTER TABLE products_sortproduct OWNER TO postgres;

--
-- Name: products_sortproduct_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_sortproduct_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_sortproduct_id_seq OWNER TO postgres;

--
-- Name: products_sortproduct_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_sortproduct_id_seq OWNED BY products_sortproduct.id;


--
-- Name: products_typeproduct; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_typeproduct (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    translate character varying(255) NOT NULL,
    main boolean NOT NULL
);


ALTER TABLE products_typeproduct OWNER TO postgres;

--
-- Name: products_typeproduct_fields; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_typeproduct_fields (
    id integer NOT NULL,
    typeproduct_id integer NOT NULL,
    fieldproduct_id integer NOT NULL
);


ALTER TABLE products_typeproduct_fields OWNER TO postgres;

--
-- Name: products_typeproduct_fields_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_typeproduct_fields_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_typeproduct_fields_id_seq OWNER TO postgres;

--
-- Name: products_typeproduct_fields_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_typeproduct_fields_id_seq OWNED BY products_typeproduct_fields.id;


--
-- Name: products_typeproduct_filters; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_typeproduct_filters (
    id integer NOT NULL,
    typeproduct_id integer NOT NULL,
    filterproduct_id integer NOT NULL
);


ALTER TABLE products_typeproduct_filters OWNER TO postgres;

--
-- Name: products_typeproduct_filters_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_typeproduct_filters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_typeproduct_filters_id_seq OWNER TO postgres;

--
-- Name: products_typeproduct_filters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_typeproduct_filters_id_seq OWNED BY products_typeproduct_filters.id;


--
-- Name: products_typeproduct_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_typeproduct_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_typeproduct_id_seq OWNER TO postgres;

--
-- Name: products_typeproduct_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_typeproduct_id_seq OWNED BY products_typeproduct.id;


--
-- Name: products_typeproduct_sort_fields; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE products_typeproduct_sort_fields (
    id integer NOT NULL,
    typeproduct_id integer NOT NULL,
    sortproduct_id integer NOT NULL
);


ALTER TABLE products_typeproduct_sort_fields OWNER TO postgres;

--
-- Name: products_typeproduct_sort_fields_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE products_typeproduct_sort_fields_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_typeproduct_sort_fields_id_seq OWNER TO postgres;

--
-- Name: products_typeproduct_sort_fields_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE products_typeproduct_sort_fields_id_seq OWNED BY products_typeproduct_sort_fields.id;


--
-- Name: api_bonus id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_bonus ALTER COLUMN id SET DEFAULT nextval('api_bonus_id_seq'::regclass);


--
-- Name: api_company id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_company ALTER COLUMN id SET DEFAULT nextval('api_company_id_seq'::regclass);


--
-- Name: api_email id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_email ALTER COLUMN id SET DEFAULT nextval('api_email_id_seq'::regclass);


--
-- Name: api_emailconfig id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_emailconfig ALTER COLUMN id SET DEFAULT nextval('api_emailconfig_id_seq'::regclass);


--
-- Name: api_ordermanagermodel id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_ordermanagermodel ALTER COLUMN id SET DEFAULT nextval('api_ordermanagermodel_id_seq'::regclass);


--
-- Name: api_ordermodel id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_ordermodel ALTER COLUMN id SET DEFAULT nextval('api_ordermodel_id_seq'::regclass);


--
-- Name: api_ordermodel_managers_orders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_ordermodel_managers_orders ALTER COLUMN id SET DEFAULT nextval('api_ordermodel_managers_orders_id_seq'::regclass);


--
-- Name: api_tradeitem id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_tradeitem ALTER COLUMN id SET DEFAULT nextval('api_tradeitem_id_seq'::regclass);


--
-- Name: api_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user ALTER COLUMN id SET DEFAULT nextval('api_user_id_seq'::regclass);


--
-- Name: api_user_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user_groups ALTER COLUMN id SET DEFAULT nextval('api_user_groups_id_seq'::regclass);


--
-- Name: api_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('api_user_user_permissions_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: content_gallery_image id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY content_gallery_image ALTER COLUMN id SET DEFAULT nextval('content_gallery_image_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: products_fieldproduct id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_fieldproduct ALTER COLUMN id SET DEFAULT nextval('products_fieldproduct_id_seq'::regclass);


--
-- Name: products_filterproduct id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_filterproduct ALTER COLUMN id SET DEFAULT nextval('products_filterproduct_id_seq'::regclass);


--
-- Name: products_objectfieldproduct_content id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_objectfieldproduct_content ALTER COLUMN id SET DEFAULT nextval('products_objectfieldproduct_content_id_seq'::regclass);


--
-- Name: products_realproduct id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_realproduct ALTER COLUMN id SET DEFAULT nextval('products_realproduct_id_seq'::regclass);


--
-- Name: products_selectfieldproduct_content id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_selectfieldproduct_content ALTER COLUMN id SET DEFAULT nextval('products_selectfieldproduct_content_id_seq'::regclass);


--
-- Name: products_sortproduct id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_sortproduct ALTER COLUMN id SET DEFAULT nextval('products_sortproduct_id_seq'::regclass);


--
-- Name: products_typeproduct id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct ALTER COLUMN id SET DEFAULT nextval('products_typeproduct_id_seq'::regclass);


--
-- Name: products_typeproduct_fields id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_fields ALTER COLUMN id SET DEFAULT nextval('products_typeproduct_fields_id_seq'::regclass);


--
-- Name: products_typeproduct_filters id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_filters ALTER COLUMN id SET DEFAULT nextval('products_typeproduct_filters_id_seq'::regclass);


--
-- Name: products_typeproduct_sort_fields id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_sort_fields ALTER COLUMN id SET DEFAULT nextval('products_typeproduct_sort_fields_id_seq'::regclass);


--
-- Data for Name: api_bonus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY api_bonus (id, name, activate_sum, discount, type, description, manager_id) FROM stdin;
1123	bonus	0	1	total	1	84
88	bonus	100000	222222	percent	lkjlkj	21
89	bonus	200000	10	percent	oaksdlkas;ld	21
91	bonus	999999	15	percent	15 процентов	14
92	bonus	333333	33333	total	33 богатыря	14
90	bonus	100000	10000	total	Бонус десятка	14
94	bonus	150000	15000	total	При покупке не 150 000 руб, бонус 15 000 руб.	14
95	bonus	23123	12	percent	вафавфываыф	2
96	bonus	10000	10	percent	111m  jf jhsd jn sdjn fjsdn jhs djh jhsd jhsd jhs djhs djh sdjh sjhd jhsd jhsd sd	79
97	bonus	12221	1000	total	323 .ru/static/documents \nBA%D0%B0%20%D	79
98	bonus	100000	50	percent	50% бонус!	1
99	bonus	500000	400000	total	Скидка в 400 тысяч рублей	1
51	bonus	100000	10000	total	При покупке на сумму более 100 000 руб. скидка 10 000 руб.	18
52	bonus	500000	100000	total	При покупке на 500 000 руб. скидка 100 000 руб.	18
\.


--
-- Name: api_bonus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_bonus_id_seq', 1123, true);


--
-- Data for Name: api_company; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY api_company (id, name, critical_time, payment_interval) FROM stdin;
2	Компания тест 12	2 days	2 days
1	ООО Рефтерминал	00:03:20	00:03:20
3	ООО Рефтерминал-Азия	2 days	2 days
4	ИП Дударчук Р.В.	2 days	2 days
\.


--
-- Name: api_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_company_id_seq', 4, true);


--
-- Data for Name: api_email; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY api_email (id, subject_name, type, template) FROM stdin;
2	Заполнен договор	full_contract_email_manager	<p><strong>Поздравляем!</strong> Договор заполнен.&nbsp;</p>\r\n\r\n<p><em>Текст </em>о том, <s>что</s> договор заполнен.</p>
1	Заявка на контейнер(ы) получена. Менеджер позвонит	full_contract_email_client	<p><strong>Заявка на контейнер(ы) получена</strong>. Менеджер позвонит вам в <em>ближайшее</em> время и решит вопросы по оплате.</p>\r\n\r\n<p>К письму прикреплён файл договора. Вопросы можно будет обсудить с менеджером.</p>
3	Вопрос с сайта	send_feedback	<p>Имя: {name}</p>\r\n\r\n<p>Телефон: {phone}</p>\r\n\r\n<p>Описание/вопрос: {description}</p>
\.


--
-- Name: api_email_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_email_id_seq', 3, true);


--
-- Data for Name: api_emailconfig; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY api_emailconfig (id, company_email, email_domain, smtp_login, smtp_password, email_from, smtp_server, smtp_port) FROM stdin;
1	web@rftu.ru	mail.refterminal.ru	postmaster@	b5a98821eef61d717058464b78e333ca	manager	smtp.mailgun.org	587
\.


--
-- Name: api_emailconfig_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_emailconfig_id_seq', 1, true);


--
-- Data for Name: api_ordermanagermodel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY api_ordermanagermodel (id, manager_id, last_time) FROM stdin;
179	25	\N
180	25	\N
181	25	\N
182	25	\N
183	23	\N
184	25	\N
185	25	\N
186	3	\N
187	25	\N
188	3	\N
189	3	\N
190	25	\N
191	25	\N
192	25	\N
193	25	\N
194	25	\N
195	23	\N
196	25	\N
197	25	\N
198	25	\N
199	25	\N
200	23	\N
201	23	\N
202	25	\N
203	23	\N
204	23	\N
205	2	2017-11-22 06:22:55.318228+00
206	14	\N
207	23	\N
208	14	2017-11-22 08:12:33.032567+00
209	2	2017-11-22 08:12:33.250049+00
210	23	\N
211	14	2017-11-23 01:55:21.047014+00
212	14	\N
213	23	\N
214	14	\N
215	14	2017-11-24 02:03:41.10802+00
216	25	\N
217	1	2017-11-27 03:19:24.982296+00
218	1	2017-11-27 03:21:05.474586+00
219	1	2017-11-27 05:32:06.009376+00
220	1	2017-11-27 05:34:58.421944+00
221	1	2017-11-27 05:40:10.126476+00
222	1	2017-11-27 05:43:36.613081+00
223	2	2017-11-27 05:50:42.55876+00
225	1	\N
224	1	2017-11-27 06:10:50.636076+00
226	1	2017-11-27 06:19:10.450815+00
227	1	2017-11-27 06:22:06.225894+00
228	79	\N
229	79	\N
230	1	2017-11-27 12:36:01.118445+00
231	1	\N
232	25	\N
233	79	\N
234	1	2017-11-27 14:17:03.646135+00
235	1	2017-11-27 14:21:48.686072+00
236	25	\N
237	79	\N
238	23	\N
239	14	2017-11-29 03:34:27.439646+00
240	23	\N
241	80	\N
242	80	\N
243	80	\N
244	25	\N
245	80	\N
246	25	\N
247	25	\N
248	25	\N
249	25	\N
250	25	\N
251	25	\N
252	23	\N
253	80	\N
254	25	\N
255	25	2018-01-23 01:03:05.068408+00
256	25	\N
257	25	\N
258	23	\N
259	23	\N
260	25	\N
261	23	\N
262	23	\N
263	23	\N
264	23	\N
265	79	\N
266	23	\N
267	23	\N
268	25	\N
269	25	2018-04-15 01:34:47.240267+00
270	25	\N
271	23	\N
272	25	\N
273	23	\N
274	79	\N
275	23	\N
276	23	\N
277	79	\N
278	25	\N
279	25	\N
280	25	\N
281	25	\N
282	23	\N
283	79	\N
284	80	\N
285	23	\N
286	25	\N
287	25	\N
288	25	2018-10-18 17:21:47.531+00
289	25	\N
290	25	2018-10-18 17:22:40.980687+00
291	25	\N
292	25	\N
293	80	\N
294	23	\N
295	23	\N
296	79	\N
297	80	\N
298	80	\N
299	23	\N
300	79	\N
301	25	2018-11-10 23:30:29.237224+00
302	23	\N
303	23	\N
304	88	2018-11-15 07:48:34.00203+00
305	79	\N
306	80	\N
\.


--
-- Name: api_ordermanagermodel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_ordermanagermodel_id_seq', 306, true);


--
-- Data for Name: api_ordermodel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY api_ordermodel (id, properties) FROM stdin;
377	{}
379	{}
380	{}
381	{}
382	{}
383	{}
384	{}
385	{}
386	{}
387	{}
388	{}
389	{}
390	{}
391	{}
393	{}
394	{}
395	{}
396	{}
397	{}
398	{}
399	{}
401	{}
402	{}
403	{}
404	{}
405	{}
406	{}
407	{}
409	{}
410	{}
411	{}
412	{}
413	{}
414	{}
415	{}
416	{}
417	{}
418	{}
419	{}
420	{}
421	{}
422	{}
423	{}
424	{}
425	{}
426	{}
427	{}
428	{}
429	{}
430	{}
431	{}
432	{}
433	{}
434	{}
435	{}
436	{}
494	{}
437	{}
438	{}
439	{}
495	{}
440	{}
441	{}
442	{}
443	{}
445	{}
408	{"fio": "d", "inn": "2", "type": "private", "email": "1@gmail.com", "phone": "+7 (121) 2", "snils": "2", "address": "1", "pasport": "1213121312", "propiska": "2"}
446	{}
447	{}
448	{}
449	{}
450	{}
451	{}
452	{}
453	{}
454	{}
455	{}
456	{}
457	{}
496	{}
458	{}
459	{}
460	{}
461	{}
462	{}
463	{}
464	{}
465	{}
466	{}
467	{}
468	{}
469	{}
470	{}
471	{}
472	{}
473	{}
474	{}
475	{}
476	{}
477	{}
478	{}
479	{}
480	{}
481	{}
482	{}
483	{}
484	{}
486	{}
487	{}
488	{}
444	{"fio": "Луговской", "inn": "231231", "kpp": "312312312", "bank": "ВФЫв", "okpo": "1231231241", "type": "organization", "email": "deniallugo@gmail.com", "oktmo": "314324", "okved": "4134", "phone": "+7 (914) 668-28-76", "address": "Владивосток Владивосток Ленинская, 5", "bik_bank": "132132131", "dolznost": "Дууу", "kpp_bank": "312312312", "org_name": "Аыфаф", "post_adress": "31231", "checking_account": "321312312312312312312312312312", "correspondent_account": "23123213123123123123", "foreign_currency_account": "2312312312312312"}
490	{}
491	{}
492	{}
485	{"fio": "ФИО рук", "inn": "111", "kpp": "222", "bank": "банк", "okpo": "333", "type": "organization", "email": "mail@mail.ru", "oktmo": "7777", "okved": "222", "phone": "+7 (111) 111-11-11", "address": "Владивосток адрес", "bik_bank": "111", "dolznost": "Должность рук", "kpp_bank": "222", "org_name": "Организация 123", "post_adress": "690000", "checking_account": "444", "correspondent_account": "333", "foreign_currency_account": "555"}
497	{}
498	{}
493	{"fio": "ФИО рук", "inn": "77777", "kpp": "88888", "bank": "Крипто банк", "okpo": "101010101", "type": "organization", "email": "sotnikov@rudoit.ru", "oktmo": "12121212", "okved": "99999", "phone": "+7 (777) 777-77-77", "address": "Владивосток улица дом", "bik_bank": "11111", "dolznost": "Должность рук", "kpp_bank": "22222", "org_name": "Организация 7", "post_adress": "690000", "checking_account": "44444", "correspondent_account": "33333", "foreign_currency_account": "55555"}
499	{}
500	{}
501	{}
502	{}
503	{}
400	{"fio": "Ковальчук", "inn": "123123123", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (123) 124-12-41", "snils": "12312312312", "address": "Владивосток", "pasport": "1249812940", "propiska": "Владивосток"}
505	{}
489	{"fio": "фыафыафыва", "inn": "2131231312", "type": "private", "email": "lugovskoy@rudoit.com", "phone": "+7 (112) 312-31-23", "snils": "12312312312", "address": "312312", "pasport": "3123123123", "propiska": "dasfsdfafadsfasd"}
596	{}
597	{}
598	{}
599	{}
600	{}
601	{}
602	{}
506	{}
507	{}
508	{}
509	{}
510	{}
511	{}
512	{}
513	{}
514	{}
504	{"fio": "Тор Одинсон", "inn": "1234567890", "kpp": "123456789", "bank": "0", "city": "Асгард", "okpo": "123", "type": "organization", "email": "thor.allmighty@asgard.com", "oktmo": "12345", "okved": "123456", "phone": "+7 (183) 828-23-27", "address": "Главный замок, покои Тора", "bik_bank": "0", "dolznost": "Бог грома", "kpp_bank": "0", "org_name": "Асгард 000", "post_adress": "00000000", "checking_account": "0", "correspondent_account": "0", "foreign_currency_account": "0"}
515	{}
516	{}
517	{}
518	{}
519	{}
520	{}
521	{}
392	{"fio": "ФИОПИО", "inn": "111111", "kpp": "2222", "bank": "банк1", "city": "Владивосток", "okpo": "444", "type": "organization", "email": "sotnikov@rudoit.ru", "oktmo": "555", "okved": "3333", "phone": "+7 (111) 111-11-11", "address": "ул. Улица2", "bik_bank": "666666666", "dolznost": "ДолжностьМолжность", "kpp_bank": "77777", "org_name": "Организация 1", "post_adress": "690000", "checking_account": "99999", "correspondent_account": "88888", "foreign_currency_account": "101010101"}
523	{}
524	{}
525	{}
526	{}
527	{}
528	{}
529	{}
530	{}
531	{}
532	{}
533	{}
534	{}
535	{}
536	{}
537	{}
538	{}
539	{}
540	{}
541	{}
542	{"fio": "Ковальчук", "inn": "1231231231", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (123) 124-12-41", "snils": "12312312313", "address": "Владивосток", "pasport": "1234124124", "propiska": "Владивосток"}
543	{"fio": "dasklaskfj", "inn": "1231231231", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (123) 123-12-31", "snils": "12312312312", "address": "123123123", "pasport": "1231231231", "propiska": "123123123123"}
544	{"fio": "ksdofksdlf", "inn": "1231231231", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (124) 124-12-41", "snils": "12312312312", "address": "123124124124", "pasport": "1234124124", "propiska": "123124124124"}
546	{}
545	{"fio": "asdasdasd", "inn": "1231231231", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (123) 123-12-31", "snils": "13212312312", "address": "123123123", "pasport": "123123123", "propiska": "123123123"}
547	{"fio": "kjkjkjkj", "inn": "1231231231", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (123) 123-12-31", "snils": "12312312312", "address": "123123123", "pasport": "123123123", "propiska": "12312312312"}
549	{}
550	{}
378	{"fio": "Луговской", "inn": "2312312323", "type": "private", "email": "lugovskoy@rudoit.com", "phone": "+7 (914) 668-28-76", "snils": "31312312321", "address": "3123123", "pasport": "3341234123", "propiska": "3141234123412412341243242"}
551	{}
552	{}
548	{"fio": "лщлмщлщл", "inn": "1231231231", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (123) 123-12-31", "snils": "12312312312", "address": "123123123", "pasport": "1231231231", "propiska": "123123123123"}
553	{"fio": "kovalchuk", "inn": "123123123", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (123) 123-12-31", "snils": "12312312312", "address": "1234124124", "pasport": "1241241241", "propiska": "124124124124"}
554	{"fio": "Фамилия", "inn": "1231231231", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (123) 123-12-31", "snils": "12312312312", "address": "123123123", "pasport": "123123123", "propiska": "123123123123"}
556	{}
557	{}
558	{}
559	{}
555	{"fio": "лщлщлщ", "inn": "1231231231", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (123) 123-12-31", "snils": "12312312312", "address": "123123", "pasport": "123123", "propiska": "123123"}
561	{"fio": "ko", "inn": "12312312", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (121) 231-23-12", "snils": "123", "address": "123", "pasport": "123", "propiska": "123"}
562	{}
563	{}
564	{}
560	{"fio": "kovalchuk", "inn": "123123", "type": "private", "email": "kovalchuc_andrey@mail.ru", "phone": "+7 (123) 123-12-31", "snils": "12312312", "address": "123", "pasport": "123", "propiska": "123"}
565	{}
566	{}
567	{}
568	{}
569	{}
570	{}
571	{}
572	{}
573	{}
574	{}
575	{}
576	{}
522	{"fio": "аааа", "inn": "111111", "type": "private", "email": "sotnikov@rudoit.ru", "phone": "+7 (222) 222-22-2", "snils": "22222", "address": "владивосток", "pasport": "111111", "propiska": "Владик"}
577	{}
578	{}
579	{}
580	{}
581	{}
582	{}
583	{}
584	{}
585	{}
586	{}
587	{}
588	{}
589	{}
590	{}
591	{}
592	{}
593	{}
594	{}
595	{}
603	{}
604	{}
605	{}
606	{}
607	{}
608	{}
609	{}
610	{}
611	{}
612	{}
613	{}
614	{}
615	{}
616	{}
617	{}
618	{}
619	{}
620	{}
621	{}
622	{}
623	{}
624	{}
625	{}
626	{}
627	{}
628	{}
629	{}
630	{}
631	{}
632	{}
633	{}
634	{}
635	{}
636	{}
637	{}
638	{}
639	{}
640	{}
641	{}
642	{}
643	{}
644	{}
645	{}
646	{}
647	{}
648	{}
649	{}
650	{}
651	{}
652	{}
653	{}
654	{}
655	{}
656	{}
657	{}
658	{}
659	{}
660	{}
661	{}
662	{}
663	{}
664	{}
665	{}
666	{}
667	{}
668	{}
669	{}
670	{}
671	{}
672	{}
673	{}
674	{}
675	{}
676	{}
677	{}
678	{}
679	{}
680	{}
681	{}
682	{}
683	{}
684	{}
685	{}
686	{}
687	{}
688	{}
689	{}
690	{}
691	{}
692	{}
693	{}
694	{}
695	{}
696	{}
697	{}
698	{}
699	{}
700	{}
701	{}
702	{}
703	{}
704	{}
705	{}
706	{}
707	{}
708	{}
709	{}
710	{}
711	{}
712	{}
713	{}
714	{}
715	{}
716	{}
717	{}
718	{}
719	{}
720	{}
721	{}
722	{}
723	{}
724	{}
725	{}
726	{}
727	{}
728	{}
729	{}
730	{}
731	{}
732	{}
733	{}
734	{}
735	{}
736	{}
737	{}
738	{}
739	{}
740	{}
741	{}
742	{}
743	{}
744	{}
745	{}
746	{}
747	{}
748	{}
749	{}
750	{}
751	{}
752	{}
753	{}
754	{}
755	{}
756	{}
757	{}
758	{}
759	{}
760	{}
761	{}
762	{}
763	{}
764	{}
765	{}
766	{}
767	{}
768	{}
769	{}
770	{}
771	{}
772	{}
773	{}
774	{}
775	{}
776	{}
777	{}
778	{}
779	{}
780	{}
781	{}
782	{}
783	{}
784	{}
785	{}
786	{}
787	{}
788	{}
789	{}
790	{}
791	{}
792	{}
793	{}
794	{}
795	{}
796	{}
797	{}
798	{}
799	{}
800	{}
801	{}
802	{}
803	{}
804	{}
805	{}
806	{}
807	{}
808	{}
809	{}
810	{}
811	{}
812	{}
813	{}
814	{}
815	{}
816	{}
817	{}
818	{}
819	{}
820	{}
821	{}
822	{}
823	{}
824	{}
825	{}
826	{}
827	{}
828	{}
829	{}
830	{}
831	{}
832	{}
833	{}
834	{}
835	{}
836	{}
837	{}
838	{}
839	{}
840	{}
841	{}
842	{}
843	{}
844	{}
845	{}
846	{}
847	{}
848	{}
849	{}
850	{}
851	{}
852	{}
853	{}
854	{}
855	{}
856	{}
857	{}
858	{}
859	{}
860	{}
861	{}
862	{}
863	{}
864	{}
865	{}
866	{}
867	{}
868	{}
869	{}
870	{}
871	{}
872	{}
873	{}
874	{}
875	{}
876	{}
877	{}
878	{}
879	{}
880	{}
881	{}
882	{}
883	{}
884	{}
885	{}
886	{}
887	{}
888	{}
889	{}
890	{}
891	{}
892	{}
893	{}
894	{}
895	{}
896	{}
897	{}
898	{}
899	{}
900	{}
901	{}
902	{}
903	{}
904	{}
905	{}
906	{}
907	{}
908	{}
909	{}
910	{}
911	{}
912	{}
913	{}
914	{}
915	{}
916	{}
917	{}
918	{}
919	{}
920	{}
921	{}
922	{}
923	{}
925	{}
926	{}
927	{}
928	{}
924	{"fio": "Сотников Алексей Алексеевич", "inn": "1111", "type": "private", "email": "sotnikov@rudoit.ru", "phone": "+7 (950) 291-24-56", "snils": "22222222222", "address": "Владивосток", "pasport": "0101234567", "propiska": "Владик"}
929	{}
930	{}
931	{}
932	{}
933	{}
934	{}
935	{}
936	{}
937	{}
938	{}
939	{}
940	{}
941	{}
942	{}
943	{}
944	{}
945	{}
946	{}
947	{}
948	{}
949	{}
950	{}
951	{}
952	{}
953	{}
954	{}
955	{}
956	{}
957	{}
958	{}
959	{}
960	{}
961	{}
962	{}
963	{}
964	{}
965	{}
966	{}
967	{}
968	{}
969	{}
970	{}
971	{}
972	{}
973	{}
974	{}
975	{}
976	{}
977	{}
978	{}
979	{}
980	{}
981	{}
982	{}
983	{}
984	{}
985	{}
986	{}
987	{}
988	{}
989	{}
990	{}
991	{}
992	{}
993	{}
994	{}
995	{}
996	{}
997	{}
998	{}
999	{}
1000	{}
1001	{}
1002	{}
1003	{}
1004	{}
1005	{}
1006	{}
1007	{}
1008	{}
1009	{}
1010	{}
1011	{}
1012	{}
1013	{}
1014	{}
1015	{}
1016	{}
1017	{}
1018	{}
1019	{}
1020	{}
1021	{}
1022	{}
1023	{}
1024	{}
1025	{}
1026	{}
1027	{}
1028	{}
1029	{}
1030	{}
1031	{}
1032	{}
1033	{}
1034	{}
1035	{}
1036	{}
1037	{}
1038	{}
1039	{}
1040	{}
1041	{}
1042	{}
1043	{}
1044	{}
1045	{}
1046	{}
1047	{}
1048	{}
1049	{}
1050	{}
1051	{}
1052	{}
1053	{}
1054	{}
1055	{}
1056	{}
1057	{}
1058	{}
1059	{}
1060	{}
1061	{}
1062	{}
1063	{}
1064	{}
1065	{}
1066	{}
1067	{}
1068	{}
1069	{}
1070	{}
1071	{}
1072	{}
1073	{}
1074	{}
1075	{}
1076	{}
1077	{}
1078	{}
1079	{}
1080	{}
1081	{}
1082	{}
1083	{}
1084	{}
1085	{}
1086	{}
1087	{}
1088	{}
1089	{}
1090	{}
1091	{}
1092	{}
1093	{}
1094	{}
1095	{}
1096	{}
1097	{}
1098	{}
1099	{}
1100	{}
1101	{}
1102	{}
1103	{}
1104	{}
1105	{}
1106	{}
1107	{}
1108	{}
1109	{}
1110	{}
1111	{}
1112	{}
1113	{}
1114	{}
1115	{}
1116	{}
1117	{}
1118	{}
1119	{}
1120	{}
1121	{}
1122	{}
1123	{}
1124	{}
1125	{}
1126	{}
1127	{}
1128	{}
1129	{}
1130	{}
1131	{}
1132	{}
1133	{}
1134	{}
1135	{}
1136	{}
1137	{}
1138	{}
1139	{}
1140	{}
1141	{}
1142	{}
1143	{}
1144	{}
1145	{}
1146	{}
1147	{}
1148	{}
1149	{}
1150	{}
1151	{}
1152	{}
1153	{}
1154	{}
1155	{}
1156	{}
1157	{}
1158	{}
1159	{}
1160	{}
1161	{}
1162	{}
1163	{}
1164	{}
1165	{}
1166	{}
1167	{}
1168	{}
1169	{}
1170	{}
1171	{}
1172	{}
1173	{}
1174	{}
1175	{}
1176	{}
1177	{}
1178	{}
1179	{}
1180	{}
1181	{}
1182	{}
1183	{}
1184	{}
1185	{}
1186	{}
1187	{}
1188	{}
1189	{}
1190	{}
1191	{}
1192	{}
1193	{}
1194	{}
1195	{}
1196	{}
1197	{}
1198	{}
1199	{}
1200	{}
1201	{}
1202	{}
1203	{}
1204	{}
1205	{}
1206	{}
1207	{}
1208	{}
1209	{}
1210	{}
1211	{}
1212	{}
1213	{}
1214	{}
1215	{}
1216	{}
1217	{}
1218	{}
1219	{}
1220	{}
1221	{}
1222	{}
1223	{}
1224	{}
1225	{}
1226	{}
1227	{}
1228	{}
1229	{}
1230	{}
1231	{}
1232	{}
1233	{}
1234	{}
1235	{}
1236	{}
1237	{}
1238	{}
1239	{}
1240	{}
1241	{}
1242	{}
1243	{}
1244	{}
1245	{}
1246	{}
1247	{}
1248	{}
1249	{}
1250	{}
1251	{}
1252	{}
1253	{}
1254	{}
1255	{}
1256	{}
1257	{}
1258	{}
1259	{}
1260	{}
1261	{}
1262	{}
1263	{}
1264	{}
1265	{}
1266	{}
1267	{}
1268	{}
1269	{}
1270	{}
1271	{}
1272	{}
1273	{}
1274	{}
1275	{}
1276	{}
1277	{}
1278	{}
1279	{}
1280	{}
1281	{}
1282	{}
1283	{}
1284	{}
1285	{}
1286	{}
1287	{}
1288	{}
1289	{}
1290	{}
1291	{}
1292	{}
1293	{}
1294	{}
1295	{}
1296	{}
1297	{}
1298	{}
1299	{}
1300	{"fio": "Рогалик Арменка Бенедиктович", "inn": "1234567891", "type": "private", "email": "armenka@mail.ru", "phone": "+7 (000) 000-00-00", "snils": "12345678900", "address": "25 регион", "pasport": "1234123456", "propiska": "25 регион"}
1301	{}
1302	{}
1303	{}
1304	{}
1305	{}
1306	{}
1307	{}
1308	{}
1309	{}
1310	{}
1311	{}
1312	{}
1313	{}
1314	{}
1315	{}
1316	{}
1317	{}
1318	{}
1319	{}
1320	{}
1321	{}
1322	{}
1323	{}
1324	{}
1325	{}
1326	{}
1327	{}
1328	{}
1329	{}
1330	{}
1331	{}
1332	{}
1333	{}
1334	{}
1335	{}
1336	{}
1337	{}
1338	{}
1339	{}
1340	{}
1341	{}
1342	{}
1343	{}
1344	{}
1345	{}
1346	{}
1347	{}
1348	{}
1349	{}
1350	{}
1351	{}
1352	{}
1353	{}
1354	{}
1355	{}
1356	{}
1357	{}
1358	{}
1359	{}
1360	{}
1361	{}
1362	{}
1363	{}
1364	{}
1365	{}
1366	{}
1367	{}
1368	{}
1369	{}
1370	{}
1371	{}
1372	{}
1373	{}
1374	{}
1375	{}
1376	{}
1377	{}
1378	{}
1379	{}
1380	{}
1381	{}
1382	{}
1383	{}
1384	{}
1385	{}
1386	{}
1387	{}
1388	{}
1389	{}
1390	{}
1391	{}
1392	{}
1393	{}
1394	{}
1395	{}
1396	{}
1397	{}
1398	{}
1399	{}
1400	{}
1401	{}
1402	{}
1403	{}
1404	{}
1405	{}
1406	{}
1407	{}
1408	{}
1409	{}
1410	{}
1411	{}
1412	{}
1413	{}
1414	{}
1415	{}
1416	{}
1417	{}
1418	{}
1419	{}
1420	{}
1421	{}
1422	{}
1423	{}
1424	{}
1425	{}
1426	{}
1427	{}
1428	{}
1429	{}
1430	{}
1431	{}
1432	{}
1433	{}
1434	{}
1435	{}
1436	{}
1437	{}
1438	{}
1439	{}
1440	{}
1441	{}
1442	{}
1443	{}
1444	{}
1445	{}
1446	{}
1447	{}
1448	{}
1449	{}
1450	{}
1451	{}
1452	{}
1453	{}
1454	{}
1455	{}
1456	{}
1457	{}
1458	{}
1459	{}
1460	{}
1461	{}
1462	{}
1463	{}
1464	{}
1465	{}
1466	{}
1467	{}
1468	{}
1469	{}
1470	{}
1471	{}
1472	{}
1473	{}
1474	{}
1475	{}
1476	{}
1477	{}
1478	{}
1479	{}
1480	{}
1481	{}
1482	{}
1483	{}
1484	{}
1485	{}
1486	{}
1487	{}
1488	{}
1489	{}
1490	{}
1491	{}
1492	{}
1493	{}
1494	{}
1495	{}
1496	{}
1497	{}
1498	{}
1499	{}
1500	{}
1501	{}
1502	{}
1503	{}
1504	{}
1505	{}
1506	{}
1507	{}
1508	{}
1509	{}
1510	{}
1511	{}
1512	{}
1513	{}
1514	{}
1515	{}
1516	{}
1517	{}
1518	{}
1519	{}
1520	{}
1521	{}
1522	{}
1523	{}
1524	{}
1525	{}
1526	{}
1527	{}
1528	{}
1529	{}
1530	{}
1531	{}
1532	{}
1533	{}
1534	{}
1535	{}
1536	{}
1537	{}
1538	{}
1539	{}
1540	{}
1541	{}
1542	{}
1543	{}
1544	{}
1545	{}
1546	{}
1547	{}
1548	{}
1549	{}
1550	{}
1551	{}
1552	{}
1553	{}
1554	{}
1555	{}
1556	{}
1557	{}
1558	{}
1559	{}
1560	{}
1561	{}
1562	{}
1563	{}
1564	{}
1565	{}
1566	{}
1567	{}
1568	{}
1569	{}
1570	{}
1571	{}
1572	{}
1573	{}
1574	{}
1575	{}
1576	{}
1577	{}
1578	{}
1579	{}
1580	{}
1581	{}
1582	{}
1583	{}
1584	{}
1585	{}
1586	{}
1587	{}
1588	{}
1589	{}
1590	{}
1591	{}
1592	{}
1593	{}
1594	{}
1595	{}
1596	{}
1597	{}
1598	{}
1599	{}
1600	{}
1601	{}
1602	{}
1603	{}
1604	{}
1605	{}
1606	{}
1607	{}
1608	{}
1609	{}
1610	{}
1611	{}
1612	{}
1613	{}
1614	{}
1615	{}
1616	{}
1617	{}
1618	{}
1619	{}
1620	{}
1621	{}
1622	{}
1623	{}
1624	{}
1625	{}
1626	{}
1627	{}
1628	{}
1629	{}
1630	{}
1631	{}
1632	{}
1633	{}
1634	{}
1635	{}
1636	{}
1637	{}
1638	{}
1639	{}
1640	{}
1641	{}
1642	{}
1643	{}
1644	{}
1645	{}
1646	{}
1647	{}
1648	{}
1649	{}
1650	{}
1651	{}
1652	{}
1653	{}
1654	{}
1655	{}
1656	{}
1657	{}
1658	{}
1659	{}
1660	{}
1661	{}
1662	{}
1663	{}
1664	{}
1665	{}
1666	{}
1667	{}
1668	{}
1669	{}
1670	{}
1671	{}
1672	{}
1673	{}
1674	{}
1675	{}
1676	{}
1677	{}
1678	{}
1679	{}
1680	{}
1681	{}
1682	{}
1683	{}
1684	{}
1685	{}
1686	{}
1687	{}
1688	{}
1689	{}
1690	{}
1691	{}
1692	{}
1693	{}
1694	{}
1695	{}
1696	{}
1697	{}
1698	{}
1699	{}
1700	{}
1701	{}
1702	{}
1703	{}
1704	{}
1705	{}
1706	{}
1707	{}
1708	{}
1709	{}
1710	{}
1711	{}
1712	{}
1713	{}
1714	{}
1715	{}
1716	{}
1717	{}
1718	{}
1719	{}
1720	{}
1721	{}
1722	{}
1723	{}
1724	{}
1725	{}
1726	{}
1727	{}
1728	{}
1729	{}
1730	{}
1731	{}
1732	{}
1733	{}
1734	{}
1735	{}
1736	{}
1737	{}
1738	{}
1739	{}
1740	{}
1741	{}
1742	{}
1743	{}
1744	{}
1745	{}
1746	{}
1747	{}
1748	{}
1749	{}
1750	{}
1751	{}
1752	{}
1753	{}
1754	{}
1755	{}
1756	{}
1757	{}
1758	{}
1759	{}
1760	{}
1761	{}
1762	{}
1763	{}
1764	{}
1766	{}
1767	{}
1768	{}
1769	{}
1770	{}
1771	{}
1772	{}
1773	{}
1774	{}
1775	{}
1776	{}
1777	{}
1778	{}
1779	{}
1780	{}
1781	{}
1782	{}
1783	{}
1784	{}
1785	{}
1786	{}
1787	{}
1788	{}
1789	{}
1790	{}
1791	{}
1792	{}
1793	{}
1794	{}
1795	{}
1796	{}
1797	{}
1798	{}
1799	{}
1800	{}
1801	{}
1802	{}
1803	{}
1804	{}
1805	{}
1806	{}
1807	{}
1808	{}
1809	{}
1810	{}
1811	{}
1812	{}
1813	{}
1814	{}
1815	{}
1816	{}
1817	{}
1818	{}
1819	{}
1820	{}
1821	{}
1822	{}
1823	{}
1824	{}
1825	{}
1826	{}
1827	{}
1828	{}
1829	{}
1830	{}
1831	{}
1832	{}
1833	{}
1834	{}
1835	{}
1836	{}
1837	{}
1838	{}
1839	{}
1840	{}
1841	{}
1842	{}
1843	{}
1844	{}
1845	{}
1846	{}
1847	{}
1848	{}
1849	{}
1850	{}
1851	{}
1852	{}
1853	{}
1854	{}
1855	{}
1856	{}
1857	{}
1858	{}
1859	{}
1860	{}
1861	{}
1862	{}
1863	{}
1864	{}
1865	{}
1866	{}
1867	{}
1868	{}
1869	{}
1870	{}
1871	{}
1872	{}
1873	{}
1874	{}
1875	{}
1876	{}
1877	{}
1878	{}
1879	{}
1880	{}
1881	{}
1882	{}
1883	{}
1884	{}
1885	{}
1886	{}
1887	{}
1888	{}
1889	{}
1890	{}
1891	{}
1892	{}
1893	{}
1894	{}
1895	{}
1896	{}
1897	{}
1898	{}
1899	{}
1900	{}
1901	{}
1902	{}
1903	{}
1904	{}
1905	{}
1906	{}
1907	{}
1908	{}
1909	{}
1910	{}
1911	{}
1912	{}
1913	{}
1914	{}
1915	{}
1916	{}
1917	{}
1918	{}
1919	{}
1920	{}
1921	{}
1922	{}
1923	{}
1924	{}
1925	{}
1926	{}
1927	{}
1928	{}
1929	{}
1930	{}
1931	{}
1932	{}
1933	{}
1934	{}
1935	{}
1936	{}
1937	{}
1938	{}
1939	{}
1940	{}
1941	{}
1942	{}
1943	{}
1944	{}
1945	{}
1946	{}
1947	{}
1948	{}
1949	{}
1950	{}
1951	{}
1952	{}
1953	{}
1954	{}
1955	{}
1956	{}
2047	{}
1957	{"fio": "sdasd", "inn": "123213", "type": "private", "email": "ru@ru.ru", "phone": "+7 (321) 3", "snils": "123123", "address": "213", "pasport": "213", "propiska": "123"}
1958	{}
1959	{}
1960	{}
1961	{}
1962	{}
1963	{}
1964	{}
1965	{}
1966	{}
1967	{}
1968	{}
1969	{}
1970	{}
1971	{}
1972	{}
1973	{}
1974	{}
1975	{}
1976	{}
1977	{}
1978	{}
1979	{}
1980	{}
1981	{}
1982	{}
1983	{}
1984	{}
1985	{}
1986	{}
1987	{}
1988	{}
1989	{}
1990	{}
1991	{}
1992	{}
1993	{}
1994	{}
1995	{}
1996	{}
1997	{}
1998	{}
1999	{}
2000	{}
2001	{}
2002	{}
2003	{}
2004	{}
2005	{}
2006	{}
2007	{}
2008	{}
2009	{}
2010	{}
2011	{}
2012	{}
2013	{}
2014	{}
2015	{}
2016	{}
2017	{}
2018	{}
2019	{}
2020	{}
2021	{}
2022	{}
2023	{}
2024	{}
2025	{}
2026	{}
2027	{}
2028	{}
2029	{}
2030	{}
2031	{}
2032	{}
2033	{}
2034	{}
2035	{}
2036	{}
2037	{}
2038	{}
2039	{}
2040	{}
2041	{}
2042	{}
2043	{}
2044	{}
2045	{}
2046	{}
2048	{}
2049	{}
2050	{}
2051	{}
2052	{}
2053	{}
2054	{}
2055	{}
2056	{}
2057	{}
2058	{}
2059	{}
2060	{}
2061	{}
2062	{}
2063	{}
2064	{}
2065	{}
2066	{}
2067	{}
2068	{}
2069	{}
2070	{}
2071	{}
2072	{}
2073	{}
2074	{}
2075	{}
2076	{}
2077	{}
2078	{}
2079	{}
2080	{}
2081	{}
2082	{}
2083	{}
2084	{}
2085	{}
2086	{}
2087	{}
2088	{}
2089	{}
2090	{}
2091	{}
2092	{}
2093	{}
2094	{}
2095	{}
2096	{}
2097	{}
2098	{}
2099	{}
2100	{}
2101	{}
2102	{}
2103	{}
2104	{}
2105	{}
2106	{}
2107	{}
2108	{}
2109	{}
2110	{}
2111	{}
2112	{}
2113	{}
2114	{}
2115	{}
2116	{}
2117	{}
2118	{}
2119	{}
2120	{}
2121	{}
2122	{}
2123	{}
2124	{}
2125	{}
2126	{}
2127	{}
2128	{}
2129	{}
2130	{}
2131	{}
2132	{}
2133	{}
2134	{}
2135	{}
2136	{}
2137	{}
2138	{}
2139	{}
2140	{}
2141	{}
1765	{"fio": "Дырыгина", "inn": "1234567891", "type": "private", "email": "vt@ya.ru", "phone": "+7 (914) 070-02-77", "snils": "12356677889", "address": "Проглотим", "pasport": "7412558741", "propiska": "Впролширсаа"}
2142	{}
2143	{}
2144	{}
2145	{}
2146	{}
2147	{}
2148	{}
2149	{}
2150	{}
2151	{}
2152	{}
2153	{}
2154	{}
2155	{}
2156	{}
2157	{}
2158	{}
2159	{}
2160	{}
2161	{}
2162	{}
2163	{}
2164	{}
2165	{}
2166	{}
2167	{}
2168	{}
2169	{}
2170	{}
2171	{}
2172	{}
2173	{}
2174	{}
\.


--
-- Name: api_ordermodel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_ordermodel_id_seq', 2174, true);


--
-- Data for Name: api_ordermodel_managers_orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY api_ordermodel_managers_orders (id, ordermodel_id, ordermanagermodel_id) FROM stdin;
482	392	215
484	400	217
485	542	218
486	543	219
487	544	220
488	545	221
489	547	222
490	378	223
491	548	224
493	553	226
494	554	227
496	504	229
497	555	230
501	560	234
502	561	235
506	522	239
507	391	240
510	732	243
511	733	244
515	735	248
517	842	250
518	843	251
520	922	253
522	924	255
524	890	257
528	1017	261
531	722	264
536	1300	269
539	1095	272
540	1095	273
541	1201	274
542	1462	275
543	1576	276
546	1635	279
547	1684	280
548	1544	281
549	1708	282
551	1758	284
554	1931	287
450	396	183
557	1957	290
558	2004	291
559	1989	292
560	1989	293
561	2000	294
562	2023	295
563	2031	296
565	2061	298
567	1804	300
570	1840	303
573	2155	306
469	408	202
470	452	203
471	458	204
472	444	205
475	485	208
476	485	209
478	493	211
479	489	212
\.


--
-- Name: api_ordermodel_managers_orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_ordermodel_managers_orders_id_seq', 573, true);


--
-- Data for Name: api_tradeitem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY api_tradeitem (id, action, total_cost, container_id, manager_order_id) FROM stdin;
796	sale	550000	329	279
505	rent_year	123523	268	224
408	sale	647000	262	183
797	sale	550000	329	280
507	rent_month	999999	280	224
508	sale	100000	281	226
798	sale	550000	329	281
510	rent_month	123123	268	226
511	rent_year	999999	280	226
799	sale	680000	278	282
513	sale	100000	281	227
514	rent_month	123123	268	227
515	rent_year	999999	280	227
800	sale	680000	277	282
802	sale	120000	286	284
713	rent_year	10000	282	229
714	rent_year	100000	283	229
805	sale	550000	329	287
808	sale	555000	330	290
809	sale	20000	328	291
810	sale	20000	328	292
811	sale	120000	286	293
812	sale	555000	325	294
813	sale	585000	322	295
814	rent_month	1000	282	296
727	sale	100000	281	230
438	sale	450000	259	202
439	sale	647000	262	203
440	sale	460000	265	204
441	sale	647000	262	204
442	sale	1321	270	205
816	sale	120000	286	298
444	rent_year	312	269	205
818	rent_month	10000	283	300
448	sale	1000000	264	208
449	sale	1321	270	209
450	rent_month	120000	271	208
452	sale	1200000	274	211
453	rent_month	120000	271	211
454	sale	1000000	264	211
455	sale	1000000	264	212
456	sale	1200000	274	212
733	rent_year	123523	268	230
734	rent_month	999999	280	230
821	sale	555000	325	303
460	sale	1200000	274	215
461	rent_month	120000	271	215
462	sale	1000000	264	215
464	sale	150000	268	217
465	rent_month	123123	268	218
467	rent_year	123123	268	219
471	rent_year	123523	268	220
827	sale	120000	286	306
473	rent_year	123523	268	221
475	rent_year	123523	268	222
476	sale	1321	270	223
477	rent_month	231	269	223
479	sale	100000	281	224
745	sale	590000	281	234
746	sale	590000	281	235
750	rent_month	120000	271	239
751	sale	680000	277	240
754	sale	120000	286	243
755	sale	775000	287	244
759	sale	510000	305	248
761	sale	520000	304	250
762	sale	510000	305	251
766	sale	510000	305	250
767	sale	120000	286	253
769	sale	520000	304	255
771	sale	520000	306	257
775	sale	580000	311	261
778	sale	580000	311	264
783	sale	750000	287	269
784	sale	555000	330	269
787	sale	648000	303	272
788	sale	650000	315	273
789	rent_month	10000	283	274
792	sale	900000	272	275
793	sale	650000	317	276
\.


--
-- Name: api_tradeitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_tradeitem_id_seq', 827, true);


--
-- Data for Name: api_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY api_user (id, password, last_login, username, first_name, last_name, date_joined, email, is_staff, is_active, is_superuser, patronymic, phone, contact_email, photo, company_id) FROM stdin;
82	pbkdf2_sha256$36000$66zF1J1G1TqE$CWX5UqA/aDojAWCUpRRrnMP6ITyFeNN6yq8AgIq8y7M=	2018-01-11 09:17:54.455394+00	deniallugo@gmail.com			2018-01-11 09:17:45.716597+00	deniallugo@gmail.com	t	t	t	\N	\N	\N		\N
1	pbkdf2_sha256$36000$338cv0PW6q86$yx9Ks4lc99VNFWKTRFIU+DwERHKVEHUZjFRW/VRoq78=	2017-11-13 06:02:28.144105+00	kovalchuc_andrey@mail.ru			2017-08-29 01:55:15.836+00	kovalchuc_andrey@mail.ru	t	t	t	\N	\N	\N		1
74	pbkdf2_sha256$36000$IPuHc5ple5kL$IBGoryn3GSIHdewARu3w8uANVSEe5LCLAmHzd/Pfq5A=	\N	default			2017-11-13 06:18:26.696742+00	default	f	t	f	\N	\N	\N		\N
87	pbkdf2_sha256$36000$JdDOamKz4Rzn$7XcYx9gg+n/7+29PGq2ti2FkIDDQbdVZLz0U+sw3Z+U=	\N	cso@rftu.ru		Дырыгина Мария Васильевна	2018-10-18 01:27:01.517019+00	cso@rftu.ru	t	t	f	\N	8-984-190-10-53	cso@rftu.ru		1
77	pbkdf2_sha256$36000$hkA2YCbgstE1$/4aiH4IHBLtoBYbvKfoVT9xiDsJL4WFrGDI2C87BbeA=	\N	da@de.ru			2017-11-15 06:25:41.331292+00	da@de.ru	f	t	f	\N	\N	\N		\N
78	pbkdf2_sha256$36000$DVd4EnbpOTCs$H2esm1TflhF9wxVLcjCu4whThuqSw7lqFT1Upojk9/8=	2017-11-15 06:27:20.020225+00	ds@de.ru		Ву	2017-11-15 06:26:42.326681+00	ds@de.ru	t	t	f	\N	+79146682876	danil@gmail.com		1
79	pbkdf2_sha256$36000$1OO8KyoESmZt$Cf+ueHK1frVLhVObSWgoBWSyjUHabd8sal4k5mdBoAc=	\N	Daria@gmail.com	Тор	Одинсон	2017-11-23 02:58:38.656434+00	Daria@gmail.com	t	t	f	\N	+7 (000) 023-77-48	thor.allmighty@asgard.com	user_79/thor.jpg	1
26	pbkdf2_sha256$36000$338cv0PW6q86$yx9Ks4lc99VNFWKTRFIU+DwERHKVEHUZjFRW/VRoq78=	\N	andrey@testmail.ru	Andrey	Kovalchuk	2017-10-30 12:13:32+00	andrey@testmail.ru	f	t	f	123	123	andrey@testmail.ru	user_None/73439262_2873219_D4EBE0E320D0EEF1F1E8E8.jpg	1
84	pbkdf2_sha256$36000$KdFkVW3G36hD$4eOIgL6x2VuwZ9SX12DY/FagqHE/G1gID20gPWVTtlA=	\N	shonova@daria.ru		Дарья	2018-01-23 06:47:34.278127+00	shonova@daria.ru	f	t	f	\N	+7 (122) 323-23-23	1@1.com		\N
30	8520123qwer	\N	TESTOUSER	Имя	Фамилия	2017-11-07 10:02:34+00	my_test_email@bomba.ru	t	t	f	123	123	123@123.ru	user_None/73439262_2873219_D4EBE0E320D0EEF1F1E8E8_HAu2lub.jpg	1
89	pbkdf2_sha256$36000$4YFpWnkogjSX$yXCErk0ZVOyjA+95yP/tiRz5E3swIoDubqs+v83AFNY=	\N	refagent@rftu.ru		Кайт Илья	2018-10-24 02:11:57.645183+00	refagent@rftu.ru	f	t	f	\N	+79089887308	refagent@rftu.ru		1
86	pbkdf2_sha256$36000$N1hSHwf5eTyy$yUFHYmU4Owzj6jBx+m4IEQjLPE+bfUdeP/dzz5rnQ5s=	\N	ermine-dv@mail.ru		Дударчук Роман Валентинович	2018-04-13 02:39:20.236923+00	ermine-dv@mail.ru	f	t	f	\N	+79089919886	ermine-dv@mail.ru	user_86/IMG_1587.JPG	4
20	pbkdf2_sha256$36000$YRgjvIuDjFux$bcyDGuoSJdmhOt+15LmRTFebayYbwI/uhHNwpDEZWpI=	\N	AVK@rftu.ru	Андрей		2017-10-27 03:30:36+00	AVK@rftu.ru	t	t	t	Валерьевич	+79999999999	AVK@rftu.ru	user_20/20t.jpg	1
22	pbkdf2_sha256$36000$Zt6tCBcY1pSc$Q1TiBiOYzImNt+8652a+Gv+o4YzWS2nHscVts3e9p28=	\N	avk@rftu.ru		Андрей Коган	2017-10-27 04:11:00.630824+00	avk@rftu.ru	f	t	f	\N	322-22-32	AVK@RFTU.RU		1
88	pbkdf2_sha256$36000$z676vLbAVIlb$RJxvRFT1+K3a1Hl8n3AZcXhC97qG8ULrw6LBK83a15E=	\N	container@rftu.ru		Гулеватая Василина Егоровна	2018-10-18 01:33:27.653376+00	container@rftu.ru	f	t	f	\N	+7 (894) 190-10-58	container@rftu.ru	user_88/photo_2018-10-25_13-20-36.jpg	1
27	pbkdf2_sha256$36000$3w2heegaVKwW$SF8DKSDK5FiuHHJo1yEPsd5KIpNo1t5KO43/icvvbMk=	\N	test@testemail.ru		Test manager	2017-10-30 12:19:05+00	test@test-mail.ru	f	t	t	123	123	test@testemail.ru	user_27/73439262_2873219_D4EBE0E320D0EEF1F1E8E8.jpg	2
14	pbkdf2_sha256$36000$HBb4XtR9Ftw3$I/U7YsrpldwzQka1hF5KNDZ59NWqLy3yVt3wK+kEX7w=	2018-01-22 23:45:18.42198+00	sotnikov@rudoit.ru		Иваныч	2017-09-11 13:38:03+00	sotnikov@rudoit.ru	t	t	t	123	+7 (123) 456-78-90	111@222.io	user_14/2017-09-19_13-20-10.png	1
3	pbkdf2_sha256$36000$o791PrxFfXbo$4ScKlzWRJBeH6C1FTTZfQJumUio+rzl2ALP2GvCxohQ=	\N	daria.shonova@gmail.com	Дарья	Шонова	2017-08-31 05:24:40.196+00	daria.shonova@gmail.com	t	t	f	\N	+7 (914) 662-74-40	daria.shonova@gmail.com	user_3/thor_W6gA5f8.jpg	1
21	pbkdf2_sha256$36000$gg4U4Sh7sNJ4$HBBahVld67C3HCNBJRKMTbY4c6wT9e5xaoc6weg6gqc=	2018-02-28 00:47:32.984499+00	wildpixel.dv@gmail.com		Тест тест	2017-10-27 03:39:10+00	wildpixel.dv@gmail.com	t	t	f	Оксана	+79999999999	wildpixel.dv@gmail.com	user_21/20t.jpg	1
25	pbkdf2_sha256$36000$ISsXKJZA4uny$odCmCM+Kmw35RoZrbyhqrZ5+6aBdh4B+bJIpzSLjJFc=	\N	vt@rftu.ru		Владимир	2017-10-30 00:57:04.63436+00	vt@rftu.ru	f	t	f	\N	+7 (914) 790-45-34	vt@rftu.ru	user_25/DSC_8323.JPG	1
2	pbkdf2_sha256$36000$su8EfkNXlqXN$3dzMY9Y38+HGDe8hEuCLrVD5CtTnnujO+ts+jNji4wU=	2018-01-23 00:16:11.602696+00	de@de.ru		Данил	2017-08-30 05:24:37+00	de@de.ru	t	t	t	ods	+7 (312) 312-31-31	lugovskoy@rudoit.com	Screenshot_2017-06-30-12-10-04-1.png	1
85	pbkdf2_sha256$36000$V9J2SfnOSogb$XGLaX6252Ri2Px8GloIiLjuBn5hou/RWHp0/WVUUHzc=	\N	ref-007@rftu.ru		Спиркин Сергей Александрович	2018-04-10 04:22:48.078632+00	ref-007@rftu.ru	f	t	f	\N	+79024871936	ref-007@rftu.ru		1
80	pbkdf2_sha256$36000$suHq5nbaagyj$FdEcjLWVZAxGkz+qf6bpr8AiZWlgAOJaOS8XOIH5lHI=	\N	ice@rftu.ru		Луговая Вера Сергеевна	2017-11-29 09:18:25.315534+00	ice@rftu.ru	t	t	f	\N	+7 (909) 911-97-44	ice@rftu.ru	user_80/FqlZPl92dRk.jpg	3
23	pbkdf2_sha256$36000$JsBdXuY5YFHp$6SmcO0JQ5/wEBWFo0RGwsEJJuCpRPyiM0vMKgLbmWZc=	\N	ap@rftu.ru		Александра Поволоцкая	2017-10-27 04:28:10.899628+00	ap@rftu.ru	f	t	f	\N	+7 (914) 791-27-59	ap@rftu.ru	user_23/pp.jpg	1
81	pbkdf2_sha256$36000$K6VE8CF92GfX$9dbi6/a3WjeSS9CmifQrFffFcfpBqKR3c1p39wyQlZg=	\N	vertex@rftu.ru		Булычева Ангелина Евгеньевна	2017-11-29 09:47:57.429229+00	vertex@rftu.ru	f	f	f	\N	+79099119366	vertex@rftu.ru		3
18	pbkdf2_sha256$36000$MSzPfUmNIQ7z$v3Pt+iC6ncfsUfQSWbZwcX5Iy7XfJ6d42KHQbfPAnMI=	2018-10-21 23:22:36.815774+00	admin@refterminal.ru		Админ Иванович	2017-09-19 01:43:12+00	admin@refterminal.ru	t	t	f	Admin	+7 (888) 888-89-01	admin@refterminal.ru	20t_tWYWtbL.jpg	1
\.


--
-- Data for Name: api_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY api_user_groups (id, user_id, group_id) FROM stdin;
1	2	1
4	14	1
6	18	1
7	20	1
8	21	1
9	26	1
10	27	1
11	1	1
12	30	1
14	78	1
15	79	1
16	80	1
17	81	1
18	82	1
20	84	1
21	85	1
22	86	1
23	87	1
24	88	1
25	89	1
\.


--
-- Name: api_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_user_groups_id_seq', 25, true);


--
-- Name: api_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_user_id_seq', 89, true);


--
-- Data for Name: api_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY api_user_user_permissions (id, user_id, permission_id) FROM stdin;
1	21	60
2	30	46
3	30	47
\.


--
-- Name: api_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('api_user_user_permissions_id_seq', 3, true);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group (id, name) FROM stdin;
1	Manager
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, true);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	46
2	1	47
3	1	48
4	1	49
5	1	50
6	1	51
7	1	22
8	1	23
9	1	24
10	1	128
11	1	129
12	1	52
13	1	53
14	1	54
15	1	58
16	1	59
17	1	60
18	1	61
19	1	62
20	1	63
21	1	70
22	1	71
23	1	72
24	1	73
25	1	74
26	1	75
27	1	109
28	1	110
29	1	111
30	1	112
31	1	113
32	1	114
33	1	115
34	1	116
35	1	117
36	1	118
37	1	119
38	1	120
39	1	121
40	1	122
41	1	123
42	1	124
43	1	125
44	1	126
45	1	127
46	1	43
47	1	163
48	1	164
49	1	165
50	1	166
51	1	167
52	1	168
53	1	56
54	1	57
55	1	44
56	1	55
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 56, true);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
163	Can add Город	14	add_citycontainermodel
164	Can change Город	14	change_citycontainermodel
165	Can delete Город	14	delete_citycontainermodel
166	Can add Площадка продажи	23	add_placecontainermodel
167	Can change Площадка продажи	23	change_placecontainermodel
168	Can delete Площадка продажи	23	delete_placecontainermodel
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add field product	6	add_fieldproduct
17	Can change field product	6	change_fieldproduct
18	Can delete field product	6	delete_fieldproduct
19	Can add filter product	7	add_filterproduct
20	Can change filter product	7	change_filterproduct
21	Can delete filter product	7	delete_filterproduct
22	Can add real product	8	add_realproduct
23	Can change real product	8	change_realproduct
24	Can delete real product	8	delete_realproduct
25	Can add type product	9	add_typeproduct
26	Can change type product	9	change_typeproduct
27	Can delete type product	9	delete_typeproduct
28	Can add bool field product	10	add_boolfieldproduct
29	Can change bool field product	10	change_boolfieldproduct
30	Can delete bool field product	10	delete_boolfieldproduct
31	Can add char field product	11	add_charfieldproduct
32	Can change char field product	11	change_charfieldproduct
33	Can delete char field product	11	delete_charfieldproduct
34	Can add file field product	12	add_filefieldproduct
35	Can change file field product	12	change_filefieldproduct
36	Can delete file field product	12	delete_filefieldproduct
37	Can add float field product	13	add_floatfieldproduct
38	Can change float field product	13	change_floatfieldproduct
39	Can delete float field product	13	delete_floatfieldproduct
40	Can add select field product	14	add_selectfieldproduct
43	Can add user	15	add_user
41	Can change select field product	14	change_selectfieldproduct
42	Can delete select field product	14	delete_selectfieldproduct
44	Can change user	15	change_user
45	Can delete user	15	delete_user
46	Can add bonus	16	add_bonus
47	Can change bonus	16	change_bonus
48	Can delete bonus	16	delete_bonus
49	Can add company	17	add_company
50	Can change company	17	change_company
51	Can delete company	17	delete_company
52	Can add email	18	add_email
53	Can change email	18	change_email
54	Can delete email	18	delete_email
55	Can add конфигурация отправки email	19	add_emailconfig
56	Can change конфигурация отправки email	19	change_emailconfig
57	Can delete конфигурация отправки email	19	delete_emailconfig
58	Can add order manager model	20	add_ordermanagermodel
59	Can change order manager model	20	change_ordermanagermodel
60	Can delete order manager model	20	delete_ordermanagermodel
61	Can add order model	21	add_ordermodel
62	Can change order model	21	change_ordermodel
63	Can delete order model	21	delete_ordermodel
64	Can add Token	22	add_token
65	Can change Token	22	change_token
66	Can delete Token	22	delete_token
67	Can add object field product	23	add_objectfieldproduct
68	Can change object field product	23	change_objectfieldproduct
69	Can delete object field product	23	delete_objectfieldproduct
70	Can add Размер	23	add_measurecontainermodel
71	Can change Размер	23	change_measurecontainermodel
72	Can delete Размер	23	delete_measurecontainermodel
73	Can add image	25	add_image
74	Can change image	25	change_image
75	Can delete image	25	delete_image
76	Can add container type	14	add_containertype
77	Can change container type	14	change_containertype
78	Can delete container type	14	delete_containertype
109	Can add Типы контейнеров	11	add_containermeasure
110	Can change Типы контейнеров	11	change_containermeasure
111	Can delete Типы контейнеров	11	delete_containermeasure
112	Can add Тип контейнера	23	add_typecontainermodel
113	Can change Тип контейнера	23	change_typecontainermodel
114	Can delete Тип контейнера	23	delete_typecontainermodel
115	Can add Состояния	11	add_containersost
116	Can change Состояния	11	change_containersost
117	Can delete Состояния	11	delete_containersost
118	Can add Статусы	11	add_containerstatus
119	Can change Статусы	11	change_containerstatus
120	Can delete Статусы	11	delete_containerstatus
121	Can add Модель	11	add_containermodel
122	Can change Модель	11	change_containermodel
123	Can delete Модель	11	delete_containermodel
124	Can add Местонахождение	11	add_containerplace
125	Can change Местонахождение	11	change_containerplace
126	Can delete Местонахождение	11	delete_containerplace
127	Can add sort product	65	add_sortproduct
128	Can change sort product	65	change_sortproduct
129	Can delete sort product	65	delete_sortproduct
160	Can add trade item	98	add_tradeitem
161	Can change trade item	98	change_tradeitem
162	Can delete trade item	98	delete_tradeitem
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 195, true);


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY authtoken_token (key, created, user_id) FROM stdin;
aeea771e8a16e5e2fbabc6ffbdd8e964f92a7711	2017-10-24 15:08:11.581563+00	1
264bb9a16fdfb2f2e42e6b996df820409e85801b	2017-10-24 15:08:11.600093+00	2
ac3516de341c0e1d0af4d1eebf4c25e1f79998c5	2017-10-24 15:08:11.619966+00	3
516d12e96d263e5f596b3cfc271d6714c288683a	2017-10-24 15:08:11.758661+00	14
dd6fc347786d6cbafebf1a8c42f067272ad9df42	2017-10-24 15:08:11.800358+00	18
608f797341ae6386d69303fccb2e99299bce7414	2017-10-27 03:30:36.302621+00	20
36b1482fd2bd8da0fdcddc7236e2105578903b6a	2017-10-27 03:39:10.753693+00	21
98fc40436a52ba7cbd7179f3b497a0a991d62b6e	2017-10-27 04:11:00.723952+00	22
4ce526b455469cfeae1a18289146fbe6a3e59cad	2017-10-27 04:28:10.986615+00	23
1be771245da280c88fc9228055c676a47070ebc2	2017-10-30 00:57:04.743104+00	25
be5c2bbaed65b9fb31eda3bbe0ecdc73576f8d28	2017-10-30 12:14:32.922497+00	26
bed16fc3e2467acd3d5df5a7cc8e80583330d54c	2017-10-30 12:19:05.653875+00	27
2dfdb62845e616046934d3adca5af7f084f71970	2017-11-07 10:05:35.575857+00	30
e43ff8fd32e5cef11cc945b16b84ffca2e820cc5	2017-11-13 06:18:26.783761+00	74
9348a82148051a10710a67a83ce2e35b9b4baa81	2017-11-15 06:25:41.446517+00	77
3028788578f08e407f8155beb835014ba3e08537	2017-11-15 06:26:42.396147+00	78
28001ace094c5aa8adeb2d1f16b3566f6b311a02	2017-11-23 02:58:38.733796+00	79
5a2e49b5dd208655f893578de82fbfd65886bada	2017-11-29 09:18:25.495447+00	80
0b4fe18e44cd34cf26600e60cf394c54a49f82dc	2017-11-29 09:47:57.520783+00	81
2f657d72df42f57cf1c290f40d8a9af5eb93eed8	2018-01-11 09:17:45.835799+00	82
aee371c82b53fde9f44c8e8c8f09cd50f9dc84a1	2018-01-23 06:47:34.352447+00	84
73cd49e72549940698ecd099a06554adb0eb3d83	2018-04-10 04:22:48.15949+00	85
e1ef36f237734c973522bb0360bb7c73eccf573f	2018-04-13 02:39:20.312354+00	86
802123967c01521bd39ba304df670a77ab51ab84	2018-10-18 01:27:01.625406+00	87
f825f5375686f0523c498d3ce9a4e029a5a33c86	2018-10-18 01:33:27.730823+00	88
7f15fbfa99f5b4b3822eb8081e2bf78bb64a1014	2018-10-24 02:11:57.711537+00	89
\.


--
-- Data for Name: content_gallery_image; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY content_gallery_image (id, image, "position", object_id, content_type_id) FROM stdin;
164	content_gallery/refrizheratornyi-20-fut-carrier-69nt40.jpg	0	260	8
351	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-20.JPG	1	272	8
350	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-19.JPG	2	272	8
628	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-57.jpg	2	330	8
168	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-3.jpg	0	262	8
165	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40.jpg	1	262	8
166	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-1.jpg	2	262	8
167	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-2.jpg	3	262	8
169	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-4.jpg	4	262	8
327	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-15.png	0	264	8
632	content_gallery/refrizheratornyi-20-fut-145.JPG	2	331	8
635	content_gallery/refrizheratornyi-20-fut-148.JPG	5	331	8
638	content_gallery/refrizheratornyi-20-fut-151.JPG	8	331	8
170	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-1.jpg	0	259	8
171	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-2.jpg	1	259	8
172	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-3.JPG	2	259	8
173	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-4.jpg	3	259	8
641	content_gallery/refrizheratornyi-20-fut-154.JPG	11	331	8
175	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-6.JPG	0	258	8
174	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-5.JPG	1	258	8
176	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-7.JPG	2	258	8
177	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-8.JPG	3	258	8
270	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-21.jpg	0	269	8
329	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-17.png	1	264	8
182	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-5.JPG	0	266	8
183	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-6.JPG	1	266	8
194	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-7.JPG	0	265	8
195	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-8.JPG	1	265	8
196	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-9.JPG	2	265	8
199	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-10.JPG	2	266	8
203	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-10.jpeg	0	268	8
340	content_gallery/refrizheratornaia-ustanovka-20-fut-daikin-zestia-123-1.jpg	1	263	8
343	content_gallery/refrizheratornaia-ustanovka-20-fut-daikin-zestia-123-2.png	2	263	8
271	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-22.jpg	1	269	8
272	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-23.jpg	1	269	8
354	content_gallery/refrizheratornaia-ustanovka-40-fut-high-cube-2-9-carrier-69nt40.png	1	271	8
355	content_gallery/refrizheratornaia-ustanovka-40-fut-high-cube-2-9-carrier-69nt40-1.png	2	271	8
223	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-11.jpg	3	266	8
356	content_gallery/refrizheratornaia-ustanovka-40-fut-high-cube-2-9-carrier-69nt40-2.png	3	271	8
225	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-12.jpg	4	266	8
357	content_gallery/refrizheratornaia-ustanovka-40-fut-high-cube-2-9-carrier-69nt40-3.png	4	271	8
362	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-29.png	0	274	8
363	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-30.png	1	274	8
364	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-31.png	2	274	8
233	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-12.jpg	0	271	8
358	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-9.JPG	0	273	8
359	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-26.JPG	1	273	8
360	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-27.JPG	2	273	8
361	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-28.JPG	3	273	8
365	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-33.JPG	4	273	8
366	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-35.JPG	5	273	8
301	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-13.jpeg	0	270	8
302	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-14.jpg	0	270	8
367	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-36.jpg	0	275	8
368	content_gallery/refrizheratornaia-ustanovka-40-fut-thermo-king-1.jpg	0	267	8
191	content_gallery/refrizheratornaia-ustanovka-40-fut-thermo-king-2.jpg	1	267	8
369	content_gallery/refrizheratornaia-ustanovka-40-fut-thermo-king-3.JPG	2	267	8
317	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-16.jpg	1	270	8
318	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-32.jpg	2	270	8
321	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-34.jpg	3	270	8
322	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-11_UL7vxOD.jpg	1	268	8
375	content_gallery/refrizheratornyi-20-fut-5.jpg	0	277	8
371	content_gallery/refrizheratornyi-20-fut-1.jpg	1	277	8
372	content_gallery/refrizheratornyi-20-fut-2.jpg	2	277	8
373	content_gallery/refrizheratornyi-20-fut-3.jpg	3	277	8
374	content_gallery/refrizheratornyi-20-fut-4.jpg	4	277	8
376	content_gallery/refrizheratornyi-20-fut-6.jpg	5	277	8
377	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-thermo-king-magnum.jpg	1	278	8
378	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-thermo-king-magnum-1.jpg	2	278	8
379	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-thermo-king-magnum-2.jpg	3	278	8
380	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-thermo-king-magnum-3.jpg	0	278	8
381	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-thermo-king-magnum-4.jpg	4	278	8
382	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-thermo-king-magnum-5.jpg	5	278	8
386	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-16.jpg	0	279	8
383	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-13.jpg	1	279	8
384	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-14.jpg	2	279	8
385	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-15.jpg	3	279	8
387	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-17.jpg	4	279	8
388	content_gallery/refrizheratornyi-20-fut-7.jpg	0	281	8
450	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-43.JPG	0	301	8
451	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-44.JPG	1	301	8
452	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-45.JPG	2	301	8
453	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-46.JPG	3	301	8
393	content_gallery/refrizheratornyi-20-fut-9.jpg	1	280	8
394	content_gallery/refrizheratornyi-20-fut-8.jpg	0	282	8
395	content_gallery/refrizheratornyi-20-fut-10.jpg	0	283	8
454	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-47.JPG	4	301	8
397	content_gallery/refrizheratornyi-20-fut-12.png	0	285	8
398	content_gallery/refrizheratornyi-20-fut-13.png	1	285	8
399	content_gallery/refrizheratornyi-20-fut-14.png	2	285	8
400	content_gallery/refrizheratornyi-20-fut-15.png	3	285	8
401	content_gallery/sukhogruz-20-fut.jpg	0	286	8
629	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-55.jpg	3	330	8
633	content_gallery/refrizheratornyi-20-fut-146.JPG	3	331	8
457	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-28.jpg	0	302	8
405	content_gallery/refrizheratornaia-ustanovka-40-fut-daikin-lxe-2.jpg	0	287	8
403	content_gallery/refrizheratornaia-ustanovka-40-fut-daikin-lxe-1.JPG	1	287	8
406	content_gallery/refrizheratornaia-ustanovka-40-fut-daikin-lxe-3.jpg	2	287	8
636	content_gallery/refrizheratornyi-20-fut-149.JPG	6	331	8
639	content_gallery/refrizheratornyi-20-fut-152.JPG	9	331	8
409	content_gallery/refrizheratornyi-20-fut-16.JPG	1	284	8
642	content_gallery/refrizheratornyi-20-fut-155.JPG	12	331	8
455	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-26.jpg	1	302	8
456	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-27.jpg	2	302	8
459	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-30.jpg	0	303	8
458	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-29.jpg	1	303	8
460	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-31.jpg	2	303	8
461	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-32.jpg	3	303	8
422	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-19.jpg	0	294	8
421	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-18.jpg	1	294	8
423	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-20.jpg	2	294	8
424	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-21.jpg	3	294	8
425	content_gallery/refrizheratornyi-20-fut-17.jpg	0	295	8
426	content_gallery/refrizheratornyi-20-fut-18.jpg	1	295	8
427	content_gallery/refrizheratornyi-20-fut-19.jpg	2	295	8
428	content_gallery/refrizheratornyi-20-fut-20.jpg	3	295	8
462	content_gallery/refrizheratornyi-20-fut-daikin-lxe.jpg	0	304	8
430	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-23.jpg	0	296	8
429	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-22.jpg	1	296	8
431	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-24.jpg	2	296	8
432	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-25.jpg	3	296	8
433	content_gallery/refrizheratornyi-20-fut-21.JPG	0	297	8
434	content_gallery/refrizheratornyi-20-fut-22.JPG	1	297	8
463	content_gallery/refrizheratornyi-20-fut-daikin-lxe-1.jpg	1	304	8
437	content_gallery/refrizheratornyi-20-fut-25.JPG	0	298	8
435	content_gallery/refrizheratornyi-20-fut-23.JPG	1	298	8
436	content_gallery/refrizheratornyi-20-fut-24.JPG	2	298	8
464	content_gallery/refrizheratornyi-20-fut-daikin-lxe-2.jpg	0	305	8
465	content_gallery/refrizheratornyi-20-fut-daikin-lxe-3.jpg	1	305	8
466	content_gallery/refrizheratornyi-20-fut-daikin-lxe-4.jpg	2	305	8
469	content_gallery/refrizheratornyi-20-fut-daikin-lxe-7.JPG	0	306	8
467	content_gallery/refrizheratornyi-20-fut-daikin-lxe-5.jpg	1	306	8
439	content_gallery/refrizheratornyi-20-fut-27.jpg	0	299	8
440	content_gallery/refrizheratornyi-20-fut-28.jpg	1	299	8
438	content_gallery/refrizheratornyi-20-fut-26.jpg	2	299	8
441	content_gallery/refrizheratornyi-20-fut-29.jpg	3	299	8
442	content_gallery/refrizheratornyi-20-fut-30.jpg	4	299	8
443	content_gallery/refrizheratornyi-20-fut-31.jpg	5	299	8
468	content_gallery/refrizheratornyi-20-fut-daikin-lxe-6.JPG	2	306	8
449	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-42.JPG	0	300	8
444	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-37.JPG	1	300	8
445	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-38.JPG	2	300	8
446	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-39.JPG	3	300	8
447	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-40.JPG	4	300	8
448	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-41.JPG	5	300	8
470	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-33.JPG	0	311	8
471	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-34.JPG	1	311	8
472	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-35.JPG	2	311	8
473	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-36.JPG	3	311	8
474	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-37.JPG	4	311	8
475	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-38.JPG	5	311	8
630	content_gallery/refrizheratornyi-20-fut-143.JPG	0	331	8
478	content_gallery/refrizheratornyi-20-fut-33.JPG	0	312	8
476	content_gallery/refrizheratornyi-20-fut-11_UmSfRzx.JPG	1	312	8
477	content_gallery/refrizheratornyi-20-fut-32.JPG	2	312	8
479	content_gallery/refrizheratornyi-20-fut-34.JPG	3	312	8
480	content_gallery/refrizheratornyi-20-fut-35.JPG	4	312	8
481	content_gallery/refrizheratornyi-20-fut-36.JPG	5	312	8
482	content_gallery/refrizheratornyi-20-fut-37.JPG	6	312	8
487	content_gallery/refrizheratornyi-20-fut-daikin-lxe-8.JPG	0	272	8
488	content_gallery/refrizheratornyi-20-fut-38.JPG	0	313	8
489	content_gallery/refrizheratornyi-20-fut-39.JPG	1	313	8
490	content_gallery/refrizheratornyi-20-fut-40.JPG	2	313	8
491	content_gallery/refrizheratornyi-20-fut-41.JPG	3	313	8
492	content_gallery/refrizheratornyi-20-fut-42.JPG	4	313	8
493	content_gallery/refrizheratornyi-20-fut-43.JPG	5	313	8
516	content_gallery/refrizheratornyi-20-fut-54.JPG	10	315	8
517	content_gallery/refrizheratornyi-20-fut-55.JPG	11	315	8
518	content_gallery/refrizheratornyi-20-fut-56.JPG	12	315	8
519	content_gallery/refrizheratornyi-20-fut-57.JPG	13	315	8
494	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-39.JPG	0	314	8
498	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-43.JPG	1	314	8
495	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-40.JPG	2	314	8
496	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-41.JPG	3	314	8
497	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-42.JPG	4	314	8
499	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-44.JPG	5	314	8
500	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-45.JPG	6	314	8
501	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-46.JPG	7	314	8
502	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-47.JPG	8	314	8
503	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-48.JPG	9	314	8
504	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-49.JPG	10	314	8
505	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-50.JPG	11	314	8
512	content_gallery/refrizheratornyi-20-fut-50.JPG	0	315	8
506	content_gallery/refrizheratornyi-20-fut-44.JPG	1	315	8
507	content_gallery/refrizheratornyi-20-fut-45.JPG	2	315	8
508	content_gallery/refrizheratornyi-20-fut-46.JPG	3	315	8
509	content_gallery/refrizheratornyi-20-fut-47.JPG	4	315	8
510	content_gallery/refrizheratornyi-20-fut-48.JPG	5	315	8
511	content_gallery/refrizheratornyi-20-fut-49.JPG	6	315	8
513	content_gallery/refrizheratornyi-20-fut-51.JPG	7	315	8
514	content_gallery/refrizheratornyi-20-fut-52.JPG	8	315	8
515	content_gallery/refrizheratornyi-20-fut-53.JPG	9	315	8
520	content_gallery/refrizheratornyi-20-fut-58.JPG	0	316	8
521	content_gallery/refrizheratornyi-20-fut-59.JPG	1	316	8
522	content_gallery/refrizheratornyi-20-fut-60.JPG	2	316	8
523	content_gallery/refrizheratornyi-20-fut-61.JPG	3	316	8
524	content_gallery/refrizheratornyi-20-fut-62.JPG	4	316	8
525	content_gallery/refrizheratornyi-20-fut-63.JPG	5	316	8
526	content_gallery/refrizheratornyi-20-fut-64.JPG	6	316	8
527	content_gallery/refrizheratornyi-20-fut-65.jpg	0	317	8
528	content_gallery/refrizheratornyi-20-fut-66.jpg	1	317	8
529	content_gallery/refrizheratornyi-20-fut-67.jpg	2	317	8
530	content_gallery/refrizheratornyi-20-fut-68.jpg	3	317	8
531	content_gallery/refrizheratornyi-20-fut-69.jpg	4	317	8
532	content_gallery/refrizheratornyi-20-fut-70.jpg	5	317	8
533	content_gallery/refrizheratornyi-20-fut-71.jpg	6	317	8
534	content_gallery/refrizheratornyi-20-fut-72.jpg	7	317	8
535	content_gallery/refrizheratornyi-20-fut-73.JPG	0	318	8
536	content_gallery/refrizheratornyi-20-fut-74.JPG	1	318	8
537	content_gallery/refrizheratornyi-20-fut-75.JPG	2	318	8
538	content_gallery/refrizheratornyi-20-fut-76.JPG	3	318	8
539	content_gallery/refrizheratornyi-20-fut-77.JPG	4	318	8
540	content_gallery/refrizheratornyi-20-fut-78.JPG	5	318	8
541	content_gallery/refrizheratornyi-20-fut-79.JPG	6	318	8
542	content_gallery/refrizheratornyi-20-fut-80.JPG	7	318	8
543	content_gallery/refrizheratornyi-20-fut-81.JPG	8	318	8
544	content_gallery/refrizheratornyi-20-fut-82.JPG	9	318	8
545	content_gallery/refrizheratornyi-20-fut-83.JPG	1	319	8
546	content_gallery/refrizheratornyi-20-fut-84.JPG	2	319	8
547	content_gallery/refrizheratornyi-20-fut-85.JPG	3	319	8
549	content_gallery/refrizheratornyi-20-fut-87.JPG	4	319	8
550	content_gallery/refrizheratornyi-20-fut-88.JPG	5	319	8
551	content_gallery/refrizheratornyi-20-fut-89.JPG	6	319	8
552	content_gallery/refrizheratornyi-20-fut-90.JPG	7	319	8
548	content_gallery/refrizheratornyi-20-fut-86.JPG	0	319	8
553	content_gallery/refrizheratornyi-20-fut-91.JPG	8	319	8
554	content_gallery/refrizheratornyi-20-fut-92.JPG	9	319	8
555	content_gallery/refrizheratornyi-20-fut-93.jpg	0	320	8
556	content_gallery/refrizheratornyi-20-fut-94.jpg	1	320	8
557	content_gallery/refrizheratornyi-20-fut-95.jpg	2	320	8
558	content_gallery/refrizheratornyi-20-fut-96.jpg	3	320	8
559	content_gallery/refrizheratornyi-20-fut-97.jpg	4	320	8
560	content_gallery/refrizheratornyi-20-fut-98.jpg	5	320	8
561	content_gallery/refrizheratornyi-20-fut-99.jpg	6	320	8
562	content_gallery/refrizheratornyi-20-fut-100.jpg	7	320	8
563	content_gallery/refrizheratornyi-20-fut-101.jpg	8	320	8
623	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-49.jpg	0	330	8
627	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-56.jpg	1	330	8
631	content_gallery/refrizheratornyi-20-fut-144.JPG	1	331	8
634	content_gallery/refrizheratornyi-20-fut-147.JPG	4	331	8
567	content_gallery/refrizheratornyi-20-fut-105.jpg	0	321	8
564	content_gallery/refrizheratornyi-20-fut-102.jpg	1	321	8
565	content_gallery/refrizheratornyi-20-fut-103.jpg	2	321	8
566	content_gallery/refrizheratornyi-20-fut-104.jpg	3	321	8
568	content_gallery/refrizheratornyi-20-fut-106.jpg	4	321	8
569	content_gallery/refrizheratornyi-20-fut-107.jpg	5	321	8
570	content_gallery/refrizheratornyi-20-fut-108.jpg	6	321	8
637	content_gallery/refrizheratornyi-20-fut-150.JPG	7	331	8
640	content_gallery/refrizheratornyi-20-fut-153.JPG	10	331	8
643	content_gallery/refrizheratornyi-20-fut-156.JPG	13	331	8
644	content_gallery/refrizheratornyi-20-fut-157.JPG	14	331	8
574	content_gallery/refrizheratornyi-20-fut-112.jpg	0	322	8
571	content_gallery/refrizheratornyi-20-fut-109.jpg	1	322	8
572	content_gallery/refrizheratornyi-20-fut-110.jpg	2	322	8
573	content_gallery/refrizheratornyi-20-fut-111.jpg	3	322	8
575	content_gallery/refrizheratornyi-20-fut-113.jpg	4	322	8
576	content_gallery/refrizheratornyi-20-fut-114.jpg	5	322	8
645	content_gallery/refrizheratornyi-20-fut-158.JPG	15	331	8
646	content_gallery/refrizheratornyi-20-fut-159.JPG	16	331	8
647	content_gallery/refrizheratornyi-20-fut-160.JPG	17	331	8
648	content_gallery/refrizheratornyi-20-fut-161.JPG	18	331	8
649	content_gallery/refrizheratornyi-20-fut-162.JPG	19	331	8
650	content_gallery/refrizheratornyi-20-fut-163.JPG	20	331	8
652	content_gallery/sukhogruz-20-fut-2.jpg	0	332	8
651	content_gallery/sukhogruz-20-fut-1.jpg	1	332	8
653	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-51.JPG	0	333	8
616	content_gallery/refrizheratornyi-20-fut-142.jpg	2	328	8
654	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-52.JPG	1	333	8
655	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-53.JPG	2	333	8
656	content_gallery/refrizheratornyi-40-fut-high-cube-2-9-carrier-69nt40-54.JPG	3	333	8
587	content_gallery/refrizheratornyi-20-fut-125.jpg	0	324	8
584	content_gallery/refrizheratornyi-20-fut-122.jpg	1	324	8
585	content_gallery/refrizheratornyi-20-fut-123.jpg	2	324	8
586	content_gallery/refrizheratornyi-20-fut-124.jpg	3	324	8
588	content_gallery/refrizheratornyi-20-fut-126.jpg	4	324	8
589	content_gallery/refrizheratornyi-20-fut-127.jpg	5	324	8
606	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-18_0p7wFzX.jpg	0	327	8
607	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-24_TNcl7xO.jpg	1	327	8
608	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-25_miOVpvd.jpg	2	327	8
609	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-48.jpg	3	327	8
600	content_gallery/refrizheratornyi-20-fut-137.jpg	0	326	8
602	content_gallery/refrizheratornyi-20-fut-139.jpg	1	326	8
598	content_gallery/refrizheratornyi-20-fut-135.jpg	2	326	8
599	content_gallery/refrizheratornyi-20-fut-136.jpg	3	326	8
601	content_gallery/refrizheratornyi-20-fut-138.jpg	4	326	8
603	content_gallery/refrizheratornyi-20-fut-140.jpg	5	326	8
610	content_gallery/refrizheratornyi-20-fut-141.JPG	0	328	8
592	content_gallery/refrizheratornyi-20-fut-130.jpg	0	325	8
595	content_gallery/refrizheratornyi-20-fut-133.jpg	1	325	8
590	content_gallery/refrizheratornyi-20-fut-128.jpg	2	325	8
591	content_gallery/refrizheratornyi-20-fut-129.jpg	3	325	8
593	content_gallery/refrizheratornyi-20-fut-131.jpg	4	325	8
594	content_gallery/refrizheratornyi-20-fut-132.jpg	5	325	8
596	content_gallery/refrizheratornyi-20-fut-134.jpg	6	325	8
580	content_gallery/refrizheratornyi-20-fut-118.jpg	0	323	8
582	content_gallery/refrizheratornyi-20-fut-120.jpg	1	323	8
577	content_gallery/refrizheratornyi-20-fut-115.jpg	2	323	8
578	content_gallery/refrizheratornyi-20-fut-116.jpg	3	323	8
579	content_gallery/refrizheratornyi-20-fut-117.jpg	4	323	8
581	content_gallery/refrizheratornyi-20-fut-119.jpg	5	323	8
583	content_gallery/refrizheratornyi-20-fut-121.jpg	6	323	8
621	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-53.jpg	5	329	8
622	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-54.jpg	0	329	8
618	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-50.jpg	2	329	8
619	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-51.jpg	3	329	8
620	content_gallery/refrizheratornyi-20-fut-carrier-69nt40-52.jpg	4	329	8
\.


--
-- Name: content_gallery_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('content_gallery_image_id_seq', 656, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
425	2017-10-25 01:51:34.534012+00	217	Вентилируемый 40 фут  High Cube Модель2	3		8	2
136	2017-09-06 13:21:58.691+00	52	Танк	3		6	2
149	2017-09-06 13:26:01.916+00	55	Тип	2	[]	14	2
151	2017-09-06 13:49:45.97+00	6	По гарантии	3		7	2
181	2017-09-06 14:01:25.636+00	32	TestContaine3r	3		8	2
193	2017-09-06 14:01:25.703+00	20	TestContaine3r	3		8	2
196	2017-09-06 14:01:25.721+00	17	TestContaine3r	3		8	2
197	2017-09-06 14:01:25.726+00	16	TestContaine3r	3		8	2
200	2017-09-06 14:01:25.743+00	13	TestContaine3r	3		8	2
201	2017-09-06 14:01:25.748+00	12	TestContaine3r	3		8	2
202	2017-09-06 14:01:25.753+00	11	TestContaine3r	3		8	2
203	2017-09-06 14:01:25.758+00	10	TestContaine3r	3		8	2
204	2017-09-06 14:01:25.764+00	9	TestContaine3r	3		8	2
205	2017-09-06 14:01:25.769+00	8	TestContaine3r	3		8	2
206	2017-09-06 14:01:25.774+00	7	TestContaine3r	3		8	2
207	2017-09-06 14:01:25.778+00	6	TestContaine3r	3		8	2
267	2017-09-14 07:15:58.905+00	63	Танк	3		11	2
272	2017-09-14 07:16:17.319+00	83	Вентилируемый	1	[{"added": {}}]	60	2
273	2017-09-14 07:16:36.62+00	84	Рефрежираторный	1	[{"added": {}}]	60	2
274	2017-09-14 07:18:03.203+00	84	Рефрижираторный	2	[{"changed": {"fields": ["name", "short_name"]}}]	60	2
275	2017-09-14 07:18:19.651+00	83	Вентилируемый	2	[{"changed": {"fields": ["short_name"]}}]	60	2
276	2017-09-14 07:18:38.283+00	85	Насыпной	1	[{"added": {}}]	60	2
277	2017-09-14 07:18:47.681+00	86	Танк	1	[{"added": {}}]	60	2
278	2017-09-14 07:18:55.134+00	87	Платформа	1	[{"added": {}}]	60	2
279	2017-09-14 07:19:19.822+00	88	Открытый	1	[{"added": {}}]	60	2
280	2017-09-14 07:20:57.076+00	89	Сухогруз	1	[{"added": {}}]	60	2
281	2017-09-14 07:58:29.866+00	34	Модель	3		11	2
282	2017-09-14 07:59:32.922+00	90	Модель1	1	[{"added": {}}]	11	2
1196	2017-11-07 09:58:59.641136+00	92	Модель2_2	2	[{"changed": {"fields": ["name"]}}]	63	14
1262	2017-11-12 04:26:22.569084+00	129	40 футов High Cube XXL 3.2	2	[{"changed": {"fields": ["order_number"]}}]	59	2
1278	2017-11-12 04:33:40.624942+00	131	Насыпной	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1336	2017-11-12 07:49:51.036388+00	25	vt@rftu.ru	2	[{"changed": {"fields": ["company"]}}]	15	2
1350	2017-11-13 06:37:58.60907+00	84	Рефрижераторный	2	[{"changed": {"fields": ["name"]}}]	60	2
1358	2017-11-14 04:39:06.347441+00	39	40 фут  High Cube 2.9	2	[{"changed": {"fields": ["order_number"]}}]	11	14
1376	2017-11-15 06:17:05.297428+00	1	Manager	2	[]	3	14
1469	2017-11-16 01:39:44.863038+00	316	OrderModel object	3		21	78
1470	2017-11-16 01:39:44.873004+00	315	OrderModel object	3		21	78
1471	2017-11-16 01:39:44.876389+00	314	OrderModel object	3		21	78
1472	2017-11-16 01:39:44.883194+00	313	OrderModel object	3		21	78
1473	2017-11-16 01:39:44.888341+00	312	OrderModel object	3		21	78
1474	2017-11-16 01:39:44.894601+00	311	OrderModel object	3		21	78
1475	2017-11-16 01:39:44.898239+00	310	OrderModel object	3		21	78
1476	2017-11-16 01:39:44.901499+00	309	OrderModel object	3		21	78
1477	2017-11-16 01:39:44.905711+00	308	OrderModel object	3		21	78
1478	2017-11-16 01:39:44.909638+00	307	OrderModel object	3		21	78
1479	2017-11-16 01:39:44.913622+00	306	OrderModel object	3		21	78
1480	2017-11-16 01:39:44.917697+00	305	OrderModel object	3		21	78
1481	2017-11-16 01:39:44.921728+00	304	OrderModel object	3		21	78
1482	2017-11-16 01:39:44.925648+00	303	OrderModel object	3		21	78
1483	2017-11-16 01:39:44.929871+00	302	OrderModel object	3		21	78
1484	2017-11-16 01:39:44.933509+00	301	OrderModel object	3		21	78
1485	2017-11-16 01:39:44.937554+00	300	OrderModel object	3		21	78
1486	2017-11-16 01:39:44.941525+00	299	OrderModel object	3		21	78
1487	2017-11-16 01:39:44.951658+00	298	OrderModel object	3		21	78
1488	2017-11-16 01:39:44.955227+00	297	OrderModel object	3		21	78
1489	2017-11-16 01:39:44.958862+00	296	OrderModel object	3		21	78
1490	2017-11-16 01:39:44.962872+00	295	OrderModel object	3		21	78
1491	2017-11-16 01:39:44.966782+00	294	OrderModel object	3		21	78
1492	2017-11-16 01:39:44.970797+00	293	OrderModel object	3		21	78
1493	2017-11-16 01:39:44.974767+00	292	OrderModel object	3		21	78
1494	2017-11-16 01:39:44.978837+00	291	OrderModel object	3		21	78
1495	2017-11-16 01:39:44.982976+00	290	OrderModel object	3		21	78
1496	2017-11-16 01:39:44.986857+00	289	OrderModel object	3		21	78
283	2017-09-14 07:59:38.973+00	91	Модель	1	[{"added": {}}]	14	2
284	2017-09-14 08:00:25.152+00	1	Контейнер	2	[]	9	2
334	2017-09-17 23:46:27.259+00	103	ds	3		8	2
405	2017-10-11 00:17:45.488+00	109	name1111	2	[{"changed": {"fields": ["property"]}}]	23	2
406	2017-10-11 00:18:52.21+00	104	Владивосток	2	[]	14	2
407	2017-10-11 00:19:11.865+00	106	Уссурийск	2	[]	14	2
408	2017-10-11 00:19:30.909+00	109	Тухачевского, 26	2	[{"changed": {"fields": ["name", "translate"]}}]	23	2
409	2017-10-11 05:25:08.691+00	5	Арендуется	3		10	2
410	2017-10-11 05:25:08.698+00	4	Продается	3		10	2
1497	2017-11-16 01:39:44.990827+00	288	OrderModel object	3		21	78
411	2017-10-11 05:38:19.298+00	106	Уссурийск	2	[{"changed": {"fields": ["required"]}}]	14	2
1498	2017-11-16 01:39:44.994763+00	287	OrderModel object	3		21	78
412	2017-10-11 05:38:36.197+00	94	Местонахождение	2	[{"changed": {"fields": ["required"]}}]	14	2
414	2017-10-11 09:04:23.255+00	109	Тухачевского, 26	2	[{"changed": {"fields": ["required"]}}]	23	2
426	2017-10-27 01:32:13.749425+00	210	OrderModel object	3		21	2
427	2017-10-27 01:32:13.762375+00	209	OrderModel object	3		21	2
428	2017-10-27 01:32:13.767221+00	208	OrderModel object	3		21	2
429	2017-10-27 01:32:13.771253+00	207	OrderModel object	3		21	2
430	2017-10-27 01:32:13.775239+00	206	OrderModel object	3		21	2
431	2017-10-27 01:32:13.779249+00	205	OrderModel object	3		21	2
432	2017-10-27 01:32:13.783157+00	204	OrderModel object	3		21	2
433	2017-10-27 01:32:13.78708+00	203	OrderModel object	3		21	2
434	2017-10-27 01:32:13.79109+00	202	OrderModel object	3		21	2
435	2017-10-27 01:32:13.795162+00	201	OrderModel object	3		21	2
875	2017-10-27 01:36:19.973191+00	83	Bonus object	3		16	2
1499	2017-11-16 01:39:44.998792+00	286	OrderModel object	3		21	78
152	2017-09-06 13:49:45.975+00	5	По статусу аренды	3		7	2
153	2017-09-06 13:49:45.979+00	4	По статусу продажи	3		7	2
155	2017-09-06 13:49:45.985+00	2	По цене со скидкой	3		7	2
156	2017-09-06 13:52:52.821+00	9	Тип	1	[{"added": {}}]	7	2
157	2017-09-06 13:53:13.53+00	10	Цена	1	[{"added": {}}]	7	2
158	2017-09-06 13:53:50.935+00	11	Номер контейнера	1	[{"added": {}}]	7	2
159	2017-09-06 13:54:13.091+00	12	Размер	1	[{"added": {}}]	7	2
160	2017-09-06 13:54:53.02+00	13	Использовался в РФ	1	[{"added": {}}]	7	2
161	2017-09-06 13:55:11.701+00	14	С гарантией	1	[{"added": {}}]	7	2
162	2017-09-06 13:55:37.693+00	15	Состояние	1	[{"added": {}}]	7	2
163	2017-09-06 13:56:09.763+00	16	Не проданные	1	[{"added": {}}]	7	2
164	2017-09-06 13:58:26.054+00	66	Стоимость аренды в год	1	[{"added": {}}]	13	2
165	2017-09-06 13:58:36.333+00	67	Стоимость аренды в месяц	1	[{"added": {}}]	11	2
166	2017-09-06 14:00:30.083+00	17	Цена аренды в год	1	[{"added": {}}]	7	2
167	2017-09-06 14:00:42.561+00	18	Цена аренды в месяц	1	[{"added": {}}]	7	2
169	2017-09-06 14:01:25.576+00	44	TestContaine3r	3		8	2
170	2017-09-06 14:01:25.58+00	43	TestContaine3r	3		8	2
171	2017-09-06 14:01:25.585+00	42	TestContaine3r	3		8	2
172	2017-09-06 14:01:25.59+00	41	TestContaine3r	3		8	2
173	2017-09-06 14:01:25.596+00	40	TestContaine3r	3		8	2
174	2017-09-06 14:01:25.598+00	39	TestContaine3r	3		8	2
175	2017-09-06 14:01:25.603+00	38	TestContaine3r	3		8	2
176	2017-09-06 14:01:25.609+00	37	TestContaine3r	3		8	2
177	2017-09-06 14:01:25.614+00	36	TestContaine3r	3		8	2
178	2017-09-06 14:01:25.62+00	35	TestContaine3r	3		8	2
179	2017-09-06 14:01:25.626+00	34	TestContaine3r	3		8	2
182	2017-09-06 14:01:25.642+00	31	TestContaine3r	3		8	2
183	2017-09-06 14:01:25.648+00	30	TestContaine3r	3		8	2
184	2017-09-06 14:01:25.654+00	29	TestContaine3r	3		8	2
185	2017-09-06 14:01:25.659+00	28	TestContaine3r	3		8	2
186	2017-09-06 14:01:25.664+00	27	TestContaine3r	3		8	2
187	2017-09-06 14:01:25.67+00	26	TestContaine3r	3		8	2
188	2017-09-06 14:01:25.677+00	25	TestContaine3r	3		8	2
189	2017-09-06 14:01:25.682+00	24	TestContaine3r	3		8	2
190	2017-09-06 14:01:25.687+00	23	TestContaine3r	3		8	2
191	2017-09-06 14:01:25.693+00	22	TestContaine3r	3		8	2
192	2017-09-06 14:01:25.697+00	21	TestContaine3r	3		8	2
194	2017-09-06 14:01:25.71+00	19	TestContaine3r	3		8	2
195	2017-09-06 14:01:25.715+00	18	TestContaine3r	3		8	2
878	2017-10-27 03:34:07.628003+00	20	AVK@rftu.ru	2	[{"changed": {"fields": ["first_name", "is_staff", "is_superuser", "patronymic", "phone", "contact_email", "company", "photo"]}}]	15	2
1197	2017-11-07 09:59:12.393437+00	119	Модель test	1	[{"added": {}}]	63	14
1263	2017-11-12 04:26:32.362699+00	56	45 фут	2	[{"changed": {"fields": ["order_number"]}}]	11	2
1279	2017-11-12 04:33:57.382184+00	131	Насыпной	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1337	2017-11-12 23:52:38.264845+00	3	daria.shonova@gmail.com	2	[{"changed": {"fields": ["is_staff"]}}]	15	2
1351	2017-11-13 06:44:01.213514+00	84	Рефрижираторный	2	[{"changed": {"fields": ["name"]}}]	60	1
1359	2017-11-14 04:40:57.864053+00	39	40 фут  High Cube 2.9	2	[]	11	14
1377	2017-11-15 06:19:30.89613+00	75	danil@gmail.com	3		15	2
1500	2017-11-16 01:39:45.003144+00	285	OrderModel object	3		21	78
1501	2017-11-16 01:39:45.012635+00	284	OrderModel object	3		21	78
1502	2017-11-16 01:39:45.015744+00	283	OrderModel object	3		21	78
1503	2017-11-16 01:39:45.018862+00	282	OrderModel object	3		21	78
1504	2017-11-16 01:39:45.022643+00	281	OrderModel object	3		21	78
1505	2017-11-16 01:39:45.026621+00	280	OrderModel object	3		21	78
1506	2017-11-16 01:39:45.030627+00	279	OrderModel object	3		21	78
1507	2017-11-16 01:39:45.034685+00	278	OrderModel object	3		21	78
1508	2017-11-16 01:39:45.038542+00	277	OrderModel object	3		21	78
1627	2017-11-16 04:55:56.507705+00	137	Mitsubishi	1	[{"added": {}}]	63	18
1653	2018-01-23 00:31:28.865374+00	1	Manager	2	[]	3	2
1671	2018-10-18 05:43:00.182072+00	80	ice@rftu.ru	2	[]	15	18
413	2017-10-11 09:04:15.701+00	96	г.Владивосток, ул. Тухачевского, 27	2	[{"changed": {"fields": ["required"]}}]	64	2
436	2017-10-27 01:32:13.799234+00	200	OrderModel object	3		21	2
437	2017-10-27 01:32:13.803261+00	199	OrderModel object	3		21	2
438	2017-10-27 01:32:13.807234+00	198	OrderModel object	3		21	2
439	2017-10-27 01:32:13.811211+00	197	OrderModel object	3		21	2
440	2017-10-27 01:32:13.815295+00	196	OrderModel object	3		21	2
441	2017-10-27 01:32:13.819129+00	195	OrderModel object	3		21	2
442	2017-10-27 01:32:13.830831+00	194	OrderModel object	3		21	2
443	2017-10-27 01:32:13.845911+00	193	OrderModel object	3		21	2
444	2017-10-27 01:32:13.853562+00	192	OrderModel object	3		21	2
879	2017-10-27 03:40:07.717554+00	21	wildpixel.dv@gmail.com	2	[{"changed": {"fields": ["patronymic", "phone", "contact_email", "company", "photo"]}}]	15	2
144	2017-09-06 13:25:29.144+00	61	Открытый	1	[{"added": {}}]	11	2
145	2017-09-06 13:25:37.347+00	62	Платформа	1	[{"added": {}}]	11	2
146	2017-09-06 13:25:44.877+00	63	Танк	1	[{"added": {}}]	11	2
147	2017-09-06 13:25:52.564+00	64	Насыпной	1	[{"added": {}}]	11	2
148	2017-09-06 13:25:58.794+00	65	Вентилируемый	1	[{"added": {}}]	11	2
150	2017-09-06 13:49:45.965+00	7	Опубликован	3		7	2
1198	2017-11-07 10:04:16.385915+00	120	test размер ДхШхВ	1	[{"added": {}}]	59	14
1264	2017-11-12 04:26:37.099323+00	40	45 фут  High Cube 2.9	2	[{"changed": {"fields": ["order_number"]}}]	11	2
1280	2017-11-12 05:03:18.878375+00	77	20 фут High Cube XXL 3.2	2	[{"changed": {"fields": ["order_number"]}}]	11	2
1338	2017-11-13 06:03:47.719813+00	128	Артём1	2	[{"changed": {"fields": ["name"]}}]	99	1
1352	2017-11-13 07:24:23.558284+00	84	Рефрежираторный	2	[{"changed": {"fields": ["name"]}}]	60	2
1360	2017-11-14 04:41:17.7934+00	77	20 фут High Cube XXL 3.2	2	[{"changed": {"fields": ["order_number"]}}]	11	14
180	2017-09-06 14:01:25.631+00	33	TestContaine3r	3		8	2
1378	2017-11-15 06:22:37.822655+00	None	danil@gmail.com	1	[{"added": {}}]	15	2
1509	2017-11-16 01:39:53.489008+00	276	OrderModel object	3		21	78
1510	2017-11-16 01:39:53.499787+00	275	OrderModel object	3		21	78
1511	2017-11-16 01:39:53.51223+00	274	OrderModel object	3		21	78
1512	2017-11-16 01:39:53.527975+00	273	OrderModel object	3		21	78
1513	2017-11-16 01:39:53.55535+00	272	OrderModel object	3		21	78
1514	2017-11-16 01:39:53.564943+00	271	OrderModel object	3		21	78
1515	2017-11-16 01:39:53.572944+00	270	OrderModel object	3		21	78
1516	2017-11-16 01:39:53.576664+00	269	OrderModel object	3		21	78
1517	2017-11-16 01:39:53.580674+00	268	OrderModel object	3		21	78
1518	2017-11-16 01:39:53.584687+00	267	OrderModel object	3		21	78
1519	2017-11-16 01:39:53.595298+00	266	OrderModel object	3		21	78
1520	2017-11-16 01:39:53.598748+00	265	OrderModel object	3		21	78
1521	2017-11-16 01:39:53.601716+00	264	OrderModel object	3		21	78
1522	2017-11-16 01:39:53.604731+00	263	OrderModel object	3		21	78
1523	2017-11-16 01:39:53.616002+00	262	OrderModel object	3		21	78
1524	2017-11-16 01:39:53.62708+00	261	OrderModel object	3		21	78
1525	2017-11-16 01:39:53.631612+00	260	OrderModel object	3		21	78
1526	2017-11-16 01:39:53.635383+00	259	OrderModel object	3		21	78
1527	2017-11-16 01:39:53.639385+00	258	OrderModel object	3		21	78
285	2017-09-14 08:02:57.242+00	91	Модель	2	[{"changed": {"fields": ["translate"]}}]	14	2
299	2017-09-14 08:47:08.252+00	98	Другое место	1	[{"added": {}}]	14	2
309	2017-09-17 23:46:27.14+00	162	sada	3		8	2
310	2017-09-17 23:46:27.144+00	160	111	3		8	2
311	2017-09-17 23:46:27.149+00	159	King 123456	3		8	2
312	2017-09-17 23:46:27.152+00	156	dasd	3		8	2
313	2017-09-17 23:46:27.155+00	155	dasd	3		8	2
314	2017-09-17 23:46:27.158+00	154	12	3		8	2
315	2017-09-17 23:46:27.162+00	153	dasd	3		8	2
317	2017-09-17 23:46:27.17+00	151	1212	3		8	2
318	2017-09-17 23:46:27.174+00	150	1200	3		8	2
319	2017-09-17 23:46:27.179+00	149	King-king	3		8	2
320	2017-09-17 23:46:27.184+00	148	231	3		8	2
321	2017-09-17 23:46:27.189+00	147	King-king	3		8	2
322	2017-09-17 23:46:27.195+00	146	King-king	3		8	2
323	2017-09-17 23:46:27.2+00	145	2313вфывфыв	3		8	2
324	2017-09-17 23:46:27.206+00	144	2313вфывфыв	3		8	2
1528	2017-11-16 01:39:53.643503+00	257	OrderModel object	3		21	78
1529	2017-11-16 01:39:53.64747+00	256	OrderModel object	3		21	78
1530	2017-11-16 01:39:53.651398+00	255	OrderModel object	3		21	78
1531	2017-11-16 01:39:53.655386+00	254	OrderModel object	3		21	78
1532	2017-11-16 01:39:53.65953+00	253	OrderModel object	3		21	78
1533	2017-11-16 01:39:53.663365+00	252	OrderModel object	3		21	78
1534	2017-11-16 01:39:53.667297+00	251	OrderModel object	3		21	78
1535	2017-11-16 01:39:53.692625+00	250	OrderModel object	3		21	78
1536	2017-11-16 01:39:53.719431+00	249	OrderModel object	3		21	78
325	2017-09-17 23:46:27.212+00	143	2313вфывфыв	3		8	2
1537	2017-11-16 01:39:53.735704+00	248	OrderModel object	3		21	78
1538	2017-11-16 01:39:53.794397+00	247	OrderModel object	3		21	78
1539	2017-11-16 01:39:53.822868+00	246	OrderModel object	3		21	78
1540	2017-11-16 01:39:53.835414+00	245	OrderModel object	3		21	78
326	2017-09-17 23:46:27.217+00	142	2313вфывфыв	3		8	2
327	2017-09-17 23:46:27.222+00	141	ddas	3		8	2
328	2017-09-17 23:46:27.227+00	140	ddas	3		8	2
330	2017-09-17 23:46:27.238+00	138	ddas	3		8	2
331	2017-09-17 23:46:27.243+00	137	TestContaine3r	3		8	2
333	2017-09-17 23:46:27.253+00	104	TestContaine3r	3		8	2
402	2017-10-11 00:16:13.861+00	111	Ленинская 1	2	[{"changed": {"fields": ["property"]}}]	23	2
403	2017-10-11 00:16:49.883+00	107	Абребская 46	3		6	2
404	2017-10-11 00:17:19.521+00	106	Уссурийск	2	[]	14	2
415	2017-10-11 09:04:30.327+00	110	Абребская 1	2	[{"changed": {"fields": ["required"]}}]	23	2
416	2017-10-11 09:04:52.146+00	111	Ленинская 1	2	[{"changed": {"fields": ["required"]}}]	23	2
417	2017-10-11 09:05:25.191+00	67	Цена аренды в год	2	[{"changed": {"fields": ["required"]}}]	11	2
1541	2017-11-16 01:39:53.842551+00	244	OrderModel object	3		21	78
418	2017-10-11 09:54:06.86+00	66	Стоимость аренды в год	2	[{"changed": {"fields": ["required"]}}]	13	2
419	2017-10-11 09:54:30.071+00	66	Стоимость аренды в год	2	[{"changed": {"fields": ["required"]}}]	13	2
420	2017-10-11 10:06:43.505+00	112	test	1	[{"added": {}}]	11	2
421	2017-10-11 10:07:12.491+00	1	Контейнер	2	[]	9	2
422	2017-10-18 03:51:22.48+00	1	Ваш договор	1	[{"added": {}}]	18	1
423	2017-10-18 03:51:37.618+00	2	Заполнен договор	1	[{"added": {}}]	18	1
445	2017-10-27 01:32:13.857913+00	191	OrderModel object	3		21	2
446	2017-10-27 01:32:13.862591+00	190	OrderModel object	3		21	2
447	2017-10-27 01:32:13.872166+00	189	OrderModel object	3		21	2
232	2017-09-11 06:17:19.104+00	21	Продажа	1	[{"added": {}}]	7	2
233	2017-09-11 06:25:25.334+00	1	Контейнер	2	[]	9	2
234	2017-09-11 11:22:42.279+00	152	1200	2	[{"changed": {"fields": ["manager"]}}]	8	2
236	2017-09-11 11:24:42.404+00	151	1212	2	[{"changed": {"fields": ["manager"]}}]	8	2
237	2017-09-11 12:07:31.168+00	76	Стоимость аренды в месяц	1	[{"added": {}}]	13	2
238	2017-09-11 12:07:52.731+00	1	Контейнер	2	[]	9	2
239	2017-09-11 12:08:04.54+00	18	Цена аренды в месяц	2	[{"changed": {"fields": ["field"]}}]	7	2
240	2017-09-11 12:36:44.727+00	157	dasd	2	[{"changed": {"fields": ["property_json"]}}]	8	2
241	2017-09-11 13:09:31.035+00	1	Company object	1	[{"added": {}}]	17	2
242	2017-09-11 13:09:58.758+00	2	de@de.ru	2	[{"changed": {"fields": ["patronymic", "company", "photo"]}}]	15	2
243	2017-09-11 14:17:26.821+00	3	Bonus object	3		16	2
244	2017-09-11 14:17:26.832+00	2	Bonus object	3		16	2
245	2017-09-11 14:17:26.836+00	1	Bonus object	3		16	2
246	2017-09-11 14:44:49.494+00	2	Цена	2	[{"changed": {"fields": ["translate"]}}]	13	2
247	2017-09-11 22:18:38.616+00	22	2131 photo #1	1	[{"added": {}}]	25	2
248	2017-09-11 22:18:56.348+00	23	2131 photo #2	1	[{"added": {}}]	25	2
249	2017-09-11 22:19:01.742+00	158	2131	2	[]	8	2
250	2017-09-11 22:19:46.597+00	24	dasd photo #1	1	[{"added": {}}]	25	2
251	2017-09-11 22:19:48.346+00	157	dasd	2	[]	8	2
252	2017-09-11 23:40:36.339+00	77	20 фут 3.2	1	[{"added": {}}]	11	2
253	2017-09-11 23:42:26.663+00	47	Размер	2	[]	14	2
254	2017-09-11 23:43:17.702+00	77	20 фут High Cube XXL 3.2	2	[{"changed": {"fields": ["name"]}}]	11	2
255	2017-09-11 23:43:53.897+00	40	45 фут  High Cube 2.9	2	[{"changed": {"fields": ["name"]}}]	11	2
256	2017-09-12 05:45:26.88+00	78	Типок	1	[{"added": {}}]	26	2
880	2017-10-27 04:05:55.275752+00	206	OrderModel object	3		21	1
881	2017-10-27 04:05:55.512364+00	205	OrderModel object	3		21	1
882	2017-10-27 04:05:55.584914+00	204	OrderModel object	3		21	1
883	2017-10-27 04:05:55.60474+00	203	OrderModel object	3		21	1
884	2017-10-27 04:05:55.631267+00	201	OrderModel object	3		21	1
885	2017-10-27 04:05:55.63663+00	200	OrderModel object	3		21	1
886	2017-10-27 04:05:55.641127+00	199	OrderModel object	3		21	1
887	2017-10-27 04:05:55.652916+00	198	OrderModel object	3		21	1
888	2017-10-27 04:05:55.67607+00	197	OrderModel object	3		21	1
889	2017-10-27 04:05:55.68103+00	196	OrderModel object	3		21	1
890	2017-10-27 04:05:55.685407+00	195	OrderModel object	3		21	1
891	2017-10-27 04:05:55.690062+00	194	OrderModel object	3		21	1
892	2017-10-27 04:05:55.694408+00	193	OrderModel object	3		21	1
893	2017-10-27 04:05:55.700215+00	192	OrderModel object	3		21	1
894	2017-10-27 04:05:55.704589+00	191	OrderModel object	3		21	1
895	2017-10-27 04:05:55.708922+00	190	OrderModel object	3		21	1
896	2017-10-27 04:05:55.715787+00	189	OrderModel object	3		21	1
897	2017-10-27 04:05:55.727761+00	188	OrderModel object	3		21	1
898	2017-10-27 04:05:55.73197+00	187	OrderModel object	3		21	1
899	2017-10-27 04:05:55.736462+00	186	OrderModel object	3		21	1
900	2017-10-27 04:05:55.74104+00	185	OrderModel object	3		21	1
901	2017-10-27 04:05:55.745067+00	184	OrderModel object	3		21	1
902	2017-10-27 04:05:55.823955+00	183	OrderModel object	3		21	1
903	2017-10-27 04:05:55.85683+00	182	OrderModel object	3		21	1
904	2017-10-27 04:05:55.877751+00	181	OrderModel object	3		21	1
905	2017-10-27 04:05:55.887688+00	180	OrderModel object	3		21	1
906	2017-10-27 04:05:55.89189+00	179	OrderModel object	3		21	1
907	2017-10-27 04:05:55.895805+00	178	OrderModel object	3		21	1
908	2017-10-27 04:05:55.899682+00	177	OrderModel object	3		21	1
909	2017-10-27 04:05:55.903681+00	176	OrderModel object	3		21	1
910	2017-10-27 04:05:56.007896+00	175	OrderModel object	3		21	1
911	2017-10-27 04:05:56.064008+00	174	OrderModel object	3		21	1
257	2017-09-12 05:48:36.704+00	79	новый тип	1	[{"added": {}}]	26	2
258	2017-09-12 05:48:46.589+00	79	новый тип	3		26	2
259	2017-09-12 07:14:09.458+00	80	ЧарТест1	1	[{"added": {}}]	11	1
289	2017-09-14 08:34:04.476+00	92	Модель2	1	[{"added": {}}]	63	2
295	2017-09-14 08:40:50.219+00	94	Местонахождение	1	[{"added": {}}]	14	2
297	2017-09-14 08:46:22.044+00	96	г.Владивосток, ул. Тухачевского, 27	1	[{"added": {}}]	64	2
329	2017-09-17 23:46:27.233+00	139	ddas	3		8	2
332	2017-09-17 23:46:27.248+00	105	TestContaine3r	3		8	2
912	2017-10-27 04:05:56.093+00	173	OrderModel object	3		21	1
913	2017-10-27 04:05:56.129943+00	172	OrderModel object	3		21	1
914	2017-10-27 04:05:56.14786+00	171	OrderModel object	3		21	1
448	2017-10-27 01:32:13.876487+00	188	OrderModel object	3		21	2
449	2017-10-27 01:32:13.880846+00	187	OrderModel object	3		21	2
450	2017-10-27 01:32:13.884821+00	186	OrderModel object	3		21	2
451	2017-10-27 01:32:13.889374+00	185	OrderModel object	3		21	2
452	2017-10-27 01:32:13.894328+00	184	OrderModel object	3		21	2
453	2017-10-27 01:32:13.901027+00	183	OrderModel object	3		21	2
454	2017-10-27 01:32:13.90533+00	182	OrderModel object	3		21	2
455	2017-10-27 01:32:13.909453+00	181	OrderModel object	3		21	2
456	2017-10-27 01:32:13.913753+00	180	OrderModel object	3		21	2
457	2017-10-27 01:32:13.918323+00	179	OrderModel object	3		21	2
458	2017-10-27 01:32:13.922434+00	178	OrderModel object	3		21	2
459	2017-10-27 01:32:13.926358+00	177	OrderModel object	3		21	2
460	2017-10-27 01:32:13.930349+00	176	OrderModel object	3		21	2
461	2017-10-27 01:32:13.94065+00	175	OrderModel object	3		21	2
462	2017-10-27 01:32:13.944691+00	174	OrderModel object	3		21	2
463	2017-10-27 01:32:13.948277+00	173	OrderModel object	3		21	2
464	2017-10-27 01:32:13.951665+00	172	OrderModel object	3		21	2
465	2017-10-27 01:32:13.955887+00	171	OrderModel object	3		21	2
466	2017-10-27 01:32:13.968701+00	170	OrderModel object	3		21	2
467	2017-10-27 01:32:13.975762+00	169	OrderModel object	3		21	2
876	2017-10-27 01:36:19.983001+00	82	Bonus object	3		16	2
915	2017-10-27 04:05:56.151439+00	170	OrderModel object	3		21	1
916	2017-10-27 04:05:56.155679+00	169	OrderModel object	3		21	1
917	2017-10-27 04:05:56.159693+00	168	OrderModel object	3		21	1
918	2017-10-27 04:05:56.163722+00	167	OrderModel object	3		21	1
919	2017-10-27 04:05:56.167681+00	166	OrderModel object	3		21	1
920	2017-10-27 04:05:56.17162+00	165	OrderModel object	3		21	1
921	2017-10-27 04:05:56.175678+00	164	OrderModel object	3		21	1
922	2017-10-27 04:05:56.185652+00	163	OrderModel object	3		21	1
923	2017-10-27 04:05:56.189517+00	162	OrderModel object	3		21	1
924	2017-10-27 04:05:56.193162+00	161	OrderModel object	3		21	1
925	2017-10-27 04:05:56.196952+00	160	OrderModel object	3		21	1
926	2017-10-27 04:05:56.201076+00	159	OrderModel object	3		21	1
927	2017-10-27 04:05:56.204952+00	158	OrderModel object	3		21	1
928	2017-10-27 04:05:56.208949+00	157	OrderModel object	3		21	1
929	2017-10-27 04:05:56.213035+00	156	OrderModel object	3		21	1
930	2017-10-27 04:05:56.217156+00	155	OrderModel object	3		21	1
931	2017-10-27 04:05:56.221192+00	154	OrderModel object	3		21	1
932	2017-10-27 04:05:56.22495+00	153	OrderModel object	3		21	1
933	2017-10-27 04:05:56.228985+00	152	OrderModel object	3		21	1
934	2017-10-27 04:05:56.232892+00	151	OrderModel object	3		21	1
935	2017-10-27 04:05:56.236849+00	150	OrderModel object	3		21	1
1	2017-08-29 01:57:02.322+00	1	Состояние	1	[{"added": {}}]	10	1
2	2017-08-29 02:02:47.56+00	2	Цена	1	[{"added": {}}]	13	1
3	2017-08-29 02:03:05.141+00	3	Цена со скидкой	1	[{"added": {}}]	13	1
4	2017-08-29 02:03:31.215+00	4	Продается	1	[{"added": {}}]	10	1
5	2017-08-29 02:03:44.551+00	5	Арендуется	1	[{"added": {}}]	10	1
6	2017-08-29 02:04:04.387+00	6	Гарантия	1	[{"added": {}}]	13	1
7	2017-08-29 02:04:15.563+00	7	Опубликован	1	[{"added": {}}]	10	1
8	2017-08-29 02:04:31.315+00	1	По состоянию	1	[{"added": {}}]	7	1
9	2017-08-29 02:07:34.849+00	2	По цене со скидкой	1	[{"added": {}}]	7	1
10	2017-08-29 02:07:47.786+00	3	По цене	1	[{"added": {}}]	7	1
11	2017-08-29 02:07:59.416+00	4	По статусу продажи	1	[{"added": {}}]	7	1
12	2017-08-29 02:08:04.751+00	5	По статусу аренды	1	[{"added": {}}]	7	1
13	2017-08-29 02:08:28.028+00	6	По гарантии	1	[{"added": {}}]	7	1
14	2017-08-29 02:08:44.326+00	7	Опубликован	1	[{"added": {}}]	7	1
15	2017-08-29 02:08:48.855+00	1	Контейнер	1	[{"added": {}}]	9	1
16	2017-08-29 02:09:49.003+00	1	Контейнер	1	[{"added": {}}]	8	1
17	2017-08-29 04:45:54.152+00	7	Опубликован	2	[{"changed": {"fields": ["translate"]}}]	10	1
18	2017-08-30 12:48:56.411+00	1	Manager	1	[{"added": {}}]	3	2
120	2017-09-06 13:14:33.637+00	53	Насыпной	1	[{"added": {}}]	11	2
121	2017-09-06 13:14:44.118+00	54	Вентилируемый	1	[{"added": {}}]	11	2
936	2017-10-27 04:05:56.240893+00	149	OrderModel object	3		21	1
937	2017-10-27 04:05:56.244907+00	148	OrderModel object	3		21	1
938	2017-10-27 04:05:56.248839+00	147	OrderModel object	3		21	1
939	2017-10-27 04:05:56.253302+00	146	OrderModel object	3		21	1
940	2017-10-27 04:05:56.263731+00	145	OrderModel object	3		21	1
941	2017-10-27 04:05:56.267216+00	144	OrderModel object	3		21	1
942	2017-10-27 04:05:56.271447+00	143	OrderModel object	3		21	1
943	2017-10-27 04:05:56.27556+00	142	OrderModel object	3		21	1
944	2017-10-27 04:05:56.279467+00	141	OrderModel object	3		21	1
945	2017-10-27 04:05:56.28339+00	140	OrderModel object	3		21	1
946	2017-10-27 04:05:56.287779+00	139	OrderModel object	3		21	1
947	2017-10-27 04:05:56.2915+00	138	OrderModel object	3		21	1
948	2017-10-27 04:05:56.295309+00	137	OrderModel object	3		21	1
949	2017-10-27 04:05:56.299463+00	136	OrderModel object	3		21	1
950	2017-10-27 04:05:56.303496+00	135	OrderModel object	3		21	1
951	2017-10-27 04:05:56.30745+00	134	OrderModel object	3		21	1
952	2017-10-27 04:05:56.311374+00	133	OrderModel object	3		21	1
122	2017-09-06 13:14:47.127+00	55	Тип	1	[{"added": {}}]	14	2
123	2017-09-06 13:16:45.486+00	46	40 фут ТС	3		6	2
124	2017-09-06 13:16:45.491+00	45	20 фут TC	3		6	2
125	2017-09-06 13:16:45.494+00	44	40 фут FT	3		6	2
126	2017-09-06 13:16:45.497+00	43	20 фут FT	3		6	2
127	2017-09-06 13:16:45.502+00	42	40 фут OT	3		6	2
128	2017-09-06 13:16:45.508+00	41	20 фут OT	3		6	2
129	2017-09-06 13:16:45.516+00	37	40 фут RF High Cube	3		6	2
953	2017-10-27 04:05:56.315454+00	132	OrderModel object	3		21	1
954	2017-10-27 04:05:56.319272+00	131	OrderModel object	3		21	1
955	2017-10-27 04:05:56.323162+00	130	OrderModel object	3		21	1
956	2017-10-27 04:05:56.327671+00	129	OrderModel object	3		21	1
130	2017-09-06 13:16:45.521+00	36	40 фут RF	3		6	2
131	2017-09-06 13:18:44.787+00	56	45 фут	1	[{"added": {}}]	11	2
132	2017-09-06 13:18:51.138+00	57	10 фут	1	[{"added": {}}]	11	2
133	2017-09-06 13:18:52.83+00	47	Размер	2	[]	14	2
134	2017-09-06 13:21:58.684+00	54	Вентилируемый	3		6	2
135	2017-09-06 13:21:58.688+00	53	Насыпной	3		6	2
137	2017-09-06 13:21:58.695+00	51	Платформа	3		6	2
138	2017-09-06 13:21:58.702+00	50	Открытый	3		6	2
139	2017-09-06 13:21:58.706+00	49	Сухогрузный	3		6	2
140	2017-09-06 13:21:58.711+00	48	Рефрежераторный	3		6	2
141	2017-09-06 13:23:55.313+00	58	сокращенное	1	[{"added": {}}]	11	2
198	2017-09-06 14:01:25.732+00	15	TestContaine3r	3		8	2
199	2017-09-06 14:01:25.738+00	14	TestContaine3r	3		8	2
957	2017-10-27 04:05:56.331874+00	128	OrderModel object	3		21	1
958	2017-10-27 04:05:56.341352+00	127	OrderModel object	3		21	1
468	2017-10-27 01:32:13.982794+00	168	OrderModel object	3		21	2
469	2017-10-27 01:32:13.986415+00	167	OrderModel object	3		21	2
470	2017-10-27 01:32:13.994576+00	166	OrderModel object	3		21	2
471	2017-10-27 01:32:14.007191+00	165	OrderModel object	3		21	2
472	2017-10-27 01:32:14.019004+00	164	OrderModel object	3		21	2
473	2017-10-27 01:32:14.026647+00	163	OrderModel object	3		21	2
474	2017-10-27 01:32:14.03131+00	162	OrderModel object	3		21	2
959	2017-10-27 04:05:56.345622+00	126	OrderModel object	3		21	1
960	2017-10-27 04:05:56.350007+00	125	OrderModel object	3		21	1
961	2017-10-27 04:05:56.353844+00	124	OrderModel object	3		21	1
154	2017-09-06 13:49:45.983+00	3	По цене	3		7	2
962	2017-10-27 04:05:56.35717+00	123	OrderModel object	3		21	1
208	2017-09-06 14:01:25.782+00	5	TestContaine3r	3		8	2
963	2017-10-27 04:05:56.361173+00	122	OrderModel object	3		21	1
209	2017-09-06 14:01:25.786+00	4	TestContaine3r	3		8	2
210	2017-09-06 14:01:25.791+00	3	TestContaine3r	3		8	2
211	2017-09-06 14:01:25.796+00	2	TestContaine3r	3		8	2
212	2017-09-06 14:01:25.8+00	1	Контейнер	3		8	2
213	2017-09-06 14:12:23.431+00	1	Контейнер	2	[]	9	2
214	2017-09-06 14:13:24.45+00	68	Новый	1	[{"added": {}}]	11	2
215	2017-09-06 14:13:31.987+00	69	б/у	1	[{"added": {}}]	11	2
216	2017-09-06 14:13:33.584+00	32	Состояние	2	[]	14	2
217	2017-09-06 14:13:59.768+00	70	Свободен	1	[{"added": {}}]	11	2
218	2017-09-06 14:14:09.066+00	71	Арендован	1	[{"added": {}}]	11	2
219	2017-09-06 14:14:16.749+00	72	Продан	1	[{"added": {}}]	11	2
220	2017-09-06 14:14:17.945+00	33	Статус	2	[]	14	2
221	2017-09-07 02:19:02.783+00	6	Гарантия	3		13	2
222	2017-09-07 02:19:21.866+00	73	Гарантия	1	[{"added": {}}]	10	2
223	2017-09-07 02:19:36.586+00	19	Гарантия	1	[{"added": {}}]	7	2
224	2017-09-07 02:19:38.903+00	1	Контейнер	2	[]	9	2
225	2017-09-11 05:50:29.516+00	67	Цена аренды в год	2	[{"changed": {"fields": ["name", "translate"]}}]	11	2
226	2017-09-11 06:14:26.221+00	20	Аренда	1	[{"added": {}}]	7	2
227	2017-09-11 06:15:51.322+00	74	Аренда	1	[{"added": {}}]	10	2
228	2017-09-11 06:16:02.744+00	75	Продажа	1	[{"added": {}}]	10	2
229	2017-09-11 06:16:22.461+00	1	Контейнер	2	[]	9	2
230	2017-09-11 06:16:53.14+00	20	Аренда	2	[{"changed": {"fields": ["field"]}}]	7	2
231	2017-09-11 06:16:53.7+00	20	Аренда	2	[]	7	2
260	2017-09-12 07:14:41.186+00	81	Тест_Имя	1	[{"added": {}}]	11	1
261	2017-09-12 11:59:27.485+00	82	Рефрежераторный2	1	[{"added": {}}]	60	2
964	2017-10-27 04:05:56.371945+00	121	OrderModel object	3		21	1
965	2017-10-27 04:05:56.37528+00	120	OrderModel object	3		21	1
966	2017-10-27 04:05:56.379479+00	119	OrderModel object	3		21	1
967	2017-10-27 04:05:56.383375+00	118	OrderModel object	3		21	1
968	2017-10-27 04:05:56.387454+00	117	OrderModel object	3		21	1
969	2017-10-27 04:05:56.391492+00	116	OrderModel object	3		21	1
970	2017-10-27 04:05:56.395336+00	115	OrderModel object	3		21	1
971	2017-10-27 04:05:56.399262+00	114	OrderModel object	3		21	1
972	2017-10-27 04:05:56.403265+00	113	OrderModel object	3		21	1
973	2017-10-27 04:05:56.407322+00	112	OrderModel object	3		21	1
974	2017-10-27 04:05:56.411221+00	111	OrderModel object	3		21	1
975	2017-10-27 04:05:56.415163+00	110	OrderModel object	3		21	1
976	2017-10-27 04:05:56.419221+00	109	OrderModel object	3		21	1
977	2017-10-27 04:05:56.423221+00	108	OrderModel object	3		21	1
978	2017-10-27 04:05:56.427171+00	107	OrderModel object	3		21	1
979	2017-10-27 04:05:56.431049+00	74	OrderModel object	3		21	1
980	2017-10-27 04:05:56.435073+00	73	OrderModel object	3		21	1
981	2017-10-27 04:05:56.439029+00	72	OrderModel object	3		21	1
982	2017-10-27 04:05:56.449227+00	71	OrderModel object	3		21	1
983	2017-10-27 04:05:56.453828+00	70	OrderModel object	3		21	1
984	2017-10-27 04:05:56.457158+00	69	OrderModel object	3		21	1
985	2017-10-27 04:05:56.479343+00	68	OrderModel object	3		21	1
986	2017-10-27 04:05:56.493376+00	67	OrderModel object	3		21	1
987	2017-10-27 04:05:56.499308+00	66	OrderModel object	3		21	1
988	2017-10-27 04:05:56.503043+00	65	OrderModel object	3		21	1
989	2017-10-27 04:05:56.507073+00	64	OrderModel object	3		21	1
990	2017-10-27 04:05:56.511027+00	63	OrderModel object	3		21	1
991	2017-10-27 04:05:56.514978+00	62	OrderModel object	3		21	1
992	2017-10-27 04:05:56.519025+00	61	OrderModel object	3		21	1
993	2017-10-27 04:05:56.522935+00	60	OrderModel object	3		21	1
994	2017-10-27 04:05:56.535061+00	59	OrderModel object	3		21	1
995	2017-10-27 04:05:56.539406+00	58	OrderModel object	3		21	1
996	2017-10-27 04:05:56.543302+00	57	OrderModel object	3		21	1
997	2017-10-27 04:05:56.547224+00	56	OrderModel object	3		21	1
262	2017-09-14 05:04:09.592+00	4	d@gmail.com	2	[{"changed": {"fields": ["patronymic", "company", "photo"]}}]	15	2
263	2017-09-14 06:22:39.882+00	8	def@def.ru	2	[{"changed": {"fields": ["patronymic", "company", "photo"]}}]	15	2
264	2017-09-14 07:14:40.34+00	82	Рефрежераторный2	3		60	2
265	2017-09-14 07:15:58.897+00	65	Вентилируемый	3		11	2
266	2017-09-14 07:15:58.902+00	64	Насыпной	3		11	2
268	2017-09-14 07:15:58.91+00	62	Платформа	3		11	2
269	2017-09-14 07:15:58.914+00	61	Открытый	3		11	2
270	2017-09-14 07:15:58.918+00	60	Сухогрузный	3		11	2
271	2017-09-14 07:15:58.923+00	59	Рефрежераторный	3		11	2
475	2017-10-27 01:32:14.035591+00	161	OrderModel object	3		21	2
476	2017-10-27 01:32:14.039292+00	160	OrderModel object	3		21	2
477	2017-10-27 01:32:14.043355+00	159	OrderModel object	3		21	2
478	2017-10-27 01:32:14.047151+00	158	OrderModel object	3		21	2
479	2017-10-27 01:32:14.057816+00	157	OrderModel object	3		21	2
480	2017-10-27 01:32:14.061028+00	156	OrderModel object	3		21	2
481	2017-10-27 01:32:14.064213+00	155	OrderModel object	3		21	2
482	2017-10-27 01:32:14.067276+00	154	OrderModel object	3		21	2
483	2017-10-27 01:32:14.071276+00	153	OrderModel object	3		21	2
484	2017-10-27 01:32:14.075287+00	152	OrderModel object	3		21	2
485	2017-10-27 01:32:14.079223+00	151	OrderModel object	3		21	2
486	2017-10-27 01:32:14.083197+00	150	OrderModel object	3		21	2
487	2017-10-27 01:32:14.087304+00	149	OrderModel object	3		21	2
488	2017-10-27 01:32:14.091189+00	148	OrderModel object	3		21	2
489	2017-10-27 01:32:14.095384+00	147	OrderModel object	3		21	2
490	2017-10-27 01:32:14.099333+00	146	OrderModel object	3		21	2
491	2017-10-27 01:32:14.103184+00	145	OrderModel object	3		21	2
492	2017-10-27 01:32:14.107174+00	144	OrderModel object	3		21	2
998	2017-10-27 04:05:56.557341+00	55	OrderModel object	3		21	1
999	2017-10-27 04:05:56.561817+00	54	OrderModel object	3		21	1
1654	2018-01-23 00:32:43.053962+00	1	Manager	2	[]	3	2
1000	2017-10-27 04:05:56.565779+00	53	OrderModel object	3		21	1
1001	2017-10-27 04:05:56.576081+00	52	OrderModel object	3		21	1
1002	2017-10-27 04:05:56.580449+00	51	OrderModel object	3		21	1
1003	2017-10-27 04:05:56.584569+00	50	OrderModel object	3		21	1
1004	2017-10-27 04:05:56.590101+00	49	OrderModel object	3		21	1
1005	2017-10-27 04:05:56.59495+00	48	OrderModel object	3		21	1
1006	2017-10-27 04:05:56.598859+00	47	OrderModel object	3		21	1
1007	2017-10-27 04:05:56.602868+00	46	OrderModel object	3		21	1
1008	2017-10-27 04:05:56.606868+00	45	OrderModel object	3		21	1
1009	2017-10-27 04:05:56.610991+00	44	OrderModel object	3		21	1
1010	2017-10-27 04:05:56.614952+00	43	OrderModel object	3		21	1
1011	2017-10-27 04:05:56.618756+00	42	OrderModel object	3		21	1
1012	2017-10-27 04:05:56.628858+00	41	OrderModel object	3		21	1
1013	2017-10-27 04:05:56.631948+00	40	OrderModel object	3		21	1
1014	2017-10-27 04:05:56.634757+00	39	OrderModel object	3		21	1
1015	2017-10-27 04:05:56.638852+00	38	OrderModel object	3		21	1
1016	2017-10-27 04:05:56.643059+00	37	OrderModel object	3		21	1
1017	2017-10-27 04:05:56.646979+00	36	OrderModel object	3		21	1
1018	2017-10-27 04:05:56.651021+00	35	OrderModel object	3		21	1
286	2017-09-14 08:03:29.674+00	91	Модель	2	[{"changed": {"fields": ["translate"]}}]	14	2
287	2017-09-14 08:09:59.543+00	91	Модель	2	[{"changed": {"fields": ["translate"]}}]	14	2
288	2017-09-14 08:13:03.091+00	91	Модель	2	[{"changed": {"fields": ["translate"]}}]	14	2
290	2017-09-14 08:34:42.049+00	93	Другая модель	1	[{"added": {}}]	11	2
291	2017-09-14 08:34:59.053+00	93	Другая модель	2	[]	11	2
292	2017-09-14 08:35:15.443+00	91	Модель	2	[]	14	2
293	2017-09-14 08:39:12.488+00	93	Другая модель	2	[]	11	2
294	2017-09-14 08:40:35.761+00	16	Местонахождение	3		6	2
296	2017-09-14 08:46:06.265+00	95	г.Владивосток, ул. Тухачевского, 26	1	[{"added": {}}]	64	2
298	2017-09-14 08:46:29.621+00	97	г.Владивосток, ул. Тухачевского, 28	1	[{"added": {}}]	64	2
300	2017-09-14 08:47:40.651+00	98	Другое место	3		14	2
301	2017-09-14 08:48:41.068+00	99	Другое место	1	[{"added": {}}]	11	2
302	2017-09-14 08:48:46.694+00	1	Контейнер	2	[]	9	2
303	2017-09-15 10:22:22.062+00	22	Модель	1	[{"added": {}}]	7	2
304	2017-09-15 10:22:36.901+00	23	Местонахождение	1	[{"added": {}}]	7	2
305	2017-09-17 13:21:34.001+00	9	danil@danil.ru	3		15	2
306	2017-09-17 13:24:49.44+00	16	danil@danil.ru	1	[{"added": {}}]	15	2
307	2017-09-17 14:50:24.354+00	14	sotnikov@rudoit.ru	2	[{"changed": {"fields": ["patronymic", "phone", "contact_email", "company", "photo"]}}]	15	2
308	2017-09-17 23:46:27.131+00	164	adssa	3		8	2
493	2017-10-27 01:32:14.111334+00	143	OrderModel object	3		21	2
494	2017-10-27 01:32:14.115539+00	142	OrderModel object	3		21	2
495	2017-10-27 01:32:14.119304+00	141	OrderModel object	3		21	2
496	2017-10-27 01:32:14.130195+00	140	OrderModel object	3		21	2
497	2017-10-27 01:32:14.13425+00	139	OrderModel object	3		21	2
498	2017-10-27 01:32:14.146949+00	138	OrderModel object	3		21	2
499	2017-10-27 01:32:14.154791+00	137	OrderModel object	3		21	2
500	2017-10-27 01:32:14.158458+00	136	OrderModel object	3		21	2
501	2017-10-27 01:32:14.163113+00	135	OrderModel object	3		21	2
502	2017-10-27 01:32:14.168189+00	134	OrderModel object	3		21	2
503	2017-10-27 01:32:14.171808+00	133	OrderModel object	3		21	2
504	2017-10-27 01:32:14.175816+00	132	OrderModel object	3		21	2
505	2017-10-27 01:32:14.179553+00	131	OrderModel object	3		21	2
506	2017-10-27 01:32:14.183599+00	130	OrderModel object	3		21	2
507	2017-10-27 01:32:14.187261+00	129	OrderModel object	3		21	2
508	2017-10-27 01:32:14.191457+00	128	OrderModel object	3		21	2
509	2017-10-27 01:32:14.195635+00	127	OrderModel object	3		21	2
510	2017-10-27 01:32:14.200689+00	126	OrderModel object	3		21	2
511	2017-10-27 01:32:14.204778+00	125	OrderModel object	3		21	2
512	2017-10-27 01:32:14.208806+00	124	OrderModel object	3		21	2
513	2017-10-27 01:32:14.213037+00	123	OrderModel object	3		21	2
514	2017-10-27 01:32:14.221881+00	122	OrderModel object	3		21	2
515	2017-10-27 01:32:14.225414+00	121	OrderModel object	3		21	2
516	2017-10-27 01:32:14.228598+00	120	OrderModel object	3		21	2
517	2017-10-27 01:32:14.231935+00	119	OrderModel object	3		21	2
518	2017-10-27 01:32:14.235378+00	118	OrderModel object	3		21	2
519	2017-10-27 01:32:14.239362+00	117	OrderModel object	3		21	2
520	2017-10-27 01:32:14.243441+00	116	OrderModel object	3		21	2
521	2017-10-27 01:32:14.247415+00	115	OrderModel object	3		21	2
522	2017-10-27 01:32:14.251408+00	114	OrderModel object	3		21	2
523	2017-10-27 01:32:14.25536+00	113	OrderModel object	3		21	2
524	2017-10-27 01:32:14.25946+00	112	OrderModel object	3		21	2
525	2017-10-27 01:32:14.263344+00	111	OrderModel object	3		21	2
604	2017-10-27 01:33:17.839931+00	202	OrderModel object	3		21	2
605	2017-10-27 01:33:17.855943+00	201	OrderModel object	3		21	2
606	2017-10-27 01:33:17.860323+00	200	OrderModel object	3		21	2
607	2017-10-27 01:33:17.870011+00	199	OrderModel object	3		21	2
608	2017-10-27 01:33:17.874945+00	198	OrderModel object	3		21	2
609	2017-10-27 01:33:17.878675+00	197	OrderModel object	3		21	2
610	2017-10-27 01:33:17.881746+00	196	OrderModel object	3		21	2
611	2017-10-27 01:33:17.885182+00	195	OrderModel object	3		21	2
612	2017-10-27 01:33:17.889186+00	194	OrderModel object	3		21	2
613	2017-10-27 01:33:17.893105+00	193	OrderModel object	3		21	2
614	2017-10-27 01:33:17.897259+00	192	OrderModel object	3		21	2
615	2017-10-27 01:33:17.901036+00	191	OrderModel object	3		21	2
616	2017-10-27 01:33:17.905098+00	190	OrderModel object	3		21	2
617	2017-10-27 01:33:17.915847+00	189	OrderModel object	3		21	2
618	2017-10-27 01:33:17.919783+00	188	OrderModel object	3		21	2
619	2017-10-27 01:33:17.923461+00	187	OrderModel object	3		21	2
877	2017-10-27 01:36:19.988243+00	81	Bonus object	3		16	2
1019	2017-10-27 04:05:56.655338+00	34	OrderModel object	3		21	1
1020	2017-10-27 04:05:56.659875+00	33	OrderModel object	3		21	1
1021	2017-10-27 04:05:56.667137+00	32	OrderModel object	3		21	1
1022	2017-10-27 04:05:56.681944+00	31	OrderModel object	3		21	1
1023	2017-10-27 04:05:56.697826+00	30	OrderModel object	3		21	1
1024	2017-10-27 04:05:56.702362+00	29	OrderModel object	3		21	1
235	2017-09-11 11:24:30.762+00	152	1200	2	[]	8	2
316	2017-09-17 23:46:27.167+00	152	1212	3		8	2
340	2017-09-17 23:46:27.291+00	65	вфывф	3		8	2
341	2017-09-17 23:46:27.296+00	64	King	3		8	2
342	2017-09-17 23:46:27.301+00	63	King 1234	3		8	2
343	2017-09-17 23:46:27.305+00	62	King 123456	3		8	2
344	2017-09-17 23:46:27.311+00	61	King 123456	3		8	2
345	2017-09-17 23:46:27.317+00	60	King	3		8	2
346	2017-09-17 23:46:27.324+00	59	King 1234567	3		8	2
347	2017-09-17 23:46:27.329+00	58	King 1234567	3		8	2
348	2017-09-17 23:46:27.333+00	57	King 123456	3		8	2
349	2017-09-17 23:46:27.338+00	56	King 12345	3		8	2
350	2017-09-17 23:46:27.344+00	55	King 1234	3		8	2
351	2017-09-17 23:46:27.349+00	54	King 333	3		8	2
352	2017-09-17 23:46:27.354+00	53	King 222	3		8	2
1025	2017-10-27 04:05:56.706189+00	28	OrderModel object	3		21	1
1026	2017-10-27 04:05:56.710409+00	27	OrderModel object	3		21	1
1027	2017-10-27 04:05:56.720848+00	26	OrderModel object	3		21	1
1028	2017-10-27 04:05:56.724477+00	25	OrderModel object	3		21	1
1029	2017-10-27 04:05:56.727948+00	24	OrderModel object	3		21	1
1030	2017-10-27 04:05:56.731423+00	23	OrderModel object	3		21	1
1031	2017-10-27 04:05:56.734843+00	22	OrderModel object	3		21	1
1032	2017-10-27 04:05:56.73896+00	21	OrderModel object	3		21	1
1033	2017-10-27 04:05:56.743045+00	20	OrderModel object	3		21	1
1034	2017-10-27 04:05:56.746985+00	19	OrderModel object	3		21	1
1035	2017-10-27 04:05:56.753484+00	18	OrderModel object	3		21	1
1036	2017-10-27 04:05:56.757617+00	17	OrderModel object	3		21	1
1037	2017-10-27 04:05:56.761654+00	16	OrderModel object	3		21	1
1038	2017-10-27 04:05:56.765491+00	15	OrderModel object	3		21	1
1039	2017-10-27 04:05:56.769845+00	14	OrderModel object	3		21	1
1040	2017-10-27 04:05:56.777996+00	13	OrderModel object	3		21	1
1041	2017-10-27 04:05:56.783403+00	12	OrderModel object	3		21	1
1042	2017-10-27 04:05:56.787534+00	11	OrderModel object	3		21	1
1043	2017-10-27 04:05:56.791413+00	10	OrderModel object	3		21	1
1044	2017-10-27 04:05:56.795542+00	9	OrderModel object	3		21	1
1045	2017-10-27 04:05:56.804788+00	8	OrderModel object	3		21	1
1046	2017-10-27 04:05:56.808806+00	7	OrderModel object	3		21	1
1047	2017-10-27 04:05:56.813478+00	6	OrderModel object	3		21	1
1048	2017-10-27 04:05:56.817844+00	5	OrderModel object	3		21	1
1049	2017-10-27 04:05:56.824283+00	4	OrderModel object	3		21	1
1050	2017-10-27 04:05:56.828367+00	3	OrderModel object	3		21	1
1051	2017-10-27 04:05:56.840018+00	2	OrderModel object	3		21	1
1052	2017-10-27 04:05:56.844215+00	1	OrderModel object	3		21	1
1199	2017-11-07 10:05:11.744026+00	120	test размер ДхШхВ	3		59	14
1265	2017-11-12 04:30:10.7016+00	84	Рефрижераторный	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1281	2017-11-12 05:03:50.199822+00	39	40 фут  High Cube 2.9	2	[{"changed": {"fields": ["order_number"]}}]	11	2
353	2017-09-17 23:46:27.359+00	52	King 123	3		8	2
354	2017-09-17 23:46:27.363+00	51	King 123	3		8	2
355	2017-09-17 23:46:27.367+00	50	TestContainer123	3		8	2
356	2017-09-17 23:46:27.372+00	49	TestContaine3r	3		8	2
367	2017-09-19 23:58:03.8+00	1	Контейнер	2	[]	9	2
368	2017-10-05 06:59:53.021+00	101	Скидка	1	[{"added": {}}]	10	2
369	2017-10-09 01:22:04.725+00	102	Тухачевского, 26	1	[{"added": {}}]	11	2
370	2017-10-09 01:22:30.067+00	103	Город	1	[{"added": {}}]	14	2
371	2017-10-09 01:22:58.509+00	104	Владивосток	1	[{"added": {}}]	14	2
373	2017-10-09 01:23:31.809+00	105	Аврорская, 1	1	[{"added": {}}]	11	2
374	2017-10-09 01:23:34.174+00	104	Владивосток	2	[]	14	2
375	2017-10-09 01:23:54.094+00	106	Уссурийск	1	[{"added": {}}]	14	2
376	2017-10-09 01:23:55.971+00	103	Город	2	[]	14	2
377	2017-10-09 01:24:50.56+00	107	Абребская 46	1	[{"added": {}}]	11	2
378	2017-10-09 01:25:06.991+00	108	Ленинская 1	1	[{"added": {}}]	11	2
379	2017-10-09 01:25:09.124+00	106	Уссурийск	2	[]	14	2
380	2017-10-09 01:25:17.581+00	105	Аврорская, 1	2	[{"changed": {"fields": ["required"]}}]	11	2
381	2017-10-09 01:25:22.726+00	104	Владивосток	2	[{"changed": {"fields": ["required"]}}]	14	2
382	2017-10-09 01:25:29.88+00	103	Город	2	[]	14	2
526	2017-10-27 01:32:23.261597+00	110	OrderModel object	3		21	2
527	2017-10-27 01:32:23.277576+00	109	OrderModel object	3		21	2
528	2017-10-27 01:32:23.282255+00	108	OrderModel object	3		21	2
529	2017-10-27 01:32:23.286793+00	107	OrderModel object	3		21	2
530	2017-10-27 01:32:23.291529+00	74	OrderModel object	3		21	2
531	2017-10-27 01:32:23.297748+00	73	OrderModel object	3		21	2
532	2017-10-27 01:32:23.301857+00	72	OrderModel object	3		21	2
533	2017-10-27 01:32:23.305783+00	71	OrderModel object	3		21	2
534	2017-10-27 01:32:23.309637+00	70	OrderModel object	3		21	2
535	2017-10-27 01:32:23.313506+00	69	OrderModel object	3		21	2
536	2017-10-27 01:32:23.323632+00	68	OrderModel object	3		21	2
537	2017-10-27 01:32:23.327302+00	67	OrderModel object	3		21	2
538	2017-10-27 01:32:23.330897+00	66	OrderModel object	3		21	2
539	2017-10-27 01:32:23.334712+00	65	OrderModel object	3		21	2
540	2017-10-27 01:32:23.338646+00	64	OrderModel object	3		21	2
541	2017-10-27 01:32:23.34266+00	63	OrderModel object	3		21	2
542	2017-10-27 01:32:23.346637+00	62	OrderModel object	3		21	2
543	2017-10-27 01:32:23.35063+00	61	OrderModel object	3		21	2
544	2017-10-27 01:32:23.354602+00	60	OrderModel object	3		21	2
545	2017-10-27 01:32:23.3587+00	59	OrderModel object	3		21	2
546	2017-10-27 01:32:23.362523+00	58	OrderModel object	3		21	2
547	2017-10-27 01:32:23.366613+00	57	OrderModel object	3		21	2
548	2017-10-27 01:32:23.370623+00	56	OrderModel object	3		21	2
549	2017-10-27 01:32:23.374571+00	55	OrderModel object	3		21	2
550	2017-10-27 01:32:23.378681+00	54	OrderModel object	3		21	2
1053	2017-10-27 04:06:14.490613+00	106	OrderManagerModel object	3		20	1
1054	2017-10-27 04:06:14.504094+00	105	OrderManagerModel object	3		20	1
1055	2017-10-27 04:06:14.507816+00	104	OrderManagerModel object	3		20	1
56	2017-09-01 03:26:53.344+00	29	Тип контейнера	2	[]	23	1
142	2017-09-06 13:25:06.727+00	59	Рефрежераторный	1	[{"added": {}}]	11	2
143	2017-09-06 13:25:20.67+00	60	Сухогрузный	1	[{"added": {}}]	11	2
551	2017-10-27 01:32:23.382552+00	53	OrderModel object	3		21	2
552	2017-10-27 01:32:23.386635+00	52	OrderModel object	3		21	2
553	2017-10-27 01:32:23.390521+00	51	OrderModel object	3		21	2
554	2017-10-27 01:32:23.401137+00	50	OrderModel object	3		21	2
555	2017-10-27 01:32:23.415834+00	49	OrderModel object	3		21	2
556	2017-10-27 01:32:23.427084+00	48	OrderModel object	3		21	2
557	2017-10-27 01:32:23.44272+00	47	OrderModel object	3		21	2
558	2017-10-27 01:32:23.455001+00	46	OrderModel object	3		21	2
559	2017-10-27 01:32:23.459025+00	45	OrderModel object	3		21	2
560	2017-10-27 01:32:23.462734+00	44	OrderModel object	3		21	2
561	2017-10-27 01:32:23.46691+00	43	OrderModel object	3		21	2
562	2017-10-27 01:32:23.470677+00	42	OrderModel object	3		21	2
563	2017-10-27 01:32:23.474703+00	41	OrderModel object	3		21	2
564	2017-10-27 01:32:23.478662+00	40	OrderModel object	3		21	2
565	2017-10-27 01:32:23.482663+00	39	OrderModel object	3		21	2
566	2017-10-27 01:32:23.48682+00	38	OrderModel object	3		21	2
567	2017-10-27 01:32:23.490849+00	37	OrderModel object	3		21	2
568	2017-10-27 01:32:23.494746+00	36	OrderModel object	3		21	2
569	2017-10-27 01:32:23.49863+00	35	OrderModel object	3		21	2
570	2017-10-27 01:32:23.502772+00	34	OrderModel object	3		21	2
571	2017-10-27 01:32:23.513138+00	33	OrderModel object	3		21	2
572	2017-10-27 01:32:23.516879+00	32	OrderModel object	3		21	2
573	2017-10-27 01:32:23.520491+00	31	OrderModel object	3		21	2
574	2017-10-27 01:32:23.523785+00	30	OrderModel object	3		21	2
575	2017-10-27 01:32:23.526985+00	29	OrderModel object	3		21	2
576	2017-10-27 01:32:23.530831+00	28	OrderModel object	3		21	2
577	2017-10-27 01:32:23.534774+00	27	OrderModel object	3		21	2
578	2017-10-27 01:32:23.538769+00	26	OrderModel object	3		21	2
579	2017-10-27 01:32:23.54299+00	25	OrderModel object	3		21	2
580	2017-10-27 01:32:23.547599+00	24	OrderModel object	3		21	2
581	2017-10-27 01:32:23.550776+00	23	OrderModel object	3		21	2
582	2017-10-27 01:32:23.554885+00	22	OrderModel object	3		21	2
583	2017-10-27 01:32:23.559009+00	21	OrderModel object	3		21	2
584	2017-10-27 01:32:23.562974+00	20	OrderModel object	3		21	2
585	2017-10-27 01:32:23.56684+00	19	OrderModel object	3		21	2
586	2017-10-27 01:32:23.570747+00	18	OrderModel object	3		21	2
587	2017-10-27 01:32:23.574746+00	17	OrderModel object	3		21	2
588	2017-10-27 01:32:23.579523+00	16	OrderModel object	3		21	2
589	2017-10-27 01:32:23.645657+00	15	OrderModel object	3		21	2
590	2017-10-27 01:32:23.719434+00	14	OrderModel object	3		21	2
591	2017-10-27 01:32:23.742429+00	13	OrderModel object	3		21	2
592	2017-10-27 01:32:23.766714+00	12	OrderModel object	3		21	2
593	2017-10-27 01:32:23.774888+00	11	OrderModel object	3		21	2
594	2017-10-27 01:32:23.779029+00	10	OrderModel object	3		21	2
595	2017-10-27 01:32:23.782779+00	9	OrderModel object	3		21	2
596	2017-10-27 01:32:23.786859+00	8	OrderModel object	3		21	2
597	2017-10-27 01:32:23.790972+00	7	OrderModel object	3		21	2
598	2017-10-27 01:32:23.794905+00	6	OrderModel object	3		21	2
599	2017-10-27 01:32:23.798873+00	5	OrderModel object	3		21	2
600	2017-10-27 01:32:23.802839+00	4	OrderModel object	3		21	2
601	2017-10-27 01:32:23.806965+00	3	OrderModel object	3		21	2
602	2017-10-27 01:32:23.810941+00	2	OrderModel object	3		21	2
603	2017-10-27 01:32:23.814851+00	1	OrderModel object	3		21	2
620	2017-10-27 01:33:17.927339+00	186	OrderModel object	3		21	2
621	2017-10-27 01:33:17.931455+00	185	OrderModel object	3		21	2
622	2017-10-27 01:33:17.935375+00	184	OrderModel object	3		21	2
623	2017-10-27 01:33:17.939358+00	183	OrderModel object	3		21	2
624	2017-10-27 01:33:17.943795+00	182	OrderModel object	3		21	2
625	2017-10-27 01:33:17.947396+00	181	OrderModel object	3		21	2
626	2017-10-27 01:33:17.951325+00	180	OrderModel object	3		21	2
627	2017-10-27 01:33:17.957423+00	179	OrderModel object	3		21	2
628	2017-10-27 01:33:17.961492+00	178	OrderModel object	3		21	2
629	2017-10-27 01:33:17.966236+00	177	OrderModel object	3		21	2
630	2017-10-27 01:33:17.970863+00	176	OrderModel object	3		21	2
631	2017-10-27 01:33:17.975534+00	175	OrderModel object	3		21	2
632	2017-10-27 01:33:17.979494+00	174	OrderModel object	3		21	2
633	2017-10-27 01:33:17.983688+00	173	OrderModel object	3		21	2
634	2017-10-27 01:33:17.993123+00	172	OrderModel object	3		21	2
635	2017-10-27 01:33:17.996598+00	171	OrderModel object	3		21	2
636	2017-10-27 01:33:18.006544+00	170	OrderModel object	3		21	2
637	2017-10-27 01:33:18.010098+00	169	OrderModel object	3		21	2
638	2017-10-27 01:33:18.013622+00	168	OrderModel object	3		21	2
639	2017-10-27 01:33:18.017791+00	167	OrderModel object	3		21	2
640	2017-10-27 01:33:18.021489+00	166	OrderModel object	3		21	2
641	2017-10-27 01:33:18.025537+00	165	OrderModel object	3		21	2
642	2017-10-27 01:33:18.029534+00	164	OrderModel object	3		21	2
643	2017-10-27 01:33:18.033263+00	163	OrderModel object	3		21	2
644	2017-10-27 01:33:18.03735+00	162	OrderModel object	3		21	2
645	2017-10-27 01:33:18.041607+00	161	OrderModel object	3		21	2
646	2017-10-27 01:33:18.045463+00	160	OrderModel object	3		21	2
647	2017-10-27 01:33:18.049364+00	159	OrderModel object	3		21	2
648	2017-10-27 01:33:18.053255+00	158	OrderModel object	3		21	2
649	2017-10-27 01:33:18.057242+00	157	OrderModel object	3		21	2
650	2017-10-27 01:33:18.061358+00	156	OrderModel object	3		21	2
651	2017-10-27 01:33:18.065202+00	155	OrderModel object	3		21	2
652	2017-10-27 01:33:18.075715+00	154	OrderModel object	3		21	2
653	2017-10-27 01:33:18.078591+00	153	OrderModel object	3		21	2
654	2017-10-27 01:33:18.083178+00	152	OrderModel object	3		21	2
655	2017-10-27 01:33:18.087067+00	151	OrderModel object	3		21	2
656	2017-10-27 01:33:18.091099+00	150	OrderModel object	3		21	2
1056	2017-10-27 04:06:14.512133+00	103	OrderManagerModel object	3		20	1
1057	2017-10-27 04:06:14.515982+00	102	OrderManagerModel object	3		20	1
1058	2017-10-27 04:06:14.52708+00	89	OrderManagerModel object	3		20	1
1059	2017-10-27 04:06:14.530899+00	88	OrderManagerModel object	3		20	1
1060	2017-10-27 04:06:14.533749+00	87	OrderManagerModel object	3		20	1
1061	2017-10-27 04:06:14.537749+00	86	OrderManagerModel object	3		20	1
1062	2017-10-27 04:06:14.541786+00	85	OrderManagerModel object	3		20	1
1063	2017-10-27 04:06:14.545715+00	84	OrderManagerModel object	3		20	1
1064	2017-10-27 04:06:14.549669+00	83	OrderManagerModel object	3		20	1
1065	2017-10-27 04:06:14.553714+00	82	OrderManagerModel object	3		20	1
1066	2017-10-27 04:06:14.557827+00	81	OrderManagerModel object	3		20	1
1067	2017-10-27 04:06:14.561771+00	80	OrderManagerModel object	3		20	1
1068	2017-10-27 04:06:14.571561+00	79	OrderManagerModel object	3		20	1
1069	2017-10-27 04:06:14.574622+00	78	OrderManagerModel object	3		20	1
1070	2017-10-27 04:06:14.577652+00	77	OrderManagerModel object	3		20	1
1071	2017-10-27 04:06:14.581686+00	76	OrderManagerModel object	3		20	1
1072	2017-10-27 04:06:14.618073+00	75	OrderManagerModel object	3		20	1
1073	2017-10-27 04:06:14.710302+00	74	OrderManagerModel object	3		20	1
1074	2017-10-27 04:06:14.726665+00	73	OrderManagerModel object	3		20	1
1075	2017-10-27 04:06:14.733386+00	72	OrderManagerModel object	3		20	1
1076	2017-10-27 04:06:14.737308+00	71	OrderManagerModel object	3		20	1
1077	2017-10-27 04:06:14.741308+00	70	OrderManagerModel object	3		20	1
1078	2017-10-27 04:06:14.745264+00	69	OrderManagerModel object	3		20	1
1079	2017-10-27 04:06:14.74925+00	68	OrderManagerModel object	3		20	1
1080	2017-10-27 04:06:14.753413+00	67	OrderManagerModel object	3		20	1
1081	2017-10-27 04:06:14.757856+00	66	OrderManagerModel object	3		20	1
1082	2017-10-27 04:06:14.762087+00	65	OrderManagerModel object	3		20	1
1083	2017-10-27 04:06:14.76526+00	64	OrderManagerModel object	3		20	1
1084	2017-10-27 04:06:14.769253+00	63	OrderManagerModel object	3		20	1
1085	2017-10-27 04:06:14.799119+00	62	OrderManagerModel object	3		20	1
1086	2017-10-27 04:06:14.810762+00	61	OrderManagerModel object	3		20	1
1087	2017-10-27 04:06:14.816447+00	60	OrderManagerModel object	3		20	1
1088	2017-10-27 04:06:14.820105+00	59	OrderManagerModel object	3		20	1
1089	2017-10-27 04:06:14.824099+00	58	OrderManagerModel object	3		20	1
1090	2017-10-27 04:06:14.828081+00	57	OrderManagerModel object	3		20	1
1091	2017-10-27 04:06:14.832119+00	56	OrderManagerModel object	3		20	1
1092	2017-10-27 04:06:14.836063+00	55	OrderManagerModel object	3		20	1
19	2017-08-30 13:26:23.398+00	8	Описание	1	[{"added": {}}]	11	2
20	2017-08-30 13:27:40.153+00	9	Тип	1	[{"added": {}}]	14	2
21	2017-08-30 13:28:18.397+00	10	Размер	1	[{"added": {}}]	14	2
22	2017-08-30 13:30:09.263+00	11	Год	1	[{"added": {}}]	13	2
23	2017-08-30 13:30:24.483+00	12	Модель	1	[{"added": {}}]	11	2
24	2017-08-30 13:31:31.061+00	13	Внутренние размеры	1	[{"added": {}}]	11	2
25	2017-08-30 13:31:48.532+00	14	Вместимость в м3	1	[{"added": {}}]	13	2
26	2017-08-30 13:32:05.01+00	15	Вес тары	1	[{"added": {}}]	13	2
27	2017-08-30 13:33:37.871+00	16	Местонахождение	1	[{"added": {}}]	11	2
28	2017-08-30 13:35:34.138+00	17	Использовался в РФ	1	[{"added": {}}]	10	2
29	2017-08-30 13:36:07.738+00	18	Гарантия	1	[{"added": {}}]	10	2
30	2017-08-30 13:36:24.737+00	19	Срок гарантии	1	[{"added": {}}]	13	2
31	2017-08-30 13:36:54.006+00	20	Описание гарантии	1	[{"added": {}}]	11	2
32	2017-08-30 13:38:56.725+00	21	Номер контейнера	1	[{"added": {}}]	11	2
33	2017-08-30 13:39:34.427+00	22	Акт технического состояния	1	[{"added": {}}]	12	2
34	2017-08-31 00:44:15.936+00	23	Контейнер обычный (test)	1	[{"added": {}}]	11	1
35	2017-08-31 00:44:28.436+00	24	Контейнер необычный (test)	1	[{"added": {}}]	11	1
36	2017-08-31 00:44:32.544+00	9	Тип	2	[]	14	1
37	2017-08-31 00:45:47.501+00	1	Контейнер	2	[]	9	1
38	2017-08-31 01:39:39.3+00	8	По типу	1	[{"added": {}}]	7	1
39	2017-08-31 01:39:41.179+00	1	Контейнер	2	[]	9	1
40	2017-08-31 02:09:30.825+00	9	Тип	2	[]	14	1
41	2017-08-31 02:09:40.38+00	9	Тип	2	[{"changed": {"fields": ["property"]}}]	14	1
42	2017-08-31 02:09:56.214+00	9	Тип	2	[]	14	1
43	2017-08-31 02:13:36.466+00	9	Тип	2	[]	14	1
44	2017-09-01 00:03:37.597+00	27	select_test1	2	[]	14	1
45	2017-09-01 00:04:08.817+00	1	Контейнер	2	[]	9	1
46	2017-09-01 02:20:57.268+00	28	Тип	1	[{"added": {}}]	23	1
47	2017-09-01 02:22:06.844+00	1	Контейнер	2	[]	9	1
48	2017-09-01 03:13:39.276+00	28	Тип	3		6	1
49	2017-09-01 03:19:01.504+00	18	Гарантия	3		6	1
50	2017-09-01 03:20:18.74+00	29	Тип контейнера	1	[{"added": {}}]	23	1
51	2017-09-01 03:21:37.696+00	29	Тип контейнера	2	[]	23	1
52	2017-09-01 03:21:53.552+00	29	Тип контейнера	2	[{"changed": {"fields": ["property"]}}]	23	1
53	2017-09-01 03:25:08.008+00	29	Тип контейнера	2	[{"changed": {"fields": ["property"]}}]	23	1
54	2017-09-01 03:25:21.54+00	29	Тип контейнера	2	[]	23	1
1093	2017-10-27 04:06:14.840377+00	54	OrderManagerModel object	3		20	1
55	2017-09-01 03:26:37.54+00	29	Тип контейнера	2	[]	23	1
657	2017-10-27 01:33:18.095084+00	149	OrderModel object	3		21	2
658	2017-10-27 01:33:18.134221+00	148	OrderModel object	3		21	2
659	2017-10-27 01:33:18.14964+00	147	OrderModel object	3		21	2
660	2017-10-27 01:33:18.165688+00	146	OrderModel object	3		21	2
661	2017-10-27 01:33:18.17481+00	145	OrderModel object	3		21	2
662	2017-10-27 01:33:18.17851+00	144	OrderModel object	3		21	2
663	2017-10-27 01:33:18.183175+00	143	OrderModel object	3		21	2
664	2017-10-27 01:33:18.186898+00	142	OrderModel object	3		21	2
665	2017-10-27 01:33:18.190733+00	141	OrderModel object	3		21	2
666	2017-10-27 01:33:18.19485+00	140	OrderModel object	3		21	2
1094	2017-10-27 04:06:14.848984+00	53	OrderManagerModel object	3		20	1
1095	2017-10-27 04:06:14.86442+00	52	OrderManagerModel object	3		20	1
1096	2017-10-27 04:06:14.878071+00	51	OrderManagerModel object	3		20	1
1097	2017-10-27 04:06:14.8841+00	50	OrderManagerModel object	3		20	1
1098	2017-10-27 04:06:14.888158+00	49	OrderManagerModel object	3		20	1
667	2017-10-27 01:33:18.198862+00	139	OrderModel object	3		21	2
668	2017-10-27 01:33:18.203027+00	138	OrderModel object	3		21	2
669	2017-10-27 01:33:18.221994+00	137	OrderModel object	3		21	2
670	2017-10-27 01:33:18.226983+00	136	OrderModel object	3		21	2
671	2017-10-27 01:33:18.241134+00	135	OrderModel object	3		21	2
672	2017-10-27 01:33:18.256795+00	134	OrderModel object	3		21	2
673	2017-10-27 01:33:18.260437+00	133	OrderModel object	3		21	2
674	2017-10-27 01:33:18.264486+00	132	OrderModel object	3		21	2
675	2017-10-27 01:33:18.268449+00	131	OrderModel object	3		21	2
676	2017-10-27 01:33:18.277909+00	130	OrderModel object	3		21	2
677	2017-10-27 01:33:18.285157+00	129	OrderModel object	3		21	2
678	2017-10-27 01:33:18.288541+00	128	OrderModel object	3		21	2
679	2017-10-27 01:33:18.292541+00	127	OrderModel object	3		21	2
680	2017-10-27 01:33:18.296482+00	126	OrderModel object	3		21	2
681	2017-10-27 01:33:18.300521+00	125	OrderModel object	3		21	2
682	2017-10-27 01:33:18.304499+00	124	OrderModel object	3		21	2
683	2017-10-27 01:33:18.308532+00	123	OrderModel object	3		21	2
684	2017-10-27 01:33:18.312403+00	122	OrderModel object	3		21	2
685	2017-10-27 01:33:18.316382+00	121	OrderModel object	3		21	2
686	2017-10-27 01:33:18.326613+00	120	OrderModel object	3		21	2
687	2017-10-27 01:33:18.329692+00	119	OrderModel object	3		21	2
688	2017-10-27 01:33:18.332626+00	118	OrderModel object	3		21	2
689	2017-10-27 01:33:18.336866+00	117	OrderModel object	3		21	2
690	2017-10-27 01:33:18.340343+00	116	OrderModel object	3		21	2
691	2017-10-27 01:33:18.344636+00	115	OrderModel object	3		21	2
692	2017-10-27 01:33:18.348507+00	114	OrderModel object	3		21	2
693	2017-10-27 01:33:18.352515+00	113	OrderModel object	3		21	2
694	2017-10-27 01:33:18.356481+00	112	OrderModel object	3		21	2
695	2017-10-27 01:33:18.360415+00	111	OrderModel object	3		21	2
696	2017-10-27 01:33:18.364393+00	110	OrderModel object	3		21	2
697	2017-10-27 01:33:18.368355+00	109	OrderModel object	3		21	2
698	2017-10-27 01:33:18.372423+00	108	OrderModel object	3		21	2
699	2017-10-27 01:33:18.376393+00	107	OrderModel object	3		21	2
700	2017-10-27 01:33:18.380311+00	74	OrderModel object	3		21	2
701	2017-10-27 01:33:18.384313+00	73	OrderModel object	3		21	2
702	2017-10-27 01:33:18.388338+00	72	OrderModel object	3		21	2
703	2017-10-27 01:33:18.39236+00	71	OrderModel object	3		21	2
774	2017-10-27 01:33:53.428992+00	101	OrderManagerModel object	3		20	2
775	2017-10-27 01:33:53.451769+00	100	OrderManagerModel object	3		20	2
776	2017-10-27 01:33:53.456205+00	99	OrderManagerModel object	3		20	2
777	2017-10-27 01:33:53.460829+00	98	OrderManagerModel object	3		20	2
778	2017-10-27 01:33:53.46529+00	97	OrderManagerModel object	3		20	2
779	2017-10-27 01:33:53.469626+00	96	OrderManagerModel object	3		20	2
780	2017-10-27 01:33:53.474163+00	95	OrderManagerModel object	3		20	2
781	2017-10-27 01:33:53.485338+00	94	OrderManagerModel object	3		20	2
782	2017-10-27 01:33:53.489609+00	93	OrderManagerModel object	3		20	2
783	2017-10-27 01:33:53.493264+00	92	OrderManagerModel object	3		20	2
784	2017-10-27 01:33:53.496434+00	91	OrderManagerModel object	3		20	2
785	2017-10-27 01:33:53.49959+00	90	OrderManagerModel object	3		20	2
786	2017-10-27 01:33:53.502654+00	89	OrderManagerModel object	3		20	2
787	2017-10-27 01:33:53.506629+00	88	OrderManagerModel object	3		20	2
788	2017-10-27 01:33:53.510836+00	87	OrderManagerModel object	3		20	2
789	2017-10-27 01:33:53.514949+00	86	OrderManagerModel object	3		20	2
790	2017-10-27 01:33:53.525408+00	85	OrderManagerModel object	3		20	2
791	2017-10-27 01:33:53.530533+00	84	OrderManagerModel object	3		20	2
792	2017-10-27 01:33:53.535304+00	83	OrderManagerModel object	3		20	2
793	2017-10-27 01:33:53.544607+00	82	OrderManagerModel object	3		20	2
794	2017-10-27 01:33:53.667323+00	81	OrderManagerModel object	3		20	2
795	2017-10-27 01:33:53.857352+00	80	OrderManagerModel object	3		20	2
796	2017-10-27 01:33:54.003474+00	79	OrderManagerModel object	3		20	2
797	2017-10-27 01:33:54.037198+00	78	OrderManagerModel object	3		20	2
798	2017-10-27 01:33:54.049834+00	77	OrderManagerModel object	3		20	2
799	2017-10-27 01:33:54.0586+00	76	OrderManagerModel object	3		20	2
800	2017-10-27 01:33:54.074641+00	75	OrderManagerModel object	3		20	2
801	2017-10-27 01:33:54.083246+00	74	OrderManagerModel object	3		20	2
802	2017-10-27 01:33:54.097132+00	73	OrderManagerModel object	3		20	2
803	2017-10-27 01:33:54.109774+00	72	OrderManagerModel object	3		20	2
804	2017-10-27 01:33:54.119522+00	71	OrderManagerModel object	3		20	2
805	2017-10-27 01:33:54.125822+00	70	OrderManagerModel object	3		20	2
806	2017-10-27 01:33:54.129468+00	69	OrderManagerModel object	3		20	2
807	2017-10-27 01:33:54.277512+00	68	OrderManagerModel object	3		20	2
808	2017-10-27 01:33:54.385287+00	67	OrderManagerModel object	3		20	2
809	2017-10-27 01:33:54.406109+00	66	OrderManagerModel object	3		20	2
810	2017-10-27 01:33:54.412738+00	65	OrderManagerModel object	3		20	2
811	2017-10-27 01:33:54.416734+00	64	OrderManagerModel object	3		20	2
812	2017-10-27 01:33:54.420973+00	63	OrderManagerModel object	3		20	2
813	2017-10-27 01:33:54.424739+00	62	OrderManagerModel object	3		20	2
814	2017-10-27 01:33:54.436373+00	61	OrderManagerModel object	3		20	2
815	2017-10-27 01:33:54.448573+00	60	OrderManagerModel object	3		20	2
816	2017-10-27 01:33:54.473192+00	59	OrderManagerModel object	3		20	2
817	2017-10-27 01:33:54.506807+00	58	OrderManagerModel object	3		20	2
818	2017-10-27 01:33:54.510341+00	57	OrderManagerModel object	3		20	2
819	2017-10-27 01:33:54.51641+00	56	OrderManagerModel object	3		20	2
820	2017-10-27 01:33:54.520676+00	55	OrderManagerModel object	3		20	2
821	2017-10-27 01:33:54.524521+00	54	OrderManagerModel object	3		20	2
822	2017-10-27 01:33:54.528601+00	53	OrderManagerModel object	3		20	2
823	2017-10-27 01:33:54.532472+00	52	OrderManagerModel object	3		20	2
824	2017-10-27 01:33:54.53651+00	51	OrderManagerModel object	3		20	2
825	2017-10-27 01:33:54.546996+00	50	OrderManagerModel object	3		20	2
826	2017-10-27 01:33:54.549641+00	49	OrderManagerModel object	3		20	2
827	2017-10-27 01:33:54.552539+00	48	OrderManagerModel object	3		20	2
828	2017-10-27 01:33:54.556438+00	47	OrderManagerModel object	3		20	2
829	2017-10-27 01:33:54.560584+00	46	OrderManagerModel object	3		20	2
830	2017-10-27 01:33:54.564627+00	45	OrderManagerModel object	3		20	2
831	2017-10-27 01:33:54.568433+00	44	OrderManagerModel object	3		20	2
832	2017-10-27 01:33:54.572567+00	43	OrderManagerModel object	3		20	2
833	2017-10-27 01:33:54.576468+00	42	OrderManagerModel object	3		20	2
335	2017-09-17 23:46:27.264+00	102	ddas231	3		8	2
1099	2017-10-27 04:06:14.892045+00	48	OrderManagerModel object	3		20	1
1100	2017-10-27 04:06:14.902662+00	47	OrderManagerModel object	3		20	1
1101	2017-10-27 04:06:14.906029+00	46	OrderManagerModel object	3		20	1
1102	2017-10-27 04:06:14.908961+00	45	OrderManagerModel object	3		20	1
1103	2017-10-27 04:06:14.913024+00	44	OrderManagerModel object	3		20	1
1104	2017-10-27 04:06:14.917064+00	43	OrderManagerModel object	3		20	1
1105	2017-10-27 04:06:14.921019+00	42	OrderManagerModel object	3		20	1
1106	2017-10-27 04:06:14.925109+00	41	OrderManagerModel object	3		20	1
1107	2017-10-27 04:06:14.932937+00	40	OrderManagerModel object	3		20	1
1108	2017-10-27 04:06:14.969399+00	39	OrderManagerModel object	3		20	1
1109	2017-10-27 04:06:14.980986+00	38	OrderManagerModel object	3		20	1
1110	2017-10-27 04:06:15.013912+00	37	OrderManagerModel object	3		20	1
1111	2017-10-27 04:06:15.027169+00	36	OrderManagerModel object	3		20	1
1112	2017-10-27 04:06:15.031008+00	35	OrderManagerModel object	3		20	1
1113	2017-10-27 04:06:15.034109+00	34	OrderManagerModel object	3		20	1
1114	2017-10-27 04:06:15.03708+00	33	OrderManagerModel object	3		20	1
1115	2017-10-27 04:06:15.04067+00	32	OrderManagerModel object	3		20	1
1116	2017-10-27 04:06:15.044753+00	31	OrderManagerModel object	3		20	1
1117	2017-10-27 04:06:15.048663+00	30	OrderManagerModel object	3		20	1
1118	2017-10-27 04:06:15.052822+00	29	OrderManagerModel object	3		20	1
1119	2017-10-27 04:06:15.056737+00	28	OrderManagerModel object	3		20	1
1120	2017-10-27 04:06:15.060999+00	27	OrderManagerModel object	3		20	1
1121	2017-10-27 04:06:15.064884+00	26	OrderManagerModel object	3		20	1
1122	2017-10-27 04:06:15.068801+00	25	OrderManagerModel object	3		20	1
336	2017-09-17 23:46:27.27+00	69	ddas231	3		8	2
337	2017-09-17 23:46:27.275+00	68	ddas231	3		8	2
338	2017-09-17 23:46:27.28+00	67	ddas	3		8	2
339	2017-09-17 23:46:27.286+00	66	вфывф	3		8	2
383	2017-10-09 01:25:35.588+00	102	Тухачевского, 26	2	[{"changed": {"fields": ["required"]}}]	11	2
384	2017-10-09 01:28:59.875+00	223	Рефрижираторный 20 фут Модель1	2	[{"changed": {"fields": ["property_json"]}}]	8	2
385	2017-10-09 01:29:36.221+00	217	Вентилируемый 40 фут  High Cube Модель2	2	[{"changed": {"fields": ["property_json"]}}]	8	2
386	2017-10-09 08:29:56.573+00	103	Город	2	[{"changed": {"fields": ["required"]}}]	14	2
387	2017-10-09 08:30:24.579+00	74	Аренда	2	[{"changed": {"fields": ["required"]}}]	10	2
388	2017-10-09 08:30:32.619+00	75	Продажа	2	[{"changed": {"fields": ["required"]}}]	10	2
389	2017-10-09 08:30:49.266+00	66	Стоимость аренды в год	2	[{"changed": {"fields": ["required"]}}]	13	2
390	2017-10-09 23:11:10.621+00	2	Со скидкой	1	[{"added": {}}]	65	2
391	2017-10-09 23:11:33.434+00	24	Город	1	[{"added": {}}]	7	2
392	2017-10-09 23:11:36.179+00	1	Контейнер	2	[]	9	2
393	2017-10-10 01:18:33.675+00	22	Акт технического состояния	2	[{"changed": {"fields": ["required"]}}]	12	2
394	2017-10-11 00:12:52.176+00	109	name1111	1	[{"added": {}}]	23	2
395	2017-10-11 00:13:29.43+00	110	name11111	1	[{"added": {}}]	23	2
396	2017-10-11 00:13:40.936+00	104	Владивосток	2	[]	14	2
397	2017-10-11 00:13:55.981+00	105	Аврорская, 1	3		6	2
398	2017-10-11 00:13:55.987+00	102	Тухачевского, 26	3		6	2
399	2017-10-11 00:14:07.336+00	108	Ленинская 1	3		11	2
400	2017-10-11 00:15:20.031+00	111	Ленинская 1	1	[{"added": {}}]	23	2
401	2017-10-11 00:15:51.941+00	110	Абребская 1	2	[{"changed": {"fields": ["name", "translate", "property"]}}]	23	2
704	2017-10-27 01:33:25.349876+00	70	OrderModel object	3		21	2
705	2017-10-27 01:33:25.406452+00	69	OrderModel object	3		21	2
706	2017-10-27 01:33:25.414357+00	68	OrderModel object	3		21	2
707	2017-10-27 01:33:25.419068+00	67	OrderModel object	3		21	2
708	2017-10-27 01:33:25.424448+00	66	OrderModel object	3		21	2
709	2017-10-27 01:33:25.428018+00	65	OrderModel object	3		21	2
710	2017-10-27 01:33:25.432089+00	64	OrderModel object	3		21	2
711	2017-10-27 01:33:25.435752+00	63	OrderModel object	3		21	2
712	2017-10-27 01:33:25.439575+00	62	OrderModel object	3		21	2
713	2017-10-27 01:33:25.442837+00	61	OrderModel object	3		21	2
714	2017-10-27 01:33:25.446798+00	60	OrderModel object	3		21	2
715	2017-10-27 01:33:25.45094+00	59	OrderModel object	3		21	2
716	2017-10-27 01:33:25.454884+00	58	OrderModel object	3		21	2
717	2017-10-27 01:33:25.458838+00	57	OrderModel object	3		21	2
718	2017-10-27 01:33:25.467001+00	56	OrderModel object	3		21	2
719	2017-10-27 01:33:25.477909+00	55	OrderModel object	3		21	2
720	2017-10-27 01:33:25.48217+00	54	OrderModel object	3		21	2
721	2017-10-27 01:33:25.486842+00	53	OrderModel object	3		21	2
722	2017-10-27 01:33:25.491534+00	52	OrderModel object	3		21	2
723	2017-10-27 01:33:25.495044+00	51	OrderModel object	3		21	2
724	2017-10-27 01:33:25.498789+00	50	OrderModel object	3		21	2
725	2017-10-27 01:33:25.502832+00	49	OrderModel object	3		21	2
726	2017-10-27 01:33:25.506904+00	48	OrderModel object	3		21	2
727	2017-10-27 01:33:25.510916+00	47	OrderModel object	3		21	2
728	2017-10-27 01:33:25.514984+00	46	OrderModel object	3		21	2
729	2017-10-27 01:33:25.51886+00	45	OrderModel object	3		21	2
730	2017-10-27 01:33:25.522851+00	44	OrderModel object	3		21	2
731	2017-10-27 01:33:25.527023+00	43	OrderModel object	3		21	2
732	2017-10-27 01:33:25.531007+00	42	OrderModel object	3		21	2
733	2017-10-27 01:33:25.535032+00	41	OrderModel object	3		21	2
734	2017-10-27 01:33:25.53922+00	40	OrderModel object	3		21	2
735	2017-10-27 01:33:25.543131+00	39	OrderModel object	3		21	2
736	2017-10-27 01:33:25.55368+00	38	OrderModel object	3		21	2
737	2017-10-27 01:33:25.557756+00	37	OrderModel object	3		21	2
738	2017-10-27 01:33:25.574572+00	36	OrderModel object	3		21	2
739	2017-10-27 01:33:25.581859+00	35	OrderModel object	3		21	2
740	2017-10-27 01:33:25.585698+00	34	OrderModel object	3		21	2
1123	2017-10-27 04:06:15.072741+00	24	OrderManagerModel object	3		20	1
1124	2017-10-27 04:06:15.076786+00	23	OrderManagerModel object	3		20	1
1125	2017-10-27 04:06:15.080698+00	22	OrderManagerModel object	3		20	1
1126	2017-10-27 04:06:15.09101+00	21	OrderManagerModel object	3		20	1
1127	2017-10-27 04:06:15.095135+00	20	OrderManagerModel object	3		20	1
741	2017-10-27 01:33:25.589617+00	33	OrderModel object	3		21	2
742	2017-10-27 01:33:25.593552+00	32	OrderModel object	3		21	2
743	2017-10-27 01:33:25.597436+00	31	OrderModel object	3		21	2
744	2017-10-27 01:33:25.602729+00	30	OrderModel object	3		21	2
745	2017-10-27 01:33:25.607102+00	29	OrderModel object	3		21	2
746	2017-10-27 01:33:25.611548+00	28	OrderModel object	3		21	2
747	2017-10-27 01:33:25.614956+00	27	OrderModel object	3		21	2
748	2017-10-27 01:33:25.618954+00	26	OrderModel object	3		21	2
749	2017-10-27 01:33:25.622936+00	25	OrderModel object	3		21	2
750	2017-10-27 01:33:25.62686+00	24	OrderModel object	3		21	2
751	2017-10-27 01:33:25.630815+00	23	OrderModel object	3		21	2
752	2017-10-27 01:33:25.634829+00	22	OrderModel object	3		21	2
753	2017-10-27 01:33:25.79773+00	21	OrderModel object	3		21	2
754	2017-10-27 01:33:25.908509+00	20	OrderModel object	3		21	2
755	2017-10-27 01:33:25.943972+00	19	OrderModel object	3		21	2
756	2017-10-27 01:33:25.955736+00	18	OrderModel object	3		21	2
757	2017-10-27 01:33:25.969023+00	17	OrderModel object	3		21	2
758	2017-10-27 01:33:25.97967+00	16	OrderModel object	3		21	2
759	2017-10-27 01:33:25.98789+00	15	OrderModel object	3		21	2
760	2017-10-27 01:33:25.99153+00	14	OrderModel object	3		21	2
761	2017-10-27 01:33:25.995292+00	13	OrderModel object	3		21	2
762	2017-10-27 01:33:25.999067+00	12	OrderModel object	3		21	2
763	2017-10-27 01:33:26.002848+00	11	OrderModel object	3		21	2
764	2017-10-27 01:33:26.006759+00	10	OrderModel object	3		21	2
765	2017-10-27 01:33:26.010899+00	9	OrderModel object	3		21	2
766	2017-10-27 01:33:26.014869+00	8	OrderModel object	3		21	2
767	2017-10-27 01:33:26.018815+00	7	OrderModel object	3		21	2
768	2017-10-27 01:33:26.022799+00	6	OrderModel object	3		21	2
769	2017-10-27 01:33:26.030657+00	5	OrderModel object	3		21	2
770	2017-10-27 01:33:26.034824+00	4	OrderModel object	3		21	2
771	2017-10-27 01:33:26.045745+00	3	OrderModel object	3		21	2
772	2017-10-27 01:33:26.051552+00	2	OrderModel object	3		21	2
773	2017-10-27 01:33:26.05498+00	1	OrderModel object	3		21	2
834	2017-10-27 01:33:54.580544+00	41	OrderManagerModel object	3		20	2
835	2017-10-27 01:33:54.590855+00	40	OrderManagerModel object	3		20	2
836	2017-10-27 01:33:54.59328+00	39	OrderManagerModel object	3		20	2
837	2017-10-27 01:33:54.596587+00	38	OrderManagerModel object	3		20	2
838	2017-10-27 01:33:54.600374+00	37	OrderManagerModel object	3		20	2
839	2017-10-27 01:33:54.604477+00	36	OrderManagerModel object	3		20	2
840	2017-10-27 01:33:54.608643+00	35	OrderManagerModel object	3		20	2
841	2017-10-27 01:33:54.612435+00	34	OrderManagerModel object	3		20	2
842	2017-10-27 01:33:54.616441+00	33	OrderManagerModel object	3		20	2
843	2017-10-27 01:33:54.620396+00	32	OrderManagerModel object	3		20	2
844	2017-10-27 01:33:54.624487+00	31	OrderManagerModel object	3		20	2
845	2017-10-27 01:33:54.628723+00	30	OrderManagerModel object	3		20	2
846	2017-10-27 01:33:54.632376+00	29	OrderManagerModel object	3		20	2
847	2017-10-27 01:33:54.636492+00	28	OrderManagerModel object	3		20	2
848	2017-10-27 01:33:54.64035+00	27	OrderManagerModel object	3		20	2
849	2017-10-27 01:33:54.644392+00	26	OrderManagerModel object	3		20	2
850	2017-10-27 01:33:54.648422+00	25	OrderManagerModel object	3		20	2
851	2017-10-27 01:33:54.652278+00	24	OrderManagerModel object	3		20	2
852	2017-10-27 01:33:54.664376+00	23	OrderManagerModel object	3		20	2
853	2017-10-27 01:33:54.67473+00	22	OrderManagerModel object	3		20	2
854	2017-10-27 01:33:54.677544+00	21	OrderManagerModel object	3		20	2
855	2017-10-27 01:33:54.680404+00	20	OrderManagerModel object	3		20	2
856	2017-10-27 01:33:54.684294+00	19	OrderManagerModel object	3		20	2
857	2017-10-27 01:33:54.68841+00	18	OrderManagerModel object	3		20	2
858	2017-10-27 01:33:54.69231+00	17	OrderManagerModel object	3		20	2
859	2017-10-27 01:33:54.696401+00	16	OrderManagerModel object	3		20	2
860	2017-10-27 01:33:54.700362+00	15	OrderManagerModel object	3		20	2
861	2017-10-27 01:33:54.704246+00	14	OrderManagerModel object	3		20	2
862	2017-10-27 01:33:54.708368+00	13	OrderManagerModel object	3		20	2
863	2017-10-27 01:33:54.712228+00	12	OrderManagerModel object	3		20	2
864	2017-10-27 01:33:54.716314+00	11	OrderManagerModel object	3		20	2
865	2017-10-27 01:33:54.720375+00	10	OrderManagerModel object	3		20	2
866	2017-10-27 01:33:54.72417+00	9	OrderManagerModel object	3		20	2
867	2017-10-27 01:33:54.728283+00	8	OrderManagerModel object	3		20	2
868	2017-10-27 01:33:54.732177+00	7	OrderManagerModel object	3		20	2
869	2017-10-27 01:33:54.736465+00	6	OrderManagerModel object	3		20	2
870	2017-10-27 01:33:54.740335+00	5	OrderManagerModel object	3		20	2
871	2017-10-27 01:33:54.754398+00	4	OrderManagerModel object	3		20	2
872	2017-10-27 01:33:54.775291+00	3	OrderManagerModel object	3		20	2
873	2017-10-27 01:33:54.79599+00	2	OrderManagerModel object	3		20	2
424	2017-10-18 03:52:43.937+00	1	EmailConfig object	2	[{"changed": {"fields": ["company_email"]}}]	19	1
1200	2017-11-07 10:05:35.589097+00	30	my_test_email@bomba.ru	1	[{"added": {}}]	15	1
1128	2017-10-27 04:06:15.099501+00	19	OrderManagerModel object	3		20	1
1129	2017-10-27 04:06:15.106817+00	18	OrderManagerModel object	3		20	1
1130	2017-10-27 04:06:15.110314+00	17	OrderManagerModel object	3		20	1
1131	2017-10-27 04:06:15.113319+00	16	OrderManagerModel object	3		20	1
1132	2017-10-27 04:06:15.116987+00	15	OrderManagerModel object	3		20	1
1133	2017-10-27 04:06:15.125314+00	14	OrderManagerModel object	3		20	1
1134	2017-10-27 04:06:15.197591+00	13	OrderManagerModel object	3		20	1
1135	2017-10-27 04:06:15.209815+00	12	OrderManagerModel object	3		20	1
1136	2017-10-27 04:06:15.216577+00	11	OrderManagerModel object	3		20	1
1137	2017-10-27 04:06:15.220549+00	10	OrderManagerModel object	3		20	1
1138	2017-10-27 04:06:15.224555+00	9	OrderManagerModel object	3		20	1
1139	2017-10-27 04:06:15.228636+00	8	OrderManagerModel object	3		20	1
1140	2017-10-27 04:06:15.232547+00	7	OrderManagerModel object	3		20	1
1141	2017-10-27 04:06:15.23659+00	6	OrderManagerModel object	3		20	1
1142	2017-10-27 04:06:15.240644+00	5	OrderManagerModel object	3		20	1
1143	2017-10-27 04:06:15.244541+00	4	OrderManagerModel object	3		20	1
1144	2017-10-27 04:06:15.248483+00	3	OrderManagerModel object	3		20	1
1145	2017-10-27 04:06:15.258747+00	2	OrderManagerModel object	3		20	1
1146	2017-10-27 04:06:15.261824+00	1	OrderManagerModel object	3		20	1
1266	2017-11-12 04:30:17.911124+00	89	Сухогруз	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1282	2017-11-12 05:06:47.443767+00	38	40 фут	2	[{"changed": {"fields": ["order_number"]}}]	11	2
57	2017-09-01 03:27:09.82+00	29	Тип контейнера	2	[]	23	1
58	2017-09-01 03:27:38.91+00	29	Тип контейнера	2	[{"changed": {"fields": ["property"]}}]	23	1
59	2017-09-01 03:28:38.568+00	29	Тип контейнера	2	[{"changed": {"fields": ["property"]}}]	23	1
60	2017-09-01 03:28:51.824+00	29	Тип контейнера	2	[{"changed": {"fields": ["property"]}}]	23	1
61	2017-09-01 03:33:52.836+00	29	Тип контейнера	2	[]	23	1
62	2017-09-01 03:34:08.197+00	29	Тип контейнера	2	[]	23	1
63	2017-09-01 03:35:03.628+00	29	Тип контейнера	2	[]	23	1
64	2017-09-01 03:35:15.451+00	29	Тип контейнера	2	[]	23	1
65	2017-09-01 03:35:24.94+00	29	Тип контейнера	2	[]	23	1
66	2017-09-01 03:37:57.872+00	29	Тип контейнера	2	[]	23	1
67	2017-09-01 07:11:52.476+00	1	Контейнер	2	[]	9	1
68	2017-09-01 14:03:42.464+00	30	Имя типа	1	[{"added": {}}]	11	1
70	2017-09-01 14:05:50.44+00	9	Тип	2	[]	14	1
874	2017-10-27 01:34:00.719013+00	1	OrderManagerModel object	3		20	2
1147	2017-10-30 00:19:46.704592+00	14	sotnikov@rudoit.ru	2	[{"changed": {"fields": ["is_staff", "is_superuser"]}}]	15	1
1148	2017-10-30 01:59:18.3481+00	1	Company object	2	[{"changed": {"fields": ["payment_interval", "critical_time"]}}]	17	1
1180	2017-10-30 12:35:27.263137+00	1	Manager	2	[]	3	1
1184	2017-11-07 02:12:53.21904+00	113	Ковальчука, 5	1	[{"added": {}}, {"added": {"name": "\\u0421\\u0432\\u044f\\u0437\\u044c selectfieldproduct-fieldproduct", "object": "SelectFieldProduct_content object"}}]	100	1
1185	2017-11-07 05:17:18.156125+00	2	Заполнен договор	2	[{"changed": {"fields": ["template"]}}]	18	14
1201	2017-11-07 10:05:46.993114+00	121	без пробега по РФ	1	[{"added": {}}]	61	14
1209	2017-11-07 12:35:40.290086+00	104	Владивосток	2	[]	14	1
1215	2017-11-08 13:58:52.48882+00	28	torely@yandex.ru	3		15	1
1248	2017-11-08 14:03:48.887366+00	31		3		15	1
1250	2017-11-08 14:09:27.583441+00	70	1234	1	[{"added": {}}]	15	1
1251	2017-11-10 10:38:07.358406+00	88	Open Top	2	[{"changed": {"fields": ["name"]}}]	60	2
1267	2017-11-12 04:30:25.505374+00	88	Open Top	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1313	2017-11-12 05:48:55.759217+00	71	321	1	[{"added": {}}]	15	14
1339	2017-11-13 06:04:14.341278+00	128	Артём	2	[{"changed": {"fields": ["name"]}}]	99	1
1361	2017-11-14 04:41:35.118395+00	129	40 футов High Cube XXL 3.2	2	[]	59	14
1379	2017-11-15 06:23:39.836832+00	None	da@dan.ru	1	[{"added": {}}]	15	2
1542	2017-11-16 01:39:53.847393+00	243	OrderModel object	3		21	78
1543	2017-11-16 01:39:53.858774+00	242	OrderModel object	3		21	78
1544	2017-11-16 01:39:53.86166+00	241	OrderModel object	3		21	78
1545	2017-11-16 01:39:53.865253+00	240	OrderModel object	3		21	78
1546	2017-11-16 01:39:53.936694+00	239	OrderModel object	3		21	78
1547	2017-11-16 01:39:54.01284+00	238	OrderModel object	3		21	78
1548	2017-11-16 01:39:54.028387+00	237	OrderModel object	3		21	78
1549	2017-11-16 01:39:54.032028+00	236	OrderModel object	3		21	78
1550	2017-11-16 01:39:54.061037+00	235	OrderModel object	3		21	78
1551	2017-11-16 01:39:54.076228+00	234	OrderModel object	3		21	78
1552	2017-11-16 01:39:54.082609+00	233	OrderModel object	3		21	78
1553	2017-11-16 01:39:54.093141+00	232	OrderModel object	3		21	78
1554	2017-11-16 01:39:54.111097+00	231	OrderModel object	3		21	78
1555	2017-11-16 01:39:54.115833+00	230	OrderModel object	3		21	78
1556	2017-11-16 01:39:54.127412+00	229	OrderModel object	3		21	78
1628	2017-11-16 04:56:12.823467+00	138	Thermo King	1	[{"added": {}}]	63	18
1655	2018-01-23 06:47:34.354128+00	84	shonova@daria.ru	1	[{"added": {}}]	15	2
1672	2018-10-18 05:43:03.639106+00	80	ice@rftu.ru	2	[]	15	18
1149	2017-10-30 02:02:34.687592+00	239	Вентилируемый 20 фут Модель1	2	[{"changed": {"fields": ["property_json"]}}]	8	1
1186	2017-11-07 08:38:43.957548+00	1	Заявка на контейнер(ы) получена. Менеджер позвонит	2	[{"changed": {"fields": ["subject_name", "template"]}}]	18	14
1202	2017-11-07 10:06:11.105422+00	121	без пробега по РФ	3		61	14
1210	2017-11-07 12:35:44.467365+00	125	Артем	1	[{"added": {}}]	99	1
1212	2017-11-08 07:58:15.319611+00	22	avk@rftu.ru	2	[{"changed": {"fields": ["company", "phone"]}}]	15	14
1249	2017-11-08 14:04:01.508945+00	68		1	[{"added": {}}]	15	1
1252	2017-11-10 10:38:28.301706+00	87	Flatrack	2	[{"changed": {"fields": ["name"]}}]	60	2
1268	2017-11-12 04:30:30.776532+00	86	Танк	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1314	2017-11-12 05:54:03.222785+00	71	321	3		15	14
1340	2017-11-13 06:04:40.765033+00	127	Находка	2	[{"changed": {"fields": ["name"]}}]	99	1
1362	2017-11-14 04:41:45.918716+00	56	45 фут	2	[]	11	14
1380	2017-11-15 06:25:41.448569+00	77	da@de.ru	1	[{"added": {}}]	15	2
1557	2017-11-16 01:40:05.700279+00	178	Владимир-Заказ#178	3		20	78
1558	2017-11-16 01:40:05.713801+00	177	-Заказ#177	3		20	78
1559	2017-11-16 01:40:05.717315+00	176	-Заказ#176	3		20	78
1560	2017-11-16 01:40:05.72596+00	175	-Заказ#175	3		20	78
1561	2017-11-16 01:40:05.729395+00	174	Иваныч-Заказ#174	3		20	78
1562	2017-11-16 01:40:05.732307+00	173	Иваныч-Заказ#173	3		20	78
1563	2017-11-16 01:40:05.735207+00	172	Иваныч-Заказ#172	3		20	78
1564	2017-11-16 01:40:05.739222+00	171	Иваныч-Заказ#171	3		20	78
1565	2017-11-16 01:40:05.743417+00	170	Иваныч-Заказ#170	3		20	78
1566	2017-11-16 01:40:05.747406+00	169	Иваныч-Заказ#169	3		20	78
1567	2017-11-16 01:40:05.7513+00	168	Иваныч-Заказ#168	3		20	78
1568	2017-11-16 01:40:05.760081+00	167	Иваныч-Заказ#167	3		20	78
1569	2017-11-16 01:40:05.763105+00	166	Иваныч-Заказ#166	3		20	78
1570	2017-11-16 01:40:05.767132+00	165	Иваныч-Заказ#165	3		20	78
1571	2017-11-16 01:40:05.771122+00	164	Иваныч-Заказ#164	3		20	78
1572	2017-11-16 01:40:05.77514+00	163	Иваныч-Заказ#163	3		20	78
1573	2017-11-16 01:40:05.779753+00	162	Иваныч-Заказ#162	3		20	78
1574	2017-11-16 01:40:05.788909+00	161	Иваныч-Заказ#161	3		20	78
1575	2017-11-16 01:40:05.79794+00	160	Иваныч-Заказ#160	3		20	78
1576	2017-11-16 01:40:05.801406+00	159	Иваныч-Заказ#159	3		20	78
1577	2017-11-16 01:40:05.804481+00	158	Иваныч-Заказ#158	3		20	78
1578	2017-11-16 01:40:05.807378+00	157	Иваныч-Заказ#157	3		20	78
1579	2017-11-16 01:40:05.81144+00	156	Иваныч-Заказ#156	3		20	78
1580	2017-11-16 01:40:05.815411+00	155	Иваныч-Заказ#155	3		20	78
1581	2017-11-16 01:40:05.820454+00	154	Иваныч-Заказ#154	3		20	78
1582	2017-11-16 01:40:05.823463+00	153	Иваныч-Заказ#153	3		20	78
1583	2017-11-16 01:40:05.827291+00	152	Иваныч-Заказ#152	3		20	78
1584	2017-11-16 01:40:05.836451+00	151	Иваныч-Заказ#151	3		20	78
1585	2017-11-16 01:40:05.841008+00	150	Иваныч-Заказ#150	3		20	78
1586	2017-11-16 01:40:05.845069+00	149	Иваныч-Заказ#149	3		20	78
1587	2017-11-16 01:40:05.848849+00	148	Иваныч-Заказ#148	3		20	78
1588	2017-11-16 01:40:05.852118+00	147	Иваныч-Заказ#147	3		20	78
1589	2017-11-16 01:40:05.85515+00	146	Шонова-Заказ#146	3		20	78
1590	2017-11-16 01:40:05.859107+00	145	Иваныч-Заказ#145	3		20	78
1591	2017-11-16 01:40:05.868599+00	144	Иваныч-Заказ#144	3		20	78
1592	2017-11-16 01:40:05.872096+00	143	Шонова-Заказ#143	3		20	78
1593	2017-11-16 01:40:05.881368+00	142	Иваныч-Заказ#142	3		20	78
1594	2017-11-16 01:40:05.884359+00	141	Иваныч-Заказ#141	3		20	78
1595	2017-11-16 01:40:05.899828+00	140	Иваныч-Заказ#140	3		20	78
1596	2017-11-16 01:40:05.911356+00	139	Иваныч-Заказ#139	3		20	78
1597	2017-11-16 01:40:05.918138+00	138	Александра-Заказ#138	3		20	78
1598	2017-11-16 01:40:05.921881+00	137	Иваныч-Заказ#137	3		20	78
1599	2017-11-16 01:40:05.925958+00	136	Александра-Заказ#136	3		20	78
1600	2017-11-16 01:40:05.929876+00	135	Иваныч-Заказ#135	3		20	78
1601	2017-11-16 01:40:05.933845+00	134	Александра-Заказ#134	3		20	78
1602	2017-11-16 01:40:05.937899+00	133	Иваныч-Заказ#133	3		20	78
1603	2017-11-16 01:40:05.941935+00	132	Александра-Заказ#132	3		20	78
1604	2017-11-16 01:40:05.945933+00	131	Александра-Заказ#131	3		20	78
1605	2017-11-16 01:40:05.949897+00	130	Александра-Заказ#130	3		20	78
1606	2017-11-16 01:40:05.953793+00	129	Александра-Заказ#129	3		20	78
1607	2017-11-16 01:40:05.960022+00	128	Иваныч-Заказ#128	3		20	78
1608	2017-11-16 01:40:05.96284+00	127	Александра-Заказ#127	3		20	78
1609	2017-11-16 01:40:05.967004+00	126	Александра-Заказ#126	3		20	78
1610	2017-11-16 01:40:05.977499+00	125	Александра-Заказ#125	3		20	78
1611	2017-11-16 01:40:05.990617+00	124	Александра-Заказ#124	3		20	78
1612	2017-11-16 01:40:05.994897+00	123	Иваныч-Заказ#123	3		20	78
1613	2017-11-16 01:40:06.002878+00	111	Данил-Заказ#111	3		20	78
1614	2017-11-16 01:40:06.00653+00	110	Иваныч-Заказ#110	3		20	78
1615	2017-11-16 01:40:06.01125+00	109	Александра-Заказ#109	3		20	78
1616	2017-11-16 01:40:06.014877+00	108	Иваныч-Заказ#108	3		20	78
1617	2017-11-16 01:40:06.018755+00	107	Данил-Заказ#107	3		20	78
1629	2017-11-17 01:03:08.045865+00	139	Рефрижераторная установка	1	[{"added": {}}]	60	18
1656	2018-01-25 10:44:45.399925+00	23	ap@rftu.ru	2	[]	15	18
1673	2018-10-18 05:57:27.407335+00	23	ap@rftu.ru	2	[{"changed": {"fields": ["last_name"]}}]	15	18
1187	2017-11-07 08:39:11.367621+00	85	bonus-Иваныч	2	[{"changed": {"fields": ["activate_sum", "discount"]}}]	16	14
1203	2017-11-07 10:06:42.343066+00	122	Резервный фонд	1	[{"added": {}}]	62	14
1213	2017-11-08 08:07:40.118486+00	31		1	[{"added": {}}]	15	14
1253	2017-11-10 10:38:40.850684+00	84	Рефрижераторный	2	[{"changed": {"fields": ["name"]}}]	60	2
1269	2017-11-12 04:30:39.376218+00	87	Flatrack	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1315	2017-11-12 05:55:22.360492+00	72	2222	1	[{"added": {}}]	15	14
1341	2017-11-13 06:04:59.571738+00	127	Находка1	2	[{"changed": {"fields": ["name"]}}]	99	1
1363	2017-11-14 04:41:55.077483+00	40	45 фут  High Cube 2.9	2	[]	11	14
1381	2017-11-15 06:26:42.405469+00	78	ds@de.ru	1	[{"added": {}}]	15	2
1618	2017-11-16 03:09:12.243266+00	106	Уссурийск	3		14	14
1630	2017-11-17 01:03:43.387507+00	140	Термос-контейнер	1	[{"added": {}}]	60	18
1632	2017-11-17 02:52:35.62902+00	3	Вопрос с сайта	1	[{"added": {}}]	18	1
1633	2017-11-23 01:32:00.660682+00	141	Без установки	1	[{"added": {}}]	63	14
1637	2017-11-29 09:18:25.502895+00	80	Lugovaya	1	[{"added": {}}]	15	18
1645	2018-01-11 09:18:10.895479+00	142	Москва	1	[{"added": {}}]	99	82
1657	2018-01-29 08:27:11.753851+00	1	EmailConfig object	2	[{"changed": {"fields": ["company_email", "email_domain", "smtp_password"]}}]	19	2
1663	2018-10-18 01:27:01.627291+00	87	cso@rftu.ru	1	[{"added": {}}]	15	18
1674	2018-10-18 05:58:44.758421+00	81	vertex@rftu.ru	2	[{"changed": {"fields": ["is_staff", "is_active"]}}]	15	18
1675	2018-10-24 02:11:57.712821+00	89	refagent@rftu.ru	1	[{"added": {}}]	15	18
69	2017-09-01 14:03:47.08+00	29	Тип контейнера	2	[]	23	1
71	2017-09-01 15:34:36.753+00	29	Тип контейнера	2	[]	23	1
72	2017-09-01 16:04:36.117+00	29	Тип контейнера	2	[]	23	1
73	2017-09-01 16:05:23.392+00	25	char_test1	2	[{"changed": {"fields": ["content"]}}]	11	1
74	2017-09-01 16:05:33.864+00	26	char_test2	2	[{"changed": {"fields": ["content"]}}]	11	1
75	2017-09-01 16:18:44.936+00	26	char_test2	2	[]	11	1
76	2017-09-01 16:19:41.324+00	29	Тип контейнера	2	[]	23	1
77	2017-09-01 16:19:49.564+00	29	Тип контейнера	2	[]	23	1
78	2017-09-01 16:22:19.38+00	29	Тип контейнера	2	[]	23	1
79	2017-09-01 16:22:29.291+00	29	Тип контейнера	2	[]	23	1
80	2017-09-04 06:20:04.74+00	9	Тип	2	[]	14	1
81	2017-09-04 06:20:25.218+00	1	Контейнер	2	[]	9	1
82	2017-09-04 06:21:25.076+00	27	select_test1	2	[]	14	1
83	2017-09-05 03:30:57.016+00	1	Контейнер photo #1	1	[{"added": {}}]	25	1
84	2017-09-05 03:58:10.088+00	9	Тип	2	[]	14	1
85	2017-09-06 03:26:27.696+00	12	Модель	3		11	2
86	2017-09-06 03:26:41.889+00	31	Модель	1	[{"added": {}}]	14	2
87	2017-09-06 03:27:34.106+00	9	Тип	3		6	2
88	2017-09-06 03:27:49.117+00	1	Состояние	3		6	2
89	2017-09-06 03:28:04.064+00	32	Состояние	1	[{"added": {}}]	14	2
90	2017-09-06 03:29:43.598+00	33	Статус	1	[{"added": {}}]	14	2
91	2017-09-06 11:46:26.246+00	27	select_test1	3		6	2
92	2017-09-06 11:46:26.249+00	26	char_test2	3		6	2
93	2017-09-06 11:46:26.254+00	25	char_test1	3		6	2
94	2017-09-06 11:46:26.258+00	24	Контейнер необычный (test)	3		6	2
95	2017-09-06 11:46:57.235+00	30	Имя типа	3		6	2
96	2017-09-06 11:46:57.241+00	23	Контейнер обычный (test)	3		6	2
97	2017-09-06 11:47:26.982+00	29	Тип контейнера	3		6	2
98	2017-09-06 11:59:44.892+00	31	Модель	3		6	2
1188	2017-11-07 09:07:02.48802+00	85	bonus-Иваныч	2	[{"changed": {"fields": ["description"]}}]	16	14
1204	2017-11-07 10:10:40.033915+00	123	тестовый тип	1	[{"added": {}}]	60	14
1214	2017-11-08 08:11:18.990626+00	128	Артём	1	[{"added": {}}]	99	14
1254	2017-11-10 10:40:02.686273+00	39	40 фут  High Cube 2.9	2	[{"changed": {"fields": ["name"]}}]	11	2
1270	2017-11-12 04:30:46.901016+00	83	Вентилируемый	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1316	2017-11-12 06:20:48.508946+00	73	danil@gmail.com	1	[{"added": {}}]	15	2
1342	2017-11-13 06:06:08.391234+00	127	Находка1	3		99	1
1364	2017-11-14 04:42:34.862664+00	57	10 фут	2	[{"changed": {"fields": ["order_number"]}}]	11	14
1382	2017-11-15 06:27:08.562536+00	78	ds@de.ru	2	[{"changed": {"fields": ["company", "last_name", "contact_email", "phone", "is_staff"]}}]	15	2
1619	2017-11-16 03:20:02.461859+00	110	Абребская 1	3		23	2
1620	2017-11-16 03:20:02.52119+00	111	Ленинская 1	3		23	2
1631	2017-11-17 01:16:51.388766+00	140	Термос-контейнер	2	[{"changed": {"fields": ["short_name"]}}]	60	18
1634	2017-11-23 01:35:31.50991+00	141	-	2	[{"changed": {"fields": ["name"]}}]	63	14
1638	2017-11-29 09:30:45.026467+00	3	ООО Рефтерминал-Азия	1	[{"added": {}}]	17	18
1646	2018-01-11 09:18:59.908331+00	143	ул. Московская, 1	1	[{"added": {}}, {"added": {"name": "\\u0421\\u0432\\u044f\\u0437\\u044c selectfieldproduct-fieldproduct", "object": "SelectFieldProduct_content object"}}]	100	82
1658	2018-04-10 04:22:48.161531+00	85	ref-007@rftu.ru	1	[{"added": {}}]	15	18
1664	2018-10-18 01:29:24.775883+00	87	cso@rftu.ru	2	[{"changed": {"fields": ["company", "last_name", "contact_email", "phone", "is_staff"]}}]	15	18
1189	2017-11-07 09:08:21.933942+00	86	Бонус от админа - тест-Иваныч	1	[{"added": {}}]	16	14
1205	2017-11-07 10:12:42.608476+00	123	тестовый тип	3		60	14
1255	2017-11-10 10:40:02.947748+00	39	40 фут  High Cube 2.9	2	[]	11	2
1271	2017-11-12 04:31:13.230656+00	85	Насыпной	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1317	2017-11-12 07:39:39.425827+00	68		3		15	2
1318	2017-11-12 07:39:39.4402+00	70	1234	3		15	2
1319	2017-11-12 07:39:39.4458+00	72	2222	3		15	2
1343	2017-11-13 06:06:39.004869+00	133	Находка	1	[{"added": {}}]	99	1
1365	2017-11-14 04:43:42.947703+00	87	Flat Rack	2	[{"changed": {"fields": ["name"]}}]	60	14
1383	2017-11-15 07:19:07.81767+00	92	Carrier 69NT40	2	[{"changed": {"fields": ["name"]}}]	63	18
1621	2017-11-16 03:20:20.830547+00	128	Артём	3		99	14
1635	2017-11-23 02:58:38.734754+00	79	Daria@gmail.com	1	[{"added": {}}]	15	2
1639	2017-11-29 09:46:33.053802+00	80	ace@rftu.ru	2	[{"changed": {"fields": ["company", "last_name", "email", "contact_email", "phone", "is_staff"]}}]	15	18
1647	2018-01-12 00:52:36.439059+00	1	Manager	2	[]	3	82
1659	2018-04-10 04:23:43.482717+00	85	ref-007@rftu.ru	2	[{"changed": {"fields": ["company", "last_name", "contact_email", "phone"]}}]	15	18
1665	2018-10-18 01:29:30.379142+00	87	cso@rftu.ru	2	[]	15	18
1676	2018-10-24 02:13:08.132756+00	89	refagent@rftu.ru	2	[{"changed": {"fields": ["company", "last_name", "contact_email", "phone"]}}]	15	18
1190	2017-11-07 09:09:15.37926+00	2	Компания тест 2	1	[{"added": {}}]	17	14
1206	2017-11-07 10:14:26.817834+00	124	Днепровска, 25	1	[{"added": {}}]	100	14
1256	2017-11-10 10:40:31.750858+00	129	40 футов High Cube XXL 3.2	1	[{"added": {}}]	59	2
1272	2017-11-12 04:31:40.453593+00	85	Насыпной	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1320	2017-11-12 07:45:29.35935+00	23	ap@rftu.ru	2	[{"changed": {"fields": ["company"]}}]	15	2
1344	2017-11-13 06:06:50.141333+00	133	Находка1	2	[{"changed": {"fields": ["name"]}}]	99	1
1366	2017-11-14 06:33:32.337844+00	134	Общая цена	1	[{"added": {}}]	13	2
1384	2017-11-16 01:09:59.838188+00	119	Модель test	3		63	18
1622	2017-11-16 03:20:26.26692+00	133	Находка	3		99	14
1636	2017-11-23 02:58:56.266045+00	79	Daria@gmail.com	2	[{"changed": {"fields": ["company", "last_name", "contact_email", "phone", "is_staff"]}}]	15	2
1640	2017-11-29 09:47:12.463056+00	80	ace@rftu.ru	2	[]	15	18
1648	2018-01-15 05:56:31.077974+00	144	Симферопольское шоссе 22, строение 1	1	[{"added": {}}]	100	18
1660	2018-04-13 02:39:20.31379+00	86	ermine-dv	1	[{"added": {}}]	15	18
1666	2018-10-18 01:29:56.30859+00	87	cso@rftu.ru	2	[{"changed": {"fields": ["last_name"]}}]	15	18
1191	2017-11-07 09:09:36.690642+00	2	Компания тест 12	2	[{"changed": {"fields": ["name"]}}]	17	14
1207	2017-11-07 10:15:07.213766+00	124	Днепровска, 25	2	[{"added": {"name": "\\u0421\\u0432\\u044f\\u0437\\u044c selectfieldproduct-fieldproduct", "object": "SelectFieldProduct_content object"}}]	100	14
1257	2017-11-12 04:25:06.891721+00	35	20 фут	2	[{"changed": {"fields": ["order_number"]}}]	11	2
1273	2017-11-12 04:32:14.931599+00	87	Flatrack	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1321	2017-11-12 07:46:10.964959+00	11	b@gmail.com	3		15	2
1322	2017-11-12 07:46:10.979693+00	10	dan@dan.ru	3		15	2
1323	2017-11-12 07:46:10.983635+00	16	danil@danil.ru	3		15	2
1324	2017-11-12 07:46:11.004988+00	73	danil@gmail.com	3		15	2
1325	2017-11-12 07:46:11.019834+00	13	d@d.ru	3		15	2
1326	2017-11-12 07:46:11.026908+00	8	def@def.ru	3		15	2
1327	2017-11-12 07:46:11.046573+00	12	def@d.ru	3		15	2
1328	2017-11-12 07:46:11.06135+00	15	den@den.ru	3		15	2
1329	2017-11-12 07:46:11.065897+00	4	d@gmail.com	3		15	2
1330	2017-11-12 07:46:11.069739+00	6	s@gmail.com	3		15	2
1345	2017-11-13 06:17:42.741268+00	133	Находка	2	[{"changed": {"fields": ["name"]}}]	99	1
1353	2017-11-14 04:32:29.815323+00	87	Flatrack	2	[{"changed": {"fields": ["order_number"]}}]	60	14
1367	2017-11-14 06:33:43.2513+00	134	Общая цена	2	[{"changed": {"fields": ["required"]}}]	13	2
1370	2017-11-15 00:27:38.91346+00	217	Вентилируемый 40 фут  High Cube Модель2	3		8	2
1371	2017-11-15 06:01:09.898335+00	18	admin@refterminal.ru	2	[{"changed": {"fields": ["password"]}}]	15	14
1385	2017-11-16 01:10:09.492228+00	90	Модель1	3		11	18
1623	2017-11-16 03:20:56.007131+00	1	ООО Рефтерминал	2	[{"changed": {"fields": ["name"]}}]	17	14
1641	2017-11-29 09:47:57.522053+00	81	vertex	1	[{"added": {}}]	15	18
1649	2018-01-22 04:29:49.291431+00	21	wildpixel.dv@gmail.com	2	[]	15	18
1661	2018-04-13 02:39:51.918451+00	4	ИП Дударчук Р.В.	1	[{"added": {}}]	17	18
1667	2018-10-18 01:33:27.732245+00	88	container@rftu.ru	1	[{"added": {}}]	15	18
99	2017-09-06 11:59:55.648+00	34	Модель	1	[{"added": {}}]	11	2
100	2017-09-06 13:07:23.139+00	35	20 фут	1	[{"added": {}}]	11	2
101	2017-09-06 13:07:42.568+00	36	40 фут RF	1	[{"added": {}}]	11	2
102	2017-09-06 13:08:16.852+00	37	40 фут RF High Cube	1	[{"added": {}}]	11	2
103	2017-09-06 13:08:33.478+00	38	40 фут	1	[{"added": {}}]	11	2
104	2017-09-06 13:08:52.701+00	39	40 фут  High Cube	1	[{"added": {}}]	11	2
105	2017-09-06 13:09:11.354+00	40	45 фут  High Cube	1	[{"added": {}}]	11	2
106	2017-09-06 13:09:32.174+00	41	20 фут OT	1	[{"added": {}}]	11	2
107	2017-09-06 13:09:51.633+00	42	40 фут OT	1	[{"added": {}}]	11	2
108	2017-09-06 13:10:12.328+00	43	20 фут FT	1	[{"added": {}}]	11	2
109	2017-09-06 13:10:23.687+00	44	40 фут FT	1	[{"added": {}}]	11	2
110	2017-09-06 13:10:40.347+00	45	20 фут TC	1	[{"added": {}}]	11	2
111	2017-09-06 13:10:52.266+00	46	40 фут ТС	1	[{"added": {}}]	11	2
112	2017-09-06 13:11:11.99+00	47	Размеры	1	[{"added": {}}]	14	2
113	2017-09-06 13:11:28.878+00	10	Размер	3		6	2
114	2017-09-06 13:11:36.59+00	47	Размер	2	[{"changed": {"fields": ["name", "translate"]}}]	14	2
115	2017-09-06 13:12:30.782+00	48	Рефрежераторный	1	[{"added": {}}]	11	2
116	2017-09-06 13:12:41.279+00	49	Сухогрузный	1	[{"added": {}}]	11	2
117	2017-09-06 13:13:57.286+00	50	Открытый	1	[{"added": {}}]	11	2
118	2017-09-06 13:14:08.261+00	51	Платформа	1	[{"added": {}}]	11	2
119	2017-09-06 13:14:18.373+00	52	Танк	1	[{"added": {}}]	11	2
1176	2017-10-30 12:14:32.958569+00	26	andrey@testmail.ru	1	[{"added": {}}]	15	1
1192	2017-11-07 09:11:48.269142+00	27	test@test-mail.ru	2	[{"changed": {"fields": ["company", "last_name", "email"]}}]	15	14
1208	2017-11-07 10:16:48.803698+00	124	Днепровская, 25	2	[{"changed": {"fields": ["name"]}}]	100	14
1258	2017-11-12 04:25:31.014731+00	130	20 футов High Cube 2.9	1	[{"added": {}}]	59	2
1274	2017-11-12 04:32:38.006081+00	85	Насыпной	3		60	2
1331	2017-11-12 07:48:31.568782+00	7	a@gmail.com	3		15	2
1346	2017-11-13 06:18:26.785051+00	74	default	1	[{"added": {}}]	15	1
1354	2017-11-14 04:32:39.523078+00	83	Вентилируемый	2	[]	60	14
1368	2017-11-14 06:34:52.423401+00	1	По цене	2	[{"changed": {"fields": ["field"]}}]	65	2
1372	2017-11-15 06:05:00.506125+00	18	admin@refterminal.ru	2	[{"changed": {"fields": ["is_staff"]}}]	15	14
1386	2017-11-16 01:12:40.764364+00	258	Рефрижераторный 20 фут Carrier 69NT40	2	[{"changed": {"fields": ["name", "property_json"]}}]	8	78
1624	2017-11-16 03:21:11.30058+00	109	Тухачевского, 26	3		23	2
1642	2017-11-29 09:49:21.590197+00	81	vertex@rftu.ru	2	[{"changed": {"fields": ["company", "last_name", "email", "contact_email", "phone", "is_staff"]}}]	15	18
1650	2018-01-22 23:48:10.980802+00	83	testManager	1	[{"added": {}}]	15	14
1662	2018-04-13 02:41:05.574308+00	86	ermine-dv@mail.ru	2	[{"changed": {"fields": ["company", "last_name", "email", "contact_email", "phone"]}}]	15	18
1668	2018-10-18 01:49:40.619405+00	88	container@rftu.ru	2	[{"changed": {"fields": ["company", "last_name", "contact_email", "phone"]}}]	15	18
168	2017-09-06 14:01:25.571+00	45	TestContaine3r	3		8	2
1177	2017-10-30 12:15:26.663398+00	26	andrey@testmail.ru	2	[{"changed": {"fields": ["password"]}}]	15	1
1193	2017-11-07 09:17:12.879869+00	28	torely@yandex.ru	1	[{"added": {}}]	15	14
1259	2017-11-12 04:25:40.507713+00	38	40 фут	2	[{"changed": {"fields": ["order_number"]}}]	11	2
1275	2017-11-12 04:32:57.332647+00	131	Насыпной	1	[{"added": {}}]	60	2
1332	2017-11-12 07:48:42.850772+00	3	daria.shonova@gmail.com	2	[{"changed": {"fields": ["company"]}}]	15	2
1347	2017-11-13 06:31:05.519937+00	84	Рефрижераторный1	2	[{"changed": {"fields": ["name"]}}]	60	1
1355	2017-11-14 04:33:19.517076+00	84	Рефрижераторный	2	[{"changed": {"fields": ["name"]}}]	60	14
1369	2017-11-14 06:35:39.582306+00	10	Цена	2	[{"changed": {"fields": ["field"]}}]	7	2
1373	2017-11-15 06:10:28.046819+00	75	danil@gmail.com	1	[{"added": {}}]	15	2
1387	2017-11-16 01:12:59.711439+00	259	Рефрижераторный 20 фут Carrier 69NT40	2	[{"changed": {"fields": ["name", "property_json"]}}]	8	78
1625	2017-11-16 04:55:07.598202+00	135	Daikin LXE	1	[{"added": {}}]	63	18
1643	2017-11-29 10:00:08.507605+00	80	ice@rftu.ru	2	[{"changed": {"fields": ["email", "contact_email"]}}]	15	18
1651	2018-01-22 23:55:45.128457+00	83	testManager@bbb.cc	2	[{"changed": {"fields": ["company", "last_name", "email", "contact_email", "phone", "is_staff"]}}]	15	14
1669	2018-10-18 05:40:22.395697+00	80	ice@rftu.ru	2	[]	15	18
1178	2017-10-30 12:19:45.327036+00	27	test@testemail.ru	2	[{"changed": {"fields": ["is_staff", "is_superuser", "patronymic", "phone", "contact_email", "company", "photo"]}}]	15	1
1194	2017-11-07 09:21:47.718299+00	114	Находка	1	[{"added": {}}]	99	14
1260	2017-11-12 04:25:46.659396+00	39	40 фут  High Cube 2.9	2	[{"changed": {"fields": ["order_number"]}}]	11	2
1276	2017-11-12 04:33:15.10336+00	131	Насыпной	2	[{"changed": {"fields": ["order_number"]}}]	60	2
1333	2017-11-12 07:48:53.087314+00	19	dinamik6336@inbox.ru	3		15	2
1348	2017-11-13 06:31:27.73806+00	84	Рефрижераторный	2	[{"changed": {"fields": ["name"]}}]	60	1
1356	2017-11-14 04:33:32.742109+00	131	Насыпной	3		60	14
1374	2017-11-15 06:10:52.405929+00	75	danil@gmail.com	2	[{"changed": {"fields": ["company", "last_name", "contact_email", "phone", "is_staff"]}}]	15	2
1388	2017-11-16 01:39:31.098695+00	257	Open Top 40 фут Модель2_2	3		8	78
1389	2017-11-16 01:39:31.1217+00	256	Рефрижераторный 20 фут Carrier 69NT40	3		8	78
1390	2017-11-16 01:39:31.125362+00	255	Сухогруз 45 фут  High Cube 2.9 Модель2_2	3		8	78
1391	2017-11-16 01:39:31.130058+00	254	Рефрижераторный 20 футов High Cube 2.9	3		8	78
1392	2017-11-16 01:39:31.133886+00	253	Сухогруз 20 фут High Cube XXL 3.2	3		8	78
1393	2017-11-16 01:39:31.137683+00	251	Рефрижераторный 40 фут Модель1	3		8	78
1394	2017-11-16 01:39:31.141602+00	250	Сухогруз 20 фут Модель1	3		8	78
1395	2017-11-16 01:39:31.145609+00	249	Вентилируемый 20 фут	3		8	78
1396	2017-11-16 01:39:31.149693+00	238	Рефрижераторный 20 фут Модель1	3		8	78
1397	2017-11-16 01:39:31.15357+00	237	Рефрижераторный 40 фут  High Cube	3		8	78
1398	2017-11-16 01:39:31.157679+00	236	45 фут	3		8	78
1399	2017-11-16 01:39:31.167981+00	228	Рефрижераторный 20 фут Модель1	3		8	78
1400	2017-11-16 01:39:31.17125+00	215	Вентилируемый 40 фут Модель2	3		8	78
1401	2017-11-16 01:39:31.175104+00	214	Платформа 45 фут Модель1	3		8	78
1402	2017-11-16 01:39:31.17903+00	211	Танк 20 фут High Cube XXL 3.2 Модель1	3		8	78
1403	2017-11-16 01:39:31.182998+00	210	Насыпной 10 фут Модель2	3		8	78
1404	2017-11-16 01:39:31.186992+00	204	Вентилируемый 45 фут undefined	3		8	78
1405	2017-11-16 01:39:31.191037+00	203	Рефрижераторный	3		8	78
1406	2017-11-16 01:39:31.194994+00	202	Вентилируемый 40 фут	3		8	78
1407	2017-11-16 01:39:31.198969+00	201	Рефрижераторный	3		8	78
1408	2017-11-16 01:39:31.203198+00	199	Рефрижераторный 20 фут Модель1	3		8	78
1626	2017-11-16 04:55:32.527452+00	136	Daikin ZeSTIA	1	[{"added": {}}]	63	18
1644	2017-12-01 03:32:21.029094+00	21	wildpixel.dv@gmail.com	2	[{"changed": {"fields": ["last_name", "is_staff"]}}]	15	18
1652	2018-01-22 23:58:35.858548+00	83	testManager@bbb.cc	3		15	14
1670	2018-10-18 05:42:54.36033+00	80	ice@rftu.ru	2	[{"changed": {"fields": ["password"]}}]	15	18
357	2017-09-17 23:46:27.376+00	48	TestContaine3r	3		8	2
358	2017-09-17 23:46:27.382+00	47	TestContaine3r	3		8	2
359	2017-09-17 23:46:27.392+00	46	TestContaine3r	3		8	2
360	2017-09-18 09:51:34.975+00	100	testField	1	[{"added": {}}]	11	1
361	2017-09-19 01:41:25.144+00	17	admin@refterminal.ru	1	[{"added": {}}]	15	2
362	2017-09-19 01:42:54.181+00	17	admin@refterminal.ru	3		15	2
363	2017-09-19 01:43:52.103+00	18	admin@refterminal.ru	2	[{"changed": {"fields": ["patronymic", "phone", "contact_email", "company", "photo"]}}]	15	2
364	2017-09-19 05:27:15.723+00	1	Контейнер	2	[]	9	2
365	2017-09-19 05:43:05.357+00	196	Рефрижираторный 20 фут weqewqe	3		8	2
366	2017-09-19 23:49:24.246+00	1	По цене	1	[{"added": {}}]	65	2
372	2017-10-09 01:23:02.019+00	103	Город	2	[]	14	2
1179	2017-10-30 12:22:02.51256+00	27	test@testemail.ru	2	[{"changed": {"fields": ["is_superuser"]}}]	15	1
1195	2017-11-07 09:22:05.191875+00	116	Находка	1	[{"added": {}}]	99	14
1261	2017-11-12 04:26:08.690989+00	77	20 фут High Cube XXL 3.2	2	[{"changed": {"fields": ["order_number"]}}]	11	2
1277	2017-11-12 04:33:27.699894+00	86	Танк	2	[]	60	2
1334	2017-11-12 07:49:39.252248+00	24	lugovskoy@rudoit.com	3		15	2
1335	2017-11-12 07:49:39.582329+00	5	teonijen@gmail.com	3		15	2
1349	2017-11-13 06:31:43.360745+00	84	Рефрижераторный1	2	[{"changed": {"fields": ["name"]}}]	60	1
1357	2017-11-14 04:38:44.760586+00	38	40 фут	2	[{"changed": {"fields": ["order_number"]}}]	11	14
1375	2017-11-15 06:13:51.446383+00	21	wildpixel.dv@gmail.com	2	[{"changed": {"fields": ["password"]}}]	15	18
1409	2017-11-16 01:39:44.545825+00	376	OrderModel object	3		21	78
1410	2017-11-16 01:39:44.558437+00	375	OrderModel object	3		21	78
1411	2017-11-16 01:39:44.562418+00	374	OrderModel object	3		21	78
1412	2017-11-16 01:39:44.566162+00	373	OrderModel object	3		21	78
1413	2017-11-16 01:39:44.570674+00	372	OrderModel object	3		21	78
1414	2017-11-16 01:39:44.578668+00	371	OrderModel object	3		21	78
1415	2017-11-16 01:39:44.58275+00	370	OrderModel object	3		21	78
1416	2017-11-16 01:39:44.586659+00	369	OrderModel object	3		21	78
1417	2017-11-16 01:39:44.590319+00	368	OrderModel object	3		21	78
1418	2017-11-16 01:39:44.598648+00	367	OrderModel object	3		21	78
1419	2017-11-16 01:39:44.603206+00	366	OrderModel object	3		21	78
1420	2017-11-16 01:39:44.639474+00	365	OrderModel object	3		21	78
1421	2017-11-16 01:39:44.650411+00	364	OrderModel object	3		21	78
1422	2017-11-16 01:39:44.654052+00	363	OrderModel object	3		21	78
1423	2017-11-16 01:39:44.658076+00	362	OrderModel object	3		21	78
1424	2017-11-16 01:39:44.662209+00	361	OrderModel object	3		21	78
1425	2017-11-16 01:39:44.665971+00	360	OrderModel object	3		21	78
1426	2017-11-16 01:39:44.670072+00	359	OrderModel object	3		21	78
1427	2017-11-16 01:39:44.673982+00	358	OrderModel object	3		21	78
1428	2017-11-16 01:39:44.684325+00	357	OrderModel object	3		21	78
1429	2017-11-16 01:39:44.687847+00	356	OrderModel object	3		21	78
1430	2017-11-16 01:39:44.691411+00	355	OrderModel object	3		21	78
1431	2017-11-16 01:39:44.695319+00	354	OrderModel object	3		21	78
1432	2017-11-16 01:39:44.699184+00	353	OrderModel object	3		21	78
1433	2017-11-16 01:39:44.70314+00	352	OrderModel object	3		21	78
1434	2017-11-16 01:39:44.707311+00	351	OrderModel object	3		21	78
1435	2017-11-16 01:39:44.711212+00	350	OrderModel object	3		21	78
1436	2017-11-16 01:39:44.715246+00	349	OrderModel object	3		21	78
1437	2017-11-16 01:39:44.719086+00	348	OrderModel object	3		21	78
1438	2017-11-16 01:39:44.723046+00	347	OrderModel object	3		21	78
1439	2017-11-16 01:39:44.726993+00	346	OrderModel object	3		21	78
1440	2017-11-16 01:39:44.73104+00	345	OrderModel object	3		21	78
1441	2017-11-16 01:39:44.735173+00	344	OrderModel object	3		21	78
1442	2017-11-16 01:39:44.739205+00	343	OrderModel object	3		21	78
1443	2017-11-16 01:39:44.743104+00	342	OrderModel object	3		21	78
1444	2017-11-16 01:39:44.747227+00	341	OrderModel object	3		21	78
1445	2017-11-16 01:39:44.751099+00	340	OrderModel object	3		21	78
1446	2017-11-16 01:39:44.761859+00	339	OrderModel object	3		21	78
1447	2017-11-16 01:39:44.765921+00	338	OrderModel object	3		21	78
1448	2017-11-16 01:39:44.769991+00	337	OrderModel object	3		21	78
1449	2017-11-16 01:39:44.773899+00	336	OrderModel object	3		21	78
1450	2017-11-16 01:39:44.777814+00	335	OrderModel object	3		21	78
1451	2017-11-16 01:39:44.781907+00	334	OrderModel object	3		21	78
1452	2017-11-16 01:39:44.785946+00	333	OrderModel object	3		21	78
1453	2017-11-16 01:39:44.789892+00	332	OrderModel object	3		21	78
1454	2017-11-16 01:39:44.793748+00	331	OrderModel object	3		21	78
1455	2017-11-16 01:39:44.797857+00	330	OrderModel object	3		21	78
1456	2017-11-16 01:39:44.802683+00	329	OrderModel object	3		21	78
1457	2017-11-16 01:39:44.806653+00	328	OrderModel object	3		21	78
1458	2017-11-16 01:39:44.813551+00	327	OrderModel object	3		21	78
1459	2017-11-16 01:39:44.817896+00	326	OrderModel object	3		21	78
1460	2017-11-16 01:39:44.821637+00	325	OrderModel object	3		21	78
1461	2017-11-16 01:39:44.825549+00	324	OrderModel object	3		21	78
1462	2017-11-16 01:39:44.831316+00	323	OrderModel object	3		21	78
1463	2017-11-16 01:39:44.841055+00	322	OrderModel object	3		21	78
1464	2017-11-16 01:39:44.844739+00	321	OrderModel object	3		21	78
1465	2017-11-16 01:39:44.848052+00	320	OrderModel object	3		21	78
1466	2017-11-16 01:39:44.851385+00	319	OrderModel object	3		21	78
1467	2017-11-16 01:39:44.855033+00	318	OrderModel object	3		21	78
1468	2017-11-16 01:39:44.858871+00	317	OrderModel object	3		21	78
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 1676, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_content_type (id, app_label, model) FROM stdin;
99	products	citycontainermodel
100	products	placecontainermodel
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	products	fieldproduct
7	products	filterproduct
8	products	realproduct
9	products	typeproduct
10	products	boolfieldproduct
11	products	charfieldproduct
12	products	filefieldproduct
13	products	floatfieldproduct
14	products	selectfieldproduct
15	api	user
16	api	bonus
17	api	company
18	api	email
19	api	emailconfig
20	api	ordermanagermodel
21	api	ordermodel
22	authtoken	token
23	products	objectfieldproduct
24	products	measurecontainermodel
25	content_gallery	image
26	products	containertype
59	products	containermeasure
60	products	typecontainermodel
61	products	containersost
62	products	containerstatus
63	products	containermodel
64	products	containerplace
65	products	sortproduct
98	api	tradeitem
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_content_type_id_seq', 100, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2017-10-25 01:04:52.532857+00
2	api	0001_initial	2017-10-25 01:04:54.328812+00
3	admin	0001_initial	2017-10-25 01:04:54.601476+00
4	admin	0002_logentry_remove_auto_add	2017-10-25 01:04:54.624158+00
5	contenttypes	0002_remove_content_type_name	2017-10-25 01:04:54.680104+00
6	products	0001_initial	2017-10-25 01:04:56.32557+00
7	products	0002_auto_20170831_1226	2017-10-25 01:04:56.395475+00
8	products	0003_auto_20170831_1227	2017-10-25 01:04:56.425472+00
9	products	0004_auto_20170831_1229	2017-10-25 01:04:56.458719+00
10	products	0005_auto_20170831_1233	2017-10-25 01:04:56.493933+00
11	products	0006_remove_selectfieldproduct_property	2017-10-25 01:04:56.531691+00
12	products	0007_objectfieldproduct	2017-10-25 01:04:56.746645+00
13	products	0008_auto_20170901_1313	2017-10-25 01:04:56.857215+00
14	products	0009_auto_20170901_1324	2017-10-25 01:04:56.915521+00
15	products	0010_auto_20170901_1324	2017-10-25 01:04:56.97008+00
16	products	0011_measurecontainermodel	2017-10-25 01:04:56.988786+00
17	products	0012_delete_measurecontainermodel	2017-10-25 01:04:56.998574+00
18	products	0013_measurecontainermodel	2017-10-25 01:04:57.010369+00
19	products	0014_delete_measurecontainermodel	2017-10-25 01:04:57.019277+00
20	products	0015_auto_20170908_1206	2017-10-25 01:04:57.055262+00
21	products	0016_auto_20170912_1225	2017-10-25 01:04:57.22063+00
22	products	0017_auto_20170912_2241	2017-10-25 01:04:57.344446+00
23	products	0018_fieldproduct_required	2017-10-25 01:04:57.439429+00
24	products	0019_auto_20170914_1808	2017-10-25 01:04:57.476111+00
25	products	0020_containermeasure_containermodel_containerplace_containersost_containerstatus_typecontainermodel	2017-10-25 01:04:57.506615+00
26	products	0021_auto_20170915_1736	2017-10-25 01:04:57.541185+00
27	products	0022_containermeasure_containermodel_containerplace_containersost_containerstatus_typecontainermodel	2017-10-25 01:04:57.606409+00
28	products	0023_auto_20170919_1931	2017-10-25 01:04:58.156929+00
29	products	0024_sortproduct_field	2017-10-25 01:04:58.243798+00
30	products	0025_auto_20170922_1627	2017-10-25 01:04:58.269291+00
31	auth	0001_initial	2017-10-25 01:04:58.510083+00
32	auth	0002_alter_permission_name_max_length	2017-10-25 01:04:58.539914+00
33	auth	0003_alter_user_email_max_length	2017-10-25 01:04:58.565792+00
34	auth	0004_alter_user_username_opts	2017-10-25 01:04:58.586477+00
35	auth	0005_alter_user_last_login_null	2017-10-25 01:04:58.609425+00
36	auth	0006_require_contenttypes_0002	2017-10-25 01:04:58.633369+00
37	auth	0007_alter_validators_add_error_messages	2017-10-25 01:04:58.652404+00
38	auth	0008_alter_user_username_max_length	2017-10-25 01:04:58.692225+00
39	api	0002_auto_20170828_2352	2017-10-25 01:04:59.404715+00
40	api	0003_auto_20170922_1627	2017-10-25 01:04:59.463281+00
41	api	0004_auto_20170925_1127	2017-10-25 01:04:59.71098+00
42	api	0005_auto_20170926_1902	2017-10-25 01:04:59.872596+00
43	api	0006_auto_20170926_2145	2017-10-25 01:05:00.001698+00
44	api	0007_auto_20170927_1041	2017-10-25 01:05:00.068608+00
45	api	0008_auto_20171003_2323	2017-10-25 01:05:00.1423+00
46	api	0009_auto_20171004_2113	2017-10-25 01:05:00.186961+00
47	api	0010_auto_20171014_2250	2017-10-25 01:05:00.222963+00
48	api	0011_auto_20171019_2206	2017-10-25 01:05:00.264004+00
49	authtoken	0001_initial	2017-10-25 01:05:00.371247+00
50	authtoken	0002_auto_20160226_1747	2017-10-25 01:05:00.524499+00
51	content_gallery	0001_initial	2017-10-25 01:05:01.134672+00
52	content_gallery	0002_auto_20170305_1520	2017-10-25 01:05:01.283008+00
53	content_gallery	0003_auto_20170305_1729	2017-10-25 01:05:01.340173+00
54	content_gallery	0004_auto_20170309_2234	2017-10-25 01:05:01.428582+00
55	content_gallery	0005_auto_20170320_0028	2017-10-25 01:05:01.56021+00
56	content_gallery	0006_auto_20170504_2334	2017-10-25 01:05:01.699774+00
57	content_gallery	0007_auto_20170616_1845	2017-10-25 01:05:01.806284+00
58	products	0019_auto_20171004_1641	2017-10-25 01:05:01.944845+00
59	products	0019_auto_20171003_2355	2017-10-25 01:05:02.010593+00
60	products	0026_merge_20171004_2048	2017-10-25 01:05:02.070694+00
61	products	0025_auto_20171006_1301	2017-10-25 01:05:02.148636+00
62	products	0027_merge_20171006_1434	2017-10-25 01:05:02.205582+00
63	sessions	0001_initial	2017-10-25 01:05:02.712355+00
64	api	0012_auto_20171114_2224	2017-11-14 23:12:54.405146+00
65	products	0028_auto_20171114_2224	2017-11-14 23:12:54.935656+00
66	products	0029_auto_20171116_1555	2017-11-16 15:55:53.864536+00
106	products	0002_products	2017-12-08 03:59:35.332805+00
107	products	0003_auto_20171208_1403	2017-12-08 04:03:41.382612+00
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_migrations_id_seq', 107, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
ts2lqjcw1rr0j75hi5ir9rey9ak04nf9	YTc5MDZkOWRhYmZjMDEyMjkxNDVmYmRhM2ZiYTUxMzllYzVlNTMyNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiODVkOTQ1Y2QwZjE5NzAxYTI0YzgyOGZiNTkyMmZmNWE1OGE2MjgyIn0=	2017-11-08 01:25:47.265496+00
fohb6gn1xdci9nsbqyr8r8wq4vwfo0dl	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-11-08 01:50:39.519487+00
zkdp7f41rg4yzrvmvbqyb67daakczu4c	NmVhZWQyZGMzOGFhMjc4OGYyMjk3NGExMTlmMWM2N2U4MzA1NWI2YTp7Il9hdXRoX3VzZXJfaWQiOiIyNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMWExMTAzZDIxMzc2NTEyNDY4NGU1YjA0Y2ZmZmFiYmQwYzgxZTg4OCJ9	2017-11-12 22:17:43.076043+00
99iy061gm1osar125ck4e3nksyyc5lh9	ODZlOGZkMDgzODEyNTk4ZGNiNTRmMzlmMTk2ZGRjYWYwM2YzYjkxOTp7Il9hdXRoX3VzZXJfaWQiOiIxNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMDQxNjcyMjZiMTRiZDZiMTFiYjc3YzY2OTRjMDE1NWM1N2ZiZjRmMiJ9	2017-11-26 05:35:28.450771+00
tash9ltu8mc3v7b0hc1xa9xwn4wddqil	MTYxNTQ3NmVhOTdmNmNmNzQ0NjRjYWNhN2U1NmUzNDJkZjYwNDc2Nzp7Il9hdXRoX3VzZXJfaWQiOiIxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmY5MGZmNGFiNzlhZWQ1N2NjYmNmZmE1NGQwMjA2NmM5YjIxODFmZCJ9	2017-11-29 06:11:28.258976+00
jd4y9ivkin9ti8ir503nnciio2nv0o50	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-11-29 08:17:52.548446+00
gxbk3arpz1u5utryoypp94lklvk73931	MTYxNTQ3NmVhOTdmNmNmNzQ0NjRjYWNhN2U1NmUzNDJkZjYwNDc2Nzp7Il9hdXRoX3VzZXJfaWQiOiIxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmY5MGZmNGFiNzlhZWQ1N2NjYmNmZmE1NGQwMjA2NmM5YjIxODFmZCJ9	2017-12-07 02:52:28.891675+00
5sq291ea5s8t424kr0xmbep8o0qot2nk	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2018-01-04 05:58:21.535606+00
ow1z8ogtuzqkoowek1dy7bygy5z2qp1j	MTYxNTQ3NmVhOTdmNmNmNzQ0NjRjYWNhN2U1NmUzNDJkZjYwNDc2Nzp7Il9hdXRoX3VzZXJfaWQiOiIxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmY5MGZmNGFiNzlhZWQ1N2NjYmNmZmE1NGQwMjA2NmM5YjIxODFmZCJ9	2018-01-25 08:17:23.801262+00
skoezzyyx3d8vtly0dii7tlf723qjuj7	ODZlOGZkMDgzODEyNTk4ZGNiNTRmMzlmMTk2ZGRjYWYwM2YzYjkxOTp7Il9hdXRoX3VzZXJfaWQiOiIxNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMDQxNjcyMjZiMTRiZDZiMTFiYjc3YzY2OTRjMDE1NWM1N2ZiZjRmMiJ9	2018-02-05 23:45:18.427704+00
a46kpk7joagoiu72hwqu29wom9tv6ag0	MTYxNTQ3NmVhOTdmNmNmNzQ0NjRjYWNhN2U1NmUzNDJkZjYwNDc2Nzp7Il9hdXRoX3VzZXJfaWQiOiIxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmY5MGZmNGFiNzlhZWQ1N2NjYmNmZmE1NGQwMjA2NmM5YjIxODFmZCJ9	2018-08-11 12:16:18.137546+00
7k3up0s1rc4zjoid9psm0vx2ad2j1wgv	YTc5MDZkOWRhYmZjMDEyMjkxNDVmYmRhM2ZiYTUxMzllYzVlNTMyNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiODVkOTQ1Y2QwZjE5NzAxYTI0YzgyOGZiNTkyMmZmNWE1OGE2MjgyIn0=	2017-11-13 12:36:38.627584+00
0o8lwl08s7ca4v70y16e6hqd47ajvipo	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-09-25 12:06:46.039+00
1hy278wu9dxqg16u16vc1v32r77jaynm	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-19 06:55:22.392+00
32rzphao04gzlcm4azfyi98yoqgn4ixi	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-11-07 00:48:34.561+00
34xso6xfb491wrxgij40mvp3smzol7jk	YTc5MDZkOWRhYmZjMDEyMjkxNDVmYmRhM2ZiYTUxMzllYzVlNTMyNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiODVkOTQ1Y2QwZjE5NzAxYTI0YzgyOGZiNTkyMmZmNWE1OGE2MjgyIn0=	2017-09-26 07:11:13.266+00
6reamobbnr25ykoawre3pwggw679xtvz	YTc5MDZkOWRhYmZjMDEyMjkxNDVmYmRhM2ZiYTUxMzllYzVlNTMyNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiODVkOTQ1Y2QwZjE5NzAxYTI0YzgyOGZiNTkyMmZmNWE1OGE2MjgyIn0=	2017-11-07 05:32:13.237+00
6nyesnc4xhx37jpsr62d3kfdpa8r2piy	ODZlOGZkMDgzODEyNTk4ZGNiNTRmMzlmMTk2ZGRjYWYwM2YzYjkxOTp7Il9hdXRoX3VzZXJfaWQiOiIxNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMDQxNjcyMjZiMTRiZDZiMTFiYjc3YzY2OTRjMDE1NWM1N2ZiZjRmMiJ9	2017-11-13 00:20:20.875392+00
yr5ezi57co5hjcl9mw76dt8p2eu6r6yh	YTc5MDZkOWRhYmZjMDEyMjkxNDVmYmRhM2ZiYTUxMzllYzVlNTMyNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiODVkOTQ1Y2QwZjE5NzAxYTI0YzgyOGZiNTkyMmZmNWE1OGE2MjgyIn0=	2017-11-27 06:02:28.168952+00
k6tafnttsy5bzmj09bsn71cdy68j1tol	MTYxNTQ3NmVhOTdmNmNmNzQ0NjRjYWNhN2U1NmUzNDJkZjYwNDc2Nzp7Il9hdXRoX3VzZXJfaWQiOiIxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmY5MGZmNGFiNzlhZWQ1N2NjYmNmZmE1NGQwMjA2NmM5YjIxODFmZCJ9	2017-12-13 03:45:36.255377+00
x7o4eys2dzq2sx2jy3iw8parr860yre3	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2018-02-06 00:16:11.608245+00
zg0l8crmrqw0zxu8n7y8fzs1m64irj2o	MTYxNTQ3NmVhOTdmNmNmNzQ0NjRjYWNhN2U1NmUzNDJkZjYwNDc2Nzp7Il9hdXRoX3VzZXJfaWQiOiIxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmY5MGZmNGFiNzlhZWQ1N2NjYmNmZmE1NGQwMjA2NmM5YjIxODFmZCJ9	2018-11-01 05:42:54.374743+00
dio9lhov3o8c4n6y5lehwhfl3ln8nioy	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-09-25 22:17:32.626+00
ebujrt58megrw8qog93wg2vf61dt8r5z	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-25 09:03:43.748+00
f1c8lnepcxdbayxb8kdvgixlxmrqw81t	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-05 12:25:50.002+00
gxr774ekygs9pv2m4todn95lbzmkkmh4	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-03 01:39:32.902+00
ih2qsbj2zm6jcz6lzpcvb5aahx23u6jt	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-01 22:08:38.881+00
jh2g4v6o9jlrrs8ipz73pbiissz34ylr	YTc5MDZkOWRhYmZjMDEyMjkxNDVmYmRhM2ZiYTUxMzllYzVlNTMyNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiODVkOTQ1Y2QwZjE5NzAxYTI0YzgyOGZiNTkyMmZmNWE1OGE2MjgyIn0=	2017-10-19 05:48:43.072+00
jsy47nlqjz1jmva57cd5zsdp7u1hd6su	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-19 06:55:21.253+00
kkem8v0pnp3do8bgo4e6120ycijkss5d	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-09-25 13:09:01.331+00
hfqnqk9lsqfq1uufkgtbhhoybbuwtl6k	MTYxNTQ3NmVhOTdmNmNmNzQ0NjRjYWNhN2U1NmUzNDJkZjYwNDc2Nzp7Il9hdXRoX3VzZXJfaWQiOiIxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmY5MGZmNGFiNzlhZWQ1N2NjYmNmZmE1NGQwMjA2NmM5YjIxODFmZCJ9	2017-11-29 06:13:51.507565+00
mm4s248v9ycr5094bzpxbym16qq0p3yf	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-11-30 03:19:31.013676+00
6jryg0szclswyh6o9f315wqd06ylmo86	MTYxNTQ3NmVhOTdmNmNmNzQ0NjRjYWNhN2U1NmUzNDJkZjYwNDc2Nzp7Il9hdXRoX3VzZXJfaWQiOiIxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmY5MGZmNGFiNzlhZWQ1N2NjYmNmZmE1NGQwMjA2NmM5YjIxODFmZCJ9	2017-12-13 03:48:48.881435+00
9w1f1oq5moob5bcbwo7npvazld0gtojx	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-23 01:18:31.569+00
agd6i70ttcf4q8flwakxc6u5z6aw37ub	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-09-25 11:21:59.693+00
bwrhr6nzl5xxmzvc1tg09qeqk8rpy2qt	ZTIyYzU4ZDkzMDgwNzU5Y2VjZTFlNTRjOWEyNDUwODNiNjJjMGVlZTp7Il9hdXRoX3VzZXJfaWQiOiIyMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYzJkYmYwMzAzNjU2MDlhNjYyZjg3ZjU5M2EwNGFiMzI4Y2Y3MzlhMiJ9	2018-03-14 00:47:32.989435+00
5lpm2mq3pdc7j00x8romf4789jwx24le	MTYxNTQ3NmVhOTdmNmNmNzQ0NjRjYWNhN2U1NmUzNDJkZjYwNDc2Nzp7Il9hdXRoX3VzZXJfaWQiOiIxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmY5MGZmNGFiNzlhZWQ1N2NjYmNmZmE1NGQwMjA2NmM5YjIxODFmZCJ9	2018-11-04 23:22:36.82083+00
mbm58clyxasoxdczi0pmc37tctbnj1qo	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-09-25 12:35:22.089+00
mc1plg3066rk8j4pnjiqxx2bnkmovac6	YTc5MDZkOWRhYmZjMDEyMjkxNDVmYmRhM2ZiYTUxMzllYzVlNTMyNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiODVkOTQ1Y2QwZjE5NzAxYTI0YzgyOGZiNTkyMmZmNWE1OGE2MjgyIn0=	2017-09-12 01:55:18.746+00
nnnchli9ylm8dlmyne6sfys9z6dutm0z	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-18 09:12:58.548+00
ocgimtcvuzro5ee62mlbcpljwdx6rkt5	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-09-13 12:46:17.407+00
ohfknln5ar9bzpjsaklugojkqj49iezv	YTc5MDZkOWRhYmZjMDEyMjkxNDVmYmRhM2ZiYTUxMzllYzVlNTMyNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiODVkOTQ1Y2QwZjE5NzAxYTI0YzgyOGZiNTkyMmZmNWE1OGE2MjgyIn0=	2017-10-08 15:14:03.305+00
p7b0hkswxocn0l4se4ub3kcvkwgdoob9	YTc5MDZkOWRhYmZjMDEyMjkxNDVmYmRhM2ZiYTUxMzllYzVlNTMyNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiODVkOTQ1Y2QwZjE5NzAxYTI0YzgyOGZiNTkyMmZmNWE1OGE2MjgyIn0=	2017-09-20 03:04:43.017+00
qrijtdetvia4v4jnpx486t3csjsw7m4i	YTc5MDZkOWRhYmZjMDEyMjkxNDVmYmRhM2ZiYTUxMzllYzVlNTMyNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiODVkOTQ1Y2QwZjE5NzAxYTI0YzgyOGZiNTkyMmZmNWE1OGE2MjgyIn0=	2017-10-23 04:05:16.137+00
r93n0fuu9f2w5ig6gd4ybrk985cv2um8	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-01 23:45:43.371+00
y2fu8ia3wmc76k6w9xbxph5ee2g2257g	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-12-19 03:58:09.44081+00
kyu7vaxgurvcfrnouezvb0wgvp8stvw2	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-09-20 01:25:41.692+00
ma2ij9veqneksp8spccr5qflecbrf6ff	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-09-25 11:24:23.982+00
k9mvrvudr02o2ewr90ys5r5chh0xsku4	MTYxNTQ3NmVhOTdmNmNmNzQ0NjRjYWNhN2U1NmUzNDJkZjYwNDc2Nzp7Il9hdXRoX3VzZXJfaWQiOiIxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmY5MGZmNGFiNzlhZWQ1N2NjYmNmZmE1NGQwMjA2NmM5YjIxODFmZCJ9	2018-04-24 04:21:21.579226+00
6pr7pdf2mrlow58774dkewt473xsnlns	MTYxNTQ3NmVhOTdmNmNmNzQ0NjRjYWNhN2U1NmUzNDJkZjYwNDc2Nzp7Il9hdXRoX3VzZXJfaWQiOiIxOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmY5MGZmNGFiNzlhZWQ1N2NjYmNmZmE1NGQwMjA2NmM5YjIxODFmZCJ9	2018-02-01 04:50:36.667728+00
shw3nkrw0sx0pqrpbvw0dtmbyi4a0viu	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-01 13:53:38.244+00
w98lwb2b3saysuednhbrn88a0fll8pej	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-25 05:23:58.64+00
xzbqtflpp99xwvxa9di96kk0mkh9ms0x	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-23 08:29:31.821+00
ygeiqr5vwmpw33n4d1bu62tt4dct04vn	Y2EyNzI1OTFhNzhkNDUxOWExYjA4NWMzM2QxMGU2OTY3YmJjODk0Yzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MzA4ZTgyYmM4N2QzZDQ1OTI4N2VlZWJhODgyYzJiYzIyY2YyOGU4In0=	2017-10-26 10:14:13.201+00
\.


--
-- Data for Name: products_boolfieldproduct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_boolfieldproduct (fieldproduct_ptr_id, content) FROM stdin;
7	\N
17	\N
74	\N
75	\N
101	\N
73	\N
\.


--
-- Data for Name: products_charfieldproduct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_charfieldproduct (fieldproduct_ptr_id, content) FROM stdin;
20	\N
122	\N
35	\N
130	\N
38	\N
39	\N
77	\N
129	\N
56	\N
40	\N
57	\N
92	\N
135	\N
136	\N
137	\N
138	\N
141	\N
8	\N
13	\N
21	\N
58	RT
67	\N
68	\N
69	\N
70	\N
71	\N
72	\N
80	char_test1
81	Другое Тест Имя
93	\N
95	\N
96	\N
97	\N
99	\N
100	\N
112	\N
\.


--
-- Data for Name: products_fieldproduct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_fieldproduct (id, name, translate, polymorphic_ctype_id, required, order_number) FROM stdin;
2	Цена	tsena	13	t	0
3	Цена со скидкой	tsena-so-skidkoi	13	t	0
7	Опубликован	publish	10	t	0
8	Описание	opisanie	11	t	0
11	Год	god	13	t	0
13	Внутренние размеры	vnutrennie-razmery	11	t	0
14	Вместимость в м3	vmestimost-v-m3	13	t	0
15	Вес тары	ves-tary	13	t	0
17	Использовался в РФ	ispolzovalsia-v-rf	10	t	0
21	Номер контейнера	nomer-konteinera	11	t	0
22	Акт технического состояния	akt-tekhnicheskogo-sostoianiia	12	f	0
32	Состояние	sostoianie	14	t	0
33	Статус	status	14	t	0
47	Размер	razmer	14	t	0
55	Тип	tip	14	t	0
58	сокращенное	sokrashchennoe	11	t	0
66	Стоимость аренды в год	stoimost-arendy-v-god	13	f	0
67	Цена аренды в год	tsena-arendy-v-god	11	f	0
68	Новый	novyi	11	t	0
69	б/у	bu	11	t	0
70	Свободен	svoboden	11	t	0
71	Арендован	arendovan	11	t	0
73	Гарантия	garantiia	10	f	0
20	Описание гарантии	opisanie-garantii	11	f	0
19	Срок гарантии	srok-garantii	13	f	0
113	Ковальчука, 5	kovalchuka-5	100	t	0
114	Находка	nakhodka	99	t	0
122	Резервный фонд	rezervnyi-fond	62	t	0
124	Днепровская, 25	dneprovska-25	100	t	0
125	Артем	artem	99	t	0
72	Продан	prodan	11	t	0
74	Аренда	arenda	10	f	0
75	Продажа	prodazha	10	f	0
76	Стоимость аренды в месяц	stoimost-arendy-v-mesiats	13	t	0
78	Типок	tipok	26	t	0
80	ЧарТест1	chartest1	11	t	0
81	Тест_Имя	test_imia	11	t	0
116	Находка	nakhodka-1	99	t	0
104	Владивосток	vladivostok	14	f	0
91	Модель	model	14	f	0
93	Другая модель	drugaia-model	11	f	0
94	Местонахождение	mestonakhozhdenie	14	f	0
95	г.Владивосток, ул. Тухачевского, 26	gvladivostok-ul-tukhachevskogo-26	64	t	0
96	г.Владивосток, ул. Тухачевского, 27	gvladivostok-ul-tukhachevskogo-27	64	f	0
97	г.Владивосток, ул. Тухачевского, 28	gvladivostok-ul-tukhachevskogo-28	64	t	0
99	Другое место	drugoe-mesto	11	t	0
100	testField	-1	11	t	0
101	Скидка	skidka	10	t	0
103	Город	gorod	14	f	0
112	test	test	11	t	0
35	20 фут	20-fut	11	t	1
130	20 футов High Cube 2.9	20-futov-high-cube-29	59	t	2
77	20 фут High Cube XXL 3.2	20-fut-32	11	t	5
129	40 футов High Cube XXL 3.2	40-futov-high-cube-xxl-32	59	t	6
136	Daikin ZeSTIA	daikin-zestia	63	t	2
137	Mitsubishi	mitsubishi	63	t	3
56	45 фут	45-fut	11	t	7
83	Вентилируемый	ventiliruemyi-1-1-1-1-1	60	t	6
40	45 фут  High Cube 2.9	45-fut-high-cube	11	t	8
57	10 фут	10-fut	11	t	10
89	Сухогруз	sukhogruz-1-1	60	t	2
88	Open Top	otkrytyi-1-1	60	t	3
138	Thermo King	thermo-king	63	t	4
84	Рефрижераторный	refrezhiratornyi-1-1-1-1-1	60	t	1
86	Танк	tank-1-1	60	t	4
38	40 фут	40-fut	11	t	3
87	Flat Rack	platforma-1-1	60	t	5
139	Рефрижераторная установка	refrizheratornaia-ustanovka	60	t	7
134	Общая цена	obshchaia-tsena	13	f	0
92	Carrier 69NT40	model2	63	t	0
39	40 фут  High Cube 2.9	40-fut-high-cube	11	t	4
135	Daikin LXE	daikin-lxe	63	t	1
141	-	bez-ustanovki	63	t	-1
140	Термос-контейнер	termos-konteiner	60	t	8
142	Москва	moskva	99	t	0
143	ул. Московская, 1	ul-moskovskaia-1	100	t	0
144	Симферопольское шоссе 22, строение 1	simferopolskoe-shosse-22-stroenie-1	100	t	0
\.


--
-- Name: products_fieldproduct_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_fieldproduct_id_seq', 144, true);


--
-- Data for Name: products_filefieldproduct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_filefieldproduct (fieldproduct_ptr_id, content) FROM stdin;
22	
\.


--
-- Data for Name: products_filterproduct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_filterproduct (id, name, translate, field_id) FROM stdin;
9	Тип	tip	55
11	Номер контейнера	nomer-konteinera	21
12	Размер	razmer	47
13	Использовался в РФ	ispolzovalsia-v-rf	17
15	Состояние	sostoianie	32
17	Цена аренды в год	tsena-arendy-v-god	66
18	Цена аренды в месяц	tsena-arendy-v-mesiats	76
19	Гарантия	garantiia	73
20	Аренда	arenda	74
21	Продажа	prodazha	75
22	Модель	model	91
23	Местонахождение	mestonakhozhdenie	94
24	Город	gorod	103
10	Цена	tsena	134
\.


--
-- Name: products_filterproduct_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_filterproduct_id_seq', 24, true);


--
-- Data for Name: products_floatfieldproduct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_floatfieldproduct (fieldproduct_ptr_id, content) FROM stdin;
2	\N
3	\N
11	\N
14	\N
15	\N
66	\N
76	\N
19	\N
134	\N
\.


--
-- Data for Name: products_objectfieldproduct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_objectfieldproduct (fieldproduct_ptr_id, property) FROM stdin;
113	{"gorod": "Владивосток", "address": "Ковальчука, 5"}
124	{"gorod": "Владивосток", "address": "Днепровска, 25"}
83	{"name": "Вентилируемый", "short_name": "TC"}
84	{"name": "Рефрижераторный", "short_name": "RF"}
89	{"name": "Сухогруз", "short_name": "GP"}
88	{"name": "Open Top", "short_name": "OT"}
87	{"name": "Flat Rack", "short_name": "FT"}
139	{"name": "Рефрижераторная установка", "short_name": "RV"}
140	{"name": "Термос-контейнер", "short_name": "TR"}
143	{"gorod": "Москва", "address": "ул. Московская, 1"}
144	\N
86	{"name": "Танк", "short_name": "TC"}
\.


--
-- Data for Name: products_objectfieldproduct_content; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_objectfieldproduct_content (id, objectfieldproduct_id, fieldproduct_id) FROM stdin;
\.


--
-- Name: products_objectfieldproduct_content_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_objectfieldproduct_content_id_seq', 1, false);


--
-- Data for Name: products_realproduct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_realproduct (id, name, property_json, price, manager_id, type_id, created_at, edited_at, translate) FROM stdin;
274	Сухогруз 20 фут -	{"god": 2007.0, "tip": {"name": "Сухогруз", "short_name": "GP"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 1500000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Продан", "publish": false, "opisanie": "хороший контейнер", "prodazha": true, "ves-tary": 1500.0, "garantiia": false, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 0.0, "obshchaia-tsena": 1200000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "rftu-0123", "tsena-so-skidkoi": 1200000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "12.43x23.45x45.67", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0", "akt-tekhnicheskogo-sostoianiia": "media/DSCN7823.jpeg"}	0	14	1	2017-11-23 01:26:21.403759+00	2017-12-08 04:00:23.765913+00	sukhogruz-20-fut-
316	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 650000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Без пробега по России! Индивидуальный подход! При покупке от 2-х контейнеров предусмотрены скидки!", "prodazha": true, "ves-tary": 0.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 650000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "KKFU 6704263", "tsena-so-skidkoi": 650000.0, "opisanie-garantii": "двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-09 09:02:52.614358+00	2018-04-11 02:35:56.239275+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1
262	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2007.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 657000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": true, "status": "Резервный фонд", "publish": true, "opisanie": "Прошел техобслуживание. Все системы в норме.", "prodazha": true, "ves-tary": 4710.0, "garantiia": false, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 0.0, "obshchaia-tsena": 647000.0, "vmestimost-v-m3": 67.4, "nomer-konteinera": "RRSU 1627526", "tsena-so-skidkoi": 647000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "12.04x2.35x2.69", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-11-16 03:47:44.459566+00	2018-03-23 02:42:36.258031+00	refrizheratornyi-40-fut-high-cube-29-carrier-69nt40
259	Рефрижераторный 20 фут Daikin LXE	{"god": 2004.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Daikin LXE", "tsena": 460000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Свободен", "publish": true, "opisanie": "Рефконтейнер Daikin, 2004г. 20RE", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 3.0, "obshchaia-tsena": 450000.0, "vmestimost-v-m3": 28.0, "nomer-konteinera": "KKTU, 6072150", "tsena-so-skidkoi": 450000.0, "opisanie-garantii": "На двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "5.51x2.28x2.26", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2017-11-15 07:27:22.917625+00	2018-02-20 00:59:09.81164+00	refrizheratornyi-20-fut-daikin-lxe
260	Flat Rack 20 фут High Cube XXL 3.2 Carrier 69NT40	{"god": 0.0, "tip": {"name": "Flat Rack", "short_name": "FT"}, "gorod": {"gorod": "Уссурийск", "address": "Ленинская 1"}, "model": "Carrier 69NT40", "tsena": 0.0, "arenda": false, "razmer": "20 фут High Cube XXL 3.2", "skidka": false, "status": "Свободен", "publish": false, "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "drugoe-mesto": "Ленинская 1", "srok-garantii": 0.0, "obshchaia-tsena": 0.0, "vmestimost-v-m3": 0.0, "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0", "akt-tekhnicheskogo-sostoianiia": "media/card-bot-contest.gif"}	0	14	1	2017-11-16 02:54:04.828786+00	2017-12-08 04:00:23.758164+00	flat-rack-20-fut-high-cube-xxl-32-carrier-69nt40
319	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 650000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Без пробега по России! Индивидуальный подход! При покупке от 2-х контейнеров предусмотрены скидки!", "prodazha": true, "ves-tary": 4740.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 650000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "KKFU 6995338", "tsena-so-skidkoi": 650000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-10 02:57:28.342423+00	2018-04-11 02:34:58.338682+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1
309	Рефрижераторный 20 фут -	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 0.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": false, "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "srok-garantii": 0.0, "obshchaia-tsena": "0", "vmestimost-v-m3": 0.0, "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	84	1	2018-01-29 07:30:45.60264+00	2018-01-29 07:30:45.611021+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1
268	Рефрижераторный 20 фут Carrier 69NT40	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 150000.0, "arenda": true, "razmer": "20 фут", "skidka": false, "status": "Арендован", "publish": false, "opisanie": "1111111111111111111111111111\\n1111111111111111111111111111\\n1111111111111111111111111111\\n1111111111111111111111111111\\n11111111111111\\n1111111111111111111111111111\\n\\n1111111111111111111\\n1111111111111111111111111111111111111111111111111", "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 0.0, "obshchaia-tsena": 150000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "123123", "tsena-so-skidkoi": 150000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 123523.0, "stoimost-arendy-v-mesiats": "123123"}	0	1	1	2017-11-21 04:04:18.812836+00	2017-12-08 04:00:23.799751+00	refrizheratornyi-20-fut-carrier-69nt40
261	Open Top 20 фут High Cube XXL 3.2 Carrier 69NT40	{"god": 0.0, "tip": {"name": "Open Top", "short_name": "OT"}, "gorod": {"gorod": "Уссурийск", "address": "Ленинская 1"}, "model": "Carrier 69NT40", "tsena": 0.0, "arenda": false, "razmer": "20 фут High Cube XXL 3.2", "skidka": false, "status": "Свободен", "publish": false, "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "drugoe-mesto": "Ленинская 1", "srok-garantii": 0.0, "obshchaia-tsena": 0.0, "vmestimost-v-m3": 0.0, "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	14	1	2017-11-16 03:06:44.078608+00	2017-12-08 04:00:23.786959+00	open-top-20-fut-high-cube-xxl-32-carrier-69nt40
271	Рефрижераторная установка 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2010.0, "tip": {"name": "Рефрижераторная установка", "short_name": "RV"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 2000000.0, "arenda": true, "razmer": "40 фут  High Cube 2.9", "skidka": true, "status": "Арендован", "publish": false, "opisanie": "описание контейнера, текст длиной 300 символов, это сделано по согласованию с менеджерами, изначально длина была 100 символов, попросили увеличить до 200, сделали с запасом 300 описание контейнера, текст длиной 300 символов, это сделано по согласованию с менеджерами, изначально длина была 100 символов", "prodazha": true, "ves-tary": 12344.0, "garantiia": true, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 1800000.0, "vmestimost-v-m3": 50.0, "nomer-konteinera": "rftu-123456", "tsena-so-skidkoi": 1800000.0, "opisanie-garantii": "гарантия на петли и запорный механизм", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "12.22x33.33x44.44", "stoimost-arendy-v-god": 999999.0, "stoimost-arendy-v-mesiats": "120000", "akt-tekhnicheskogo-sostoianiia": "media/DSCN7823.jpeg"}	0	14	1	2017-11-22 05:07:32.26938+00	2017-12-08 04:00:23.793601+00	refrizheratornaia-ustanovka-40-fut-high-cube-29-carrier-69nt40
258	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2003.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 460000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Свободен", "publish": false, "opisanie": "Рефконтейнер Carrier 20RE", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 3.0, "obshchaia-tsena": 450000.0, "vmestimost-v-m3": 28.0, "nomer-konteinera": "KKTU, 6074934", "tsena-so-skidkoi": 450000.0, "opisanie-garantii": "На двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "5.51x2.28x2.26", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2017-11-15 06:56:05.276488+00	2017-12-08 04:00:23.805744+00	refrizheratornyi-20-fut-carrier-69nt40-1
294	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 650000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Арендован", "publish": true, "opisanie": "Стоиомсть указана без учета НДС. С НДС -\\n 725 000 руб. Индивидуальный подход к клиентам. Предоставляем скидки!", "prodazha": true, "ves-tary": 4740.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 650000.0, "vmestimost-v-m3": 67.0, "nomer-konteinera": "RFTU 1701114", "tsena-so-skidkoi": 650000.0, "opisanie-garantii": "На двигтель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-12-25 00:18:30.687631+00	2018-03-23 02:20:59.26622+00	refrizheratornyi-40-fut-high-cube-29-carrier-69nt40-1-1-1-1
282	Рефрижераторный 20 футов High Cube 2.9 Carrier 69NT40	{"god": 2010.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 0.0, "arenda": true, "razmer": "20 футов High Cube 2.9", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Контейнер", "prodazha": false, "ves-tary": 10000.0, "garantiia": true, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 10.0, "obshchaia-tsena": "1000", "vmestimost-v-m3": 1000.0, "nomer-konteinera": "123", "tsena-so-skidkoi": 0.0, "opisanie-garantii": "на всё", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0.12x2.11x12.12", "stoimost-arendy-v-god": 10000.0, "stoimost-arendy-v-mesiats": "1000"}	0	79	1	2017-11-27 08:23:59.219736+00	2018-11-15 11:33:58.578285+00	refrizheratornyi-20-futov-high-cube-29-carrier-69nt40
279	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 770000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Продан", "publish": true, "opisanie": "Рефконтейнер Carrier Scrooll, 40фут.\\nКонтейнер в отличном состоянии, полностью проверен и готов к работе.\\nГТД (грузовая таможенная декларация) прилагается.", "prodazha": true, "ves-tary": 4800.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 770000.0, "vmestimost-v-m3": 67.0, "nomer-konteinera": "GESU 9470817", "tsena-so-skidkoi": 770000.0, "opisanie-garantii": "На двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "11.65x2.28x2.55", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2017-11-24 02:46:32.949311+00	2018-04-11 04:42:02.68657+00	refrizheratornyi-40-fut-high-cube-29-carrier-69nt40-1
304	Рефрижераторный 20 фут Daikin LXE	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Днепровска, 25"}, "model": "Daikin LXE", "tsena": 540000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Продан", "publish": true, "opisanie": "Рефконтейнер Daikin 20фут. - 2008год.\\nВ отличном техническом состоянии. Не использовался в РФ.", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 520000.0, "vmestimost-v-m3": 28.0, "nomer-konteinera": "RFTU 1711149", "tsena-so-skidkoi": 520000.0, "opisanie-garantii": "Гарантия три месяца на двигатель-компрессор.", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "5.51x2.28x2.26", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2017-12-25 01:36:31.088962+00	2018-01-23 00:59:45.063806+00	refrizheratornyi-20-fut-daikin-lxe-1-1-1
281	Рефрижераторный 20 фут -	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 600000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Продан", "publish": false, "opisanie": "123123123", "prodazha": true, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "drugoe-mesto": "123", "srok-garantii": 0.0, "obshchaia-tsena": 590000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "123", "tsena-so-skidkoi": 590000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	1	1	2017-11-24 03:31:46.498695+00	2017-12-08 04:00:23.824059+00	refrizheratornyi-20-fut-daikin-lxe-1
269	Рефрижераторный 20 фут Carrier 69NT40	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 2131.0, "arenda": true, "razmer": "20 фут", "skidka": true, "status": "Арендован", "publish": false, "opisanie": "321312321", "prodazha": true, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 0.0, "obshchaia-tsena": 213.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "31321312", "tsena-so-skidkoi": 213.0, "ispolzovalsia-v-rf": true, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 312.0, "stoimost-arendy-v-mesiats": "231", "akt-tekhnicheskogo-sostoianiia": "media/photo_2017-11-22_15-51-50 (4).jpeg"}	0	2	1	2017-11-21 07:33:39.005214+00	2017-12-08 04:00:23.829606+00	refrizheratornyi-20-fut-carrier-69nt40-1-1
270	Рефрижераторный 20 фут Mitsubishi	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Mitsubishi", "tsena": 1321.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Продан", "publish": false, "opisanie": "ТЕСТОВЫЙ КОНТЕЙНЕР", "prodazha": true, "ves-tary": 0.0, "garantiia": false, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 0.0, "obshchaia-tsena": 1321.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "выфвфывф", "tsena-so-skidkoi": 1321.0, "ispolzovalsia-v-rf": true, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0", "akt-tekhnicheskogo-sostoianiia": "media/_DSC0565.jpeg"}	0	2	1	2017-11-22 01:01:43.630575+00	2017-12-12 07:36:39.282162+00	refrizheratornyi-20-fut-mitsubishi
283	Рефрижераторный 20 фут -	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 0.0, "arenda": true, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Контейнер", "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "drugoe-mesto": "Ковальчука, 5", "srok-garantii": 0.0, "obshchaia-tsena": "10000", "vmestimost-v-m3": 0.0, "nomer-konteinera": "1000", "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 100000.0, "stoimost-arendy-v-mesiats": "10000"}	0	79	1	2017-11-27 08:25:50.108874+00	2018-11-09 08:08:08.125642+00	refrizheratornyi-20-fut-daikin-lxe-1-1
295	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 650000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Арендован", "publish": true, "opisanie": "Стоимость указана без учета НДС. С НДС -\\n 725 000 руб. Индивидульный подход к клиентам. Предоставляем скидки!", "prodazha": true, "ves-tary": 4800.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 650000.0, "vmestimost-v-m3": 67.0, "nomer-konteinera": "RFTU 1701120", "tsena-so-skidkoi": 650000.0, "opisanie-garantii": "На двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-12-25 00:22:40.184081+00	2018-03-23 02:20:36.375967+00	refrizheratornyi-20-fut-mitsubishi-1-1-1
266	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2004.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 480000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Резервный фонд", "publish": true, "opisanie": "Контейнер находится в России с 10.11.2017, цена указана с учетом НДС.", "prodazha": true, "ves-tary": 4730.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 480000.0, "vmestimost-v-m3": 67.9, "nomer-konteinera": "GESU 9165201", "tsena-so-skidkoi": 480000.0, "opisanie-garantii": "На двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "11.66x2.28x2.56", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-11-20 05:13:06.052705+00	2018-03-23 02:22:32.279712+00	refrizheratornyi-40-fut-high-cube-29-carrier-69nt40-1-1
275	Вентилируемый 40 фут  High Cube 2.9 Daikin ZeSTIA	{"god": 0.0, "tip": {"name": "Вентилируемый", "short_name": "TC"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Daikin ZeSTIA", "tsena": 0.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Свободен", "publish": false, "opisanie": "this.sum = b.activate_sum;\\n        this.bonus = b.discount;\\n        this.bonusType = b.type === 'total' ? this.types[0] : this.types[1];\\n        this.text = b.description;", "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "б/у", "drugoe-mesto": "Ковальчука, 5", "srok-garantii": 0.0, "obshchaia-tsena": 0.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "1223", "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	79	1	2017-11-23 03:09:19.269623+00	2017-12-08 04:00:23.842155+00	ventiliruemyi-40-fut-high-cube-29-daikin-zestia
312	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2006.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 580000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Продан", "publish": true, "opisanie": "Стоимость без НДС. Скидки оговариваются индивидуально!!!", "prodazha": true, "ves-tary": 4700.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": "0", "vmestimost-v-m3": 67.0, "nomer-konteinera": "NYKU 7007454", "tsena-so-skidkoi": 0.0, "opisanie-garantii": "на двигатель- компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-01-31 07:55:48.484181+00	2018-03-23 02:16:17.466517+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1
280	Рефрижераторный 20 фут -	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 0.0, "arenda": true, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": false, "opisanie": "123", "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "drugoe-mesto": "1231", "srok-garantii": 0.0, "obshchaia-tsena": 999999.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "123123123", "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 999999.0, "stoimost-arendy-v-mesiats": "999999"}	0	1	1	2017-11-24 03:21:44.457655+00	2017-12-08 04:00:23.865834+00	refrizheratornyi-20-fut-mitsubishi-1
310	Рефрижераторный 20 фут -	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 0.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": false, "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "srok-garantii": 0.0, "obshchaia-tsena": "0", "vmestimost-v-m3": 0.0, "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	84	1	2018-01-29 07:30:50.209632+00	2018-01-29 07:30:50.224087+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1
265	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2003.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 430000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": true, "status": "Свободен", "publish": false, "opisanie": "Рефконтейнер без пробега по России, вышел с таможни 17.11.2017", "prodazha": true, "ves-tary": 4800.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 410000.0, "vmestimost-v-m3": 67.0, "nomer-konteinera": "NYKU 7038218", "tsena-so-skidkoi": 410000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "27000"}	0	23	1	2017-11-20 05:12:17.516481+00	2018-03-23 02:25:28.880461+00	refrizheratornyi-40-fut-high-cube-29-carrier-69nt40-1-1-1
314	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 650000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Свободен", "publish": false, "opisanie": "КОнтейнер без пробега по России! \\nОфициальные документы! Индивидуальный подход!", "prodazha": true, "ves-tary": 4670.0, "garantiia": true, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 650000.0, "vmestimost-v-m3": 67.6, "nomer-konteinera": "KKFU 6701140", "tsena-so-skidkoi": 650000.0, "opisanie-garantii": "двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-09 08:56:41.111324+00	2018-04-11 02:36:38.689006+00	refrizheratornyi-40-fut-high-cube-29-carrier-69nt40-1-1-1-1-1-1-1-1-1
311	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2006.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 580000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Продан", "publish": true, "opisanie": "Стоимость без НДС. Скидки оговариваются индивидуально!!!", "prodazha": true, "ves-tary": 4500.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 580000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "NYKU 7003628", "tsena-so-skidkoi": 580000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-01-31 05:48:37.036276+00	2018-04-05 03:05:03.853408+00	refrizheratornyi-40-fut-high-cube-29-carrier-69nt40-1-1-1-1-1-1-1-1
330	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 590000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Продан", "publish": true, "opisanie": "Рефагрегат Carrier THINLINE - 2008 год. \\nМодель высочайшего качества и надежности - проверено на практике.\\nСконструирован для работы в суровых условиях.\\nБесплатная доставка!!! Сахалин, Магадан, Камчатка.", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 555000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "OOLU 3853956", "tsena-so-skidkoi": 555000.0, "opisanie-garantii": "Гарантия 3(три) месяца на двигатель-компрессор.", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "5.51x2.28x2.27", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2018-04-13 00:58:31.11055+00	2018-10-18 17:19:20.977786+00	refrizheratornyi-20-fut-carrier-69nt40-1-1-1-1-1-1-1-1-1-1
264	Рефрижераторный 20 фут Daikin ZeSTIA	{"god": 2001.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Daikin ZeSTIA", "tsena": 1111111.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Продан", "publish": false, "opisanie": "тестовый контейнер", "prodazha": true, "ves-tary": 2345.0, "garantiia": false, "sostoianie": "б/у", "drugoe-mesto": "Ковальчука, 5", "srok-garantii": 0.0, "obshchaia-tsena": 1000000.0, "vmestimost-v-m3": 12.3, "nomer-konteinera": "123 wer", "tsena-so-skidkoi": 1000000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "11.11x22.22x3.33", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0", "akt-tekhnicheskogo-sostoianiia": "media/2017-09-19_13-20-25.png"}	0	14	1	2017-11-17 01:46:46.099571+00	2017-12-08 04:00:23.875999+00	refrizheratornyi-20-fut-daikin-zestia
267	Рефрижераторная установка 40 фут Thermo King	{"god": 2017.0, "tip": {"name": "Рефрижераторная установка", "short_name": "RV"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Thermo King", "tsena": 800000.0, "arenda": false, "razmer": "40 фут", "skidka": true, "status": "Свободен", "publish": false, "opisanie": "Рефустановка ThermoKing,MAGNUM PLUS,МР4000 НОВАЯ. \\nДля контейнеров 10,20,40 футов.\\nТемпературный режим -35 +25 \\nФреон R-404.", "prodazha": true, "ves-tary": 800.0, "garantiia": true, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 365.0, "obshchaia-tsena": 720000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "MAGNUM PLUS,МР4000", "tsena-so-skidkoi": 720000.0, "opisanie-garantii": "На двигатель-компрессор, конденсатор, вентиляторы испарителя и конденсатора.", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2017-11-21 01:25:11.167236+00	2017-12-27 06:36:44.229756+00	refrizheratornaia-ustanovka-40-fut-thermo-king
306	Рефрижераторный 20 фут Daikin LXE	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Daikin LXE", "tsena": 540000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Резервный фонд", "publish": true, "opisanie": "Рефконтейнер Daikin - 2008год.", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 520000.0, "vmestimost-v-m3": 28.0, "nomer-konteinera": "RFTU 1711160", "tsena-so-skidkoi": 520000.0, "opisanie-garantii": "Гарантия три месяца на двигатель-компресор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "5.51x2.28x2.26", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2017-12-25 03:16:10.751479+00	2018-02-16 03:54:31.489643+00	refrizheratornyi-20-fut-daikin-lxe-1-1-1-1-1
285	Рефрижераторный 20 фут -	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 0.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": false, "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "srok-garantii": 0.0, "obshchaia-tsena": 0.0, "vmestimost-v-m3": 0.0, "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	14	1	2017-11-29 03:20:16.317042+00	2017-12-08 04:00:23.90423+00	refrizheratornyi-20-fut-mitsubishi-1-1
305	Рефрижераторный 20 фут Daikin LXE	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Daikin LXE", "tsena": 540000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Свободен", "publish": true, "opisanie": "Рефконтейнер Daikin - 2008год.", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 520000.0, "vmestimost-v-m3": 28.0, "nomer-konteinera": "RFTU 1711154", "tsena-so-skidkoi": 520000.0, "opisanie-garantii": "Гарантия на двигатель-компрессор.", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "5.51x2.28x2.26", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2017-12-25 01:55:02.453484+00	2018-04-11 02:57:43.752842+00	refrizheratornyi-20-fut-daikin-lxe-1-1-1-1
272	Рефрижераторный 20 фут Daikin LXE	{"god": 2014.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Daikin LXE", "tsena": 900000.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Работаем нал, б/нал, с НДС и без НДС. Весь пакет документов.", "prodazha": true, "ves-tary": 2980.0, "garantiia": true, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 900000.0, "vmestimost-v-m3": 28.3, "nomer-konteinera": "CRLU 3166974", "tsena-so-skidkoi": 900000.0, "opisanie-garantii": "на двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "5.51x2.28x2.27", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-11-23 00:59:07.377427+00	2018-05-15 00:46:36.097899+00	refrizheratornyi-20-fut-carrier-69nt40-1-1-1
298	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2007.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 550000.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Продан", "publish": true, "opisanie": "Индивидуальный подход к клиентам! Предусмотрены скидки!", "prodazha": true, "ves-tary": 3100.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 550000.0, "vmestimost-v-m3": 28.0, "nomer-konteinera": "RFTU 1711112", "tsena-so-skidkoi": 550000.0, "opisanie-garantii": "на двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-12-25 00:45:23.956863+00	2018-03-23 02:20:06.827086+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1
276	Рефрижераторная установка 40 фут Daikin LXE	{"god": 2013.0, "tip": {"name": "Рефрижераторная установка", "short_name": "RV"}, "gorod": {"gorod": "Владивосток", "address": "Днепровска, 25"}, "model": "Daikin LXE", "tsena": 750000.0, "arenda": false, "razmer": "40 фут", "skidka": false, "status": "Свободен", "publish": false, "prodazha": true, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 0.0, "obshchaia-tsena": 750000.0, "vmestimost-v-m3": 0.0, "tsena-so-skidkoi": 750000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-11-23 05:53:11.010617+00	2017-12-08 04:00:23.885396+00	refrizheratornaia-ustanovka-40-fut-daikin-lxe-1
263	Рефрижераторная установка 20 фут Daikin ZeSTIA 123	{"god": 0.0, "tip": {"name": "Рефрижераторная установка", "short_name": "RV"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 1221.0, "arenda": true, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": false, "opisanie": "Описание bsdj jsd dkjd kjsd fkjhsd hf sdhj fjshd fjhds fjhsd jhf sdjh fjhsd fjhs dfhjs dfhj sjdhf jhsd fjhs djh sdjh fjshd fjhs djhf sjhd jhsd fjhsd fjhs dfjhs jhsd fjhds jhs dfjh sdjh sdjhf sjh jhs jshd jhs djh", "prodazha": true, "ves-tary": 0.0, "garantiia": true, "sostoianie": "Новый", "drugoe-mesto": "Енисейская", "drugaia-model": "Daikin ZeSTIA 123", "srok-garantii": 20.0, "obshchaia-tsena": 1221122.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "1234", "tsena-so-skidkoi": 1221122.0, "opisanie-garantii": "Гарантия, да-да. Она есть! jshbfjhs djhs djh sjhd hjsd  hsdbefj shdsd hsd udsu hd iushd iusdh iush diuh siduh siduhsiu duisdh iusdh iusdh iusdh", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 10000.0, "stoimost-arendy-v-mesiats": "1222"}	0	3	1	2017-11-16 22:08:36.565561+00	2017-12-08 04:00:23.895034+00	refrizheratornaia-ustanovka-20-fut-daikin-zestia-123
287	Рефрижераторная установка 40 фут Daikin LXE	{"god": 2015.0, "tip": {"name": "Рефрижераторная установка", "short_name": "RV"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Daikin LXE", "tsena": 775000.0, "arenda": false, "razmer": "40 фут", "skidka": true, "status": "Продан", "publish": true, "opisanie": "Рефустановка Daikin, новая с завода.\\nПодойдет на любой 20/40 фут. рефконтейнер.\\nРежимы работы +25 -25 градусов цельсия.\\nФреон - 134а", "prodazha": true, "ves-tary": 416.0, "garantiia": true, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 365.0, "obshchaia-tsena": 750000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "LXE 10 E 136", "tsena-so-skidkoi": 750000.0, "opisanie-garantii": "Гарантия на все основные агрегаты: двигатель компрессор, конденсатор, вентиляторы испарителя и конденсатора.", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2017-11-29 23:43:06.327105+00	2018-04-15 01:31:27.227713+00	refrizheratornaia-ustanovka-40-fut-daikin-lxe
278	Рефрижераторный 40 фут  High Cube 2.9 Thermo King Magnum	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "tsena": 690000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": true, "status": "Свободен", "publish": true, "opisanie": "Выпускаемая с 2003 года модель рефрижераторного контейнера, оснащенная компрессором спирального типа Copeland ® Digital Scroll ™, обладающим высокой устойчивостью к скачкам напряжения и перепадам температуры. Рефконтейнер работает с использованием хладагента R-404A, благодаря чему обеспечивается охлаждение вплоть до -35 градусов Цельсия.", "prodazha": true, "ves-tary": 4500.0, "garantiia": false, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "drugaia-model": "Thermo King Magnum", "srok-garantii": 90.0, "obshchaia-tsena": 680000.0, "vmestimost-v-m3": 67.5, "nomer-konteinera": "RRSU 1735515", "tsena-so-skidkoi": 680000.0, "opisanie-garantii": "на двигатель", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "11.66x2.28x2.56", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-11-24 01:44:06.39878+00	2018-08-09 06:45:22.23195+00	refrizheratornyi-40-fut-high-cube-29-thermo-king-magnum-scrool-1
329	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 585000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Продан", "publish": true, "opisanie": "Модель Carrier THINLINE - самый надежный агрегат, проверено на практике.\\nПростое управление температурой в отсеке до десятых градуса +29 - 29 С.\\nЛучшая цена!!! Скидка - 35 000 рублей!!! при оплате контейнера до прибытия в порт Владивосток.", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Контейнер в пути из Ю. Кореи", "srok-garantii": 90.0, "obshchaia-tsena": 550000.0, "vmestimost-v-m3": 28.5, "nomer-konteinera": "OOLU 3853684", "tsena-so-skidkoi": 550000.0, "opisanie-garantii": "Гарантия на двигатель-компрессор три месяца.", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "5.51x2.28x2.27", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2018-04-12 00:54:01.028813+00	2018-11-05 11:03:11.47916+00	refrizheratornyi-20-fut-carrier-69nt40-1-1-1-1-1-1-1-1-1
299	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2007.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 550000.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Продан", "publish": true, "opisanie": "Индивидуальный подход к клиентам! Предусмотрены скидки!", "prodazha": true, "ves-tary": 3100.0, "garantiia": false, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 0.0, "obshchaia-tsena": 550000.0, "vmestimost-v-m3": 28.0, "nomer-konteinera": "RFTU 1711128", "tsena-so-skidkoi": 550000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-12-25 00:48:24.176896+00	2018-01-17 02:48:57.996139+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1
273	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2006.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 500000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Свободен", "publish": true, "opisanie": "Весь пакет документов. Оплата нал, б/н, с НДС, без НДС", "prodazha": true, "ves-tary": 2940.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 490000.0, "vmestimost-v-m3": 28.2, "nomer-konteinera": "TRIU 6610467", "tsena-so-skidkoi": 490000.0, "opisanie-garantii": "Двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "5.15x2.28x2.27", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-11-23 01:16:09.982969+00	2017-12-08 04:00:23.913959+00	refrizheratornyi-20-fut-carrier-69nt40-1-1-1-1
307	Рефрижераторный 20 фут -	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 0.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": false, "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "srok-garantii": 0.0, "obshchaia-tsena": "0", "vmestimost-v-m3": 0.0, "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	84	1	2018-01-29 07:30:34.062036+00	2018-01-29 07:30:34.068083+00	refrizheratornyi-20-fut-
331	Рефрижераторный 20 фут -	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 0.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": false, "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "srok-garantii": 0.0, "obshchaia-tsena": "0", "vmestimost-v-m3": 0.0, "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	86	1	2018-04-23 05:10:21.553828+00	2018-04-23 05:10:21.567775+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1
297	Рефрижераторная установка 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2016.0, "tip": {"name": "Рефрижераторная установка", "short_name": "RV"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 930000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Продан", "publish": true, "opisanie": "Индивидуальный подход к клиентам. Предоставляем скидки!", "prodazha": true, "ves-tary": 700.0, "garantiia": true, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 365.0, "obshchaia-tsena": 930000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "541-500", "tsena-so-skidkoi": 930000.0, "opisanie-garantii": "с момента запуска рефустановки.", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-12-25 00:33:17.664296+00	2018-03-23 02:16:46.663495+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1
300	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2007.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 550000.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Продан", "publish": true, "opisanie": "Индивидуальный подход к клиентам! Предусмотрены скидки!", "prodazha": true, "ves-tary": 3100.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 550000.0, "vmestimost-v-m3": 28.0, "nomer-konteinera": "RFTU 1711133", "tsena-so-skidkoi": 550000.0, "opisanie-garantii": "на двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-12-25 00:51:08.277477+00	2018-03-23 02:16:01.429609+00	refrizheratornyi-20-fut-carrier-69nt40-1-1-1-1-1-1
296	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 650000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Арендован", "publish": true, "opisanie": "Стоимость указана без НДС. С НДС - 725000 руб Индивидуальный подход. Предоставляются скидки!", "prodazha": true, "ves-tary": 4740.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 650000.0, "vmestimost-v-m3": 67.6, "nomer-konteinera": "RFTU 1701135", "tsena-so-skidkoi": 650000.0, "opisanie-garantii": "На двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-12-25 00:30:55.735218+00	2018-03-23 02:18:17.65765+00	refrizheratornyi-40-fut-high-cube-29-carrier-69nt40-1-1-1-1-1
332	Сухогруз 20 фут -	{"god": 2013.0, "tip": {"name": "Сухогруз", "short_name": "GP"}, "gorod": {"gorod": "Владивосток", "address": "Днепровска, 25"}, "model": "-", "tsena": 180000.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Продан", "publish": true, "opisanie": "Контейнер в отличном состоянии, без пробега по РФ", "prodazha": true, "ves-tary": 3890.0, "garantiia": false, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 0.0, "obshchaia-tsena": "0", "vmestimost-v-m3": 76.0, "nomer-konteinera": "6943084", "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	88	1	2018-11-15 07:39:22.662468+00	2018-11-15 07:45:33.845205+00	sukhogruz-20-fut--1
318	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 650000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Без пробега по России! Индивидуальный подход! При покупке от 2-х контейнеров предусмотрены скидки!", "prodazha": true, "ves-tary": 4740.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 650000.0, "vmestimost-v-m3": 67.7, "nomer-konteinera": "KKFU 6711730", "tsena-so-skidkoi": 650000.0, "opisanie-garantii": "Двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-10 02:54:59.776865+00	2018-04-11 02:35:17.777862+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1
315	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 650000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Без пробега по России! Официальные документы! \\nИндивидуальный подход!", "prodazha": true, "ves-tary": 4670.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 650000.0, "vmestimost-v-m3": 67.6, "nomer-konteinera": "KKFU 6702297", "tsena-so-skidkoi": 650000.0, "opisanie-garantii": "двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-09 08:58:28.628501+00	2018-04-27 09:06:59.291936+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1
320	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 650000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Без пробега по России! Индивидуальный подход! При покупке от 2-х контейнеров предусмотрены скидки!", "prodazha": true, "ves-tary": 4740.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 650000.0, "vmestimost-v-m3": 67.7, "nomer-konteinera": "KKFU 6995404", "tsena-so-skidkoi": 650000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-10 02:59:09.827607+00	2018-04-11 02:34:40.952911+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1
308	Рефрижераторный 20 фут -	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 0.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": false, "prodazha": false, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "srok-garantii": 0.0, "obshchaia-tsena": "0", "vmestimost-v-m3": 0.0, "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	84	1	2018-01-29 07:30:40.438222+00	2018-01-29 07:30:40.449969+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1
286	Сухогруз 40 фут  High Cube 2.9 -	{"god": 0.0, "tip": {"name": "Сухогруз", "short_name": "GP"}, "gorod": {"gorod": "Москва", "address": "ул. Московская, 1"}, "model": "-", "tsena": 120000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Данный контейнер находится в Москве по адресу: Симферопольское шоссе, д. 22 с. 1", "prodazha": true, "ves-tary": 3820.0, "garantiia": false, "sostoianie": "б/у", "drugoe-mesto": "ул.Симферопольское шоссе, 22 с. 1", "srok-garantii": 0.0, "obshchaia-tsena": 120000.0, "vmestimost-v-m3": 67.0, "nomer-konteinera": "HJCU 1964806", "tsena-so-skidkoi": 120000.0, "ispolzovalsia-v-rf": true, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	80	1	2017-11-29 11:35:41.77316+00	2018-11-17 18:16:47.649193+00	sukhogruz-40-fut-high-cube-29--1
301	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2002.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 440000.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Арендован", "publish": true, "opisanie": "Индивидуальный подход к клиентам! Стоимость указана с учетом НДС.", "prodazha": true, "ves-tary": 4700.0, "garantiia": false, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 0.0, "obshchaia-tsena": "0", "vmestimost-v-m3": 67.0, "nomer-konteinera": "SEBU 8333903", "tsena-so-skidkoi": 0.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-12-25 00:54:13.31096+00	2018-03-23 02:19:37.326363+00	refrizheratornyi-20-fut-carrier-69nt40-1-1-1-1-1-1-1
303	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 680000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": true, "status": "Свободен", "publish": true, "opisanie": "Рефконтейнер Carrier 2008 год. Контейнер в отличном техническом состоянии, корпус полностью герметичен.\\n\\nСтоимость указана без НДС!", "prodazha": true, "ves-tary": 4800.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 648000.0, "vmestimost-v-m3": 67.0, "nomer-konteinera": "RFTU 1701156", "tsena-so-skidkoi": 648000.0, "opisanie-garantii": "Гарантия на основной агрегат (компрессор)", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "11.65x2.28x2.55", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2017-12-25 01:26:25.546015+00	2018-04-27 09:06:13.009019+00	refrizheratornyi-40-fut-high-cube-29-carrier-69nt40-1-1-1-1-1-1-1
322	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 585000.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Без пробега по России! Индивидуальный подход! При покупке от 2-х контейнеров предусмотрены скидки!", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 585000.0, "vmestimost-v-m3": 28.5, "nomer-konteinera": "OOLU 3851932", "tsena-so-skidkoi": 585000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-10 04:26:31.108498+00	2018-10-27 00:21:30.69057+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1
324	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 585000.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Без пробега по России! Индивидуальный подход! При покупке от 2-х контейнеров предусмотрены скидки!", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 585000.0, "vmestimost-v-m3": 28.5, "nomer-konteinera": "OOLU 3852179", "tsena-so-skidkoi": 585000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-10 06:37:08.128433+00	2018-04-11 02:24:27.333213+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1
277	Рефрижераторный 40 фут  High Cube 2.9 Thermo King Magnum Scrool	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 690000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": true, "status": "Свободен", "publish": true, "opisanie": "Выпускаемая с 2003 года модель рефрижераторного контейнера, оснащенная компрессором спирального типа Copeland ® Digital Scroll ™, обладающим высокой устойчивостью к скачкам напряжения и перепадам температуры. Рефконтейнер работает с использованием хладагента R-404A, благодаря чему обеспечивается охлаждение вплоть до -35 градусов Цельсия.", "prodazha": true, "ves-tary": 4500.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "drugaia-model": "Thermo King Magnum Scrool", "srok-garantii": 90.0, "obshchaia-tsena": 680000.0, "vmestimost-v-m3": 67.5, "nomer-konteinera": "RRSU 1734479", "tsena-so-skidkoi": 680000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "11.66x2.28x2.56", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-11-24 01:34:06.895277+00	2018-08-09 06:45:31.864594+00	refrizheratornyi-40-fut-high-cube-29-thermo-king-magnum-scrool
325	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 585000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Свободен", "publish": true, "opisanie": "При покупке контейнера за наличный расчет предусмотрена скидка!\\n\\nБез пробега по России! \\nИндивидуальный подход!", "prodazha": true, "ves-tary": 3190.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 555000.0, "vmestimost-v-m3": 28.6, "nomer-konteinera": "OOLU 3852651", "tsena-so-skidkoi": 555000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-10 08:05:44.024911+00	2018-11-14 06:34:44.224678+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1
284	Рефрижераторная установка 20 фут Daikin LXE	{"god": 0.0, "tip": {"name": "Рефрижераторная установка", "short_name": "RV"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Daikin LXE", "tsena": 775000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Свободен", "publish": true, "opisanie": "Рефрежераторный агрегат DAIKIN LXE 10E выпускаемый одноименной Японской корпорацией зарекомендовал себя, как надежный и неприхотливый в эксплуатации рефконтейнер.\\nПри его производстве использованы самые передовые инновационные технологии:\\nКомпрессор - спиральный (5,5 кВт). Данная модель защищена от гидроударов, а запас мощности позволяет справляться с любыми тепловыми нагрузками.\\nБлагодаря высочайшей надежности агрегата и низкой стоимости запасных частей потребители DAIKIN имеют самые низкие изд", "prodazha": true, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 0.0, "obshchaia-tsena": 760000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "10Е", "tsena-so-skidkoi": 760000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2017-11-28 05:50:04.444073+00	2018-08-28 09:51:52.815056+00	refrizheratornaia-ustanovka-20-fut-daikin-lxe
327	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 585000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Свободен", "publish": true, "opisanie": "Carrier Thinline, модель высочайшего качества и надежности.\\nОборудован компрессором 06D с увеличенным ходом поршней для лучшей охлаждающей способности.  \\nПодготовлен к работе в экстремальных условиях севера.\\nСКИДКА - 35 000 рублей при условии предварительной оплаты контейнера.", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 550000.0, "vmestimost-v-m3": 28.5, "nomer-konteinera": "OOLU 3853236", "tsena-so-skidkoi": 550000.0, "opisanie-garantii": "Гарантия на основной агрегат три месяца.", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2018-04-11 01:31:14.375457+00	2018-04-12 01:42:21.432267+00	refrizheratornyi-20-fut-carrier-69nt40-1-1-1-1-1-1-1-1
321	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 585000.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Без пробега по России! Индивидуальный подход! При покупке от 2-х контейнеров предусмотрены скидки!", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 585000.0, "vmestimost-v-m3": 28.5, "nomer-konteinera": "OOLU 3851737", "tsena-so-skidkoi": 585000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-10 04:04:09.508937+00	2018-04-11 02:25:05.197812+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1
326	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 585000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Свободен", "publish": true, "opisanie": "Скидка предоставляется при покупке от 2-х контейнеров и более! \\n\\nВсе контейнеры без пробега по России! Индивидуальный подход!", "prodazha": true, "ves-tary": 3190.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 550000.0, "vmestimost-v-m3": 28.6, "nomer-konteinera": "OOLU 3853030", "tsena-so-skidkoi": 550000.0, "opisanie-garantii": "Гарантия распространяется на основной агрегат.", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-10 08:13:00.567999+00	2018-11-11 13:20:54.806265+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1
313	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 650000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Свободен", "publish": false, "opisanie": "Без пробегапо России! Официальные документы! \\nиндивидуальный подход!", "prodazha": true, "ves-tary": 4670.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 650000.0, "vmestimost-v-m3": 67.6, "nomer-konteinera": "KKFU 6663040", "tsena-so-skidkoi": 650000.0, "opisanie-garantii": "Двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0.12x0.24x0.29", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-09 08:51:57.68363+00	2018-04-11 02:36:53.554465+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1
317	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 650000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Без пробега по России! Индивидуальный подход! При покупке от 2-х контейнеров предусмотрены скидки!", "prodazha": true, "ves-tary": 4670.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 650000.0, "vmestimost-v-m3": 67.6, "nomer-konteinera": "KKFU 6706543", "tsena-so-skidkoi": 650000.0, "opisanie-garantii": "Двигатель-компрессор", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-09 09:05:19.0539+00	2018-06-13 08:21:53.125125+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1
302	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 680000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": true, "status": "Продан", "publish": true, "opisanie": "Рефконтейнер Carrier-2008год.\\nПроизведена полная диагностика рефагрегата. Контейнер готов к работе. (t +25 -25)\\nСтоимость указана без НДС!", "prodazha": true, "ves-tary": 4800.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 648000.0, "vmestimost-v-m3": 67.0, "nomer-konteinera": "RFTU 1701140", "tsena-so-skidkoi": 648000.0, "opisanie-garantii": "Гарантия три месяца на двигатель-компрессор.", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "11.65x2.28x2.55", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2017-12-25 01:14:32.272846+00	2018-04-11 02:56:06.393615+00	refrizheratornyi-40-fut-high-cube-29-carrier-69nt40-1-1-1-1-1-1
328	Рефрижераторный 20 фут -	{"god": 0.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "-", "tsena": 25000.0, "arenda": false, "razmer": "20 фут", "skidka": true, "status": "Продан", "publish": true, "opisanie": "Производим качественную покраску рефконтейнеров с соблюдением технологии. Используется специальная 3х компонентная краска HEMPATEX. Стоимость покраски от 20 000 рублей.\\nНанесение бренда на Ваш контейнер с логотипом компании.\\n8914-790-45-34 Владимир.", "prodazha": true, "ves-tary": 0.0, "garantiia": false, "sostoianie": "Новый", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 0.0, "obshchaia-tsena": 20000.0, "vmestimost-v-m3": 0.0, "nomer-konteinera": "HEMPATEX", "tsena-so-skidkoi": 20000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	25	1	2018-04-11 05:04:44.094015+00	2018-11-10 23:27:18.130378+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1
323	Рефрижераторный 20 фут Carrier 69NT40	{"god": 2008.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Ковальчука, 5"}, "model": "Carrier 69NT40", "tsena": 585000.0, "arenda": false, "razmer": "20 фут", "skidka": false, "status": "Свободен", "publish": true, "opisanie": "Без пробега по России! Индивидуальный подход! \\n\\nОфициальные документы!\\n\\nПри покупке от 2-х контейнеров предусмотрены скидки!", "prodazha": true, "ves-tary": 3050.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 90.0, "obshchaia-tsena": 585000.0, "vmestimost-v-m3": 28.5, "nomer-konteinera": "OOLU 3852116", "tsena-so-skidkoi": 585000.0, "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "0x0x0", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	23	1	2018-04-10 04:28:28.719592+00	2018-04-11 05:18:49.63297+00	refrizheratornyi-20-fut-mitsubishi-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1
333	Рефрижераторный 40 фут  High Cube 2.9 Carrier 69NT40	{"god": 2007.0, "tip": {"name": "Рефрижераторный", "short_name": "RF"}, "gorod": {"gorod": "Владивосток", "address": "Днепровска, 25"}, "model": "Carrier 69NT40", "tsena": 725000.0, "arenda": false, "razmer": "40 фут  High Cube 2.9", "skidka": true, "status": "Свободен", "publish": true, "opisanie": "Представляем Вашему вниманию рефрижераторный контейнер с установкой Carrier, модель 2007 года выпуска. Состояние техническое - отличное. Внешние недочеты присутствуют, но, незначительны.  Установка проверена и работает исправно во всех диапазонах температур.", "prodazha": true, "ves-tary": 4850.0, "garantiia": true, "sostoianie": "б/у", "drugoe-mesto": "Днепровская, 25", "srok-garantii": 60.0, "obshchaia-tsena": 720000.0, "vmestimost-v-m3": 68.0, "nomer-konteinera": "KKFU6991857", "tsena-so-skidkoi": 720000.0, "opisanie-garantii": "Гарантия на компрессор.", "ispolzovalsia-v-rf": false, "vnutrennie-razmery": "11.65x2.28x2.57", "stoimost-arendy-v-god": 0.0, "stoimost-arendy-v-mesiats": "0"}	0	89	1	2018-11-15 07:45:03.338761+00	2018-11-15 07:55:14.874513+00	refrizheratornyi-40-fut-high-cube-29-carrier-69nt40-1-1-1-1-1-1-1-1-1-1
\.


--
-- Name: products_realproduct_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_realproduct_id_seq', 333, true);


--
-- Data for Name: products_selectfieldproduct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_selectfieldproduct (fieldproduct_ptr_id) FROM stdin;
32
33
47
55
78
91
94
103
104
114
116
125
142
\.


--
-- Data for Name: products_selectfieldproduct_content; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_selectfieldproduct_content (id, selectfieldproduct_id, fieldproduct_id) FROM stdin;
1	32	68
2	32	69
3	33	72
4	33	70
5	33	71
6	47	35
7	47	38
8	47	39
9	47	40
10	47	57
11	47	56
12	47	77
13	55	83
14	55	84
16	55	86
17	55	87
18	55	88
19	55	89
21	91	92
22	94	96
23	94	97
24	94	95
25	103	104
29	104	113
33	33	122
35	104	124
38	47	129
39	47	130
42	91	135
43	91	136
44	91	137
45	91	138
46	55	139
47	55	140
48	91	141
49	103	142
50	142	143
\.


--
-- Name: products_selectfieldproduct_content_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_selectfieldproduct_content_id_seq', 50, true);


--
-- Data for Name: products_sortproduct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_sortproduct (id, name, translate, field_id) FROM stdin;
2	Со скидкой	so-skidkoi	101
1	По цене	po-tsene	134
\.


--
-- Name: products_sortproduct_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_sortproduct_id_seq', 2, true);


--
-- Data for Name: products_typeproduct; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_typeproduct (id, name, translate, main) FROM stdin;
1	Контейнер	konteiner	t
\.


--
-- Data for Name: products_typeproduct_fields; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_typeproduct_fields (id, typeproduct_id, fieldproduct_id) FROM stdin;
1	1	2
2	1	3
3	1	7
4	1	8
5	1	11
6	1	13
7	1	14
8	1	15
9	1	17
10	1	19
11	1	20
12	1	21
13	1	22
14	1	32
15	1	33
16	1	47
17	1	55
18	1	66
19	1	67
20	1	73
21	1	74
22	1	75
23	1	76
24	1	91
25	1	93
26	1	94
27	1	96
28	1	99
29	1	101
30	1	103
31	1	104
36	1	134
\.


--
-- Name: products_typeproduct_fields_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_typeproduct_fields_id_seq', 36, true);


--
-- Data for Name: products_typeproduct_filters; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_typeproduct_filters (id, typeproduct_id, filterproduct_id) FROM stdin;
1	1	9
2	1	10
3	1	11
4	1	12
5	1	13
6	1	15
7	1	17
8	1	18
9	1	19
10	1	20
11	1	21
12	1	22
13	1	23
14	1	24
\.


--
-- Name: products_typeproduct_filters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_typeproduct_filters_id_seq', 14, true);


--
-- Name: products_typeproduct_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_typeproduct_id_seq', 1, true);


--
-- Data for Name: products_typeproduct_sort_fields; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY products_typeproduct_sort_fields (id, typeproduct_id, sortproduct_id) FROM stdin;
1	1	1
2	1	2
\.


--
-- Name: products_typeproduct_sort_fields_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('products_typeproduct_sort_fields_id_seq', 2, true);


--
-- Name: api_bonus api_bonus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_bonus
    ADD CONSTRAINT api_bonus_pkey PRIMARY KEY (id);


--
-- Name: api_company api_company_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_company
    ADD CONSTRAINT api_company_name_key UNIQUE (name);


--
-- Name: api_company api_company_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_company
    ADD CONSTRAINT api_company_pkey PRIMARY KEY (id);


--
-- Name: api_email api_email_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_email
    ADD CONSTRAINT api_email_pkey PRIMARY KEY (id);


--
-- Name: api_email api_email_subject_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_email
    ADD CONSTRAINT api_email_subject_name_key UNIQUE (subject_name);


--
-- Name: api_email api_email_type_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_email
    ADD CONSTRAINT api_email_type_key UNIQUE (type);


--
-- Name: api_emailconfig api_emailconfig_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_emailconfig
    ADD CONSTRAINT api_emailconfig_pkey PRIMARY KEY (id);


--
-- Name: api_ordermanagermodel api_ordermanagermodel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_ordermanagermodel
    ADD CONSTRAINT api_ordermanagermodel_pkey PRIMARY KEY (id);


--
-- Name: api_ordermodel_managers_orders api_ordermodel_managers__ordermodel_id_ordermanag_e820e80f_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_ordermodel_managers_orders
    ADD CONSTRAINT api_ordermodel_managers__ordermodel_id_ordermanag_e820e80f_uniq UNIQUE (ordermodel_id, ordermanagermodel_id);


--
-- Name: api_ordermodel_managers_orders api_ordermodel_managers_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_ordermodel_managers_orders
    ADD CONSTRAINT api_ordermodel_managers_orders_pkey PRIMARY KEY (id);


--
-- Name: api_ordermodel api_ordermodel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_ordermodel
    ADD CONSTRAINT api_ordermodel_pkey PRIMARY KEY (id);


--
-- Name: api_tradeitem api_tradeitem_container_id_manager_order_id_09a3d243_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_tradeitem
    ADD CONSTRAINT api_tradeitem_container_id_manager_order_id_09a3d243_uniq UNIQUE (container_id, manager_order_id);


--
-- Name: api_tradeitem api_tradeitem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_tradeitem
    ADD CONSTRAINT api_tradeitem_pkey PRIMARY KEY (id);


--
-- Name: api_user api_user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user
    ADD CONSTRAINT api_user_email_key UNIQUE (email);


--
-- Name: api_user_groups api_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user_groups
    ADD CONSTRAINT api_user_groups_pkey PRIMARY KEY (id);


--
-- Name: api_user_groups api_user_groups_user_id_group_id_9c7ddfb5_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user_groups
    ADD CONSTRAINT api_user_groups_user_id_group_id_9c7ddfb5_uniq UNIQUE (user_id, group_id);


--
-- Name: api_user api_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user
    ADD CONSTRAINT api_user_pkey PRIMARY KEY (id);


--
-- Name: api_user_user_permissions api_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user_user_permissions
    ADD CONSTRAINT api_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: api_user_user_permissions api_user_user_permissions_user_id_permission_id_a06dd704_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user_user_permissions
    ADD CONSTRAINT api_user_user_permissions_user_id_permission_id_a06dd704_uniq UNIQUE (user_id, permission_id);


--
-- Name: api_user api_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user
    ADD CONSTRAINT api_user_username_key UNIQUE (username);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: content_gallery_image content_gallery_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY content_gallery_image
    ADD CONSTRAINT content_gallery_image_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: products_boolfieldproduct products_boolfieldproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_boolfieldproduct
    ADD CONSTRAINT products_boolfieldproduct_pkey PRIMARY KEY (fieldproduct_ptr_id);


--
-- Name: products_charfieldproduct products_charfieldproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_charfieldproduct
    ADD CONSTRAINT products_charfieldproduct_pkey PRIMARY KEY (fieldproduct_ptr_id);


--
-- Name: products_fieldproduct products_fieldproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_fieldproduct
    ADD CONSTRAINT products_fieldproduct_pkey PRIMARY KEY (id);


--
-- Name: products_fieldproduct products_fieldproduct_translate_cfdcae05_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_fieldproduct
    ADD CONSTRAINT products_fieldproduct_translate_cfdcae05_uniq UNIQUE (translate);


--
-- Name: products_filefieldproduct products_filefieldproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_filefieldproduct
    ADD CONSTRAINT products_filefieldproduct_pkey PRIMARY KEY (fieldproduct_ptr_id);


--
-- Name: products_filterproduct products_filterproduct_field_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_filterproduct
    ADD CONSTRAINT products_filterproduct_field_id_key UNIQUE (field_id);


--
-- Name: products_filterproduct products_filterproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_filterproduct
    ADD CONSTRAINT products_filterproduct_pkey PRIMARY KEY (id);


--
-- Name: products_floatfieldproduct products_floatfieldproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_floatfieldproduct
    ADD CONSTRAINT products_floatfieldproduct_pkey PRIMARY KEY (fieldproduct_ptr_id);


--
-- Name: products_objectfieldproduct_content products_objectfieldprod_objectfieldproduct_id_fi_fc8a7dda_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_objectfieldproduct_content
    ADD CONSTRAINT products_objectfieldprod_objectfieldproduct_id_fi_fc8a7dda_uniq UNIQUE (objectfieldproduct_id, fieldproduct_id);


--
-- Name: products_objectfieldproduct_content products_objectfieldproduct_content_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_objectfieldproduct_content
    ADD CONSTRAINT products_objectfieldproduct_content_pkey PRIMARY KEY (id);


--
-- Name: products_objectfieldproduct products_objectfieldproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_objectfieldproduct
    ADD CONSTRAINT products_objectfieldproduct_pkey PRIMARY KEY (fieldproduct_ptr_id);


--
-- Name: products_realproduct products_realproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_realproduct
    ADD CONSTRAINT products_realproduct_pkey PRIMARY KEY (id);


--
-- Name: products_realproduct products_realproduct_translate_35e202ce_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_realproduct
    ADD CONSTRAINT products_realproduct_translate_35e202ce_uniq UNIQUE (translate);


--
-- Name: products_selectfieldproduct_content products_selectfieldprod_selectfieldproduct_id_fi_a9716d01_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_selectfieldproduct_content
    ADD CONSTRAINT products_selectfieldprod_selectfieldproduct_id_fi_a9716d01_uniq UNIQUE (selectfieldproduct_id, fieldproduct_id);


--
-- Name: products_selectfieldproduct_content products_selectfieldproduct_content_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_selectfieldproduct_content
    ADD CONSTRAINT products_selectfieldproduct_content_pkey PRIMARY KEY (id);


--
-- Name: products_selectfieldproduct products_selectfieldproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_selectfieldproduct
    ADD CONSTRAINT products_selectfieldproduct_pkey PRIMARY KEY (fieldproduct_ptr_id);


--
-- Name: products_sortproduct products_sortproduct_field_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_sortproduct
    ADD CONSTRAINT products_sortproduct_field_id_key UNIQUE (field_id);


--
-- Name: products_sortproduct products_sortproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_sortproduct
    ADD CONSTRAINT products_sortproduct_pkey PRIMARY KEY (id);


--
-- Name: products_typeproduct_fields products_typeproduct_fie_typeproduct_id_fieldprod_6f6e6084_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_fields
    ADD CONSTRAINT products_typeproduct_fie_typeproduct_id_fieldprod_6f6e6084_uniq UNIQUE (typeproduct_id, fieldproduct_id);


--
-- Name: products_typeproduct_fields products_typeproduct_fields_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_fields
    ADD CONSTRAINT products_typeproduct_fields_pkey PRIMARY KEY (id);


--
-- Name: products_typeproduct_filters products_typeproduct_fil_typeproduct_id_filterpro_c4c8df93_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_filters
    ADD CONSTRAINT products_typeproduct_fil_typeproduct_id_filterpro_c4c8df93_uniq UNIQUE (typeproduct_id, filterproduct_id);


--
-- Name: products_typeproduct_filters products_typeproduct_filters_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_filters
    ADD CONSTRAINT products_typeproduct_filters_pkey PRIMARY KEY (id);


--
-- Name: products_typeproduct products_typeproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct
    ADD CONSTRAINT products_typeproduct_pkey PRIMARY KEY (id);


--
-- Name: products_typeproduct_sort_fields products_typeproduct_sor_typeproduct_id_sortprodu_7eea54c4_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_sort_fields
    ADD CONSTRAINT products_typeproduct_sor_typeproduct_id_sortprodu_7eea54c4_uniq UNIQUE (typeproduct_id, sortproduct_id);


--
-- Name: products_typeproduct_sort_fields products_typeproduct_sort_fields_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_sort_fields
    ADD CONSTRAINT products_typeproduct_sort_fields_pkey PRIMARY KEY (id);


--
-- Name: api_bonus_manager_id_a55d3e93; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_bonus_manager_id_a55d3e93 ON api_bonus USING btree (manager_id);


--
-- Name: api_company_name_16600b64_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_company_name_16600b64_like ON api_company USING btree (name varchar_pattern_ops);


--
-- Name: api_email_subject_name_30429308_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_email_subject_name_30429308_like ON api_email USING btree (subject_name varchar_pattern_ops);


--
-- Name: api_email_type_6e92a013_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_email_type_6e92a013_like ON api_email USING btree (type varchar_pattern_ops);


--
-- Name: api_ordermanagermodel_manager_id_f4ff64c9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_ordermanagermodel_manager_id_f4ff64c9 ON api_ordermanagermodel USING btree (manager_id);


--
-- Name: api_ordermodel_managers_orders_ordermanagermodel_id_723a9676; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_ordermodel_managers_orders_ordermanagermodel_id_723a9676 ON api_ordermodel_managers_orders USING btree (ordermanagermodel_id);


--
-- Name: api_ordermodel_managers_orders_ordermodel_id_cbfe8eb7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_ordermodel_managers_orders_ordermodel_id_cbfe8eb7 ON api_ordermodel_managers_orders USING btree (ordermodel_id);


--
-- Name: api_tradeitem_container_id_cc4b2e9c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_tradeitem_container_id_cc4b2e9c ON api_tradeitem USING btree (container_id);


--
-- Name: api_tradeitem_manager_order_id_f3d27420; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_tradeitem_manager_order_id_f3d27420 ON api_tradeitem USING btree (manager_order_id);


--
-- Name: api_user_company_id_cb885a1c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_user_company_id_cb885a1c ON api_user USING btree (company_id);


--
-- Name: api_user_email_9ef5afa6_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_user_email_9ef5afa6_like ON api_user USING btree (email varchar_pattern_ops);


--
-- Name: api_user_groups_group_id_3af85785; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_user_groups_group_id_3af85785 ON api_user_groups USING btree (group_id);


--
-- Name: api_user_groups_user_id_a5ff39fa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_user_groups_user_id_a5ff39fa ON api_user_groups USING btree (user_id);


--
-- Name: api_user_user_permissions_permission_id_305b7fea; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_user_user_permissions_permission_id_305b7fea ON api_user_user_permissions USING btree (permission_id);


--
-- Name: api_user_user_permissions_user_id_f3945d65; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_user_user_permissions_user_id_f3945d65 ON api_user_user_permissions USING btree (user_id);


--
-- Name: api_user_username_cf4e88d2_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX api_user_username_cf4e88d2_like ON api_user USING btree (username varchar_pattern_ops);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: content_gallery_image_content_type_id_6bd3c1d0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX content_gallery_image_content_type_id_6bd3c1d0 ON content_gallery_image USING btree (content_type_id);


--
-- Name: content_gallery_image_position_a9b8bae5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX content_gallery_image_position_a9b8bae5 ON content_gallery_image USING btree ("position");


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: products_fieldproduct_polymorphic_ctype_id_54b1ae27; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_fieldproduct_polymorphic_ctype_id_54b1ae27 ON products_fieldproduct USING btree (polymorphic_ctype_id);


--
-- Name: products_fieldproduct_translate_cfdcae05_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_fieldproduct_translate_cfdcae05_like ON products_fieldproduct USING btree (translate varchar_pattern_ops);


--
-- Name: products_filterproduct_translate_95ebf4c5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_filterproduct_translate_95ebf4c5 ON products_filterproduct USING btree (translate);


--
-- Name: products_filterproduct_translate_95ebf4c5_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_filterproduct_translate_95ebf4c5_like ON products_filterproduct USING btree (translate varchar_pattern_ops);


--
-- Name: products_objectfieldproduc_objectfieldproduct_id_cd86c24c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_objectfieldproduc_objectfieldproduct_id_cd86c24c ON products_objectfieldproduct_content USING btree (objectfieldproduct_id);


--
-- Name: products_objectfieldproduct_content_fieldproduct_id_aa9e8c8c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_objectfieldproduct_content_fieldproduct_id_aa9e8c8c ON products_objectfieldproduct_content USING btree (fieldproduct_id);


--
-- Name: products_realproduct_manager_id_ff39c14e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_realproduct_manager_id_ff39c14e ON products_realproduct USING btree (manager_id);


--
-- Name: products_realproduct_translate_35e202ce_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_realproduct_translate_35e202ce_like ON products_realproduct USING btree (translate varchar_pattern_ops);


--
-- Name: products_realproduct_type_id_3d83807c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_realproduct_type_id_3d83807c ON products_realproduct USING btree (type_id);


--
-- Name: products_selectfieldproduc_selectfieldproduct_id_7d56d910; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_selectfieldproduc_selectfieldproduct_id_7d56d910 ON products_selectfieldproduct_content USING btree (selectfieldproduct_id);


--
-- Name: products_selectfieldproduct_content_fieldproduct_id_0b91e313; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_selectfieldproduct_content_fieldproduct_id_0b91e313 ON products_selectfieldproduct_content USING btree (fieldproduct_id);


--
-- Name: products_sortproduct_translate_f2152565; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_sortproduct_translate_f2152565 ON products_sortproduct USING btree (translate);


--
-- Name: products_sortproduct_translate_f2152565_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_sortproduct_translate_f2152565_like ON products_sortproduct USING btree (translate varchar_pattern_ops);


--
-- Name: products_typeproduct_fields_fieldproduct_id_b407c4c4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_typeproduct_fields_fieldproduct_id_b407c4c4 ON products_typeproduct_fields USING btree (fieldproduct_id);


--
-- Name: products_typeproduct_fields_typeproduct_id_f9f1b880; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_typeproduct_fields_typeproduct_id_f9f1b880 ON products_typeproduct_fields USING btree (typeproduct_id);


--
-- Name: products_typeproduct_filters_filterproduct_id_003900e4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_typeproduct_filters_filterproduct_id_003900e4 ON products_typeproduct_filters USING btree (filterproduct_id);


--
-- Name: products_typeproduct_filters_typeproduct_id_3d077107; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_typeproduct_filters_typeproduct_id_3d077107 ON products_typeproduct_filters USING btree (typeproduct_id);


--
-- Name: products_typeproduct_sort_fields_sortproduct_id_8efc8751; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_typeproduct_sort_fields_sortproduct_id_8efc8751 ON products_typeproduct_sort_fields USING btree (sortproduct_id);


--
-- Name: products_typeproduct_sort_fields_typeproduct_id_2dd4560f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_typeproduct_sort_fields_typeproduct_id_2dd4560f ON products_typeproduct_sort_fields USING btree (typeproduct_id);


--
-- Name: products_typeproduct_translate_2666ea88; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_typeproduct_translate_2666ea88 ON products_typeproduct USING btree (translate);


--
-- Name: products_typeproduct_translate_2666ea88_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX products_typeproduct_translate_2666ea88_like ON products_typeproduct USING btree (translate varchar_pattern_ops);


--
-- Name: api_bonus api_bonus_manager_id_a55d3e93_fk_api_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_bonus
    ADD CONSTRAINT api_bonus_manager_id_a55d3e93_fk_api_user_id FOREIGN KEY (manager_id) REFERENCES api_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_ordermanagermodel api_ordermanagermodel_manager_id_f4ff64c9_fk_api_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_ordermanagermodel
    ADD CONSTRAINT api_ordermanagermodel_manager_id_f4ff64c9_fk_api_user_id FOREIGN KEY (manager_id) REFERENCES api_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_ordermodel_managers_orders api_ordermodel_manag_ordermanagermodel_id_723a9676_fk_api_order; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_ordermodel_managers_orders
    ADD CONSTRAINT api_ordermodel_manag_ordermanagermodel_id_723a9676_fk_api_order FOREIGN KEY (ordermanagermodel_id) REFERENCES api_ordermanagermodel(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_ordermodel_managers_orders api_ordermodel_manag_ordermodel_id_cbfe8eb7_fk_api_order; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_ordermodel_managers_orders
    ADD CONSTRAINT api_ordermodel_manag_ordermodel_id_cbfe8eb7_fk_api_order FOREIGN KEY (ordermodel_id) REFERENCES api_ordermodel(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_tradeitem api_tradeitem_container_id_cc4b2e9c_fk_products_realproduct_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_tradeitem
    ADD CONSTRAINT api_tradeitem_container_id_cc4b2e9c_fk_products_realproduct_id FOREIGN KEY (container_id) REFERENCES products_realproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_tradeitem api_tradeitem_manager_order_id_f3d27420_fk_api_order; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_tradeitem
    ADD CONSTRAINT api_tradeitem_manager_order_id_f3d27420_fk_api_order FOREIGN KEY (manager_order_id) REFERENCES api_ordermanagermodel(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_user api_user_company_id_cb885a1c_fk_api_company_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user
    ADD CONSTRAINT api_user_company_id_cb885a1c_fk_api_company_id FOREIGN KEY (company_id) REFERENCES api_company(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_user_groups api_user_groups_group_id_3af85785_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user_groups
    ADD CONSTRAINT api_user_groups_group_id_3af85785_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_user_groups api_user_groups_user_id_a5ff39fa_fk_api_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user_groups
    ADD CONSTRAINT api_user_groups_user_id_a5ff39fa_fk_api_user_id FOREIGN KEY (user_id) REFERENCES api_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_user_user_permissions api_user_user_permis_permission_id_305b7fea_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user_user_permissions
    ADD CONSTRAINT api_user_user_permis_permission_id_305b7fea_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: api_user_user_permissions api_user_user_permissions_user_id_f3945d65_fk_api_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY api_user_user_permissions
    ADD CONSTRAINT api_user_user_permissions_user_id_f3945d65_fk_api_user_id FOREIGN KEY (user_id) REFERENCES api_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_api_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_api_user_id FOREIGN KEY (user_id) REFERENCES api_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: content_gallery_image content_gallery_imag_content_type_id_6bd3c1d0_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY content_gallery_image
    ADD CONSTRAINT content_gallery_imag_content_type_id_6bd3c1d0_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_api_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_api_user_id FOREIGN KEY (user_id) REFERENCES api_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_boolfieldproduct products_boolfieldpr_fieldproduct_ptr_id_174dd207_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_boolfieldproduct
    ADD CONSTRAINT products_boolfieldpr_fieldproduct_ptr_id_174dd207_fk_products_ FOREIGN KEY (fieldproduct_ptr_id) REFERENCES products_fieldproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_charfieldproduct products_charfieldpr_fieldproduct_ptr_id_423a7f07_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_charfieldproduct
    ADD CONSTRAINT products_charfieldpr_fieldproduct_ptr_id_423a7f07_fk_products_ FOREIGN KEY (fieldproduct_ptr_id) REFERENCES products_fieldproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_fieldproduct products_fieldproduc_polymorphic_ctype_id_54b1ae27_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_fieldproduct
    ADD CONSTRAINT products_fieldproduc_polymorphic_ctype_id_54b1ae27_fk_django_co FOREIGN KEY (polymorphic_ctype_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_filefieldproduct products_filefieldpr_fieldproduct_ptr_id_8d1f3a10_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_filefieldproduct
    ADD CONSTRAINT products_filefieldpr_fieldproduct_ptr_id_8d1f3a10_fk_products_ FOREIGN KEY (fieldproduct_ptr_id) REFERENCES products_fieldproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_filterproduct products_filterprodu_field_id_153241f5_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_filterproduct
    ADD CONSTRAINT products_filterprodu_field_id_153241f5_fk_products_ FOREIGN KEY (field_id) REFERENCES products_fieldproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_floatfieldproduct products_floatfieldp_fieldproduct_ptr_id_bf8e8fcd_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_floatfieldproduct
    ADD CONSTRAINT products_floatfieldp_fieldproduct_ptr_id_bf8e8fcd_fk_products_ FOREIGN KEY (fieldproduct_ptr_id) REFERENCES products_fieldproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_objectfieldproduct_content products_objectfield_fieldproduct_id_aa9e8c8c_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_objectfieldproduct_content
    ADD CONSTRAINT products_objectfield_fieldproduct_id_aa9e8c8c_fk_products_ FOREIGN KEY (fieldproduct_id) REFERENCES products_fieldproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_objectfieldproduct products_objectfield_fieldproduct_ptr_id_138848db_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_objectfieldproduct
    ADD CONSTRAINT products_objectfield_fieldproduct_ptr_id_138848db_fk_products_ FOREIGN KEY (fieldproduct_ptr_id) REFERENCES products_fieldproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_objectfieldproduct_content products_objectfield_objectfieldproduct_i_cd86c24c_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_objectfieldproduct_content
    ADD CONSTRAINT products_objectfield_objectfieldproduct_i_cd86c24c_fk_products_ FOREIGN KEY (objectfieldproduct_id) REFERENCES products_objectfieldproduct(fieldproduct_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_realproduct products_realproduct_manager_id_ff39c14e_fk_api_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_realproduct
    ADD CONSTRAINT products_realproduct_manager_id_ff39c14e_fk_api_user_id FOREIGN KEY (manager_id) REFERENCES api_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_realproduct products_realproduct_type_id_3d83807c_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_realproduct
    ADD CONSTRAINT products_realproduct_type_id_3d83807c_fk_products_ FOREIGN KEY (type_id) REFERENCES products_typeproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_selectfieldproduct_content products_selectfield_fieldproduct_id_0b91e313_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_selectfieldproduct_content
    ADD CONSTRAINT products_selectfield_fieldproduct_id_0b91e313_fk_products_ FOREIGN KEY (fieldproduct_id) REFERENCES products_fieldproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_selectfieldproduct products_selectfield_fieldproduct_ptr_id_73c09ff3_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_selectfieldproduct
    ADD CONSTRAINT products_selectfield_fieldproduct_ptr_id_73c09ff3_fk_products_ FOREIGN KEY (fieldproduct_ptr_id) REFERENCES products_fieldproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_selectfieldproduct_content products_selectfield_selectfieldproduct_i_7d56d910_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_selectfieldproduct_content
    ADD CONSTRAINT products_selectfield_selectfieldproduct_i_7d56d910_fk_products_ FOREIGN KEY (selectfieldproduct_id) REFERENCES products_selectfieldproduct(fieldproduct_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_sortproduct products_sortproduct_field_id_7143ae82_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_sortproduct
    ADD CONSTRAINT products_sortproduct_field_id_7143ae82_fk_products_ FOREIGN KEY (field_id) REFERENCES products_fieldproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_typeproduct_fields products_typeproduct_fieldproduct_id_b407c4c4_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_fields
    ADD CONSTRAINT products_typeproduct_fieldproduct_id_b407c4c4_fk_products_ FOREIGN KEY (fieldproduct_id) REFERENCES products_fieldproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_typeproduct_filters products_typeproduct_filterproduct_id_003900e4_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_filters
    ADD CONSTRAINT products_typeproduct_filterproduct_id_003900e4_fk_products_ FOREIGN KEY (filterproduct_id) REFERENCES products_filterproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_typeproduct_sort_fields products_typeproduct_sortproduct_id_8efc8751_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_sort_fields
    ADD CONSTRAINT products_typeproduct_sortproduct_id_8efc8751_fk_products_ FOREIGN KEY (sortproduct_id) REFERENCES products_sortproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_typeproduct_sort_fields products_typeproduct_typeproduct_id_2dd4560f_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_sort_fields
    ADD CONSTRAINT products_typeproduct_typeproduct_id_2dd4560f_fk_products_ FOREIGN KEY (typeproduct_id) REFERENCES products_typeproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_typeproduct_filters products_typeproduct_typeproduct_id_3d077107_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_filters
    ADD CONSTRAINT products_typeproduct_typeproduct_id_3d077107_fk_products_ FOREIGN KEY (typeproduct_id) REFERENCES products_typeproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: products_typeproduct_fields products_typeproduct_typeproduct_id_f9f1b880_fk_products_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY products_typeproduct_fields
    ADD CONSTRAINT products_typeproduct_typeproduct_id_f9f1b880_fk_products_ FOREIGN KEY (typeproduct_id) REFERENCES products_typeproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

