# -*- coding: utf-8 -*-
import os
import django
from django.urls import reverse

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ref_terminal_backend.settings")
django.setup()

from products.models.main import FloatFieldProduct, TypeProduct, BoolFieldProduct, CharFieldProduct, SelectFieldProduct, FilterProduct
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404
from products.models import RealProduct
from api.models import OrderModel, OrderManagerModel, User
from api.models.order import TradeItem, ManagerQuest


NACHALNAYA_TSENA_AUKCION_NAME = 'Начальная цена, аукцион'
NACHALNAYA_TSENA_AUKCION_SLUG = 'nachalnaya-tsena-aukcion'


def a():
    ffp_ct = ContentType.objects.get_for_model(FloatFieldProduct)
    bfp_ct = ContentType.objects.get_for_model(BoolFieldProduct)
    data = [
        (NACHALNAYA_TSENA_AUKCION_NAME, NACHALNAYA_TSENA_AUKCION_SLUG),
        ('Минимальная ставка, аукцион', 'minimaljnaya-stavka-aukcion'),
        ('Цена выкупа, аукцион', 'tsena-vykupa-aukcion'),
        ('Время проведения аукциона (в часах)', 'vremya-provedeniya-aukciona'),
    ]

    type_product = get_object_or_404(TypeProduct, name='Контейнер')
    fields = type_product.fields.values_list('translate', flat=True)

    for name, translate in data:
        if FloatFieldProduct.objects.filter(translate=translate).first() is None:
            p = FloatFieldProduct.objects.create(
                name=name, translate=translate,
                required=False, polymorphic_ctype=ffp_ct,
            )
            print('created: ', p.id)
            if translate not in fields:
                type_product.fields.add(p)
                type_product.save()

    name, translate = 'Аукцион', 'auction'
    if BoolFieldProduct.objects.filter(translate=translate).first() is None:
        p = BoolFieldProduct.objects.create(
            name=name, translate=translate,
            required=False, polymorphic_ctype=bfp_ct,
        )
        print('created: ', p.id)
        if translate not in fields:
            type_product.fields.add(p)
            type_product.save()

    filter_field = BoolFieldProduct.objects.filter(translate=translate).first()
    if filter_field is not None and FilterProduct.objects.filter(translate=translate).first() is None:
        fp = FilterProduct.objects.create(name=name, translate=translate, field=filter_field)
        type_product.filters.add(fp)
        print('created filter for: ', filter_field)


def b():
    status_field = SelectFieldProduct.objects.filter(translate='status').first()
    if status_field is None or status_field.content.filter(translate='reserved').exists():
        print('nope.')
        return None
    field = CharFieldProduct.objects.create(name='Зарезервирован', translate='reserved')
    status_field.content.add(field)
    print('added to:', status_field)


def c_func():
    ffp_ct = ContentType.objects.get_for_model(FloatFieldProduct)
    type_product = get_object_or_404(TypeProduct, name='Контейнер')
    fields = type_product.fields.values_list('translate', flat=True)

    float_field = FloatFieldProduct.objects.filter(translate=NACHALNAYA_TSENA_AUKCION_SLUG).first()
    if float_field is None:
        float_field = FloatFieldProduct.objects.create(
            name=NACHALNAYA_TSENA_AUKCION_NAME, translate=NACHALNAYA_TSENA_AUKCION_SLUG,
            required=False, polymorphic_ctype=ffp_ct,
        )
        print('created field: ', float_field.id)

    if float_field.translate not in fields:
        type_product.fields.add(float_field)
        print('assigned field: ', float_field.id)

    filter_field = FilterProduct.objects.filter(translate=NACHALNAYA_TSENA_AUKCION_SLUG).first()
    if filter_field is None:
        filter_field = FilterProduct.objects.create(
            name=NACHALNAYA_TSENA_AUKCION_NAME, translate=NACHALNAYA_TSENA_AUKCION_SLUG,
            field=float_field,
        )
        print('created filter: ', filter_field.id)
        if filter_field.translate not in fields:
            type_product.filters.add(filter_field)
            type_product.save()
            print('assigned filter: ', filter_field.id)


def d():
    bfp_ct = ContentType.objects.get_for_model(BoolFieldProduct)
    type_product = get_object_or_404(TypeProduct, name='Контейнер')
    fields = type_product.fields.values_list('translate', flat=True)

    name, translate = 'Модерация пройдена', RealProduct.MODERATION_PASSED_PROPERTY_FIELD
    p = BoolFieldProduct.objects.filter(translate=translate).first()
    if p is None:
        p = BoolFieldProduct.objects.create(
            name=name, translate=translate,
            required=False, polymorphic_ctype=bfp_ct,
        )
        print('created: ', p.id)
    if p.translate not in fields:
        type_product.fields.add(p)
        type_product.save()
        print('added: ', p.id)


def e():
    ffp_ct = ContentType.objects.get_for_model(FloatFieldProduct)
    type_product = get_object_or_404(TypeProduct, name='Контейнер')
    fields = type_product.fields.values_list('translate', flat=True)
    name, translate = ('Общая цена', 'obshchaia-tsena')
    if FloatFieldProduct.objects.filter(translate=translate).first() is None:
        p = FloatFieldProduct.objects.create(
            name=name, translate=translate,
            required=False, polymorphic_ctype=ffp_ct,
        )
        print('created: ', p.id)
        if translate not in fields:
            type_product.fields.add(p)
            type_product.save()
    else:
        print('nope.')


def check_auctions():
    for product in RealProduct.objects.filter(property_json__auction=True, manager__id=20):
        completed = OrderModel.complete_auction(product)


def check_quest():
    quest = ManagerQuest.objects.get(id=8)
    m = User.objects.get(id=20)
    quest.get_orders_for_managers([m, ])
    managers = quest.get_success_managers()
    for o in OrderModel.objects.filter(id__gte=415):
        print(o.id, o.created_at)
    print(managers)


def reset_container_seo():
    containers = RealProduct.objects.all()
    for c in containers:
        c.seo_title = c.name
        if c.property_json.get('prodazha', False):
            c.seo_title += ' Продажа'
        if c.property_json.get('arenda', False):
            c.seo_title += ' Аренда'
        c.seo_keywords = '{} {} {}'.format(
            c.property_json['tip']['name'],
            c.property_json['razmer'],
            c.property_json.get('model', ''),
        )
        description = 'Контейнер "{}".'.format(c.name)
        if c.property_json.get('opisanie'):
            description = description + ' ' + c.property_json['opisanie']
        c.seo_description = description
        c.save()


def get_container_urls():
    containers = RealProduct.objects.values_list('translate', flat=True)
    urls = []
    for c in containers:
        url = 'http://localhost:8000/api/products/container/{}'.format(c)
        import requests
        if requests.get(url).status_code != 404:
            urls.append('/container/{}'.format(c))
    return urls


def reset_required_opisanie_garantii():
    type_product = get_object_or_404(TypeProduct, name='Контейнер')
    field = type_product.fields.filter(translate='opisanie-garantii').first()
    if field is not None:
        field.required = False
        field.save()
        print('field is updated')


def main():
    a()
    b()
    c_func()
    d()
    e()
    return None


if __name__ == '__main__':
    main()
