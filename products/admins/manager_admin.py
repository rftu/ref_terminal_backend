from django.contrib.admin import AdminSite

from products.admins.super_admin import ContainerMeasure, ContainerModel, \
    TypeContainerModel, ContainerStatus, ContainerSost, \
    ProductAdmin, TypeContainerAdmin, ContainerSostAdmin, ContainerStatusAdmin, \
    ContainerModelAdmin, ContainerMeasureAdmin, CityContainerModel, CityAdmin, \
    PlaceModel, PlaceContainerModel, SpecialOfferAdmin, ContainerTrackingAdmin
from products.models import RealProduct, SpecialOffer, ContainerTracking


class ManagerAdmin(AdminSite):
    site_header = 'Кабинет менеджера'
    site_title = 'РефТерминал'


manager_admin = ManagerAdmin(name='manager_admin')

manager_admin.register(ContainerMeasure, ContainerMeasureAdmin)
manager_admin.register(ContainerStatus, ContainerStatusAdmin)
manager_admin.register(ContainerSost, ContainerSostAdmin)
manager_admin.register(TypeContainerModel, TypeContainerAdmin)
manager_admin.register(ContainerModel, ContainerModelAdmin)
manager_admin.register(CityContainerModel, CityAdmin)
manager_admin.register(PlaceContainerModel, PlaceModel)
manager_admin.register(SpecialOffer, SpecialOfferAdmin)
manager_admin.register(ContainerTracking, ContainerTrackingAdmin)
