# Register your models here.
from content_gallery.admin import ImageAdminInline
from django import forms
from django.conf.urls import url
from django.contrib import admin, messages
from django.contrib.admin.utils import quote
from django.forms import BaseInlineFormSet
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.http import urlquote
from polymorphic.admin import PolymorphicParentModelAdmin, \
    PolymorphicChildModelAdmin
from solo.admin import SingletonModelAdmin

from api.models.order import TradeItem
from products.forms import TypeContainerForm
from products.models import RealProduct, SpecialOffer, ContainerTracking, ContainerTrackingStep
from products.models.main import FilterProduct, FieldProduct, TypeProduct, \
    CharFieldProduct, SelectFieldProduct, FloatFieldProduct, \
    BoolFieldProduct, FileFieldProduct, ObjectFieldProduct, SortProduct


class FieldProductChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = FieldProduct
    list_display = ('name', 'translate', )

    def save_model(self, request, obj, form, change):
        obj.save()
        TypeProduct.objects.get(translate='konteiner').fields.add(obj)


@admin.register(CharFieldProduct)
class CharFieldProductAdmin(FieldProductChildAdmin):
    base_model = CharFieldProduct
    show_in_index = True


@admin.register(SelectFieldProduct)
class SelectFieldProductAdmin(CharFieldProductAdmin):
    base_model = SelectFieldProduct
    show_in_index = True
    fields = ('name', 'order_number')


@admin.register(FloatFieldProduct)
class FloatFieldProductAdmin(CharFieldProductAdmin):
    base_model = FloatFieldProduct
    show_in_index = True


@admin.register(BoolFieldProduct)
class BoolFieldProductAdmin(CharFieldProductAdmin):
    base_model = BoolFieldProduct
    show_in_index = True


@admin.register(FileFieldProduct)
class FileFieldProductAdmin(CharFieldProductAdmin):
    base_model = FileFieldProduct
    show_in_index = True


@admin.register(ObjectFieldProduct)
class ObjectFieldProductAdmin(FieldProductChildAdmin):
    base_model = ObjectFieldProduct
    show_in_index = True


@admin.register(FieldProduct)
class FieldProductParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = FieldProduct
    child_models = (
        CharFieldProduct, BoolFieldProduct, FloatFieldProduct,
        SelectFieldProduct, FileFieldProduct, ObjectFieldProduct
    )
    list_display = ('name', 'translate', )
    # list_filter = (PolymorphicChildModelFilter,)  # This is optional.


@admin.register(RealProduct)
class ProductAdmin(admin.ModelAdmin):
    inlines = [
        ImageAdminInline,
    ]
    list_display = ('name', 'id', 'manager', 'moderation_passed', 'edit_moderation_passed_action', )
    list_per_page = 10
    search_fields = ('translate', )
    readonly_fields = ('moderation_passed', 'edit_moderation_passed_action', )

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                '^(?P<pk>[^/]+)/change_moderation_passed/$',
                self.admin_site.admin_view(self.container_change_moderation_passed),
                name='container_change_moderation_passed'
            )
        ]
        return custom_urls + urls

    def container_change_moderation_passed(self, request, pk=None):
        container = RealProduct.objects.get(pk=pk)
        new_is_moderated = container.set_invert_moderation_passed()

        if self.has_change_permission(request, container):
            obj_url = reverse(
                'admin:%s_%s_change' % ('products', 'realproduct'),
                args=(quote(container.pk),),
                current_app=self.admin_site.name,
            )
            obj_repr = format_html('<a href="{}">{}</a>', urlquote(obj_url), container)
            redirect_url = obj_url
        else:
            obj_repr = force_text(container)
            redirect_url = reverse('admin:products_realproduct_changelist')
        msg = format_html(
            'Для контейнера "{obj}" установлен статус модерации: {is_moderated}.',
            obj=obj_repr, is_moderated=(new_is_moderated and 'Пройдено' or 'Отклонено'),
        )
        messages.success(request, msg)

        return redirect(redirect_url)

    def moderation_passed(self, obj):
        return obj.is_moderated

    moderation_passed.short_description = 'Модерация пройдена'
    moderation_passed.boolean = True

    def edit_moderation_passed_action(self, obj):
        return format_html(
            '<a class="button" href="{}">{}</a>',
            reverse(
                'admin:container_change_moderation_passed',
                args=(obj.pk,),
            ),
            obj.is_moderated and 'Отклонить' or 'Подтвердить'
        )

    edit_moderation_passed_action.short_description = 'Изменить статус модерациии'
    edit_moderation_passed_action.allow_tags = True


class ContainerMeasure(CharFieldProduct):
    class Meta:
        proxy = True
        verbose_name = 'Размер контейнера'
        verbose_name_plural = 'Размеры контейнеров'


@admin.register(ContainerMeasure)
class ContainerMeasureAdmin(admin.ModelAdmin):
    fields = ('name', 'order_number')

    def get_queryset(self, request):
        return SelectFieldProduct.objects.get(translate='razmer').content.all()

    def save_model(self, request, obj, form, change):
        obj.save()
        SelectFieldProduct.objects.get(translate='razmer').content.add(obj)


class ContainerStatus(CharFieldProduct):
    class Meta:
        proxy = True
        verbose_name = 'Статус контейнера'
        verbose_name_plural = 'Статусы контейнеров'


@admin.register(ContainerStatus)
class ContainerStatusAdmin(admin.ModelAdmin):
    fields = ('name', 'order_number')

    def get_queryset(self, request):
        return SelectFieldProduct.objects.get(translate='status').content.all()

    def save_model(self, request, obj, form, change):
        obj.save()
        SelectFieldProduct.objects.get(translate='status').content.add(obj)


class ContainerSost(CharFieldProduct):
    class Meta:
        proxy = True
        verbose_name = 'Состояние контейнера'
        verbose_name_plural = 'Состояния контейнеров'


@admin.register(ContainerSost)
class ContainerSostAdmin(admin.ModelAdmin):
    fields = ('name', 'order_number')

    def get_queryset(self, request):
        return SelectFieldProduct.objects.get(
            translate='sostoianie').content.all()

    def save_model(self, request, obj, form, change):
        obj.save()
        SelectFieldProduct.objects.get(translate='sostoianie').content.add(obj)


class TypeContainerModel(ObjectFieldProduct):
    class Meta:
        proxy = True
        verbose_name = 'Тип контейнера'
        verbose_name_plural = 'Типы контейнеров'


@admin.register(TypeContainerModel)
class TypeContainerAdmin(admin.ModelAdmin):
    form = TypeContainerForm

    def get_queryset(self, request):
        return SelectFieldProduct.objects.get(translate='tip').content.all()

    def save_model(self, request, obj, form, change):
        obj.save()
        SelectFieldProduct.objects.get(translate='tip').content.add(obj)


class ContainerModel(CharFieldProduct):
    class Meta:
        proxy = True
        verbose_name = 'Модель контейнера'
        verbose_name_plural = 'Модели контейнеров'


@admin.register(ContainerModel)
class ContainerModelAdmin(admin.ModelAdmin):
    fields = ('name', 'order_number')

    def get_queryset(self, request):
        return SelectFieldProduct.objects.get(
            translate='model').content.all()

    def save_model(self, request, obj, form, change):
        obj.save()
        SelectFieldProduct.objects.get(translate='model').content.add(obj)


@admin.register(TypeProduct)
class TypeAdmin(admin.ModelAdmin):
    pass


@admin.register(SortProduct)
class SortAdmin(admin.ModelAdmin):
    pass


@admin.register(FilterProduct)
class FilterAdmin(admin.ModelAdmin):
    pass

    def save_model(self, request, obj, form, change):
        obj.save()
        TypeProduct.objects.get(translate='konteiner').filters.add(obj)


#

class PlaceForm(forms.ModelForm):
    name = forms.CharField(label='Адрес')

    class Meta:
        model = ObjectFieldProduct
        fields = ('name', 'order_number')

        # def save(self, commit=True):
        #     type_container = super(PlaceForm, self).save(commit=False)
        #     type_container.save()
        #     name = self.cleaned_data.get('name', None)
        #
        #     type_json = {
        #         'name': name,
        #     }
        #     type_container.property = type_json
        #     type_container.save()
        #     return type_container


class CityContainerModel(SelectFieldProduct):
    class Meta:
        proxy = True
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class PlaceContainerModel(ObjectFieldProduct):
    class Meta:
        proxy = True
        verbose_name = 'Площадка продажи'
        verbose_name_plural = 'Площадки продажи'


class PlaceFormSet(BaseInlineFormSet):
    def save(self, commit=True):
        print('zopa')


class CityInline(admin.TabularInline):
    verbose_name = 'Город'
    verbose_name_plural = 'Города'

    can_delete = False
    model = PlaceContainerModel.select_field.through
    max_num = 1

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if request.method == 'GET':
            if db_field.name == 'selectfieldproduct':
                kwargs['queryset'] = SelectFieldProduct.objects.get(
                    translate='gorod').content.all()
        return super(CityInline, self).formfield_for_foreignkey(db_field,
                                                                request,
                                                                **kwargs)


@admin.register(PlaceContainerModel)
class PlaceModel(admin.ModelAdmin):
    form = PlaceForm
    fields = ('name', 'order_number')
    inlines = (CityInline,)

    class Meta:
        verbose_name = 'Площадка продажи'
        verbose_name_plural = 'Площадки продажи'

    #
    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)

        for instance in instances:
            instance.fieldproduct.property = {
                'gorod': instance.selectfieldproduct.name,
                'address': instance.fieldproduct.name
            }
            instance.fieldproduct.save()
            instance.save()
        formset.save_m2m()


@admin.register(CityContainerModel)
class CityAdmin(admin.ModelAdmin):
    fields = ('name', 'order_number')

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    def get_queryset(self, request):
        return SelectFieldProduct.objects.get(translate='gorod').content.all()

    def save_model(self, request, obj, form, change):
        obj.save()
        SelectFieldProduct.objects.get(translate='gorod').content.add(obj)


class ContainerTrackingStepAdmin(admin.TabularInline):
    model = ContainerTrackingStep
    fk_name = 'tracker'
    extra = 1
    can_delete = False


class ContainerTrackingAdmin(admin.ModelAdmin):
    inlines = [ContainerTrackingStepAdmin, ]
    list_display = ('container', 'service_type', 'departure_date', 'delivery_date', )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'container':
            if not request.user.is_superuser:
                kwargs['queryset'] = RealProduct.objects.filter(manager=request.user)
        return super(ContainerTrackingAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(ContainerTracking, ContainerTrackingAdmin)


class SpecialOfferAdmin(admin.ModelAdmin):
    exclude = ('user', )

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'containers':
            # products_to_exclude = self.model.objects.all().values_list('containers__id', flat=True)
            kwargs['queryset'] = RealProduct.objects.filter(manager=request.user)
        return super(SpecialOfferAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        return super(SpecialOfferAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        queryset = super(SpecialOfferAdmin, self).get_queryset(request)
        queryset = queryset.filter(user=request.user)
        return queryset


class TradeItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'container', 'manager_order', 'total_cost', )
    list_per_page = 10


admin.site.register(TradeItem, TradeItemAdmin)
