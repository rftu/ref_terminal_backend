from rest_framework.exceptions import APIException


class DoNotPublishContainer(APIException):
    status_code = 412
    default_detail = 'Ваш контейнер содержит не все требуемые поля для публикации'
    precondition_failed = 'publish_failed'


class EmptySpecialOffer(APIException):
    status_code = 412
    default_detail = 'Среди выбранных контейнеров не осталось свободных'
    precondition_failed = 'offer_failed'
