from django import forms

from products.models.main import ObjectFieldProduct


class TypeContainerForm(forms.ModelForm):
    name = forms.CharField(label='Имя типа')
    short_name = forms.CharField(label='Сокращенное имя')

    class Meta:
        model = ObjectFieldProduct
        fields = ('name', 'short_name', 'order_number')

    def save(self, commit=True):
        type_container = super(TypeContainerForm, self).save(commit=False)
        type_container.save()

        name = self.cleaned_data.get('name', None)
        short_name = self.cleaned_data.get('short_name', None)

        type_json = {
            'name': name,
            'short_name': short_name
        }
        type_container.property = type_json
        type_container.save()
        return type_container

    def __init__(self, *args, **kwargs):
        if kwargs.get('instance'):
            kwargs['initial'] = kwargs['instance'].property
        super(TypeContainerForm, self).__init__(*args, **kwargs)
