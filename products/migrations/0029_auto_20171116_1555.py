# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-11-16 05:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0028_auto_20171114_2224'),
    ]

    operations = [
        migrations.AlterField(
            model_name='charfieldproduct',
            name='content',
            field=models.CharField(blank=True, default=None, max_length=500, null=True, verbose_name='Содержимое поля'),
        ),
        migrations.AlterField(
            model_name='fieldproduct',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Название поля'),
        ),
        migrations.AlterField(
            model_name='fieldproduct',
            name='translate',
            field=models.SlugField(blank=True, max_length=255, unique=True, verbose_name='Транслитерация'),
        ),
        migrations.AlterField(
            model_name='filterproduct',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Название фильтра'),
        ),
        migrations.AlterField(
            model_name='filterproduct',
            name='translate',
            field=models.SlugField(blank=True, max_length=255, verbose_name='Транслитерация'),
        ),
        migrations.AlterField(
            model_name='sortproduct',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Название сортировки'),
        ),
        migrations.AlterField(
            model_name='sortproduct',
            name='translate',
            field=models.SlugField(blank=True, max_length=255, verbose_name='Транслитерация'),
        ),
        migrations.AlterField(
            model_name='typeproduct',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Название типа'),
        ),
        migrations.AlterField(
            model_name='typeproduct',
            name='translate',
            field=models.SlugField(blank=True, max_length=255, verbose_name='Транслитерация'),
        ),
    ]
