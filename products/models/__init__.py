from content_gallery.models import ContentGalleryMixin
from django.contrib.postgres.fields import JSONField
from django.core.serializers import json
from django.db import models, transaction
from django.db.models import Q
from django.utils import timezone
from django.utils.dateparse import parse_datetime

from django.utils.text import slugify

from api.models.order import User, TradeItem
from unidecode import unidecode
from products.models.main import TypeProduct


class SEOMixin(models.Model):
    seo_title = models.CharField(
        verbose_name='SEO Заголовок', max_length=250,
        blank=True, default='',
    )
    seo_description = models.CharField(
        verbose_name='SEO Описание', max_length=250,
        blank=True, default='',
    )
    seo_keywords = models.CharField(
        verbose_name='SEO Ключи', max_length=250,
        blank=True, default='',
    )

    class Meta:
        abstract = True


class RealProduct(ContentGalleryMixin, SEOMixin):
    name = models.CharField('Имя абстракции', max_length=100)
    type = models.ForeignKey(TypeProduct, verbose_name='Тип абстракции')
    property_json = JSONField(encoder=json.DjangoJSONEncoder, default=dict,
                              null=True)
    translate = models.SlugField('Транслитерация', max_length=255,
                                 unique=True, blank=True)
    price = models.FloatField(default=0)
    manager = models.ForeignKey(User, related_name='containers',
                                related_query_name='container', null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    edited_at = models.DateTimeField(auto_now=True)

    DEFAULT_AUCTION_LONGTIME_H = 48
    MODERATION_PASSED_PROPERTY_FIELD = 'moderation_passed'

    class Meta:
        verbose_name = 'Контейнер'
        verbose_name_plural = 'Контейнеры'

    def set_translate_name(self):
        while not self.translate:
            self.translate = slugify(unidecode(self.name))
            try:
                with transaction.atomic():
                    super(RealProduct, self).save()
                    break
            except:
                result = RealProduct.objects.filter(
                    translate__contains=self.translate).order_by(
                    'translate').values_list('translate', flat=True)
                if not result:
                    self.translate = slugify(unidecode(self.name))

                else:
                    try:
                        self.translate = max(result) + '-1'
                    except IndexError:
                        pass
                super(RealProduct, self).save()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.property_json[
            'obshchaia-tsena'] = self.property_json.get(
            'tsena-so-skidkoi') if self.property_json.get(
            'tsena-so-skidkoi') not in [0, None] else \
            self.property_json.get('stoimost-arendy-v-mesiats')

        if self.property_json.get('auction') is True and 'auction_endtime' not in self.property_json:
            dh = self.property_json.get('vremya-provedeniya-aukciona', self.DEFAULT_AUCTION_LONGTIME_H)
            self.property_json['auction_endtime'] = timezone.now() + timezone.timedelta(hours=dh)

        if self.MODERATION_PASSED_PROPERTY_FIELD not in self.property_json:
            self.property_json[self.MODERATION_PASSED_PROPERTY_FIELD] = bool(self.manager.is_staff
                                                                             or self.manager.company is not None)

        if not self.seo_title:
            self.seo_title = self.name
            if self.property_json.get('prodazha', False):
                self.seo_title += ' Продажа'
            if self.property_json.get('arenda', False):
                self.seo_title += ' Аренда'
            if self.property_json.get('auction', False):
                self.seo_title += ' Аукцион'

        if not self.seo_keywords:
            self.seo_keywords = ' '.join((
                self.property_json.get('tip', {}).get('name', ''),
                self.property_json.get('razmer', ''),
                self.property_json.get('model', ''),
            ))
        if not self.seo_description:
            self.seo_description = ' '.join((
                'Контейнер "{}".'.format(self.name),
                self.property_json.get('opisanie', ''),
            ))

        super(RealProduct, self).save()
        self.set_translate_name()

    @property
    def can_bet_by_time(self):
        end_time = self.property_json.get('auction_endtime', '')
        if isinstance(end_time, str):
            end_time = parse_datetime(end_time)
        return end_time is None or end_time > timezone.now()

    def __str__(self):
        return '{} №{}'.format(self.name, self.id)

    def change_container_status(self, status):
        self.property_json['status'] = status
        self.save()
        if status == 'Зарезервирован':
            SpecialOffer.deactivate_container(self)

    def reset_container_status(self):
        self.property_json['status'] = "Свободен"
        self.save()

    def set_invert_moderation_passed(self):
        new_value = not self.is_moderated
        self.property_json[self.MODERATION_PASSED_PROPERTY_FIELD] = new_value
        self.save()
        return new_value

    def check_publish_status(self):
        all_fields = list(
            TypeProduct.objects.get(
                id=self.type_id).fields.filter(required=True).values_list(
                'translate', flat=True))
        self.property_json['publish'] = True if len(
            set(all_fields).difference(set(list(
                self.property_json.keys())))) == 0 else False
        self.save()
        return self.property_json['publish']

    @property
    def is_moderated(self):
        return self.property_json.get(RealProduct.MODERATION_PASSED_PROPERTY_FIELD, True)

    def get_next_bet_amount(self):
        last = self.items.order_by('total_cost').last()
        last_cost = last and last.total_cost or 0
        min_delta = self.property_json.get('minimaljnaya-stavka-aukcion', 0)
        return int(last_cost + min_delta)


def get_next_month_date():
    return timezone.now() + timezone.timedelta(days=30)


class ContainerTracking(models.Model):

    class Meta:
        verbose_name = 'Трекинг контейнера'
        verbose_name_plural = 'Трекинг контейнеров'

    DELIVERY = 'delivery'
    SERVICE_TYPE = (
        (DELIVERY, 'Доставка товара'),
    )

    container = models.OneToOneField(
        to='products.RealProduct', on_delete=models.CASCADE,
        related_name='tracker', verbose_name='Контейнер',
    )
    service_type = models.CharField(
        verbose_name='Тип услуги', max_length=25,
        default=DELIVERY, choices=SERVICE_TYPE,
    )
    departure_date = models.DateTimeField(
        verbose_name='Дата отправления',
        default=timezone.now,
    )
    delivery_date = models.DateTimeField(
        verbose_name='Дата доставки',
        default=get_next_month_date,
    )

    def __str__(self):
        return '#{}'.format(self.container)

    @property
    def status(self):
        """
        Возвращает читаемый статус трэкинга на основе последнего этапа следования.
        Если ни одного этапа пройдено не было, то возвращает статус по умолчанию.
        """
        last_step = self.steps.last()
        if last_step is None:
            return ContainerTrackingStep.DEFAULT_STATUS_R
        return last_step.get_display_status()

    def get_departure_date_display(self):
        return self.departure_date.strftime('%d.%m.%Y') if self.departure_date else 'Не известно'

    dep_date = property(get_departure_date_display)


class ContainerTrackingStep(models.Model):

    class Meta:
        verbose_name = 'Контрольная точка'
        verbose_name_plural = 'Контрольные точки'

    class Statuses:

        ON_MY_WAY = 'on_my_way'
        AT_THE_TERMINAL = 'terminal'
        STORAGE_AT_THE_TERMINAL = 'storage_terminal'
        LOADING_THE_TRAIN = 'loading_train'
        UNLOADING_FROM_THE_TRAIN = 'unloading_train'
        TERMINAL_PROCESSING_AT_THE_PORT_OF_ARRIVAL = 'terminal_processing_arrival'
        TERMINAL_PROCESSING_AT_THE_PORT_OF_DEPARTURE = 'terminal_processing_departure'
        EXPORT_FROM_THE_CONTAINER_SITE = 'export_container_site'
        SENT = 'sent'
        LOADING = 'loading'
        UNLOADING = 'unloading'

        choices = (
            (ON_MY_WAY, 'в пути'),
            (AT_THE_TERMINAL, 'на терминале'),
            (STORAGE_AT_THE_TERMINAL, 'хранение на терминале'),
            (LOADING_THE_TRAIN, 'погрузка на поезд'),
            (UNLOADING_FROM_THE_TRAIN, 'выгрузка с поезда'),
            (TERMINAL_PROCESSING_AT_THE_PORT_OF_ARRIVAL, 'терминальная обработка в порту прибытия'),
            (TERMINAL_PROCESSING_AT_THE_PORT_OF_DEPARTURE, 'терминальная обработка в порту отправления'),
            (EXPORT_FROM_THE_CONTAINER_SITE, 'вывоз с контейнерной площадки'),
            (SENT, 'отправлено'),
            (LOADING, 'погрузка'),
            (UNLOADING, 'разгрузка'),
        )

        max_length = 32

    tracker = models.ForeignKey(
        to='products.ContainerTracking', on_delete=models.CASCADE,
        related_name='steps', verbose_name='Трэкинг',
    )
    passage_date = models.DateTimeField(
        verbose_name='Дата прохождения',
        default=timezone.now,
    )
    location = models.CharField(
        verbose_name='Местоположение', max_length=100,
    )
    status = models.CharField(
        verbose_name='Статус', max_length=Statuses.max_length,
        choices=Statuses.choices,
    )

    def get_display_status(self):
        for (k, v) in self.Statuses.choices:
            if k == str(self.status):
                return v
        return None


class SpecialOffer(models.Model):

    class Meta:
        verbose_name = 'Оптовое предложение'
        verbose_name_plural = 'Оптовые предложения'

    def __str__(self):
        return '{} #{}'.format(str(self.user), self.pk)

    user = models.ForeignKey(
        to='api.User', on_delete=models.CASCADE,
        related_name='special_offers', verbose_name='Менеджер',
    )
    containers = models.ManyToManyField(
        to='products.RealProduct',
        verbose_name='Контейнеры',
    )
    name = models.CharField('Название', max_length=100)

    def get_items(self):
        items = []
        for container in self.containers.all():
            item = TradeItem.objects.filter(container=container).first()
            items.append(item)
        return items

    def get_total_cost(self):
        return sum([int(p.property_json.get('tsena', 0)) for p in self.containers.all()])

    def get_total_with_discount(self):
        return sum([int(p.property_json.get('tsena-so-skidkoi', 0)) for p in self.containers.all()])

    total_cost = property(get_total_cost)
    total_with_discount = property(get_total_with_discount)

    @classmethod
    def deactivate_container(cls, container):
        for offer in cls.objects.filter(containers__pk__in=[container.pk, ]):
            offer.containers.remove(container)

    @classmethod
    def get_products_for_offer(cls, ids, offer_pk=None):
        to_exclude = cls.objects.filter(containers__isnull=False)
        if offer_pk is not None:
            to_exclude = to_exclude.exclude(pk=offer_pk)
        to_exclude = to_exclude.values_list('containers__pk', flat=True)
        to_exclude = list(to_exclude)

        ids = [c for c in ids if c not in to_exclude]
        qs = Q(**{
            'property_json__auction': True,
        }) | Q(**{
            'property_json__{}'.format(RealProduct.MODERATION_PASSED_PROPERTY_FIELD): False,
        })

        products = RealProduct.objects.filter(id__in=ids).exclude(qs)
        return products
