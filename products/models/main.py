from django.db import models, transaction
from django.utils.text import slugify

from django.core.serializers import json
from django.contrib.postgres.fields import JSONField

from unidecode import unidecode
from polymorphic.models import PolymorphicModel


class FieldProduct(PolymorphicModel):
    name = models.CharField('Название поля', max_length=255)
    translate = models.SlugField('Транслитерация', max_length=255, blank=True,
                                 unique=True, db_index=True)
    required = models.BooleanField('Влияет на публикацию', blank=True,
                                   default=True)
    order_number = models.IntegerField("Номер публикации", default=0)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        while not self.translate:
            self.translate = slugify(unidecode(self.name))
            try:
                with transaction.atomic():
                    super(FieldProduct, self).save()
                    break
            except:
                result = FieldProduct.objects.filter(
                    translate__contains=self.translate).order_by(
                    'translate').values_list('translate', flat=True)
                try:
                    self.translate = max(result) + '-1'
                except IndexError:
                    pass
        super(FieldProduct, self).save()

    def __str__(self):
        return self.name

    def get_type(self):
        return 'Default'

    def get_content(self):
        return 'Default'
    class Meta:
        ordering = ['order_number']


    content_child = property(get_content)
    type = property(get_type)


class SortProduct(models.Model):
    name = models.CharField('Название сортировки', max_length=255)
    translate = models.SlugField('Транслитерация', max_length=255, blank=True)
    field = models.OneToOneField(FieldProduct,
                                 on_delete=models.CASCADE,
                                 blank=True, default=None, null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.translate:
            self.translate = slugify(unidecode(self.name))
        super(SortProduct, self).save()

    def __str__(self):
        return self.name


class FilterProduct(models.Model):
    name = models.CharField('Название фильтра', max_length=255)
    translate = models.SlugField('Транслитерация', max_length=255, blank=True)
    field = models.OneToOneField(FieldProduct,
                                 on_delete=models.CASCADE,
                                 blank=True, default=None, null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.translate:
            self.translate = slugify(unidecode(self.name))
        super(FilterProduct, self).save()

    def __str__(self):
        return self.name


class TypeProduct(models.Model):
    name = models.CharField('Название типа', max_length=255)
    fields = models.ManyToManyField(FieldProduct)
    filters = models.ManyToManyField(FilterProduct, blank=True)
    sort_fields = models.ManyToManyField(SortProduct, blank=True)
    translate = models.SlugField('Транслитерация', max_length=255, blank=True)
    main = models.BooleanField('Основной (True) или побочный',
                               default=True)

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.translate:
            self.translate = slugify(unidecode(self.name))
        super(TypeProduct, self).save()


class SelectFieldProduct(FieldProduct):
    content = models.ManyToManyField(FieldProduct,
                                     verbose_name='Список вариантов',
                                     blank=True,
                                     related_name='select_field')

    def get_type(self):
        return 'select'

    def __str__(self):
        return self.name

    type = property(get_type)


class CharFieldProduct(FieldProduct):
    content = models.CharField('Содержимое поля', max_length=500, blank=True,
                               null=True, default=None)

    def get_type(self):
        return 'str'

    def get_content(self):
        return self.content

    type = property(get_type)


class BoolFieldProduct(FieldProduct):
    content = models.NullBooleanField('Содержимое поля', blank=True,
                                      default=None,
                                      null=True)

    def get_type(self):
        return 'bool'

    def get_content(self):
        return self.content

    type = property(get_type)


class FloatFieldProduct(FieldProduct):
    content = models.FloatField('Содержимое поля', blank=True, null=True,
                                default=None)

    def get_type(self):
        return 'float'

    def get_content(self):
        return self.content


class FileFieldProduct(FieldProduct):
    content = models.FileField('Файл', blank=True, null=True, default=None)

    def get_type(self):
        return 'file'

    def get_content(self):
        return self.content


class ObjectFieldProduct(FieldProduct):
    content = models.ManyToManyField(FieldProduct,
                                     verbose_name='Список вариантов',
                                     blank=True,
                                     related_name='object_field')

    property = JSONField(encoder=json.DjangoJSONEncoder, default=None,
                         null=True, blank=True)

    def get_type(self):
        return 'object'

    def get_content(self):
        return self.content
