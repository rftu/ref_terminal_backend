from rest_framework.pagination import LimitOffsetPagination


class NormalResultsSetPagination(LimitOffsetPagination):
    pass