from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer, Serializer

from api.custom_exc import NoContentArgument
from products.models.main import FieldProduct, FilterProduct, \
    SelectFieldProduct, CharFieldProduct, BoolFieldProduct, FileFieldProduct, \
    FloatFieldProduct, TypeProduct, ObjectFieldProduct, SortProduct
from products.serializers.custom_fields import Base64FileField


class OutObjectFieldProductSerializer(ModelSerializer):
    class Meta:
        model = ObjectFieldProduct
        fields = '__all__'

    def to_representation(self, instance):
        result = {"name": instance.name,
                  "translate": instance.translate,
                  "type": instance.get_type(),
                  "property": instance.property}
        return result


class OutSelectContentFieldSeriliazer(serializers.RelatedField):
    def to_representation(self, value):
        result = {}
        if value.get_type() == 'select':
            tmp_content = []
            for item in value.content.all():
                tmp_content.append(self.to_representation(item))
            result.update({"name": value.name,
                           "translate_name": value.translate,
                           "type": value.get_type(),
                           "content": tmp_content,
                           "required": value.required})
        elif value.get_type() == 'object':
            result.update({"name": value.name,
                           "translate_name": value.translate,
                           "type": value.get_type(),
                           "property": value.property,
                           "required": value.required})
        else:
            result.update({"name": value.name,
                           "translate_name": value.translate,
                           "type": value.get_type(),
                           "required": value.required})
        return result


class OutSelectSerializer(ModelSerializer):
    content = OutSelectContentFieldSeriliazer(
        queryset=FieldProduct.objects.all(), many=True)
    field_type = serializers.SerializerMethodField()

    def get_field_type(self, obj):
        return obj.get_type()

    class Meta:
        model = SelectFieldProduct
        fields = (
            'id', 'name', 'translate', 'field_type', 'content', 'required')


def check_field_in_select(content_name, select_name):
    errors = {}
    result = {}
    all_fields = SelectFieldProduct.objects.get(
        translate=select_name).content.all()
    try:
        result = all_fields.get(translate=content_name)
    except FieldProduct.DoesNotExist:
        errors.update({content_name: ValidationError(
            detail="Ошибка в поле! " + content_name.__str__() + " - ошибка в имени или наполнении").detail})
    if result:
        return None
    else:
        return errors


class AbstractFieldSerializer(ModelSerializer):
    field_type = serializers.SerializerMethodField()

    def get_field_type(self, obj):
        return obj.get_type()

    def to_internal_value(self, data):
        try:
            if isinstance(data['content'], dict):
                result = data['content']
                try:
                    if result['type'] == 'select':
                        tmp_result = self.to_internal_value(result)
                        return {data['name']: list(tmp_result.values())[0]}
                except KeyError:
                    pass
                try:
                    exc = check_field_in_select(
                        result['translate_name'], data['name'])
                    if exc:
                        raise ValidationError(exc)
                    if result['type'] == 'object':
                        raise KeyError
                    return {data['name']: result['name']}
                except KeyError:
                    exc = check_field_in_select(result['translate_name'],
                                                data['name'])
                    if exc:
                        raise ValidationError(exc)
                    return {data['name']: result['property']}
            result = super().to_internal_value(data)
            return {result['name']: result['content']}
        except KeyError:
            raise NoContentArgument(
                'Это поле: {}, не имеет содержимого'.format(data['name']))

    def to_representation(self, instance):
        result = {}
        if instance.get_type() == 'select':

            serializer = OutSelectSerializer(instance=instance)
            result.update(serializer.data)
        elif instance.get_type() == 'object':
            serializer = OutObjectFieldProductSerializer(instance=instance)
            result.update(serializer.data)
        else:
            result = {"name": instance.name,
                      "translate": instance.translate,
                      "type": instance.get_type(),
                      "required": instance.required}
            if instance.content:
                result.update({"content": instance.content})
        return result

    class Meta:
        model = FieldProduct
        fields = '__all__'


class ShortFieldSerializer(ModelSerializer):
    def to_internal_value(self, data):
        return super().to_internal_value(data)

    def to_representation(self, instance):
        return {"name": instance.name,
                "translate": instance.translate,
                "required": instance.required}

    class Meta:
        model = FieldProduct
        fields = '__all__'


class InSelectSerializer(AbstractFieldSerializer):
    content = AbstractFieldSerializer()

    class Meta:
        model = SelectFieldProduct
        fields = ('id', 'name', 'translate', 'content', 'required')


class FieldProductSerializer(AbstractFieldSerializer):
    pass


class CharFieldProductSerializer(AbstractFieldSerializer):
    class Meta:
        model = CharFieldProduct
        fields = '__all__'


class BoolFieldProductSerializer(AbstractFieldSerializer):
    class Meta:
        model = BoolFieldProduct
        fields = '__all__'


class FileFieldProductSerializer(AbstractFieldSerializer):
    content = Base64FileField(max_length=None, use_url=True)

    class Meta:
        model = FileFieldProduct
        fields = '__all__'


class FloatFieldProductSerializer(AbstractFieldSerializer):
    class Meta:
        model = FloatFieldProduct
        fields = '__all__'


class ObjectFieldProductSerializer(AbstractFieldSerializer):
    content = AbstractFieldSerializer(many=True)

    class Meta:
        model = ObjectFieldProduct
        fields = '__all__'

    def to_representation(self, instance):
        print(instance)
        result = {"name": instance.name,
                  "translate": instance.translate,
                  "type": instance.get_type(),
                  "property": instance.property,
                  "required": instance.required}
        return result


class InObjectFieldProductSerializer(AbstractFieldSerializer):
    content = serializers.CharField(max_length=30)

    class Meta:
        model = ObjectFieldProduct
        fields = '__all__'

    def validate_content(self, value):
        errors = {}
        result = {}
        print(value)
        try:
            all_fields = ObjectFieldProduct.objects.get(
                translate=value)
            result.update({"error": False})
        except ObjectFieldProduct.DoesNotExist:
            errors.update({value: ValidationError(
                detail=value.__str__() + " - неправильное поле или его содержимое").detail})
        if result:
            return value
        else:
            raise ValidationError(errors)


class NameAndTypeFieldSeriliazer(serializers.RelatedField):
    def to_representation(self, value):
        result = {"field_name": value.name,
                  "field_type": value.get_type()}
        return result


class SortProductSerializer(ModelSerializer):
    field = ShortFieldSerializer()

    class Meta:
        model = SortProduct
        fields = ('id', 'name', 'translate', 'field')


class FilterProductSerializer(ModelSerializer):
    field = AbstractFieldSerializer()

    class Meta:
        model = FilterProduct
        fields = '__all__'


class TypeProductSerializer(ModelSerializer):
    fields = serializers.StringRelatedField(many=True)
    filters = serializers.StringRelatedField(many=True)

    class Meta:
        model = TypeProduct
        fields = '__all__'


class WithoutSelectFieldSerializer(Serializer):
    str_fields = CharFieldProductSerializer(many=True, allow_null=True,
                                            required=False)
    int_fields = FloatFieldProductSerializer(many=True, allow_null=True,
                                             required=False)
    file_fields = FileFieldProductSerializer(many=True, allow_null=True,
                                             required=False)
    bool_fields = BoolFieldProductSerializer(many=True, allow_null=True,
                                             required=False)

    def to_internal_value(self, data):
        result = {}
        tmp_result = super().to_internal_value(data)
        for keys in iter(tmp_result):
            for res_dict in tmp_result.get(keys):
                result.update(res_dict)
        return result


class SelectFieldProductSerializer(AbstractFieldSerializer):
    content = WithoutSelectFieldSerializer(required=False)

    class Meta:
        model = SelectFieldProduct
        fields = ('id', 'name', 'translate', 'content', 'required')

    def to_internal_value(self, data):
        result = {}
        tmp_result = super().to_internal_value(data)
        for key, res_dict in tmp_result.items():
            result.update(res_dict)
        return result


class AllFieldSerializer(WithoutSelectFieldSerializer):
    select_fields = InSelectSerializer(many=True, allow_null=True,
                                       required=False)
