import os

from rest_framework import serializers
from rest_framework.settings import api_settings

from ref_terminal_backend.settings import MEDIA_ROOT


class Base64FileField(serializers.FileField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        import base64
        import six

        path = ''
        file_name = 'default_name'
        # Check if this is a base64 string
        if isinstance(data, six.string_types) and data not in ['']:
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                headers, data = data.split(';base64,')
                file_extension = headers.split('/')[1]
                # Generate file name:
                file_name = headers.split(':data')[0]
            # Try to decode the file. Return validation error if it fails.
            else:
                file_extension = ''
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid file')

            complete_file_name = "%s.%s" % (file_name, file_extension,)

            path = os.path.join(MEDIA_ROOT, complete_file_name)

            with open(path, "wb") as file:
                file.write(decoded_file)

        use_url = getattr(self, 'use_url', api_settings.UPLOADED_FILES_USE_URL)

        return path[9:]
