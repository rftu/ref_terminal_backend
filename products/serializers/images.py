from content_gallery.models import Image
from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer


class ContentTypeSerializer(ModelSerializer):
    class Meta:
        model = ContentType
        fields = '__all__'


class ImageSerializer(ModelSerializer):
    class Meta:
        model = Image
        fields = ('image', 'object_id', 'id')

    def create(self, validated_data):
        validated_data.update({"content_type": ContentType.objects.get(id=8)})
        return Image.objects.create(**validated_data)
