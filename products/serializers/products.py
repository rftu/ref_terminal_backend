from rest_framework.exceptions import ValidationError
from rest_framework.fields import SerializerMethodField
from rest_framework.generics import get_object_or_404
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import ModelSerializer

from api.models import OrderModel
from products.custom_exc import DoNotPublishContainer
from products.models import RealProduct, TypeProduct, ContainerTracking, SpecialOffer
from products.models.main import FieldProduct

from products.serializers import AllFieldSerializer


def delete_empty_fields(fields):
    cool_fields = {}
    for item_k, item_v in fields.items():
        if item_v in ('', None):
            continue
        cool_fields.update({item_k: item_v})
    return cool_fields


def check_fields(data, product_type):
    cool_fields = {}
    errors = {}
    fields = get_object_or_404(TypeProduct, name=product_type).fields.all()
    for item_k, item_v in data.items():
        try:
            cool_fields.update(
                {fields.get(translate=item_k).translate: item_v})
        except FieldProduct.DoesNotExist:
            errors.update({item_k: ValidationError(
                detail=item_k.__str__() + " - неправильное поле или его содержимое").detail})
    if errors:
        errors.update({"errors": True})
        return errors
    return cool_fields


class ProductSerializer(ModelSerializer):
    from api.serializers import FullUserSerializer
    type = PrimaryKeyRelatedField(queryset=TypeProduct.objects.all())
    fields = AllFieldSerializer(required=False)
    images = SerializerMethodField(allow_null=True, required=False)
    manager = FullUserSerializer(required=False)
    auction_params = SerializerMethodField(allow_null=True, required=False)

    def get_images(self, obj):
        result = []
        for item in list(obj.content_gallery.order_by('position')):
            result.append({'full_image': item.image_url,
                           'thumbnail_image': item.thumbnail_url,
                           'id': item.id})
        return result

    def get_auction_params(self, obj):
        last_item = obj.items.order_by('total_cost').last()
        orders = OrderModel.get_orders_for_auction(obj)
        can_bet = obj.can_bet_by_time and not orders.filter(auction_properties__is_win=True).exists()
        params = {
            'current_bet_amount': last_item and last_item.total_cost or obj.property_json.get('nachalnaya-tsena-aukcion', 0),
            'orders_count': orders.count(),
            'can_bet': can_bet,
            'end_time': obj.property_json.get('auction_endtime'),
        }
        return params

    class Meta:
        model = RealProduct
        fields = '__all__'
        read_only_fields = ('auction_params', 'seo_title', 'seo_description', 'seo_keywords', )

    def create(self, validated_data):
        fields = check_fields(validated_data.pop('fields'),
                              validated_data.get('type'))
        if fields.get('errors'):
            raise ValidationError(fields)
        validated_data['property_json'] = delete_empty_fields(fields)
        request = self.context.get("request", None)
        if request and hasattr(request, "user") \
            and not request.user.is_anonymous:
            validated_data['manager'] = request.user
        real_product = RealProduct.objects.create(**validated_data)
        if fields['publish']:
            result = real_product.check_publish_status()
            if not result:
                raise DoNotPublishContainer()
        return real_product

    def update(self, instance, validated_data):
        fields = check_fields(validated_data.pop('fields'),
                              validated_data.get('type'))
        if fields.get('errors'):
            raise ValidationError(fields)
        if instance.property_json['status'] == 'Зарезервирован' and fields['status'] in ('Продан', 'Арендован', ):
            ContainerTracking.objects.get_or_create(container=instance)
            SpecialOffer.deactivate_container(instance)
        instance.property_json.update(fields)
        instance.property_json = delete_empty_fields(instance.property_json)
        if fields['publish']:
            result = instance.check_publish_status()
            if not result:
                raise DoNotPublishContainer()
        return super().update(instance, validated_data)
