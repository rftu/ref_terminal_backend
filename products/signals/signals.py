from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from products.models.main import SelectFieldProduct, ObjectFieldProduct
from products.serializers import AbstractFieldSerializer


@receiver(m2m_changed, sender=ObjectFieldProduct.content.through)
def set_property_select_field(sender, **kwargs):
    instance = kwargs['instance']
    result = {}
    fields = AbstractFieldSerializer(instance.content.all(),
                                     many=True).data
    for field in fields:
        result.update({field['translate']: field['content']})
    instance.property = result
    instance.save()
