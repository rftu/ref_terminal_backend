import base64
import os

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from products.models import RealProduct
from products.models.main import TypeProduct, CharFieldProduct, \
    FloatFieldProduct, BoolFieldProduct, SelectFieldProduct, FilterProduct, \
    FileFieldProduct


class ContainerCreateTest(APITestCase):
    def setUp(self):
        from api.models import Company, User
        self.client = APIClient()
        company = Company.objects.create(name='test')
        data = {'email': 'test@te.re',
                'password': '123456aA',
                'company': company}
        self.user = User.objects.create(**data)

        self.client.force_authenticate(user=self.user)

        type_product = TypeProduct.objects.create(name='Контейнер',
                                                  translate='container')
        field_1 = CharFieldProduct.objects.create(name='Производитель',
                                                  translate='made_in')
        field_4_1 = CharFieldProduct.objects.create(name='Марка№1')
        field_4_2 = CharFieldProduct.objects.create(name='Марка№2')

        field_2 = FloatFieldProduct.objects.create(name='Цена',
                                                   translate='price')
        field_3 = BoolFieldProduct.objects.create(name='Новый',
                                                  translate='new')
        field_5 = BoolFieldProduct.objects.create(name='Опубликован',
                                                  translate='publish')
        field_4 = SelectFieldProduct.objects.create(name='Марка контейнера',
                                                    translate='container_mark')
        field_4.content.add(field_4_1, field_4_2)
        field_4.save()
        type_product.fields.add(field_1, field_2, field_3, field_5)

        filter_1 = FilterProduct.objects.create(name='По производителю',
                                                translate='po-made_in',
                                                field=field_1)
        filter_4 = FilterProduct.objects.create(name='По цене',
                                                translate='for-price',
                                                field=field_2)
        filter_5 = FilterProduct.objects.create(name='По состоянию',
                                                translate='for-status',
                                                field=field_3)
        type_product.filters.add(filter_1, filter_4, filter_5)
        type_product.save()
        self.type_product = type_product
        self.full_container_data = {
            "name": "TestContainer",
            "type": type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "made_in",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "price",
                        "content": 13000
                    }
                ],
                "file_fields": [],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "new",
                        "content": "False"
                    },
                    {
                        "name": "publish",
                        "content": "True"
                    }
                ]

            }
        }
        self.bad_data = {
            "name": "TestContainer",
            "type": self.type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "new_bad_fields",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "bad_fields",
                        "content": 13000
                    }
                ],
                "file_fields": [],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "bad",
                        "content": "False"
                    }
                ]

            }
        }

    def test_api_can_create_a_container(self):
        """
        Test the api has container creation capability.
        """
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)

    def test_api_cant_create_a_container_with_file(self):
        field_6 = FileFieldProduct.objects.create(name='Файловое поле',
                                                  translate='file_field')
        self.type_product.fields.add(field_6)
        self.type_product.save()
        with open("run_django.sh", "rb") as image_file:
            file_base64 = b'run_django:' + b'data:file/sh;base64,' + base64.b64encode(
                image_file.read())
        full_container_data = {
            "name": "TestContainer",
            "type": self.type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "made_in",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "price",
                        "content": 13000
                    }
                ],
                "file_fields": [
                    {
                        "name": "file_field",
                        "content": file_base64
                    }
                ],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "new",
                        "content": "False"
                    },
                    {
                        "name": "publish",
                        "content": "True"
                    }
                ]

            }
        }
        response = self.client.post(
            reverse('api:products:create_container'),
            full_container_data, format="json")
        file_path = response.json()['property_json']['file_field']
        os.remove('/var/www/' + file_path)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)

    def test_remove_file_fields(self):
        field_6 = FileFieldProduct.objects.create(name='Файловое поле',
                                                  translate='file_field')
        self.type_product.fields.add(field_6)
        self.type_product.save()
        with open("run_django.sh", "rb") as image_file:
            file_base64 = b'run_django:' + b'data:file/sh;base64,' + base64.b64encode(
                image_file.read())
        full_container_data = {
            "name": "TestContainer",
            "type": self.type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "made_in",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "price",
                        "content": 13000
                    }
                ],
                "file_fields": [
                    {
                        "name": "file_field",
                        "content": file_base64
                    }
                ],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "new",
                        "content": "False"
                    },
                    {
                        "name": "publish",
                        "content": "True"
                    }
                ]

            }
        }
        response = self.client.post(
            reverse('api:products:create_container'),
            full_container_data, format="json")
        container_id = response.json()['id']
        file_path = response.json()['property_json']['file_field']
        os.remove('/var/www/' + file_path)

        empty_file_container_data = {
            "name": "TestContainer",
            "type": self.type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "made_in",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "price",
                        "content": 13000
                    }
                ],
                "file_fields": [
                    {
                        "name": "file_field",
                        "content": ""
                    }
                ],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "new",
                        "content": "False"
                    },
                    {
                        "name": "publish",
                        "content": "False"
                    }
                ]

            }
        }
        property_json = response.json()['property_json']
        response = self.client.put(
            reverse('api:products:container_del_put_get',
                    kwargs={'pk': container_id}),
            empty_file_container_data,
            format="json")
        key, value = 'file_field', ''
        self.assertFalse(
            key in property_json and value == property_json[key])
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_api_create_normal_container(self):
        """
        Test the api has container correct output format.
        """
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        product_property = response.json()['property_json']
        all_fields = list(
            TypeProduct.objects.get(
                id=self.type_product.pk).fields.values_list('translate',
                                                            flat=True))
        self.assertTrue(set(list(product_property.keys())) <= set(all_fields))

    def test_api_create_bad_container(self):
        """
        Test checking the response (create) to incorrect data.
        """
        self.response = self.client.post(
            reverse('api:products:create_container'),
            self.bad_data,
            format="json")
        self.assertEqual(self.response.status_code,
                         status.HTTP_400_BAD_REQUEST)

    def test_api_update_container(self):
        """
        Test the api has container update capability.
        """
        self.response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        container = RealProduct.objects.get(
            pk=int(self.response.json()['id']))
        self.full_container_data.update({"name": "UpdatedContainer"})

        self.response = self.client.put(
            reverse('api:products:container_del_put_get',
                    kwargs={'pk': container.id}),
            self.full_container_data,
            format="json")
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)

    def test_api_update_container_bad_data(self):
        """
        Test checking the response (update) to incorrect data.
        """
        self.response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        container = RealProduct.objects.get(
            pk=int(self.response.json()['id']))

        self.response = self.client.put(
            reverse('api:products:container_del_put_get',
                    kwargs={'pk': container.id}),
            self.bad_data,
            format="json")
        self.assertEqual(self.response.status_code,
                         status.HTTP_400_BAD_REQUEST)

    # ******************** Filtering tests *************************
    def test_api_filter_containers(self):
        """
        Test for the ability to filter containers
        """
        self.response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")

        self.response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")

        self.full_container_data.update({"fields": {
            "str_fields": [
                {
                    "name": "made_in",
                    "content": "Russia"
                }
            ],
            "int_fields": [
                {
                    "name": "price",
                    "content": 15000
                }
            ],
            "file_fields": [],
            "select_fields": [],
            "bool_fields": [
                {
                    "name": "new",
                    "content": "False"
                },
                {
                    "name": "publish",
                    "content": "True"
                }
            ]

        }})
        self.response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")

        filters = {
            "type": self.type_product.translate,
            "filters":
                {
                    'for-price': '13000,14000',
                    'for-status': False
                }
        }

        response = self.client.post(
            reverse('api:products:filtered_containers'),
            filters,
            format="json")
        self.assertEqual(len(response.json()), 2)

    def test_api_bad_filter_containers(self):
        """
        Test for the ability to filter containers
        """
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        self.full_container_data.update({'name': 'Test2'})

        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")

        filters = {
            "type": self.type_product.translate,
            "filters":
                {
                    'for-priceeeeeeeeee': '13000,14000',
                    'for-statusssssssss': "False"
                }
        }

        response = self.client.post(
            reverse('api:products:filtered_containers'),
            filters,
            format="json")
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)

    # ******************** Publish status check tests *************************
    def test_api_create_check_container_publish_status(self):
        """
        Test publish status functional. Create.
        """
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")

        container = RealProduct.objects.get(
            pk=int(response.json()['id']))
        self.assertEqual(container.property_json['publish'], True)

    def test_api_bad_create_check_container_publish_status(self):
        """
        Bad Test publish status functional. Bad create.
        """
        container_data = self.full_container_data
        container_data['fields']['bool_fields'].pop(0)
        response = self.client.post(
            reverse('api:products:create_container'),
            container_data,
            format="json")

        self.assertEqual(response.status_code,
                         status.HTTP_412_PRECONDITION_FAILED)

    def test_api_update_container_publish_status_check(self):
        """
        Test publish status functional. Update not full container to full,
        and publish him
        """
        container_data = {
            "name": "TestContainer",
            "type": self.type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "made_in",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "price",
                        "content": 13000
                    }
                ],
                "file_fields": [],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "publish",
                        "content": "False"
                    }
                ]

            }
        }
        response = self.client.post(
            reverse('api:products:create_container'),
            container_data,
            format="json")
        container_id = response.json()['id']
        update_container_data = {
            "name": "TestContainer",
            "type": self.type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "made_in",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "price",
                        "content": 13000
                    }
                ],
                "file_fields": [],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "new",
                        "content": "False"
                    },
                    {
                        "name": "publish",
                        "content": True
                    }
                ]

            }
        }
        response = self.client.put(
            reverse('api:products:container_del_put_get',
                    kwargs={'pk': container_id}),
            update_container_data,
            format="json")

        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_api_bad_update_to_not_full_container_publish_status_check(self):
        """
        Test publish status functional. Update not full container to not full,
        container and publish him
        """
        container_data = {
            "name": "TestContainer",
            "type": self.type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "made_in",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "price",
                        "content": 13000
                    }
                ],
                "file_fields": [],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "publish",
                        "content": "False"
                    }
                ]

            }
        }
        response = self.client.post(
            reverse('api:products:create_container'),
            container_data,
            format="json")

        container_id = response.json()['id']
        update_container_data = {
            "name": "TestContainer",
            "type": self.type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "made_in",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "price",
                        "content": 13000
                    }
                ],
                "file_fields": [],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "publish",
                        "content": "True"
                    }
                ]

            }
        }
        response = self.client.put(
            reverse('api:products:container_del_put_get',
                    kwargs={'pk': container_id}),
            update_container_data,
            format="json")

        self.assertEqual(response.status_code,
                         status.HTTP_412_PRECONDITION_FAILED)

    def test_api_middle_update_to_not_full_container_publish_status_check(
            self):
        """
        Test publish status functional. Update not full container full (but
        request contains one empty field) container and publish him
        """
        container_data = {
            "name": "TestContainer",
            "type": self.type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "made_in",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "price",
                        "content": 13000
                    }
                ],
                "file_fields": [],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "new",
                        "content": "False"
                    },
                    {
                        "name": "publish",
                        "content": "False"
                    }
                ]

            }
        }
        response = self.client.post(
            reverse('api:products:create_container'),
            container_data,
            format="json")

        container_id = response.json()['id']
        update_container_data = {
            "name": "TestContainer",
            "type": self.type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "made_in",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "price",
                        "content": 13000
                    }
                ],
                "file_fields": [],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "new",
                        "content": ""
                    },
                    {
                        "name": "publish",
                        "content": "True"
                    }
                ]

            }
        }
        response = self.client.put(
            reverse('api:products:container_del_put_get',
                    kwargs={'pk': container_id}),
            update_container_data,
            format="json")

        self.assertEqual(response.status_code,
                         status.HTTP_412_PRECONDITION_FAILED)

    def test_create_container_with_manager(self):
        from api.models import Company, User
        url = reverse('api:main:accounts-list')
        company = Company.objects.create(name='test1')

        data = {'email': 'test@te.re',
                'password': '123456aA',
                'company': company.pk}
        response = self.client.post(url, data, format='json')
        url = reverse('api:main:get_auth_token')
        data = {
            'username': 'test@te.re',
            'password': '123456aA'}

        response = self.client.post(url, data)
        token = response.data.get('token')
        user_id = User.objects.get(username='test@te.re').id
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json",
            **{'HTTP_AUTHORIZATION': 'Token {}'.format(token), })
        print()
        self.assertEqual(response.json()['manager']['id'], user_id)

    def test_bad_create_container_with_manager(self):
        self.client.force_authenticate(user=None)
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        self.assertEqual(response.status_code, 401)

    def test_get_containers_by_manager(self):
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        response = self.client.get(
            reverse('api:products:get_manager_containers',
                    kwargs={'manager_id': self.user.id}),
            format="json",
        )
        self.assertEqual(len(response.json()), 2)

    def test_bad_get_containers_by_manager(self):
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        response = self.client.get(
            reverse('api:products:get_manager_containers',
                    kwargs={'manager_id': self.user.id + 1}),
            format="json",
        )
        self.assertEqual(response.json()['error'],
                         "Bad user id. User not found")

    def test_sort_containers(self):
        """
        without limit, only sort. In progress
        """
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        response = self.client.get(
            reverse('api:products:get_manager_containers',
                    kwargs={'manager_id': self.user.id + 1}),
            format="json",
        )

    def test_delete_empty_fields_in_container(self):
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")
        container_id = response.json()['id']
        new_fields = self.full_container_data['fields']
        dict(new_fields['bool_fields'][0]).update({'name': ''})

        response = self.client.put(
            reverse('api:products:container_del_put_get',
                    kwargs={'pk': container_id}),
            self.full_container_data,
            format="json")

        self.assertEqual(response.status_code, 200)
        self.assertFalse('' in list(response.json()['property_json'].values()))

    def test_filter(self):
        response = self.client.post(
            reverse('api:products:create_container'),
            self.full_container_data,
            format="json")

        new_container_data = {
            "name": "TestContainer",
            "type": self.type_product.pk,
            "fields": {
                "str_fields": [
                    {
                        "name": "made_in",
                        "content": "Russia"
                    }
                ],
                "int_fields": [
                    {
                        "name": "price",
                        "content": 13000
                    }
                ],
                "file_fields": [],
                "select_fields": [],
                "bool_fields": [
                    {
                        "name": "new",
                        "content": ""
                    },
                    {
                        "name": "publish",
                        "content": "True"
                    }
                ]

            }
        }

        response = self.client.post(
            reverse('api:products:create_container'),
            new_container_data,
            format="json")

