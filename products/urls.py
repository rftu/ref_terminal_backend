from django.conf.urls import url, include
from django.conf.urls.static import static

from api.views import order_pay_view, container_reset_status_view
from products.views.base_logic_views import ContainersView, \
    FilterContainerView, FiltersView, FieldsView, SelectFieldView, TypesView, \
    PublicContainersView, SortView
from products.views.image_views import ImageView
from ref_terminal_backend import settings

app_name = 'products'
urlpatterns = [
                  url(r'^container/', include([
                      url(r'^(?P<translate>[\w-]+)$',
                          PublicContainersView.as_view({'get': 'retrieve'}),
                          name='get_container'),
                      url(r'^all/$',
                          PublicContainersView.as_view({'get': 'list'})),
                      url(r'^manager/', include([
                          url(r'^(?P<manager_id>\d+)$',
                              PublicContainersView.as_view(
                                  {'post': 'list',
                                   'get': 'list'}),
                              name='get_manager_containers'),
                          url(r'^add_container/$', ContainersView.as_view(
                              {'post': 'create'}), name='create_container'),
                          url(r'container/(?P<pk>\d+)$',
                              ContainersView.as_view(
                                  {'put': 'update',
                                   'get': 'retrieve',
                                   'delete': 'destroy'}),
                              name='container_del_put_get'),
                          # TODO: Выпилить этот роут, когда-нибудь. Но не обязательно.
                          url(r'container/(?P<pk>\d+)/reset_status$',
                              container_reset_status_view,
                              name='reset_container_status'),
                          url(r'^my_containers/$', ContainersView.as_view(
                              {'post': 'list',
                               'get': 'list'}), name='get_my_containers')
                      ])),
                      # TODO переделать на get
                      url(r'^filter/$',
                          FilterContainerView.as_view({'post': 'list'}),
                          name='filtered_containers')
                  ])),
                  url(r'^filters/', include([
                      url(r'^(?P<type>\w+)$',
                          FiltersView.as_view({'get': 'list'})),
                  ])),
                  url(r'^sort/', include([
                      url(r'^(?P<type>\w+)$',
                          SortView.as_view({'get': 'list'})),
                  ])),
                  url(r'^fields/', include([
                      url(r'select/(?P<field_name>[-\w]+)$',
                          SelectFieldView.as_view()),
                      url(r'^(?P<type>\w+)$',
                          FieldsView.as_view({'get': 'list'})),
                  ])),
                  url(r'^types/$', TypesView.as_view({'get': 'list'})),
                  url(r'images/', include([
                      url(r'^(?P<pk>\d+)$',
                          ImageView.as_view({'get': 'retrieve',
                                             'delete': 'destroy',
                                             'post': 'set_main'})),
                      url(r'^add/$', ImageView.as_view({'post': 'create'}))
                  ]))

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

'''url(r'images/', include([
        url(r'^(?P<pk>\d+)$', ImageView.as_view({'get': 'retrieve',
                                                 'delete': 'destroy'})),
        url(r'^add/$', ImageView.as_view({'post': 'create'}))
    ]))
    url(r'^images/$', ImageView.as_view({'post': 'create',
                                         'delete': 'destroy'}))'''
