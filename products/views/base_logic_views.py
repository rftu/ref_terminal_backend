from functools import reduce

from django.db.models import Q, Lookup, Field
from django.db.models.expressions import RawSQL, OrderBy
from rest_framework import viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404, ListAPIView, \
    GenericAPIView
from rest_framework.parsers import MultiPartParser, JSONParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from api.models import User, OrderManagerModel
from products.models import RealProduct
from products.models.main import TypeProduct, FilterProduct, \
    SelectFieldProduct, FieldProduct
from products.pagination_class import NormalResultsSetPagination
from products.serializers import FilterProductSerializer, \
    AbstractFieldSerializer, TypeProductSerializer, \
    SelectFieldProductSerializer, SortProductSerializer
from products.serializers.products import ProductSerializer


def sort_queryset_for_numeric(param) -> RawSQL:
    return RawSQL("cast(property_json->>%s as float)", (param,))


def sort_queryset_for_char(param) -> RawSQL:
    return RawSQL("property_json->>%s", (param,))


def sort_queryset_by_bool(param) -> RawSQL:
    return RawSQL("cast(property_json->>%s as bool)", (param,))


class JsonLike(Lookup):
    def __init__(self, lhs, rhs):
        super(JsonLike, self).__init__(lhs, rhs)

    lookup_name = 'json_like'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        lhs = lhs.replace('->', '->>')
        rhs, rhs_params = self.process_rhs(compiler, connection)
        new_params = []
        for param in rhs_params:
            param.adapted = '%{}%'.format(param.adapted)
            new_params.append(param.adapted)
        params = lhs_params + new_params
        return '{lhs} ilike {rhs}'.format(lhs=lhs, rhs=rhs), params


Field.register_lookup(JsonLike)


class FilterModelMixin(GenericAPIView):
    def sort_queryset(self):
        from django.utils.datastructures import MultiValueDictKeyError
        sort_func = {
            'bool': sort_queryset_by_bool,
            'int': sort_queryset_for_numeric,
            'float': sort_queryset_for_numeric,
            'str': sort_queryset_for_char,
        }
        try:
            sort_params = [_ for _ in self.request.GET['sort'].split(',') if _]
            if not sort_params:
                sort_params.append('status')

            raw_sqls = []
            for param in sort_params:
                field = FieldProduct.objects.get(translate=param)
                func = sort_func.get(field.get_type())
                if func is None:
                    func = sort_queryset_for_char
                raw_sql = func(field.translate)
                if param == 'status':
                    raw_sql = OrderBy(raw_sql, descending=True)
                raw_sqls.append(raw_sql)

            queryset = self.queryset.order_by(*raw_sqls)

            try:
                if self.request.GET['reverse'] == '1':
                    return queryset.reverse()
                return queryset
            except KeyError:
                pass
                return queryset
        except (FieldProduct.DoesNotExist, MultiValueDictKeyError):
            return self.queryset.order_by('-created_at')

    def filter_queryset(self, queryset):
        if self.request.method == 'POST':
            filters = check_filters(self.request.data['filters'],
                                    self.request.data['type'])
            if filters.get('errors'):
                raise ValidationError(filters)
            exclude_keys = []
            for key, value in filters.items():
                try:
                    range_start, range_stop = map(float, value.split(','))
                    for item in queryset:
                        try:
                            if range_start <= float(
                                item.property_json.get(key)) <= range_stop:
                                if key not in exclude_keys:
                                    exclude_keys.append(key)
                            else:
                                queryset = queryset.exclude(id=item.id)
                        except (KeyError, TypeError):
                            queryset = queryset.exclude(id=item.id)
                except AttributeError:
                    if type(value) == list:
                        variants = list(map(lambda x: {key: x}, value))
                        lookup = "property_json__{}__json_like".format(key)
                        queryset = queryset.filter(
                            reduce(
                                lambda q, f: q | Q(**{lookup: f[key]})
                                if not isinstance(f, dict)
                                else q | Q(property_json__contains=f),
                                variants,
                                Q()))
                        if key not in exclude_keys:
                            exclude_keys.append(key)
                    elif isinstance(value, dict):
                        queryset = queryset.filter(
                            property_json__contains={key: value})
                        if key not in exclude_keys:
                            exclude_keys.append(key)
                    continue
                except (ValueError, IndexError):
                    continue
            for e in exclude_keys:
                filters.pop(e)
            try:
                queryset = queryset.filter(
                    **{"property_json__{}__json_like".format(key): value for
                       key, value in filters.items()},
                    manager=self.kwargs['manager_id'])
            except KeyError:
                queryset = queryset.filter(
                    **{"property_json__{}__json_like".format(key): value for
                       key, value in filters.items()})
            return queryset
        else:
            return queryset

    @staticmethod
    def filter_moderation_passed(queryset, manager):
        a = {'property_json__' + RealProduct.MODERATION_PASSED_PROPERTY_FIELD + '__isnull': True}
        b = {'property_json__' + RealProduct.MODERATION_PASSED_PROPERTY_FIELD: True}
        qs = Q(**a) | Q(**b)
        if manager.is_authenticated:
            qs = qs | Q(manager=manager)
        return queryset.filter(qs)


class SortView(viewsets.ModelViewSet):
    """
    Sort fields endpoint. get -> list
    """
    serializer_class = SortProductSerializer
    queryset = TypeProduct.objects.all()

    def list(self, request, *args, **kwargs):
        product_type = get_object_or_404(TypeProduct, translate=kwargs['type'])
        serializer = self.get_serializer(product_type.sort_fields, many=True)
        return Response(serializer.data)


class FiltersView(viewsets.ModelViewSet):
    """
    Filters endpoint. get -> list
    """
    serializer_class = FilterProductSerializer
    queryset = TypeProduct.objects.all()

    def list(self, request, *args, **kwargs):
        product_type = get_object_or_404(TypeProduct, translate=kwargs['type'])
        serializer = self.get_serializer(product_type.filters, many=True)
        return Response(serializer.data)


class FieldsView(viewsets.ModelViewSet):
    """
        Fields endpoint.
        Current version can:
            - get all fields for current type
                url: /api/products/fields/<type_translate_name>
                method: GET
                info: if you send bad type, the route return 404
    """
    serializer_class = AbstractFieldSerializer
    queryset = TypeProduct.objects.all()

    def list(self, request, *args, **kwargs):
        product_type = get_object_or_404(TypeProduct, translate=kwargs['type'])
        serializer = self.get_serializer(product_type.fields, many=True)
        return Response(serializer.data)


class SelectFieldView(ListAPIView):
    serializer_class = SelectFieldProductSerializer
    queryset = SelectFieldProduct.objects.all().order_by('order_number')

    def get_queryset(self):
        return get_object_or_404(self.queryset, translate=self.kwargs[
            'field_name']).content.all().order_by('order_number')


class TypesView(viewsets.ModelViewSet):
    serializer_class = TypeProductSerializer
    queryset = TypeProduct.objects.all()

    def filter_queryset(self, queryset):
        return queryset.filter(main=True)


class PublicContainersView(FilterModelMixin,
                           viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = RealProduct.objects.all()
    pagination_class = NormalResultsSetPagination
    permission_classes = (AllowAny,)

    def filter_queryset(self, queryset):
        if self.request.method == 'POST':
            return super(PublicContainersView, self).filter_queryset(queryset)
        else:
            try:
                User.objects.get(id=self.kwargs['manager_id'])
                return queryset.filter(manager=self.kwargs['manager_id'])
            except User.DoesNotExist:
                raise ValidationError({'error': 'Пользователь не найден.'
                                                ' Неверный id'})
            except KeyError:
                queryset = queryset.filter(property_json__publish=True)
                return self.filter_moderation_passed(queryset, self.request.user)

    def get_queryset(self):
        queryset = self.sort_queryset()
        return queryset

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        if isinstance(self.request.user, User):
            queryset = self.get_queryset().filter(
                Q(property_json__publish=True) |
                Q(manager=self.request.user))
            queryset = self.filter_moderation_passed(queryset, self.request.user)
        lookup_url_kwarg = 'translate'  # self.lookup_url_kwarg or self.lookup_field
        filter_kwargs = {lookup_url_kwarg: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)
        self.check_object_permissions(self.request, obj)

        return obj

    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        return super().list(request, args, kwargs)


def check_filters(data, product_type):
    cool_filters = {}
    errors = {}
    filters = get_object_or_404(TypeProduct,
                                translate=product_type).filters.all()
    for item_k, item_v in data.items():
        try:
            cool_filters.update(
                {filters.get(translate=item_k).field.translate: item_v})
        except FilterProduct.DoesNotExist:
            errors.update({item_k: ValidationError(
                detail="Поле задано некорректно " + item_k.__str__() +
                       " - неправильное имя"
                       " или значение").detail})
    if errors:
        errors.update({"errors": True})
        return errors
    return cool_filters


class ContainersView(FilterModelMixin,
                     viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = RealProduct.objects.all()
    parser_classes = (MultiPartParser, JSONParser,)
    pagination_class = NormalResultsSetPagination
    permission_classes = (IsAuthenticated,)

    def filter_queryset(self, queryset):
        if self.request.method == 'POST':
            return super(ContainersView, self).filter_queryset(
                queryset).filter(manager=self.request.user.id)
        else:
            return queryset.filter(manager=self.request.user.id)

    def perform_destroy(self, instance):
        if not OrderManagerModel.objects.filter(
            trade_items__container__in=[instance.id]).exists():
            instance.delete()
        else:
            raise ValidationError({
                "errors": "Невозможно удалить контейнер, "
                          "т.к. существует заказ с этим контейнером"})

    def get_queryset(self):
        return self.sort_queryset()


class FilterContainerView(FilterModelMixin,
                          viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = RealProduct.objects.all()
    pagination_class = NormalResultsSetPagination

    def get_queryset(self):
        return self.filter_moderation_passed(self.sort_queryset(), self.request.user)

    def filter_queryset(self, queryset):
        result = super(FilterContainerView, self).filter_queryset(queryset)
        return result.filter(property_json__publish=True)
