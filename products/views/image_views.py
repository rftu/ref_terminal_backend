from content_gallery.models import Image
from rest_framework import viewsets
from rest_framework.parsers import MultiPartParser, JSONParser
from rest_framework.permissions import IsAuthenticated

from products.serializers.images import ImageSerializer
from rest_framework.response import Response


class ImageView(viewsets.ModelViewSet):
    serializer_class = ImageSerializer
    queryset = Image.objects.all()
    parser_classes = (MultiPartParser, JSONParser,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        from products.models import RealProduct
        self_containers = [container.id for container in
                           RealProduct.objects.filter(
                               manager=self.request.user)]
        return self.queryset.filter(object_id__in=self_containers)

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    def set_main(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        try:
            main_image = queryset.get(pk=kwargs['pk'])
            container = main_image.object_id

            main_image.position = 0
            main_image.save()

            images_for_container = list(Image.objects.filter(
                object_id=container).exclude(pk=main_image.pk).order_by(
                'position'))

            for i, image in enumerate(images_for_container, start=1):
                image.position = i
                image.save()

        except Image.DoesNotExist:
            return Response({"detail": "Image not found"}, status=404)
        return Response({"status": True}, status=200)
