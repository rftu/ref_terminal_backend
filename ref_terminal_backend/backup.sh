#!/bin/sh

BACKUP_DIR=backups
POSTGRES_USER=postgres
POSTGRES_PASSWORD=ref_backend_password
POSTGRES_DB=ref_terminal_db
CONTAINER_BASE=refterminalbackend_db

# Create dir
FINAL_BACKUP_DIR=${BACKUP_DIR}-"`date +\%Y-\%m-\%d`/"

# Create dir
mkdir -p ${FINAL_BACKUP_DIR}

# Get container id
CONTAINER_ID=`docker ps -q -f name=${CONTAINER_BASE}`

# Dump
docker exec ${CONTAINER_ID} pg_dump -U ${POSTGRES_USER} ${POSTGRES_DB} | gzip > ${FINAL_BACKUP_DIR}/${POSTGRES_DB}_FULL_"`date '+%Y-%m-%d(%H:%M:%S)'`".sql.gz
