"""rft_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import os

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework_swagger.views import get_swagger_view

from products.admins.manager_admin import manager_admin

schema_view = get_swagger_view(title='Pastebin API')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^manager_admin/', manager_admin.urls),
    url(r'^api/', include([
        url(r'^products/', include('products.urls', namespace='products')),
        url(r'^main/', include('api.urls', namespace='main')),
        url(r'^swagger/$', schema_view)
    ], namespace='api')),
    url(r'^content_gallery/', include('content_gallery.urls')),
]

urlpatterns += static('/media/', document_root=os.path.join(settings.BASE_DIR, 'media'))
