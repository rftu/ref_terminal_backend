BACKUP_DIR=backups
POSTGRES_USER=postgres
POSTGRES_PASSWORD=ref_backend_password
POSTGRES_DB=ref_terminal_db3
CONTAINER_BASE=ref_terminal_backend_db


# Create dir
FINAL_BACKUP_DIR=${BACKUP_DIR}-"`date +\%Y-\%m-\%d`/"

# Create dir
mkdir -p ${FINAL_BACKUP_DIR}

# Get container id
CONTAINER_ID=`docker ps -q -f name=${CONTAINER_BASE}`

# Dump
docker exec ${CONTAINER_ID} psql  -U ${POSTGRES_USER} -d ${POSTGRES_DB} -f db_dumps/backup.sql
