#!/usr/bin/env bash

python3 manage.py collectstatic --noinput

if [ "$DEBUG" != '1' ]
then
    gunicorn ref_terminal_backend.wsgi:application \
        -b 0.0.0.0:8000 \
        -w $(nproc) \
        -k gevent \
        --timeout 120 \
        --log-level=debug
else
    python3 manage.py runserver 0.0.0.0:8000
fi