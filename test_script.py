# -*- coding: utf-8 -*-
import os
import django


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ref_terminal_backend.settings")
django.setup()


from products.models import RealProduct
from api.models import OrderModel


def main():
    container = RealProduct.objects.get(id=241)
    order = OrderModel.objects.last()
    print(order.id, order.trade_items)


if __name__ == '__main__':
    main()
